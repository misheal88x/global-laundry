package com.globallaundry.adapters;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;


import androidx.recyclerview.widget.RecyclerView;

import com.globallaundry.R;
import com.globallaundry.activity.AddressSelectionMap;
import com.globallaundry.activity.AddressSelectionMapEdit;
import com.globallaundry.api.CommonApiCalls;
import com.globallaundry.apimodel.AddressListApiResponse;
import com.globallaundry.apimodel.DeleteAddressApiResponse;
import com.globallaundry.interfaces.CommonCallback;
import com.globallaundry.interfaces.IMethodCaller;
import com.globallaundry.utils.CommonFunctions;
import com.globallaundry.utils.LanguageConstants;
import com.globallaundry.utils.SessionManager;

import java.util.List;

public class AddressAdapter1 extends RecyclerView.Adapter<AddressAdapter1.ViewHolder> {

    private final IMethodCaller iMethodCaller;
    private List<AddressListApiResponse.Datum> mData;
    private LayoutInflater mInflater;
    private ItemClickListener mClickListener;
    Activity mContext;
    int addressType;

    public int mSelectedItem = -1;

    // data is passed into the constructor
    public AddressAdapter1(Activity context, List<AddressListApiResponse.Datum> data, int addressType,
                           IMethodCaller iMethodCaller) {
        this.mData = data;
        this.mContext = context;
        this.addressType = addressType;
        this.iMethodCaller = iMethodCaller;
        this.mInflater = LayoutInflater.from(context);
    }

    // inflates the cell layout from xml when needed
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.item_addresslist, parent, false);
        return new ViewHolder(view);
    }

    // binds the data to the textview in each cell
    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        if (SessionManager.getInstance().getAppLanguageCode().equals("ar")) {
            mContext.getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
            holder.tvAddress.setGravity(Gravity.END);

        } else {
            mContext.getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
            holder.tvAddress.setGravity(Gravity.START);
        }
        holder.tvAddressType.setText(mData.get(position).getAddress_type_name());
        String mAddress = (mData.get(position).getFlatNo() == null || mData.get(position).getFlatNo().isEmpty()) ? "" : mData.get(position).getFlatNo() + ",\n";
        mAddress = mAddress + ((mData.get(position).getApartment() == null || mData.get(position).getApartment().isEmpty()) ? "" : mData.get(position).getApartment() + ",\n");
        mAddress = mAddress + ((mData.get(position).getStreetName() == null || mData.get(position).getStreetName().isEmpty()) ? "" : mData.get(position).getStreetName() + ",\n");
        mAddress = mAddress + ((mData.get(position).getCompany() == null || mData.get(position).getCompany().isEmpty()) ? "" : mData.get(position).getCompany() + ",\n");
        mAddress = mAddress + ((mData.get(position).getLandmark() == null || mData.get(position).getLandmark().isEmpty()) ? "" : mData.get(position).getLandmark() + ",\n");
        mAddress = mAddress + ((mData.get(position).getArea_name() == null || mData.get(position).getArea_name().isEmpty()) ? "" : mData.get(position).getArea_name() + ",\n");
        mAddress = mAddress + ((mData.get(position).getCity_name() == null || mData.get(position).getCity_name().isEmpty()) ? "" : mData.get(position).getCity_name() + "");
        holder.tvAddress.setText(mAddress);
        holder.tvAddress.setTag(mAddress);


        if (CommonFunctions.AddressListType.AddressList.getValue() == addressType) {
            holder.ivDelete.setVisibility(View.VISIBLE);
        } else {
            holder.ivDelete.setVisibility(View.GONE);
        }
        if (CommonFunctions.AddressListType.PickUpAddress.getValue() == addressType || CommonFunctions.AddressListType.DeliveryAddress.getValue() == addressType) {
            holder.rbMarkAsDefault.setVisibility(View.GONE);
            holder.rbMarkAsDefault.setEnabled(false);

        } else {
            holder.rbMarkAsDefault.setVisibility(View.GONE);
            holder.rbMarkAsDefault.setEnabled(true);
        }


        if (mData.get(position).getIs_default() != null && mData.get(position).getIs_default().equals("1")) {
            holder.rbMarkAsDefault.setChecked(true);
        } else {
            holder.rbMarkAsDefault.setChecked(false);
        }

        if (mData.get(position).getAddressTypeId().equals(1)){
            holder.imageAddressType.setImageResource(R.drawable.address_type_work);
        }else if (mData.get(position).getAddressTypeId().equals(2)){
            holder.imageAddressType.setImageResource(R.drawable.side_home);
        }else {
            holder.imageAddressType.setImageResource(R.drawable.side_addressbook);
        }


        holder.rbMarkAsDefault.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CommonApiCalls.getInstance().markAsDefaultAddress(mContext, mData.get(position).getUserAddressKey(), new CommonCallback.Listener() {
                    @Override
                    public void onSuccess(Object body) {

                        for (int count = 0; count < mData.size(); count++) {
                            mData.get(count).setIs_default("0");
                        }
                        mData.get(position).setIs_default("1");
                        notifyDataSetChanged();
                    }

                    @Override
                    public void onFailure(String reason) {

                    }
                });
            }
        });

        holder.llParent.setTag(position);
        holder.llParent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String mAddress = (mData.get(position).getFlatNo() != null || mData.get(position).getFlatNo().isEmpty()) ? "" : mData.get(position).getFlatNo() + ",";
                mAddress = mAddress + ((mData.get(position).getApartment() == null || mData.get(position).getApartment().isEmpty()) ? "" : mData.get(position).getApartment() + ",");
                mAddress = mAddress + ((mData.get(position).getStreetName() == null || mData.get(position).getStreetName().isEmpty()) ? "" : mData.get(position).getStreetName() + ",");
                mAddress = mAddress + ((mData.get(position).getCompany() == null || mData.get(position).getCompany().isEmpty()) ? "" : mData.get(position).getCompany() + ",");
                mAddress = mAddress + ((mData.get(position).getLandmark() == null || mData.get(position).getLandmark().isEmpty()) ? "" : mData.get(position).getLandmark() + ",");
                mAddress = mAddress + ((mData.get(position).getArea_name() == null || mData.get(position).getArea_name().isEmpty()) ? "" : mData.get(position).getArea_name() + ",");
                mAddress = mAddress + ((mData.get(position).getCity_name() == null || mData.get(position).getCity_name().isEmpty()) ? "" : mData.get(position).getCity_name() + "");

               /* if (CommonFunctions.AddressListType.PickUpAddress.getValue() == addressType) {
                    SessionManager.AddressProperties.getInstance().setPickUpAddressKey(mData.get(position).getUserAddressKey());
                    SessionManager.AddressProperties.getInstance().setPickUpAddress(mAddress);
//                    CommonFunctions.getInstance().newIntent(mContext, PickupTimeSlotActivity.class, Bundle.EMPTY, false);
                } else if (CommonFunctions.AddressListType.DeliveryAddress.getValue() == addressType) {
                    SessionManager.AddressProperties.getInstance().setDeliveryAddressKey(mData.get(position).getUserAddressKey());
                    SessionManager.AddressProperties.getInstance().setDeliveryAddress(mAddress);
//                    CommonFunctions.getInstance().newIntent(mContext, DeliveryTimeSlotActivity.class, Bundle.EMPTY, false);
                }*/

                iMethodCaller.selectedAddress(mData.get(position));


            }
        });
        holder.ivEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putString("address_key", mData.get(position).getUserAddressKey());
                bundle.putDouble("latitude", Double.parseDouble(mData.get(position).getLatitude()));
                bundle.putDouble("logitude", Double.parseDouble(mData.get(position).getLongitude()));
                bundle.putString("address_type", mData.get(position).getAddress_type_key());
                bundle.putString("address_type_name", mData.get(position).getAddress_type_name());
                bundle.putString("streetname", mData.get(position).getStreetName());
                bundle.putString("flatno", mData.get(position).getFlatNo());
                bundle.putString("apartment", mData.get(position).getApartment());
                bundle.putString("company", mData.get(position).getCompany());
                bundle.putString("landmark", mData.get(position).getLandmark());
                bundle.putString("city_key", String.valueOf(mData.get(position).getCity_key()));
                bundle.putString("city_name", mData.get(position).getCity_name());
                bundle.putString("area_key", String.valueOf(mData.get(position).getArea_key()));
                bundle.putString("area_name", mData.get(position).getArea_name());
                CommonFunctions.getInstance().newIntent(mContext, AddressSelectionMapEdit.class, bundle, false);
            }
        });


        holder.ivDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Dialog dialog = new Dialog(mContext);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.dialog_alert_new);
                dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
                TextView tvQue = dialog.findViewById(R.id.tvQue);
                TextView tvTitle = dialog.findViewById(R.id.tvTitle);
                TextView btnCancel = dialog.findViewById(R.id.btn_cancel_new);
                TextView btnYes = dialog.findViewById(R.id.btnYesNew);
                btnCancel.setText(LanguageConstants.No);
                btnYes.setText(LanguageConstants.Yes);
                tvTitle.setText(LanguageConstants.DeleteAddress);
                tvQue.setText(LanguageConstants.do_you_want_to_delete_this_Address);
                tvQue.setTextSize(13);
                btnYes.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        CommonApiCalls.getInstance().deleteAddress(mContext, mData.get(position).getUserAddressKey(), new CommonCallback.Listener() {
                            @Override
                            public void onSuccess(Object body) {
                                DeleteAddressApiResponse deleteAddressApiResponse = (DeleteAddressApiResponse) body;
                                CommonFunctions.getInstance().successResponseToast(mContext, deleteAddressApiResponse.getMessage());
                                mData.remove(mData.get(position));
                                dialog.dismiss();
//                                notifyDataSetChanged();
                                iMethodCaller.LoadListView();
                            }

                            @Override
                            public void onFailure(String reason) {

                            }
                        });
                    }
                });
                btnCancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
                Window window = dialog.getWindow();
                WindowManager.LayoutParams wlp = window.getAttributes();
                wlp.gravity = Gravity.CENTER;
                window.setAttributes(wlp);
                dialog.show();

            }
        });

    }

    @Override
    public int getItemCount() {
        return mData.size();
    }


    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView tvAddressType, tvAddress;
        ImageView ivEdit, ivDelete,imageAddressType;
        RadioButton rbMarkAsDefault;
        LinearLayout llParent;

        ViewHolder(View itemView) {
            super(itemView);
            tvAddressType = (TextView) itemView.findViewById(R.id.tvAddressType);
            tvAddress = (TextView) itemView.findViewById(R.id.tvAddress);
            ivEdit = (ImageView) itemView.findViewById(R.id.ivEdit);
            ivDelete = (ImageView) itemView.findViewById(R.id.ivDelete);
            imageAddressType = (ImageView) itemView.findViewById(R.id.imageAddressType);
            rbMarkAsDefault = (RadioButton) itemView.findViewById(R.id.rbMarkAsDefault);
            llParent = (LinearLayout) itemView.findViewById(R.id.llParent);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (mClickListener != null) mClickListener.onItemClick(view, getAdapterPosition());
        }
    }

    AddressListApiResponse.Datum getItem(int id) {
        return mData.get(id);
    }

    void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }
}