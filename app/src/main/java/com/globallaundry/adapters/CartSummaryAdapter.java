package com.globallaundry.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.globallaundry.R;
import com.globallaundry.apimodel.OrderDetailsApiResponse;
import com.globallaundry.utils.CommonFunctions;

import java.util.List;

public class CartSummaryAdapter extends RecyclerView.Adapter<CartSummaryAdapter.ViewHolder> {

    private LayoutInflater mInflater;
    List<OrderDetailsApiResponse.Item> items;
    Context mContext;
    private OrderDetailsApiResponse.Coth_type item;
    int i;


    public CartSummaryAdapter(Context context, List<OrderDetailsApiResponse.Item> items, int i) {
        this.mInflater = LayoutInflater.from(context);
        this.mContext = context;
        this.items = items;
        this.i = i;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.adapter_cart_summary, parent, false);
        return new ViewHolder(view);
    }

    // binds the data to the textview in each cell
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.tvItemName.setText(items.get(position).getCategory_name());
        Glide.with(mContext).load(items.get(position).getCategory_image()).apply(RequestOptions.circleCropTransform()).into(holder.ivItemImage);
        for (int count = 0; count < items.get(position).getCoth_types().size(); count++) {
            item = items.get(position).getCoth_types().get(count);
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View child = inflater.inflate(R.layout.item_layout, null);
            TextView tvItemName = (TextView) child.findViewById(R.id.tvItemName);
            TextView tvPrice = (TextView) child.findViewById(R.id.tvPrice);
            ImageView ivDeleteItem = (ImageView) child.findViewById(R.id.ivDeleteItem);
            tvItemName.setText(item.getItem_quantity() + " x " + item.getCloth_type_name());
            tvPrice.setText(CommonFunctions.getInstance().roundOffDecimalValue(Double.valueOf(item.getItem_total_price()), true));
            //tvPrice.setText(item.getItem_total_price());
            holder.llItems.addView(child);

        }

    }

    @Override
    public int getItemCount() {
        return items.size();
    }


    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView tvItemName;
        ImageView ivItemImage;
        ProgressBar pbLoader;
        LinearLayout llItems;

        ViewHolder(View itemView) {
            super(itemView);
            tvItemName = (TextView) itemView.findViewById(R.id.tvItemName);
            ivItemImage = (ImageView) itemView.findViewById(R.id.ivItemImage);
            llItems = (LinearLayout) itemView.findViewById(R.id.llItems);

        }

    }

}