package com.globallaundry.adapters;

import android.app.AlertDialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.globallaundry.R;
import com.globallaundry.api.CommonApiCalls;
import com.globallaundry.interfaces.CommonCallback;
import com.globallaundry.models.NotificationListApiResponse;
import com.globallaundry.models.ReadNotificationApiResponse;
import com.globallaundry.utils.CommonFunctions;
import com.globallaundry.utils.LanguageConstants;

import java.util.List;


public class NotificationListAdapter extends RecyclerView.Adapter<NotificationListAdapter.MyViewHolder> {


    private final List<NotificationListApiResponse.Datum> data;
    private Context mcontext;
    AlertDialog.Builder mDialog;
    AlertDialog mAlertDilaog;

    public NotificationListAdapter(Context mcontext, List<NotificationListApiResponse.Datum> data) {
        this.mcontext = mcontext;
        this.data = data;
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView tvDate, tvtitle, tvContent;

        LinearLayout llParent;


        public MyViewHolder(View convertView) {
            super(convertView);
            tvDate = (TextView) convertView.findViewById(R.id.tvDate);
            tvtitle = (TextView) convertView.findViewById(R.id.tvtitle);
            tvContent = (TextView) convertView.findViewById(R.id.tvContent);
            llParent = (LinearLayout) convertView.findViewById(R.id.llParent);
        }
    }



    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_mynotificationlist_item, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder mHolder, int position) {
        NotificationListApiResponse.Datum mListItem = data.get(position);

        mHolder.tvtitle.setText(mListItem.getNotification_title());
        mHolder.tvContent.setText(mListItem.getNotification_description());
        mHolder.tvDate.setText(mListItem.getCreated_at());

        if (mListItem.getIs_read().equals(1)) {
            mHolder.llParent.setBackground(ContextCompat.getDrawable(mcontext, R.drawable.notification_read_bg));
            mHolder.tvtitle.setTextColor(ContextCompat.getColor(mcontext, R.color.textcolor_gray));
            mHolder.tvContent.setTextColor(ContextCompat.getColor(mcontext, R.color.textcolor_gray));
            mHolder.tvDate.setTextColor(ContextCompat.getColor(mcontext, R.color.textcolor_gray));
        } else {
            mHolder.llParent.setBackground(ContextCompat.getDrawable(mcontext, R.drawable.notification_read_bg));
            mHolder.tvtitle.setTextColor(ContextCompat.getColor(mcontext, R.color.black));
            mHolder.tvContent.setTextColor(ContextCompat.getColor(mcontext, R.color.black));
            mHolder.tvDate.setTextColor(ContextCompat.getColor(mcontext, R.color.black));
        }

        mHolder.llParent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (mListItem.getIs_read().equals(1)){
                    showDialog(mListItem.getNotification_title(), mListItem.getNotification_description(), mListItem.getCreated_at());
                }
                else {
                    CommonApiCalls.getInstance().readedNotification(mcontext, mListItem.getNotification_key(), new CommonCallback.Listener() {
                        @Override
                        public void onSuccess(Object object) {
                            ReadNotificationApiResponse response = (ReadNotificationApiResponse) object;

                            if (response.getStatus().equals(200)){
                                if (response.getData() != null) {
                                    ReadNotificationApiResponse.Datum datas = response.getData().get(position);
                                    showDialog(datas.getNotification_title(), datas.getNotification_description(), datas.getCreated_at());
                                }
                            }

                        }

                        @Override
                        public void onFailure(String reason) {
                            CommonFunctions.getInstance().validationInfoError(mcontext, reason);
                        }
                    });
                }

                notifyDataSetChanged();
            }
        });

    }

    private void showDialog(String title, String content, String created_at) {
        mDialog = new AlertDialog.Builder(mcontext, R.style.MyAlertDialogStyle);
        final View vDialog = LayoutInflater.from(mcontext).inflate(R.layout.dialog_read_notification, null);
        mDialog.setView(vDialog);
        mAlertDilaog = mDialog.create();
        mAlertDilaog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        mAlertDilaog.show();
        TextView tvtitle = (TextView) vDialog.findViewById(R.id.tvtitle);
        TextView tvContentvalue = (TextView) vDialog.findViewById(R.id.tvContentvalue);
        TextView tvDate = (TextView) vDialog.findViewById(R.id.tvDate);
        TextView btnOk = (TextView) vDialog.findViewById(R.id.btnOk);
        tvtitle.setText(title);
        tvContentvalue.setText(content);
        tvDate.setText(created_at);

        btnOk.setText(LanguageConstants.ok);


        btnOk.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (mAlertDilaog.isShowing()) {
                    mAlertDilaog.dismiss();
                }
            }
        });

    }


    @Override
    public int getItemCount() {
        return data.size();
    }
}