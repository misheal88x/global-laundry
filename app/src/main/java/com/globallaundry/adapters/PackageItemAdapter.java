package com.globallaundry.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.globallaundry.R;
import com.globallaundry.apimodel.PackageData;
import com.globallaundry.utils.SessionManager;

import java.util.List;

public class PackageItemAdapter extends RecyclerView.Adapter<PackageItemAdapter.PackageItemHolder> {

    private Context context;
    private List<PackageData.Item.ClothType> itemList;

    public PackageItemAdapter(Context context, List<PackageData.Item.ClothType> itemList) {
        this.context = context;
        this.itemList = itemList;
    }

    @NonNull
    @Override
    public PackageItemHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.package_nested_item_layout, parent, false);

        return new PackageItemHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull PackageItemHolder holder, int position) {

        holder.package_item_layout_price.setText(itemList.get(position).getPrice());
        holder.package_item_layout_type.setText(itemList.get(position).getClothTypeName());
        holder.package_nested_item_qty.setText(itemList.get(position).getCountInPackages() + "");
    }

    @Override
    public int getItemCount() {
        return itemList.size();
    }

    static class PackageItemHolder extends RecyclerView.ViewHolder {

        public TextView package_item_layout_type_lbl,
                package_item_layout_type,
                package_item_layout_price,
                package_item_layout_price_lbl,
                package_item_layout_qty_lbl,
                package_nested_item_qty;

        public PackageItemHolder(@NonNull View v) {
            super(v);

            package_item_layout_type_lbl = v.findViewById(R.id.package_item_layout_type_lbl);
            package_item_layout_type = v.findViewById(R.id.package_item_layout_type);
            package_item_layout_price = v.findViewById(R.id.package_item_layout_price);
            package_item_layout_price_lbl = v.findViewById(R.id.package_item_layout_price_lbl);
            package_item_layout_qty_lbl = v.findViewById(R.id.package_item_layout_qty_lbl);
            package_nested_item_qty = v.findViewById(R.id.package_nested_item_qty_value);

            if (SessionManager.getInstance().getAppLanguageCode().equals("ar")) {
                package_item_layout_price_lbl.setText("السعر: ");
                package_item_layout_type_lbl.setText("النوع: ");
                package_item_layout_qty_lbl.setText("الكمية: ");
            }
        }
    }
}
