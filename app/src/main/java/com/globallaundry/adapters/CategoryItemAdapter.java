package com.globallaundry.adapters;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.globallaundry.R;
import com.globallaundry.activity.HomeActivity;
import com.globallaundry.activity.ItemDetails;
import com.globallaundry.activity.PricingActivity;
import com.globallaundry.activity.SignInActivity;
import com.globallaundry.apimodel.ClothTypeApiResponse;
import com.globallaundry.utils.CommonFunctions;
import com.globallaundry.utils.SessionManager;
import com.google.gson.Gson;

import java.util.List;

import hari.bounceview.BounceView;

public class CategoryItemAdapter extends RecyclerView.Adapter<CategoryItemAdapter.MyViewHolder> {

    private final Context context;
    private final List<ClothTypeApiResponse.Datum> responses;


    public CategoryItemAdapter(Context context, List<ClothTypeApiResponse.Datum> responses) {
        this.context = context;
        this.responses = responses;



        Log.d("UUUUUUUUUUUUUUU", responses.size() + "");

    }

    @NonNull
    @Override
    public CategoryItemAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_category_item, parent, false);

        return new CategoryItemAdapter.MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CategoryItemAdapter.MyViewHolder holder, int position) {

        ClothTypeApiResponse.Datum listResponses = responses.get(position);

        holder.txtCategoryName.setText(listResponses.getCloth_type_name());

        if (listResponses.getCloth_type_image() != null) {
            Glide.with(context).load(listResponses.getCloth_type_image()).into(holder.ivProduct);
        }


        Log.d("UUUUUUUUUUUUUUU", listResponses.getCloth_type_name());

        BounceView.addAnimTo(holder.lyoutParent);

        holder.lyoutParent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (SessionManager.getInstance().getUserKey().isEmpty()) {
                    CommonFunctions.getInstance().newIntent(context, SignInActivity.class, Bundle.EMPTY, false);
                }
                else {
                SessionManager.getInstance().setClothTypeKey(listResponses.getCloth_type_key());
                Bundle bundle = new Bundle();
                Gson gson = new Gson();
                String json = gson.toJson(responses.get(position));
                bundle.putString("SIZE", json);
                if (listResponses.getSize_type() == 3) {
                    CommonFunctions.getInstance().newIntent(context, PricingActivity.class, bundle, false);
                } else {
                    CommonFunctions.getInstance().newIntent(context, ItemDetails.class, bundle, false);
                }
            }
            }
        });

    }


    @Override
    public int getItemCount() {
        return responses.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView ivProduct;
        TextView txtCategoryName, txtStatus;
        LinearLayout lyoutParent;

        public MyViewHolder(View itemView) {
            super(itemView);

            ivProduct = (ImageView) itemView.findViewById(R.id.image);
            txtCategoryName = (TextView) itemView.findViewById(R.id.txtCategoryName);
            lyoutParent = (LinearLayout) itemView.findViewById(R.id.lyoutParent);
        }
    }
}