package com.globallaundry.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.globallaundry.R;
import com.globallaundry.apimodel.ChangeDeliveryAddressApiResponse;
import com.globallaundry.apimodel.PlaceOrderApiResponse;
import com.globallaundry.databinding.AdapterSpacialtyBinding;
import com.globallaundry.interfaces.SelectArea;

import java.util.List;

public class SelectChangeAddressAdapter extends RecyclerView.Adapter<SelectChangeAddressAdapter.MyViewHolder> {

    private final Context context;
    private final List<ChangeDeliveryAddressApiResponse.Datum> responses;
    private final SelectArea selectArea;
    AdapterSpacialtyBinding binding;

    public SelectChangeAddressAdapter(Context context, List<ChangeDeliveryAddressApiResponse.Datum> responses, SelectArea selectArea) {
        this.context = context;
        this.responses = responses;
        this.selectArea = selectArea;
    }

    @NonNull
    @Override
    public SelectChangeAddressAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        binding = DataBindingUtil.inflate(layoutInflater, R.layout.adapter_spacialty, parent, false);
        return new SelectChangeAddressAdapter.MyViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull SelectChangeAddressAdapter.MyViewHolder myViewHolder, int i) {

        final ChangeDeliveryAddressApiResponse.Datum listApiResponse = responses.get(i);

        myViewHolder.txtViewName.setText(listApiResponse.getStreet_name());


        myViewHolder.llyParent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectArea.onClick(listApiResponse.getArea_id(),listApiResponse.getUser_address_key());
            }
        });
    }

    @Override
    public int getItemCount() {
        return responses.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private final TextView txtViewName;
        private final LinearLayout llyParent;

        public MyViewHolder(@NonNull AdapterSpacialtyBinding itemView) {
            super(itemView.getRoot());

            txtViewName = itemView.txtViewName;
            llyParent = itemView.llyParent;
        }
    }
}