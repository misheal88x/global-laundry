package com.globallaundry.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.globallaundry.R;
import com.globallaundry.activity.ProfileActivity;
import com.globallaundry.apimodel.PromationAddApiResponse;
import com.globallaundry.utils.CommonFunctions;

import java.util.List;

public class HomeBannerAdapter extends PagerAdapter {

    private final List<PromationAddApiResponse.Datum> images;
    Context context;
    LayoutInflater inflater;

    public HomeBannerAdapter(Context context, List<PromationAddApiResponse.Datum> images) {
        this.context = context;
        this.images = images;
    }

    @Override
    public int getCount() {
        return images.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == ((LinearLayout) object);
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {

        ImageView sdvTour;

        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View itemView = inflater.inflate(R.layout.adapter_tour, container, false);

        sdvTour = itemView.findViewById(R.id.sdvTour);

        Glide.with(context).load(images.get(position).getImage()).into(sdvTour);
        ((ViewPager) container).addView(itemView);
        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        ((ViewPager) container).removeView((LinearLayout) object);
    }
}
