package com.globallaundry.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.globallaundry.R;
import com.globallaundry.activity.AllAvailablePackagesActivity;
import com.globallaundry.activity.CategoryItemActivity;
import com.globallaundry.activity.HomeActivity;
import com.globallaundry.activity.SignInActivity;
import com.globallaundry.apimodel.CategoryApiResponse;
import com.globallaundry.utils.CommonFunctions;
import com.globallaundry.utils.SessionManager;

import java.util.List;

import hari.bounceview.BounceView;

public class CategoryAdapter extends RecyclerView.Adapter<CategoryAdapter.MyViewHolder> {

    private final Activity context;
    private final List<CategoryApiResponse.Datum> responses;


    public CategoryAdapter(Activity context, List<CategoryApiResponse.Datum> responses) {
        this.context = context;
        this.responses = responses;


        CategoryApiResponse.Datum datum = new CategoryApiResponse.Datum();
        datum.setCategory_name("Packages");
        if (SessionManager.getInstance().getAppLanguageCode().equals("ar")) {
            datum.setCategory_name("الحزم المتوفرة");
        }
        this.responses.add(0, datum);

    }

    @NonNull
    @Override
    public CategoryAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_category, parent, false);

        return new CategoryAdapter.MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CategoryAdapter.MyViewHolder holder, int position) {

        CategoryApiResponse.Datum listResponses = responses.get(position);
        if (SessionManager.getInstance().getAppLanguageCode().equals("ar")) {
            context.getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
            holder.imgFavourite.setImageResource(R.drawable.ic_back_arrow);
        } else {
            context.getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
            holder.imgFavourite.setImageResource(R.drawable.ic_right_arrow);
        }
        holder.txtCategoryName.setText(listResponses.getCategory_name());

        if (listResponses.getCategory_image() != null) {
            Glide.with(context).load(listResponses.getCategory_image())
                    .apply(RequestOptions.circleCropTransform())
                    .into(holder.ivProduct);
        }

        BounceView.addAnimTo(holder.lyoutParent);


        Log.d("CCCCCC", responses.get(position).getCategory_name());


        holder.lyoutParent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (position == 0) {

                    if (SessionManager.getInstance().getUserKey().isEmpty()) {
                        CommonFunctions.getInstance().newIntent(context, SignInActivity.class, Bundle.EMPTY, false);
                    } else {
                        Intent intent = new Intent(context, AllAvailablePackagesActivity.class);
                        context.startActivity(intent);
                    }

                } else {
                    SessionManager.getInstance().setCategoryKey(listResponses.getCategory_key());
                    Log.d("CCCCCC----", SessionManager.getInstance().getCategoryKey());
                    CommonFunctions.getInstance().newIntent(context, CategoryItemActivity.class, Bundle.EMPTY, false);
                }

            }
        });

    }


    @Override
    public int getItemCount() {
        return responses.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private final ImageView imgFavourite;
        ImageView ivProduct;
        TextView txtCategoryName, txtStatus;
        LinearLayout lyoutParent;

        public MyViewHolder(View itemView) {
            super(itemView);

            ivProduct = (ImageView) itemView.findViewById(R.id.image);
            txtCategoryName = (TextView) itemView.findViewById(R.id.txtName);
            lyoutParent = (LinearLayout) itemView.findViewById(R.id.lyoutParent);
            imgFavourite = (ImageView) itemView.findViewById(R.id.imgFavourite);
        }
    }
}
