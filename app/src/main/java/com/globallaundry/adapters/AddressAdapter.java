package com.globallaundry.adapters;

import android.app.Activity;
import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.globallaundry.R;
import com.globallaundry.apimodel.AddressListApiResponse;
import com.globallaundry.interfaces.OnclickAddress;
import com.globallaundry.models.AddressApiResponse;
import com.globallaundry.utils.CommonFunctions;
import com.globallaundry.utils.SessionManager;

import java.util.List;

import hari.bounceview.BounceView;


public class AddressAdapter extends RecyclerView.Adapter<AddressAdapter.MyViewHolder> {

    private final Activity context;
    private final OnclickAddress onclickAddress;
    private int checked;
    private final List<AddressListApiResponse.Datum> responses;
    private int mCheckedPostion = -1;


    public AddressAdapter(Activity context, List<AddressListApiResponse.Datum> responses, OnclickAddress onclickAddress) {
        this.context = context;
        this.responses = responses;
        this.onclickAddress = onclickAddress;
    }

    @NonNull
    @Override
    public AddressAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_address, parent, false);

        return new AddressAdapter.MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull AddressAdapter.MyViewHolder holder, int position) {

        AddressListApiResponse.Datum listResponses = responses.get(position);
        if (SessionManager.getInstance().getAppLanguageCode().equals("ar")) {
            context.getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
            holder.tvhours.setGravity(Gravity.END);
            holder.txtCategoryName.setGravity(Gravity.END);
        } else {
            context.getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
            holder.tvhours.setGravity(Gravity.START);
            holder.txtCategoryName.setGravity(Gravity.START);
        }
        String mAddress = (listResponses.getFlatNo() == null || listResponses.getFlatNo().isEmpty()) ? "" : listResponses.getFlatNo() + ",\n";
        mAddress = mAddress + ((listResponses.getApartment() == null || listResponses.getApartment().isEmpty()) ? "" : listResponses.getApartment() + ",\n");
        mAddress = mAddress + ((listResponses.getStreetName() == null || listResponses.getStreetName().isEmpty()) ? "" : listResponses.getStreetName() + ",\n");
        mAddress = mAddress + ((listResponses.getCompany() == null || listResponses.getCompany().isEmpty()) ? "" : listResponses.getCompany() + ",\n");
        mAddress = mAddress + ((listResponses.getLandmark() == null || listResponses.getLandmark().isEmpty()) ? "" : listResponses.getLandmark() + ",\n");
        mAddress = mAddress + ((listResponses.getArea_name() == null || listResponses.getArea_name().isEmpty()) ? "" : listResponses.getArea_name() + ",\n");
        mAddress = mAddress + ((listResponses.getCity_name() == null || listResponses.getCity_name().isEmpty()) ? "" : listResponses.getCity_name() + "");
        holder.txtCategoryName.setText(mAddress);

        holder.tvhours.setText(listResponses.getAddress_type_name());
        // 1 - Wrok
        // 2 - Home
        // 3 - Shipping
        if (listResponses.getAddressTypeId().equals(1)){
            holder.imageAddressType.setImageResource(R.drawable.address_type_work);
        }else if (listResponses.getAddressTypeId().equals(2)){
            holder.imageAddressType.setImageResource(R.drawable.side_home);
        }else {
            holder.imageAddressType.setImageResource(R.drawable.side_addressbook);
        }

        holder.rdchecked.setChecked(position == mCheckedPostion);

        holder.lyoutParent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (position == mCheckedPostion) {
                    holder.rdchecked.setChecked(false);
                    mCheckedPostion = -1;
                    onclickAddress.onClick(listResponses.getUserAddressKey(), mCheckedPostion);
                } else {
                    mCheckedPostion = position;
                    notifyDataSetChanged();
                    onclickAddress.onClick(listResponses.getUserAddressKey(), mCheckedPostion);
                }
            }
        });


    }


    @Override
    public int getItemCount() {
        return responses.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView ivProduct;
        TextView txtCategoryName, tvhours;
        LinearLayout lyoutParent;
        RadioButton rdchecked;
        ImageView imageAddressType;

        public MyViewHolder(View itemView) {
            super(itemView);

            ivProduct = (ImageView) itemView.findViewById(R.id.image);
            txtCategoryName = (TextView) itemView.findViewById(R.id.txtName);
            tvhours = (TextView) itemView.findViewById(R.id.tvhours);
            rdchecked = (RadioButton) itemView.findViewById(R.id.rdaddress);
            lyoutParent = (LinearLayout) itemView.findViewById(R.id.lyoutParent);
            imageAddressType = (ImageView) itemView.findViewById(R.id.imageAddressType);
        }
    }
}