package com.globallaundry.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.globallaundry.R;
import com.globallaundry.apimodel.TimeSlotApiResponse;
import com.globallaundry.databinding.AdapterSpacialtyBinding;
import com.globallaundry.interfaces.SelectTime;

import java.util.List;

public class SelectPickUpTimeAdapter extends RecyclerView.Adapter<SelectPickUpTimeAdapter.MyViewHolder> {

    private final Context context;
    private final List<TimeSlotApiResponse.Time> response;
    private final SelectTime selectTime;

    AdapterSpacialtyBinding binding;

    public SelectPickUpTimeAdapter(Context context, List<TimeSlotApiResponse.Time> response, SelectTime selectTime) {
        this.context = context;
        this.response = response;
        this.selectTime = selectTime;
    }


    @NonNull
    @Override
    public SelectPickUpTimeAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        binding = DataBindingUtil.inflate(layoutInflater, R.layout.adapter_spacialty, parent, false);
        return new SelectPickUpTimeAdapter.MyViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull SelectPickUpTimeAdapter.MyViewHolder myViewHolder, int i) {

        TimeSlotApiResponse.Time listApiResponse = response.get(i);

        myViewHolder.txtViewName.setText(listApiResponse.getName());


        myViewHolder.llyParent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectTime.onClick(listApiResponse.getValue(), listApiResponse.getName());
            }
        });
    }

    @Override
    public int getItemCount() {
        return response.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private final TextView txtViewName;
        private final LinearLayout llyParent;

        public MyViewHolder(@NonNull AdapterSpacialtyBinding itemView) {
            super(itemView.getRoot());

            txtViewName = itemView.txtViewName;
            llyParent = itemView.llyParent;
        }
    }

}
