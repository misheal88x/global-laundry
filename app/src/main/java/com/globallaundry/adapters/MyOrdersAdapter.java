package com.globallaundry.adapters;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.globallaundry.R;
import com.globallaundry.activity.MyOrderDetailsActivity;
import com.globallaundry.api.CommonApiCalls;
import com.globallaundry.apimodel.ChangeAddressApiResponse;
import com.globallaundry.apimodel.OrderListApiResponse;
import com.globallaundry.interfaces.CommonCallback;
import com.globallaundry.interfaces.SelectArea;
import com.globallaundry.utils.CommonFunctions;
import com.globallaundry.utils.LanguageConstants;

import java.util.List;


public class MyOrdersAdapter extends RecyclerView.Adapter<MyOrdersAdapter.ViewHolder> {

    private List<OrderListApiResponse.Datum> mData;
    private LayoutInflater mInflater;
    private ItemClickListener mClickListener;
    Context mContext;
    private String selectAreaName;

    public MyOrdersAdapter(Context context, List<OrderListApiResponse.Datum> data) {
        this.mInflater = LayoutInflater.from(context);
        this.mData = data;
        this.mContext = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.adapter_myorders, parent, false);
        return new ViewHolder(view);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        holder.tvOrder.setText(LanguageConstants.OrderId + ":" + "#" + getItem(position).getOrderNumber());
        holder.txtCategory.setText(LanguageConstants.Category + ":");
        holder.tvCategory.setText(getItem(position).getCategoryName());
        // holder.tvTotalPrice.setText(CommonFunctions.DeciamlDigitsAfterDotValue.getWithCurrencyCode(getItem(position).getOrderTotal()));
        holder.tvTotalPrice.setText(getItem(position).getOrderTotal());
        holder.txtPickup.setText(LanguageConstants.Pickup + ":");
        holder.txtDelivery.setText(LanguageConstants.Delivery + ":");
        holder.txtOrderStatus.setText(LanguageConstants.OrderStatus + ":");
        holder.tvPickup.setText(getItem(position).getPickupDatetime());
        holder.tvDelivery.setText(getItem(position).getDeliveryDatetime());
        holder.tvOrderStatuss.setText(getItem(position).getOrderStatusLabel());

        if (!TextUtils.isEmpty(mData.get(position).getStatusColorCode())) {
            CommonFunctions.getInstance().chaneTextColorBasedOnRestaurant(mData.get(position).getStatusColorCode(), holder.tvOrderStatuss);
        }


        holder.llOrders.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle mBundle = new Bundle();
                mBundle.putString("key", getItem(position).getOrderKey());
                mBundle.putString("ISCHANGEADDRESSKEY",getItem(position).getIs_change_address());
                CommonFunctions.getInstance().newIntent(mContext, MyOrderDetailsActivity.class, mBundle, false);
            }
        });


    }

    private void callChangeAddressApi(String order_key, String selectAreaName) {
        CommonApiCalls.getInstance().changeDeliveryAddress(mContext, order_key, selectAreaName, new CommonCallback.Listener() {
            @Override
            public void onSuccess(Object body) {
                ChangeAddressApiResponse changeAddressApiResponse = (ChangeAddressApiResponse) body;
                if (changeAddressApiResponse.getStatus().equals(200)) {
                    CommonFunctions.getInstance().successResponseToast(mContext, changeAddressApiResponse.getMessage());
                } else {

                }
            }

            @Override
            public void onFailure(String reason) {

            }
        });
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView tvOrder, txtCategory, tvCategory, tvTotalPrice, txtPickup, tvPickup, txtDelivery, tvDelivery, txtOrderStatus, tvOrderStatuss;
        LinearLayout llOrders;

        ViewHolder(View itemView) {
            super(itemView);
            tvOrder = (TextView) itemView.findViewById(R.id.tvOrder);
            txtCategory = (TextView) itemView.findViewById(R.id.txtCategory);
            tvCategory = (TextView) itemView.findViewById(R.id.tvCategory);
            tvTotalPrice = (TextView) itemView.findViewById(R.id.tvTotalPrice);
            txtPickup = (TextView) itemView.findViewById(R.id.txtPickup);
            tvPickup = (TextView) itemView.findViewById(R.id.tvPickup);
            txtDelivery = (TextView) itemView.findViewById(R.id.txtDelivery);
            tvDelivery = (TextView) itemView.findViewById(R.id.tvDelivery);
            txtOrderStatus = (TextView) itemView.findViewById(R.id.txtOrderStatus);
            tvOrderStatuss = (TextView) itemView.findViewById(R.id.tvOrderStatuss);
            llOrders = (LinearLayout) itemView.findViewById(R.id.llOrders);
        }

    }

    OrderListApiResponse.Datum getItem(int id) {
        return mData.get(id);
    }

    void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }
}