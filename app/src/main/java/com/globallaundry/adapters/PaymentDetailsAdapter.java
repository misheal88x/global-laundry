package com.globallaundry.adapters;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.globallaundry.R;
import com.globallaundry.activity.PaymentActivity;
import com.globallaundry.apimodel.CalculateApiResponse;
import com.globallaundry.databinding.AdapterPaymentDetailsBinding;
import com.globallaundry.databinding.AdapterSpacialtyBinding;
import com.globallaundry.interfaces.SelectArea;
import com.globallaundry.models.SelectAreaApiResponse;
import com.globallaundry.utils.SessionManager;

import java.util.List;

public class PaymentDetailsAdapter extends RecyclerView.Adapter<PaymentDetailsAdapter.MyViewHolder> {

    private Activity context;
    private List<CalculateApiResponse.Payment_detail> responses;
    AdapterPaymentDetailsBinding binding;
    private final String currencyType;


    public PaymentDetailsAdapter(PaymentActivity context, List<CalculateApiResponse.Payment_detail> payment_details, String currencyType) {
        this.context = context;
        this.responses = payment_details;
        this.currencyType = currencyType;

    }

    @NonNull
    @Override
    public PaymentDetailsAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        binding = DataBindingUtil.inflate(layoutInflater, R.layout.adapter_payment_details, parent, false);
        return new PaymentDetailsAdapter.MyViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull PaymentDetailsAdapter.MyViewHolder myViewHolder, int i) {

        final CalculateApiResponse.Payment_detail listApiResponse = responses.get(i);
        if (SessionManager.getInstance().getAppLanguageCode().equals("ar")) {
            context.getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
            binding.txtViewName.setGravity(Gravity.END);
            binding.txtViewNamevalue.setGravity(Gravity.START);
        } else {
            context.getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
            binding.txtViewName.setGravity(Gravity.START);
            binding.txtViewNamevalue.setGravity(Gravity.END);
        }

        if (listApiResponse.getBold().equals(1)) {
            myViewHolder.txtViewName.setText(listApiResponse.getName());
            if (currencyType.equals("1")){
                myViewHolder.txtViewNamevalue.setText(listApiResponse.getValue());
            }
            else if (currencyType.equals("2")){
                myViewHolder.txtViewNamevalue.setText(listApiResponse.getUsd_value());
            }
            myViewHolder.txtViewName.setTypeface(myViewHolder.txtViewName.getTypeface(), Typeface.BOLD);
            myViewHolder.txtViewNamevalue.setTypeface(myViewHolder.txtViewNamevalue.getTypeface(), Typeface.BOLD);
            myViewHolder.txtViewName.setTextColor(ContextCompat.getColor(context, R.color.colorPrimary));
            myViewHolder.txtViewNamevalue.setTextColor(ContextCompat.getColor(context, R.color.colorPrimary));
        } else {
            myViewHolder.txtViewName.setText(listApiResponse.getName());


            if (currencyType.equals("1")){
                myViewHolder.txtViewNamevalue.setText(listApiResponse.getValue());
            }
            else if (currencyType.equals("2")){
                myViewHolder.txtViewNamevalue.setText(listApiResponse.getUsd_value());
            }

        }


    }

    @Override
    public int getItemCount() {
        return responses.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private final TextView txtViewName;
        private final TextView txtViewNamevalue;
        private final LinearLayout llyParent;

        public MyViewHolder(@NonNull AdapterPaymentDetailsBinding itemView) {
            super(itemView.getRoot());

            txtViewName = itemView.txtViewName;
            txtViewNamevalue = itemView.txtViewNamevalue;
            llyParent = itemView.llyParent;
        }
    }
}