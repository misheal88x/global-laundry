package com.globallaundry.adapters;

import android.app.Activity;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RatingBar;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;
import com.globallaundry.R;
import com.globallaundry.apimodel.ReviewListApiResponse;
import com.globallaundry.utils.SessionManager;
import java.util.List;

public class ReviewAdapter extends RecyclerView.Adapter<ReviewAdapter.MyViewHolder> {

    private final Activity context;
    private final List<ReviewListApiResponse.Datum> responses;

    public ReviewAdapter(FragmentActivity activity, List<ReviewListApiResponse.Datum> responses) {
        this.context = activity;
        this.responses = responses;
    }

    @NonNull
    @Override
    public ReviewAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_review, parent, false);
        return new ReviewAdapter.MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ReviewAdapter.MyViewHolder holder, int position) {
        ReviewListApiResponse.Datum listResponses = responses.get(position);

        if (SessionManager.getInstance().getAppLanguageCode().equals("ar")) {
            context.getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
            holder.tvName.setGravity(Gravity.END);
            holder.tvDate.setGravity(Gravity.START);
        } else {
            context.getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
            holder.tvName.setGravity(Gravity.START);
            holder.tvDate.setGravity(Gravity.END);
        }
        holder.tvName.setText(listResponses.getFirst_name());
        holder.tvDate.setText(listResponses.getCreated_at());
        holder.tvDescription.setText(listResponses.getComments());
        holder.rbBar.setRating(listResponses.getRating());
    }

    @Override
    public int getItemCount() {
        return responses.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private final RatingBar rbBar;
        TextView tvName, tvDate, tvDescription;

        public MyViewHolder(View itemView) {
            super(itemView);

            tvName = (TextView) itemView.findViewById(R.id.tvName);
            tvDate = (TextView) itemView.findViewById(R.id.tvDate);
            tvDescription = (TextView) itemView.findViewById(R.id.tvDescription);
            rbBar = (RatingBar) itemView.findViewById(R.id.rbBar);
        }
    }
}