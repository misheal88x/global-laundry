package com.globallaundry.adapters;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.globallaundry.R;
import com.globallaundry.activity.CartActivity;
import com.globallaundry.apimodel.GetSubscribedItemsTypes;
import com.globallaundry.app_model.SizeModel;
import com.globallaundry.dbmodel.ItemRealmList;
import com.globallaundry.dbmodel.RealmLibrary;
import com.globallaundry.dbmodel.SizeRealmList;
import com.globallaundry.interfaces.ChangeInCart;
import com.globallaundry.models.CacheManager;
import com.globallaundry.utils.CommonFunctions;
import com.globallaundry.utils.LanguageConstants;
import com.globallaundry.utils.SessionManager;

import java.util.ArrayList;
import java.util.List;

import io.realm.RealmResults;

public class CartAdapter extends RecyclerView.Adapter<CartAdapter.MyViewHolder> {

    private final Context context;
    private final RealmResults<ItemRealmList> responses;
    private final ChangeInCart changeInCart;


    public CartAdapter(Context context, RealmResults<ItemRealmList> responses, ChangeInCart changeInCart) {
        this.context = context;
        this.responses = responses;
        this.changeInCart = changeInCart;
    }

    @NonNull
    @Override
    public CartAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_cart_item, parent, false);

        return new CartAdapter.MyViewHolder(view);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull CartAdapter.MyViewHolder holder, int position) {

        ItemRealmList listResponses = responses.get(position);

        holder.txtCategoryName.setText(listResponses.getCloth_type_name());
        holder.tvCount.setText(listResponses.getQuantity());

        double priceValue = listResponses.getPrice() * Integer.valueOf(listResponses.getQuantity());
        holder.txtPrice.setText(SessionManager.getInstance().getCurrencyCode() + " " + priceValue);

        if (listResponses.getItem_type().equals(1)) {
            holder.txtTypeName.setText("METER");
            holder.tvType.setText(listResponses.getAdditional_quantity() + "");

            holder.lyoutSizeList.setVisibility(View.GONE);
            holder.lyplusminus.setVisibility(View.VISIBLE);
            holder.txtPrice.setVisibility(View.VISIBLE);
        } else if (listResponses.getItem_type().equals(2)) {
            holder.txtTypeName.setText("KG");
            holder.tvType.setText(listResponses.getAdditional_quantity() + "");

            holder.lyoutSizeList.setVisibility(View.GONE);
            holder.lyplusminus.setVisibility(View.VISIBLE);
            holder.txtPrice.setVisibility(View.VISIBLE);
        } else if (listResponses.getItem_type().equals(3)) {
            holder.txtTypeName.setText("");
            holder.tvType.setText("");
            // -- Size
            if (listResponses.getSize() != null) {

                if (listResponses.getSize().size() > 0) {
                    holder.lyoutSizeList.setVisibility(View.VISIBLE);
                    holder.lyplusminus.setVisibility(View.GONE);
                    holder.txtPrice.setVisibility(View.GONE);
                    addInflateView(holder.lyoutSizeList, listResponses);
                } else {
                    holder.lyoutSizeList.setVisibility(View.GONE);
                    holder.lyplusminus.setVisibility(View.GONE);
                    holder.txtPrice.setVisibility(View.GONE);
                }
            }

        } else if (listResponses.getItem_type().equals(4)) {
            holder.txtTypeName.setText("");
            holder.lyoutSizeList.setVisibility(View.GONE);
            holder.lyplusminus.setVisibility(View.VISIBLE);
            holder.txtPrice.setVisibility(View.VISIBLE);
        }


        if (listResponses.getCloth_type_image() != null) {
            Glide.with(context).load(listResponses.getCloth_type_image()).into(holder.ivProduct);
        }



        holder.lyoutParent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
        // -- Plus
        refreshDiscountField(holder, position);

        holder.txtPlus.setOnClickListener(new View.OnClickListener() {


            @Override
            public void onClick(View v) {

                Log.i("SDFSDFD", "DDDDDDDDDDDDDDDDD-");

                String quantity = holder.tvCount.getText().toString();


                holder.tvCount.setText(String.valueOf(Integer.parseInt(quantity) + 1));

                List<SizeModel> sizeModels = new ArrayList<>();
                for (int count = 0; count < responses.get(position).getSize().size(); count++) {
                    SizeModel sizeModel = new SizeModel();
                    SizeRealmList ing = responses.get(position).getSize().get(count);
                    sizeModel.setSize_key(ing.getSize_key());
                    sizeModel.setPrice(ing.getPrice());
                    sizeModel.setSize_name(ing.getSize_name());
                    sizeModel.setSize_quantity(ing.getSize_quantity());
                    sizeModels.add(sizeModel);
                }
                RealmLibrary.getInstance().insertItem(context, responses.get(position).getCloth_type_key(),
                        responses.get(position).getCloth_type_name(),
                        responses.get(position).getCloth_type_image(), responses.get(position).getItem_type(),
                        responses.get(position).getPrice(), SessionManager.getInstance().getCategoryKey(), "1", sizeModels, 0, "tet",
                        holder.tvCount.getText().toString().trim(),
                        responses.get(position).getExpress_amount(), responses.get(position).getIs_express(),
                        responses.get(position).getAdditional_quantity());

                // double priceValue = responses.get(position).getPrice() * Integer.valueOf(holder.tvCount.getText().toString().trim());
                // holder.txtPrice.setText(priceValue+" "+SessionManager.getInstance().getCurrencyCode());
                notifyDataSetChanged();
                changeInCart.onClick();
            }
        });
        // -- Minus
        holder.txtMinus.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onClick(View view) {
                final String quantity = holder.tvCount.getText().toString();
                if (Integer.parseInt(quantity) > 1) {

                    refreshDiscountField(holder, position);

                    holder.tvCount.setText(String.valueOf(Integer.parseInt(quantity) - 1));

                    List<SizeModel> sizeModels = new ArrayList<>();
                    for (int count = 0; count < responses.get(position).getSize().size(); count++) {
                        SizeModel sizeModel = new SizeModel();
                        SizeRealmList ing = responses.get(position).getSize().get(count);
                        sizeModel.setSize_key(ing.getSize_key());
                        sizeModel.setPrice(ing.getPrice());
                        sizeModel.setSize_name(ing.getSize_name());
                        sizeModel.setSize_quantity(ing.getSize_quantity());
                        sizeModels.add(sizeModel);
                    }
                    RealmLibrary.getInstance().insertItem(context, responses.get(position).getCloth_type_key(),
                            responses.get(position).getCloth_type_name(),
                            responses.get(position).getCloth_type_image(), responses.get(position).getItem_type(),
                            responses.get(position).getPrice(), SessionManager.getInstance().getCategoryKey(), "2", sizeModels, 0, "tet",
                            holder.tvCount.getText().toString().trim(),
                            responses.get(position).getExpress_amount(),
                            responses.get(position).getIs_express(), responses.get(position).getAdditional_quantity());
                    notifyDataSetChanged();
                    changeInCart.onClick();

                } else {
                    final Dialog dialog = new Dialog(context);
                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    dialog.setContentView(R.layout.dialog_alert_new);
                    dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
//                    dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
                    TextView tvQue = dialog.findViewById(R.id.tvQue);
                    TextView tvTitle = dialog.findViewById(R.id.tvTitle);
                    TextView btnCancel = dialog.findViewById(R.id.btn_cancel_new);
                    TextView btnYes = dialog.findViewById(R.id.btnYesNew);
                    btnCancel.setText(LanguageConstants.No);
                    btnYes.setText(LanguageConstants.Yes);
                    tvTitle.setText(LanguageConstants.alert);
                    tvQue.setTextSize(13);
                    tvQue.setText(LanguageConstants.do_you_want_to_delete_this_product);
                    btnYes.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            List<SizeModel> sizeModels = new ArrayList<>();
                            for (int count = 0; count < responses.get(position).getSize().size(); count++) {
                                SizeModel sizeModel = new SizeModel();
                                SizeRealmList ing = responses.get(position).getSize().get(count);
                                sizeModel.setSize_key(ing.getSize_key());
                                sizeModel.setPrice(ing.getPrice());
                                sizeModel.setSize_name(ing.getSize_name());
                                sizeModel.setSize_quantity(ing.getSize_quantity());
                                sizeModels.add(sizeModel);
                            }
                            RealmLibrary.getInstance().insertItem(context, responses.get(position).getCloth_type_key(),
                                    responses.get(position).getCloth_type_name(),
                                    responses.get(position).getCloth_type_image(), responses.get(position).getItem_type(),
                                    responses.get(position).getPrice(), SessionManager.getInstance().getCategoryKey(), "2", sizeModels, 0, "tet",
                                    holder.tvCount.getText().toString().trim(),
                                    responses.get(position).getExpress_amount(), responses.get(position).getIs_express(),
                                    responses.get(position).getAdditional_quantity());
                            notifyDataSetChanged();
                            if (responses.size() == 0) {
                                ((Activity) context).finish();
                            }
                            dialog.cancel();
                        }
                    });
                    btnCancel.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.dismiss();
                        }
                    });
                    Window window = dialog.getWindow();
                    WindowManager.LayoutParams wlp = window.getAttributes();
                    wlp.gravity = Gravity.CENTER;
                    window.setAttributes(wlp);
                    dialog.show();
//
                }
            }
        });

        holder.txtRemove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Dialog dialog = new Dialog(context);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.dialog_alert_new);
                dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
                TextView tvQue = dialog.findViewById(R.id.tvQue);
                TextView tvTitle = dialog.findViewById(R.id.tvTitle);
                TextView btnCancel = dialog.findViewById(R.id.btn_cancel_new);
                TextView btnYes = dialog.findViewById(R.id.btnYesNew);
                btnCancel.setText(LanguageConstants.No);
                btnYes.setText(LanguageConstants.Yes);
                tvTitle.setText(LanguageConstants.alert);
                tvQue.setText(LanguageConstants.do_you_want_to_delete_this_product);
                tvQue.setTextSize(13);
                btnYes.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        RealmLibrary.getInstance().deleteItem(responses.get(position).getId());
                        dialog.dismiss();
                        holder.lyoutParent.removeAllViews();
                        notifyDataSetChanged();
                        changeInCart.onClick();
                        if (responses.size() == 0) {
                            ((Activity) context).finish();
                        }
                    }
                });
                btnCancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
                Window window = dialog.getWindow();
                WindowManager.LayoutParams wlp = window.getAttributes();
                wlp.gravity = Gravity.CENTER;
                window.setAttributes(wlp);
                dialog.show();

            }
        });


    }

    private void refreshDiscountField(MyViewHolder holder, int position) {
        List<GetSubscribedItemsTypes.Item> itemList = (List<GetSubscribedItemsTypes.Item>)
                CacheManager.getInstance().get("items_types_list");
        String quantity = holder.tvCount.getText().toString();
        int checkSubscribedPackage = CommonFunctions.getInstance().findItemInMyPackages(
                responses.get(position).getCloth_type_key(),
                Integer.parseInt(quantity), itemList);

        if (checkSubscribedPackage >= 0) {
            holder.txt_discount_items.setVisibility(View.VISIBLE);
            holder.txt_discount_items.setText(
                    String.format("%s discount",
                            quantity));

            if (SessionManager.getInstance().getAppLanguageCode().equals("ar"))
            {
                holder.txt_discount_items.setText(
                        String.format("%s حسم",
                                quantity));
            }
        } else if (checkSubscribedPackage != Integer.MIN_VALUE) {
            int discount = checkSubscribedPackage + Integer.parseInt(quantity);
            if (discount > 0) {
                holder.txt_discount_items.setVisibility(View.VISIBLE);
                holder.txt_discount_items.setText(
                        String.format("%d discount",
                                discount));

                if (SessionManager.getInstance().getAppLanguageCode().equals("ar"))
                {
                    holder.txt_discount_items.setText(
                            String.format("%d حسم",
                                    discount));
                }
            }else {
                holder.txt_discount_items.setText("");
                holder.txt_discount_items.setVisibility(View.GONE);
            }

        }
    }

    @SuppressLint("SetTextI18n")
    private void addInflateView(LinearLayout lyoutSizeList, ItemRealmList listResponses) {
        for (int i = 0; i < listResponses.getSize().size(); i++) {
            final LinearLayout newView = (LinearLayout) ((Activity) context).getLayoutInflater().inflate(R.layout.inflate_type_three, null);

            TextView txtName = (TextView) newView.findViewById(R.id.txtName);
            TextView txtPrice = (TextView) newView.findViewById(R.id.txtPrice);
            TextView btnItemRemove = (TextView) newView.findViewById(R.id.btnItemRemove);
            TextView tvTotalQuantity = (TextView) newView.findViewById(R.id.tvTotalQuantity);
            TextView btnItemAdd = (TextView) newView.findViewById(R.id.btnItemAdd);

            txtName.setText(listResponses.getSize().get(i).getSize_name());
            double doubleValue = listResponses.getSize().get(i).getPrice() * listResponses.getSize().get(i).getSize_quantity();
            txtPrice.setText(SessionManager.getInstance().getCurrencyCode() + " " + doubleValue);
            tvTotalQuantity.setText(listResponses.getSize().get(i).getSize_quantity() + "");

            // --- Plus
            int finalI = i;

            btnItemAdd.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String quantity = tvTotalQuantity.getText().toString();
                    tvTotalQuantity.setText(String.valueOf(Integer.parseInt(quantity) + 1));

                    RealmLibrary.getInstance().updateSize(listResponses.getSize().get(finalI), Integer.parseInt(tvTotalQuantity.getText().toString()));

                    notifyDataSetChanged();
                    changeInCart.onClick();
                }
            });

            // --- Minus
            btnItemRemove.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String quantity = tvTotalQuantity.getText().toString();
                    int qty = Integer.parseInt(tvTotalQuantity.getText().toString()) - 1;
                    if (qty == 0) {
                        RealmLibrary.getInstance().removeSize(listResponses, listResponses.getSize().get(finalI));
                    } else {
                        tvTotalQuantity.setText(String.valueOf(Integer.parseInt(quantity) - 1));
                        RealmLibrary.getInstance().updateSize(listResponses.getSize().get(finalI), Integer.parseInt(tvTotalQuantity.getText().toString()));
                    }

                    notifyDataSetChanged();
                    changeInCart.onClick();
                }
            });
            // Add View
            lyoutSizeList.addView(newView);
        }
    }


    @Override
    public int getItemCount() {
        return responses.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView ivProduct;
        TextView txtCategoryName, tvType, txtPrice, txtTypeName, txtMinus, tvCount, txtPlus, txtRemove, txt_discount_items;
        LinearLayout lyoutParent, lyplusminus, lyoutSizeList;

        public MyViewHolder(View itemView) {
            super(itemView);

            ivProduct = (ImageView) itemView.findViewById(R.id.image);
            txtCategoryName = (TextView) itemView.findViewById(R.id.txtName);
            txtPrice = (TextView) itemView.findViewById(R.id.txtPrice);
            tvType = (TextView) itemView.findViewById(R.id.tvType);
            txtTypeName = (TextView) itemView.findViewById(R.id.txtTypeName);
            lyoutParent = (LinearLayout) itemView.findViewById(R.id.lyoutParent);
            txtMinus = (TextView) itemView.findViewById(R.id.txtMinus);
            tvCount = (TextView) itemView.findViewById(R.id.tvCount);
            txtPlus = (TextView) itemView.findViewById(R.id.txtPlus);
            lyplusminus = (LinearLayout) itemView.findViewById(R.id.lyplusminus);
            txtRemove = itemView.findViewById(R.id.txtRemove);
            lyoutSizeList = itemView.findViewById(R.id.lyoutSizeList);
            txt_discount_items = itemView.findViewById(R.id.txt_discount_lbl);


        }
    }
}