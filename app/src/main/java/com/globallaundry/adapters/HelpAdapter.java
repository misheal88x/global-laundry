package com.globallaundry.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.location.LocationManager;
import android.net.Uri;

import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;


import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.globallaundry.R;
import com.globallaundry.activity.HelpBrowser;
import com.globallaundry.apimodel.HelpApiResponse;
import com.globallaundry.utils.SessionManager;

import java.util.List;


public class HelpAdapter extends RecyclerView.Adapter<HelpAdapter.MyViewHolder> {


    private List<HelpApiResponse.Datum> mData;
    private LayoutInflater mInflater;
    Activity mContext;
    private String update;
    private LocationManager locationManager;

    public HelpAdapter(Activity context, List<HelpApiResponse.Datum> data) {
        this.mInflater = LayoutInflater.from(context);
        this.mData = data;
        this.mContext = context;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_help, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, int position) {

        HelpApiResponse.Datum data = mData.get(position);
        if (SessionManager.getInstance().getAppLanguageCode().equals("ar")) {
            mContext.getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
            myViewHolder.imgGo.setImageResource(R.drawable.ic_back_arrow);
        } else {
            mContext.getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
            myViewHolder.imgGo.setImageResource(R.drawable.ic_right_arrow);
        }
        myViewHolder.txtHelp.setText(data.getCms_title());

        myViewHolder.llParantHelp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, HelpBrowser.class);
                intent.putExtra("url", data.getCms_url());
                mContext.startActivity(intent);

            }
        });
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView txtHelp, tvAddress;

        ImageView imgGo;
        LinearLayout llParantHelp;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            txtHelp = (TextView) itemView.findViewById(R.id.txtHelp);
            imgGo = (ImageView) itemView.findViewById(R.id.imgGo);
            llParantHelp = (LinearLayout) itemView.findViewById(R.id.llParantHelp);
        }
    }
}
