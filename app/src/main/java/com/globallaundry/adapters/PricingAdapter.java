package com.globallaundry.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.globallaundry.R;
import com.globallaundry.apimodel.ClothTypeListViewApiResponse;
import com.globallaundry.utils.CommonFunctions;

import java.util.List;

public class PricingAdapter extends RecyclerView.Adapter<PricingAdapter.MyViewHolder> {

    private final Context context;
    private final List<ClothTypeListViewApiResponse.Size> responses;
    private final List<ClothTypeListViewApiResponse.Size> selectedSize;


    public PricingAdapter(Context context, List<ClothTypeListViewApiResponse.Size> responses, List<ClothTypeListViewApiResponse.Size> selectedSize) {
        this.context = context;
        this.responses = responses;
        this.selectedSize = selectedSize;
    }

    @NonNull
    @Override
    public PricingAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_pricing, parent, false);

        return new PricingAdapter.MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull PricingAdapter.MyViewHolder holder, int position) {

        ClothTypeListViewApiResponse.Size size = responses.get(position);

        holder.txtCategoryName.setText(size.getSize_type_name());
        //holder.txtPrice.setText(size.getPrice());
        holder.txtPrice.setText(CommonFunctions.getInstance().roundOffDecimalValue(Double.parseDouble(size.getPrice()), true));
        holder.lyoutParent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

        holder.btnItemAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int qty = Integer.parseInt(holder.tvTotalQuantity.getText().toString()) + 1;
                holder.tvTotalQuantity.setText(String.valueOf(qty));
                size.setQty(qty);
                if (!selectedSize.contains(size)) {
                    selectedSize.add(size);
                }
            }
        });

        holder.btnItemRemove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int qty = Integer.parseInt(holder.tvTotalQuantity.getText().toString());
                if (qty > 0) {
                    int myQty = qty - 1;
                    holder.tvTotalQuantity.setText(String.valueOf(myQty));
                    size.setQty(myQty);
                    if (selectedSize.contains(size)) {
                        if (myQty == 0) {
                            selectedSize.remove(size);
                        }
                    } else if (!selectedSize.contains(size)) {
                        selectedSize.add(size);
                    }
                }
            }
        });

    }


    @Override
    public int getItemCount() {
        return responses.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView ivProduct;
        TextView txtCategoryName, txtPrice, tvTotalQuantity, btnItemRemove, btnItemAdd;
        LinearLayout lyoutParent;

        public MyViewHolder(View itemView) {
            super(itemView);

            ivProduct = (ImageView) itemView.findViewById(R.id.image);
            txtCategoryName = (TextView) itemView.findViewById(R.id.txtName);
            txtPrice = (TextView) itemView.findViewById(R.id.txtPrice);
            btnItemAdd = itemView.findViewById(R.id.btnItemAdd);
            btnItemRemove = itemView.findViewById(R.id.btnItemRemove);
            tvTotalQuantity = itemView.findViewById(R.id.tvTotalQuantity);
            txtPrice = (TextView) itemView.findViewById(R.id.txtPrice);
            lyoutParent = (LinearLayout) itemView.findViewById(R.id.lyoutParent);
        }
    }
}