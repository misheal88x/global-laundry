package com.globallaundry.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.globallaundry.R;
import com.globallaundry.apimodel.PickUpTimeSlotApiResponse;
import com.globallaundry.databinding.AdapterSpacialtyBinding;
import com.globallaundry.interfaces.SelectArea;
import com.globallaundry.interfaces.SelectTime;
import com.globallaundry.models.SelectAreaApiResponse;

import java.util.List;

public class SelectTimeAdapter extends RecyclerView.Adapter<SelectTimeAdapter.MyViewHolder> {

    private final Context context;
    private final List<PickUpTimeSlotApiResponse.Time> responses;
    private final SelectTime selectTime;

    AdapterSpacialtyBinding binding;

    public SelectTimeAdapter(Context context, List<PickUpTimeSlotApiResponse.Time> responses, SelectTime selectTime) {
        this.context = context;
        this.responses = responses;
        this.selectTime = selectTime;
    }

    @NonNull
    @Override
    public SelectTimeAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        binding = DataBindingUtil.inflate(layoutInflater, R.layout.adapter_spacialty, parent, false);
        return new SelectTimeAdapter.MyViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull SelectTimeAdapter.MyViewHolder myViewHolder, int i) {

        PickUpTimeSlotApiResponse.Time listApiResponse = responses.get(i);

        myViewHolder.txtViewName.setText(listApiResponse.getName());


        myViewHolder.llyParent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectTime.onClick(listApiResponse.getValue(), listApiResponse.getName());
            }
        });
    }

    @Override
    public int getItemCount() {
        return responses.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private final TextView txtViewName;
        private final LinearLayout llyParent;

        public MyViewHolder(@NonNull AdapterSpacialtyBinding itemView) {
            super(itemView.getRoot());

            txtViewName = itemView.txtViewName;
            llyParent = itemView.llyParent;
        }
    }
}