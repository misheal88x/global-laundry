package com.globallaundry.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.globallaundry.R;
import com.globallaundry.api.CommonApiCalls;
import com.globallaundry.apimodel.PackageData;
import com.globallaundry.apimodel.SubscribePackageApiResponse;
import com.globallaundry.interfaces.CommonCallback;
import com.globallaundry.utils.MyApplication;
import com.globallaundry.utils.SessionManager;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class PackagesAdapter extends RecyclerView.Adapter<PackagesAdapter.SubscribedPackageViewHolder> {

    private Context context;
    private List<PackageData> dataList;
    private boolean showAll;

    public PackagesAdapter(Context context, List<PackageData> dataList, boolean showAll) {
        this.context = context;
        this.dataList = dataList;
        this.showAll = showAll;

        Log.d("XXXXXXXXXX", SessionManager.getInstance().getAccessToken());
    }

    @NonNull
    @Override
    public SubscribedPackageViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.subscribed_package_item, parent, false);
        return new SubscribedPackageViewHolder(view);
    }


    protected String getValidity(String v) {
        switch (v.toLowerCase()) {
            case "m": {
                return SessionManager.getInstance().getAppLanguageCode().equals("en") ? "Monthly" : "شهرياً";
            }

            case "w": {
                return SessionManager.getInstance().getAppLanguageCode().equals("en") ? "Weekly" : "اسبوعياً";
            }
        }
        return "UnKnown";
    }

    @SuppressLint({"DefaultLocale", "RestrictedApi"})
    @Override
    public void onBindViewHolder(@NonNull SubscribedPackageViewHolder holder, final int position) {

        holder.name.setText(dataList.get(position).getName());
        holder.packagePrice.setText(String.format("%.3f LBP", Float.parseFloat("" + dataList.get(position).getPrice())));
        holder.validity.setText(this.getValidity(dataList.get(position).getValidity()));
        holder.subscribed_package_items_expiry_date.setText(dataList.get(position).getExpiryDate());


        holder.itemsRecyclerView.setLayoutManager(new LinearLayoutManager(context));
        holder.itemsRecyclerView.setNestedScrollingEnabled(false);
        List<PackageData.Item.ClothType> types = new ArrayList<>();
        for (PackageData.Item o : dataList.get(position).getItems()) {
            for (PackageData.Item.ClothType c : o.getClothTypeList()) {
                types.add(c);
            }
        }

        PackageItemAdapter packageItemAdapter = new PackageItemAdapter(context, types);
        holder.itemsRecyclerView.setAdapter(packageItemAdapter);


        try {

            if (dataList.get(position).getExpiryDate() != null) {
                Date expiryDate = new SimpleDateFormat("yyyy-MM-dd").parse(dataList.get(position).getExpiryDate());
                Date today = new Date();
                long diff = expiryDate.getTime() - today.getTime();
                diff = (int) (diff / (1000 * 60 * 60 * 24));
                int numOfDays = (int) diff;
                int hours = (int) (diff / (1000 * 60 * 60));

                if (SessionManager.getInstance().getAppLanguageCode().equals("en")) {
                    holder.subscribed_package_items_remaining.setText(
                            String.format("%d Days, %d Hours", numOfDays, hours)
                    );
                } else {
                    holder.subscribed_package_items_remaining.setText(
                            String.format("%d يوم, %d ساعة", numOfDays, hours)
                    );
                }
            }


        } catch (ParseException e) {
            e.printStackTrace();
        }
        if (showAll) {


            holder.subscribed_package_items_remaining.setVisibility(View.GONE);
            holder.subscribed_package_items_remaining_lbl.setVisibility(View.GONE);
            holder.subscribed_package_items_expiry_date.setVisibility(View.GONE);
            holder.package_items_expiry_date_lbl.setVisibility(View.GONE);


            holder.subscribeButton.setVisibility(View.VISIBLE);
            holder.subscribeButton.setOnClickListener(view -> {


                CommonApiCalls.getInstance().subscribePackage(context, dataList.get(position).getId(), new CommonCallback.Listener() {
                    @Override
                    public void onSuccess(Object body) {
                        SubscribePackageApiResponse response = (SubscribePackageApiResponse) body;
                        SubscribePackageApiResponse.Data data = response.getData();
                        MyApplication.displayKnownErrorSuccess(context, data.getMessage());
                    }

                    @Override
                    public void onFailure(String reason) {
                        MyApplication.displayKnownError(reason);
                    }
                });

            });
        } else {
            holder.subscribeButton.setVisibility(View.GONE);
        }

    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }


    static class SubscribedPackageViewHolder extends RecyclerView.ViewHolder {

        public TextView name, validity, packagePrice, subscribed_package_items_expiry_date, subscribed_package_items_remaining;
        private TextView itemsNoLbl, package_items_expiry_date_lbl,
                subscribed_package_items_remaining_lbl, package_items_expiry_items_lbl;

        private RecyclerView itemsRecyclerView;

        public FloatingActionButton subscribeButton;

        public SubscribedPackageViewHolder(@NonNull View itemView) {
            super(itemView);

            name = itemView.findViewById(R.id.subscribed_package_name_id);
            packagePrice = itemView.findViewById(R.id.subscribed_packagePrice_id);
            validity = itemView.findViewById(R.id.subscribed_package_validity_id);
            subscribed_package_items_expiry_date = itemView.findViewById(R.id.subscribed_package_items_expiry_date);
            subscribeButton = itemView.findViewById(R.id.btnAddPackage);
            subscribed_package_items_remaining = itemView.findViewById(R.id.subscribed_package_items_remaining);
            itemsRecyclerView = itemView.findViewById(R.id.packages_item_recyclerview);


            subscribed_package_items_remaining_lbl = itemView.findViewById(R.id.subscribed_package_items_remaining_lbl);
            itemsNoLbl = itemView.findViewById(R.id.package_items_no_lbl);
            package_items_expiry_date_lbl = itemView.findViewById(R.id.package_items_expiry_date_lbl);
            package_items_expiry_items_lbl = itemView.findViewById(R.id.package_items_expiry_items_lbl);

            if (!SessionManager.getInstance().getAppLanguageCode().equals("en")) {

                itemsNoLbl.setText("عدد البدلات: ");

                package_items_expiry_date_lbl.setText("تاريخ الصلاحية:");
                subscribed_package_items_remaining_lbl.setText("متبقي:");
                package_items_expiry_items_lbl.setText("العناصر المتوفرة: ");
            }

        }
    }
}
