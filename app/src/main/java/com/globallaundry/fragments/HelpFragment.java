package com.globallaundry.fragments;

import android.Manifest;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.globallaundry.R;
import com.globallaundry.adapters.HelpAdapter;
import com.globallaundry.api.CommonApiCalls;
import com.globallaundry.apimodel.ContactusSubmitApiResponse;
import com.globallaundry.apimodel.GetContactusApiResponse;
import com.globallaundry.apimodel.HelpApiResponse;
import com.globallaundry.databinding.FragmentContactusBinding;
import com.globallaundry.databinding.FragmentHelpBinding;
import com.globallaundry.interfaces.CommonCallback;
import com.globallaundry.utils.CommonFunctions;
import com.globallaundry.utils.LanguageConstants;
import com.globallaundry.utils.SessionManager;

import lolodev.permissionswrapper.callback.OnRequestPermissionsCallBack;
import lolodev.permissionswrapper.wrapper.PermissionWrapper;


public class HelpFragment extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";


    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;
    private AppCompatActivity activity;
    FragmentHelpBinding binding;

    public HelpFragment() {
    }

    // TODO: Rename and change types and number of parameters
    public static HelpFragment newInstance(String param1, String param2) {
        HelpFragment fragment = new HelpFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_help, container, false);
        View view = binding.getRoot();

        Toolbar mToolbar = activity.findViewById(R.id.toolbar);
        ImageView ivTBIcon1 = mToolbar.findViewById(R.id.ivTBIcon1);
        ivTBIcon1.setImageResource(R.drawable.ic_plus);
        ivTBIcon1.setVisibility(View.INVISIBLE);
        LinearLayout llhometool = mToolbar.findViewById(R.id.llhometool);
        llhometool.setVisibility(View.GONE);
        initView();

        ivTBIcon1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


            }
        });
        initView();
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    private void initView() {
        callHelpApi();
    }

    private void callHelpApi() {
        CommonApiCalls.getInstance().helpList(getActivity(), new CommonCallback.Listener() {
            @Override
            public void onSuccess(Object body) {
                HelpApiResponse helpApiResponse = (HelpApiResponse) body;

                if (helpApiResponse.getData() != null && helpApiResponse.getData().size() > 0) {
                    HelpAdapter adapter = new HelpAdapter(getActivity(), helpApiResponse.getData());
                    binding.rvHelp.setAdapter(adapter);
                    binding.rvHelp.setHasFixedSize(true);//every item of the RecyclerView has a fix size
                    binding.rvHelp.setLayoutManager(new LinearLayoutManager(getContext()));
                    RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
                    binding.rvHelp.setLayoutManager(mLayoutManager);
                    binding.rvHelp.setItemAnimator(new DefaultItemAnimator());
                    binding.rvHelp.setVisibility(View.VISIBLE);
                    binding.rvHelp.setNestedScrollingEnabled(false);

                } else {
                    binding.rvHelp.setVisibility(View.GONE);
                }
            }

            @Override
            public void onFailure(String reason) {

            }
        });
    }


    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        }
        activity = (AppCompatActivity) context;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

    }


    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
