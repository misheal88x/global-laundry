package com.globallaundry.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.globallaundry.R;
import com.globallaundry.activity.AllAvailablePackagesActivity;
import com.globallaundry.adapters.PackagesAdapter;
import com.globallaundry.api.CommonApiCalls;
import com.globallaundry.apimodel.GetAvailablePackagesApiResponse;
import com.globallaundry.apimodel.GetMySubscribedPackagesApiResponse;
import com.globallaundry.apimodel.SubscribePackageApiResponse;
import com.globallaundry.interfaces.CommonCallback;
import com.globallaundry.utils.SessionManager;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

public class SubscribedPackagesFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {

    private OnFragmentInteractionListener mListener;
    private AppCompatActivity activity;
    private RecyclerView recyclerView;
    private SwipeRefreshLayout refreshLayout;
    private FloatingActionButton addPackageBtn;
    private TextView subscribed_packages_no_found;


    public SubscribedPackagesFragment() {
        // Required empty public constructor
    }

    public static SubscribedPackagesFragment newInstance(String param1, String param2) {
        SubscribedPackagesFragment fragment = new SubscribedPackagesFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_subscribed_packages, container, false);

        bindElements(view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


    }

    private void bindElements(View view) {

        recyclerView = view.findViewById(R.id.subscribed_packages_id);
        refreshLayout = view.findViewById(R.id.subscribed_packages_refresh_id);
        addPackageBtn = view.findViewById(R.id.btnAddPackage);
        subscribed_packages_no_found = view.findViewById(R.id.subscribed_packages_no_found);


        addPackageBtn.setOnClickListener(view1 -> {
            startActivity(new Intent(activity, AllAvailablePackagesActivity.class));
        });

        recyclerView.setLayoutManager(new LinearLayoutManager(activity));
        recyclerView.setNestedScrollingEnabled(false);
        refreshLayout.setOnRefreshListener(this);

        loadData();
    }


    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            if (context instanceof Activity) {
                activity = (AppCompatActivity) context;
            }
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onRefresh() {
        loadData();
    }

    private void loadData() {

        CommonApiCalls.getInstance().getMySubscribedPackages(activity, new CommonCallback.Listener() {
            @Override
            public void onSuccess(Object body) {
                GetMySubscribedPackagesApiResponse response = (GetMySubscribedPackagesApiResponse) body;
                recyclerView.setAdapter(new PackagesAdapter(activity, response.getData(), false));
                refreshLayout.setRefreshing(false);

                if (response.getData().size() == 0) {
                    recyclerView.setVisibility(View.GONE);
                    subscribed_packages_no_found.setVisibility(View.VISIBLE);

                    if (SessionManager.getInstance().getAppLanguageCode().equals("ar")) {
                        subscribed_packages_no_found.setText("لا يوجد حزم.");
                    }

                } else {
                    recyclerView.setVisibility(View.VISIBLE);
                    subscribed_packages_no_found.setVisibility(View.GONE);
                }
            } // j0bkhqm0c5izbbdu

            @Override
            public void onFailure(String reason) {

            }
        });

    }


    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
