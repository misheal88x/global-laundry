package com.globallaundry.fragments;

import android.Manifest;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import com.globallaundry.R;
import com.globallaundry.activity.AddressSelectionMap;
import com.globallaundry.api.CommonApiCalls;
import com.globallaundry.apimodel.ContactusSubmitApiResponse;
import com.globallaundry.apimodel.GetContactusApiResponse;
import com.globallaundry.databinding.FragmentContactusBinding;
import com.globallaundry.interfaces.CommonCallback;
import com.globallaundry.utils.AppConstants;
import com.globallaundry.utils.CommonFunctions;
import com.globallaundry.utils.LanguageConstants;
import com.globallaundry.utils.SessionManager;

import lolodev.permissionswrapper.callback.OnRequestPermissionsCallBack;
import lolodev.permissionswrapper.wrapper.PermissionWrapper;


public class ContactUsFragment extends Fragment implements View.OnClickListener {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";


    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;
    private AppCompatActivity activity;
    FragmentContactusBinding binding;
    private GetContactusApiResponse mGetContactusApiResponse;

    public ContactUsFragment() {
    }

    // TODO: Rename and change types and number of parameters
    public static ContactUsFragment newInstance(String param1, String param2) {
        ContactUsFragment fragment = new ContactUsFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_contactus, container, false);
        View view = binding.getRoot();

        Toolbar mToolbar = activity.findViewById(R.id.toolbar);
        ImageView ivTBIcon1 = mToolbar.findViewById(R.id.ivTBIcon1);
        ivTBIcon1.setImageResource(R.drawable.ic_plus);
        ivTBIcon1.setVisibility(View.INVISIBLE);
        LinearLayout llhometool = mToolbar.findViewById(R.id.llhometool);
        llhometool.setVisibility(View.GONE);
        initView();

        ivTBIcon1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


            }
        });
        initView();
        if (SessionManager.getInstance().getAppLanguageCode().equals("ar")) {
            activity.getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
            binding.edName.setGravity(Gravity.END);
            binding.edEmail.setGravity(Gravity.END);
            binding.edComments.setGravity(Gravity.END);
            binding.tvAppName.setGravity(Gravity.END);
        } else {
            activity.getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
            binding.edName.setGravity(Gravity.START);
            binding.edEmail.setGravity(Gravity.START);
            binding.edComments.setGravity(Gravity.START);
            binding.tvAppName.setGravity(Gravity.START);
        }
        getContactUsApi();


        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    private void initView() {
        binding.tilName.setHint(LanguageConstants.Name);
        binding.tilEmail.setHint(LanguageConstants.email);
        binding.tilMobileNumber.setHint(LanguageConstants.mobileNumber);
        binding.tilComments.setHint(LanguageConstants.comments_);
        binding.tvSubmit.setText(LanguageConstants.submit);
        binding.btnSubmit.setOnClickListener(this);
        binding.llappcontactno.setOnClickListener(this);
        if (SessionManager.getInstance().getUserKey() != null && !SessionManager.getInstance().getUserKey().isEmpty()) {
            binding.edName.setText(SessionManager.getInstance().getUserName());
            binding.edMobileNumber.setText(SessionManager.getInstance().getUserMobileNumber());
            binding.edEmail.setText(SessionManager.getInstance().getUserEmail());

        }
    }

    private void getContactUsApi() {

        CommonApiCalls.getInstance().getContactDetails(getContext(), new CommonCallback.Listener() {
            @Override
            public void onSuccess(Object body) {
                mGetContactusApiResponse = (GetContactusApiResponse) body;
                if (mGetContactusApiResponse.getData() != null) {
                    binding.tvAppName.setText(mGetContactusApiResponse.getData().getAPP_NAME());
                    binding.tvemail.setText(LanguageConstants.email + ": " + mGetContactusApiResponse.getData().getAPP_EMAIL());
                    binding.tvappcontactno.setText(LanguageConstants.mobileNumber + ": " + mGetContactusApiResponse.getData().getAPP_CONTACT_NO());
                    binding.tvaddress.setText(LanguageConstants.location + ": " + mGetContactusApiResponse.getData().getAPP_CONTACT_ADDRESS());
                }


            }

            @Override
            public void onFailure(String reason) {

            }
        });


    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        }
        activity = (AppCompatActivity) context;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnSubmit:
                if (binding.edName.getText().toString().trim().isEmpty()) {
                    CommonFunctions.getInstance().validationEmptyError(getContext(), LanguageConstants.nameFieldCantBlank);
                    return;
                }
                if (binding.edMobileNumber.getText().toString().trim().isEmpty()) {
                    CommonFunctions.getInstance().validationEmptyError(getContext(), LanguageConstants.mobileNumberFieldCantBlank);
                    return;
                }
                if (binding.edEmail.getText().toString().trim().isEmpty()) {
                    CommonFunctions.getInstance().validationEmptyError(getContext(), LanguageConstants.emailFieldCantBlank);
                    return;
                }
                if (!CommonFunctions.getInstance().isValidEmail(binding.edEmail.getText().toString().trim())) {
                    CommonFunctions.getInstance().validationEmptyError(getContext(), LanguageConstants.enterValidEmail);
                    return;
                }
                if (binding.edComments.getText().toString().trim().isEmpty()) {
                    CommonFunctions.getInstance().validationEmptyError(getContext(), LanguageConstants.txt_enter_comments);
                    return;
                } else {
                    CommonApiCalls.getInstance().contactUssubmit(getContext(), binding.edName.getText().toString(), binding.edEmail.getText().toString(),
                            binding.edMobileNumber.getText().toString(), binding.edComments.getText().toString(), new CommonCallback.Listener() {
                                @Override
                                public void onSuccess(Object object) {
                                    ContactusSubmitApiResponse body = (ContactusSubmitApiResponse) object;
                                    CommonFunctions.getInstance().successResponseToast(getActivity(), body.getMessage());
                                    binding.edComments.setText("");
                                    binding.edEmail.setText("");
                                    binding.edMobileNumber.setText("");
                                    binding.edName.setText("");
                                }

                                @Override
                                public void onFailure(String reason) {

                                }
                            });
                }

                break;
            case R.id.llappcontactno:

                new PermissionWrapper.Builder(getActivity())
                        .addPermissions(new String[]{Manifest.permission.CALL_PHONE})
                        .addPermissionRationale(LanguageConstants.DoYouWantToEnablePermission)
                        .addPermissionsGoSettings(true, LanguageConstants.DoYouWantToEnablePermission)
                        .addRequestPermissionsCallBack(new OnRequestPermissionsCallBack() {
                            @Override
                            public void onGrant() {
                                try {
                                    Intent callIntent = new Intent(Intent.ACTION_CALL);
                                    callIntent.setData(Uri.parse("tel:" + mGetContactusApiResponse.getData().getAPP_CONTACT_NO()));
                                    startActivity(callIntent);
                                } catch (ActivityNotFoundException activityException) {
                                    Log.e("Calling a Phone Number", "Call failed", activityException);
                                }
                            }

                            @Override
                            public void onDenied(String permission) {
                                CommonFunctions.getInstance().validationEmptyError(getActivity(), LanguageConstants.failed);
                            }
                        }).build().request();

                break;
        }
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
