package com.globallaundry.fragments;

import android.app.Activity;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import android.os.Handler;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.style.ClickableSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.globallaundry.R;
import com.globallaundry.databinding.FragmentReferandEarnBinding;
import com.globallaundry.utils.CommonFunctions;
import com.globallaundry.utils.LanguageConstants;
import com.globallaundry.utils.SessionManager;

import java.text.MessageFormat;

public class ReferandEarn extends Fragment implements View.OnClickListener {
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    FragmentReferandEarnBinding binding;
    private AppCompatActivity activity;
    private FragmentManager fragmentManager;

    private String mParam1;
    private String mParam2;
    int i = 0;

    private OnFragmentInteractionListener mListener;

    public ReferandEarn() {
        // Required empty public constructor
    }

    public static ReferandEarn newInstance(String param1, String param2) {
        ReferandEarn fragment = new ReferandEarn();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_referand_earn, container, false);
        View view = binding.getRoot();
        fragmentManager = activity.getSupportFragmentManager();

        if (SessionManager.getInstance().getAppLanguageCode().equals("ar")) {
            activity.getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
        } else {
            activity.getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
        }

        Toolbar mToolbar = activity.findViewById(R.id.toolbar);
        ImageView ivTBIcon1 = mToolbar.findViewById(R.id.ivTBIcon1);
        ivTBIcon1.setImageResource(R.drawable.ic_plus);
        ivTBIcon1.setVisibility(View.INVISIBLE);

        LinearLayout llhometool = mToolbar.findViewById(R.id.llhometool);
        llhometool.setVisibility(View.GONE);

        initView();

        return view;
    }

    private void initView() {
        binding.tvcopyPaste.setText(LanguageConstants.doubletab);
        binding.tvrefertxt.setText(LanguageConstants.refertxt);
        binding.tvsharevia.setText(LanguageConstants.sharevia);

        binding.whatsapp.setOnClickListener(this);
        binding.facebook.setOnClickListener(this);
        binding.share.setOnClickListener(this);

        binding.tvReferalCode.setText(SessionManager.getInstance().getReferKey());

        binding.lyoutReferalCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                i++;
                Handler handler = new Handler();
                Runnable r = new Runnable() {
                    @Override
                    public void run() {
                        i = 0;
                    }
                };

                if (i == 1) {
                    //Single click
                    handler.postDelayed(r, 250);
                }
                else if (i == 2) {
                    //Double click
                    i = 0;
                    ClipboardManager cm = (ClipboardManager) activity.getSystemService(Context.CLIPBOARD_SERVICE);
                    cm.setText(binding.tvReferalCode.getText());
                    Toast.makeText(activity, LanguageConstants.refercopy, Toast.LENGTH_SHORT).show();
                }
            }
        });

        SpannableString ss = new SpannableString(CommonFunctions.getInstance().fromHtml(MessageFormat.format(LanguageConstants.refertxt,
                "<font color=\"\">" + LanguageConstants.fifty + "</font>")));
        ClickableSpan tAndcSpan = new ClickableSpan() {
            @Override
            public void onClick(View textView) {

            }
            @Override
            public void updateDrawState(TextPaint ds) {
                super.updateDrawState(ds);
                ds.setUnderlineText(false);
            }
        };

        String val = CommonFunctions.getInstance().fromHtml(MessageFormat.format(LanguageConstants.refertxt, LanguageConstants.fifty)).toString();
        Integer startTerms = val.indexOf(LanguageConstants.fifty);
        Integer endTerms = startTerms + LanguageConstants.fifty.length();
        ss.setSpan(tAndcSpan, startTerms, endTerms, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        binding.tvrefertxt.setText(ss);
    }

    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        }
        if (context instanceof Activity) {
            activity = (AppCompatActivity) context;
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.whatsapp:
                Intent whatsappIntent = new Intent(Intent.ACTION_SEND);
                whatsappIntent.setType("text/plain");
                whatsappIntent.setPackage("com.whatsapp");
                String msgcontent = MessageFormat.format(LanguageConstants.refertext, SessionManager.getInstance().getReferKey());
                whatsappIntent.putExtra(Intent.EXTRA_TEXT, msgcontent);
                try {
                    activity.startActivity(whatsappIntent);
                } catch (android.content.ActivityNotFoundException ex) {
                    Toast.makeText(activity, LanguageConstants.whatsuptxt, Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.facebook:
                Intent facebookintent = new Intent(Intent.ACTION_SEND);
                facebookintent.setType("text/plain");
                facebookintent.setPackage("com.facebook.katana");
                String msgcontentfb = MessageFormat.format(LanguageConstants.refertext, SessionManager.getInstance().getReferKey());
                facebookintent.putExtra(Intent.EXTRA_TEXT, msgcontentfb);
                try {
                    activity.startActivity(facebookintent);
                } catch (android.content.ActivityNotFoundException ex) {
                    Toast.makeText(activity, LanguageConstants.facebooktxt, Toast.LENGTH_SHORT).show();
                }

                break;
            case R.id.share:
                String msgcontentcm = MessageFormat.format(LanguageConstants.refertext, SessionManager.getInstance().getReferKey());
                Intent share = new Intent(Intent.ACTION_SEND);
                share.setType("text/plain");
                share.putExtra(Intent.EXTRA_TEXT, msgcontentcm);

                startActivity(Intent.createChooser(share, "Title of the dialog the system will open"));
                break;
            default:
                break;
        }
    }


    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }
}
