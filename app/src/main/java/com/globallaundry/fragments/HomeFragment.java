package com.globallaundry.fragments;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.globallaundry.R;
import com.globallaundry.activity.ServicesActivity;
import com.globallaundry.activity.SignInActivity;
import com.globallaundry.adapters.HomeBannerAdapter;
import com.globallaundry.adapters.ReviewAdapter;
import com.globallaundry.api.CommonApiCalls;
import com.globallaundry.apimodel.PromationAddApiResponse;
import com.globallaundry.apimodel.ReviewListApiResponse;
import com.globallaundry.databinding.FragmentHomeBinding;
import com.globallaundry.interfaces.CommonCallback;
import com.globallaundry.utils.CommonFunctions;
import com.globallaundry.utils.LanguageConstants;
import com.globallaundry.utils.LocationOnOff_Similar_To_Google_Maps;
import com.globallaundry.utils.SessionManager;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnSuccessListener;
import com.zendesk.logger.Logger;

import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import lolodev.permissionswrapper.callback.OnRequestPermissionsCallBack;
import lolodev.permissionswrapper.wrapper.PermissionWrapper;
import zendesk.core.AnonymousIdentity;
import zendesk.core.Identity;
import zendesk.core.Zendesk;
import zendesk.support.Support;
import zendesk.support.request.RequestActivity;


public class HomeFragment extends Fragment implements View.OnClickListener {
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private String mParam1;
    private String mParam2;

    private AppCompatActivity activity;
    private FragmentManager fragmentManager;

    FragmentHomeBinding binding;
    HomeBannerAdapter mHomeBannerAdapter;
    Timer timer;
    int DELAY_TIME = 500;
    int currentPage = 0;

    private OnFragmentInteractionListener mListener;
    private String contactNumber = "";

    public HomeFragment() {
        // Required empty public constructor
    }

    public static HomeFragment newInstance(String param1, String param2) {
        HomeFragment fragment = new HomeFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_home, container, false);
        View view = binding.getRoot();
        fragmentManager = activity.getSupportFragmentManager();

        if (SessionManager.getInstance().getAppLanguageCode().equals("ar")) {
            activity.getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
        } else {
            activity.getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
        }

        Toolbar mToolbar = activity.findViewById(R.id.toolbar);
        TextView tvName = mToolbar.findViewById(R.id.tvName);
        LinearLayout llhometool = mToolbar.findViewById(R.id.llhometool);
        TextView tvPhoneNumber = mToolbar.findViewById(R.id.tvPhoneNumber);
        ImageView ivTBIcon1 = mToolbar.findViewById(R.id.ivTBIcon1);
        ivTBIcon1.setImageResource(R.drawable.ic_plus);
        ivTBIcon1.setVisibility(View.GONE);

        initView();

        if (!SessionManager.getInstance().getUserKey().isEmpty()) {
            llhometool.setVisibility(View.VISIBLE);
            tvName.setText(SessionManager.getInstance().getUserName());
            tvPhoneNumber.setText(SessionManager.getInstance().getUserMobileNumber());
        } else {
            llhometool.setVisibility(View.INVISIBLE);
        }

        return view;
    }

    private void initView() {
        binding.tvCall.setText(LanguageConstants.call);
        binding.tvLocation.setText(LanguageConstants.locateus);
        binding.tvChat.setText(LanguageConstants.chat);
        binding.tvOrderOnline.setText(LanguageConstants.orderonline);
        binding.tvOrderOnline.setOnClickListener(this);
        binding.llMap.setOnClickListener(this);
        binding.llCall.setOnClickListener(this);
        binding.llChat.setOnClickListener(this);
        binding.tvOrderOnline.setOnClickListener(this);

        callPromationApi();
        callReviewListApi();
    }

    private void callReviewListApi() {
        CommonApiCalls.getInstance().getReviewList(getActivity(), new CommonCallback.Listener() {
                    @Override
                    public void onSuccess(Object object) {
                        ReviewListApiResponse body = (ReviewListApiResponse) object;

                        if (body.getData() != null && body.getData().size() > 0) {
                            LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), RecyclerView.HORIZONTAL, false);
                            binding.rvReview.setLayoutManager(layoutManager);
                            ReviewAdapter adapter = new ReviewAdapter(getActivity(), body.getData());
                            //binding.rvReview.addItemDecoration(new DividerItemDecoration(activity, DividerItemDecoration.HORIZONTAL));
                            binding.rvReview.setAdapter(adapter);
                        }
                    }

                    @Override
                    public void onFailure(String reason) {
                        CommonFunctions.getInstance().validationInfoError(activity, reason);
                    }
                });

    }

    private void callPromationApi() {
        CommonApiCalls.getInstance().promationAdds(getActivity(), new CommonCallback.Listener() {
                    @Override
                    public void onSuccess(Object object) {
                        PromationAddApiResponse body = (PromationAddApiResponse) object;
                        if (body.getData() != null && body.getData().size() > 0) {
                            LoadTourList(body.getData());
                            setAutoTimer(body.getData());

                            contactNumber = body.getData().get(0).getContact_number();
                        }
                    }
                    @Override
                    public void onFailure(String reason) {
                        CommonFunctions.getInstance().validationInfoError(activity, reason);
                    }
                });

    }

    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        }
        if (context instanceof Activity) {
            activity = (AppCompatActivity) context;
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onClick(View v) {
        if (v == binding.llCall) {
            if (contactNumber.equals("")){
                CommonFunctions.getInstance().validationEmptyError(activity, LanguageConstants.contactNumberIsEmpty);
            }
            else {
                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:" + contactNumber));
                startActivity(intent);
            }
        }
        else if (v == binding.llMap) {
            Intent intent = new Intent(getActivity(), LocationOnOff_Similar_To_Google_Maps.class);
            startActivity(intent);
            getCurrentLocation();
        }
        else if (v == binding.llChat) {
            if (SessionManager.getInstance().getUserKey().isEmpty()) {
                CommonFunctions.getInstance().newIntent(activity, SignInActivity.class, Bundle.EMPTY, false);
            }
            else {
                // Enable logging in Support and Chat SDK
                Logger.setLoggable(true);

                Zendesk.INSTANCE.init(activity, "https://globallaundryhelphelp.zendesk.com",
                        "1c76c3c840dd7432adc7dca5b0bd28ff8f1f0b82f19dd9e3",
                        "mobile_sdk_client_dd2c0dcadc2c5139ef1b");
                Support.INSTANCE.init(Zendesk.INSTANCE);

                Identity identity = new AnonymousIdentity();
                Zendesk.INSTANCE.setIdentity(identity);
                Support.INSTANCE.init(Zendesk.INSTANCE);
                Intent requestActivityIntent = RequestActivity.builder()
                        .withRequestSubject("Testing Support SDK 2.0")
                        .withTags("sdk", "android")
                        .intent(activity);
                startActivity(requestActivityIntent);
            }

        }
        else if (v == binding.tvOrderOnline) {
            CommonFunctions.getInstance().newIntent(getActivity(), ServicesActivity.class, Bundle.EMPTY, false);
        }
    }

    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }

    private void getCurrentLocation() {
        new PermissionWrapper.Builder(getActivity())
                .addPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION})
                .addPermissionRationale("")
                .addPermissionsGoSettings(true)
                .addRequestPermissionsCallBack(new OnRequestPermissionsCallBack() {
                    @SuppressLint("")
                    @Override
                    public void onGrant() {

                        FusedLocationProviderClient mFusedLocationClient = LocationServices.getFusedLocationProviderClient(getActivity());

                        if (getActivity().checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && getActivity().checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                            return;
                        }
                        mFusedLocationClient.getLastLocation()
                                .addOnSuccessListener(getActivity(), new OnSuccessListener<Location>() {
                                    @Override
                                    public void onSuccess(Location location) {
                                        // Got last known location. In some rare situations, this can be null.
                                        if (location != null) {
                                            double mCurrentLongitude = location.getLongitude();
                                            double mCurrentLatitiude = location.getLatitude();
                                            //Toast.makeText(activity, mCurrentLatitiude + "," + mCurrentLongitude + "", Toast.LENGTH_SHORT).show();
                                        }
                                    }
                                });
                    }

                    @Override
                    public void onDenied(String permission) {
                    }
                }).build().request();


    }

    private void setAutoTimer(List<PromationAddApiResponse.Datum> data) {
        final Handler handler = new Handler();
        final Runnable timing = new Runnable() {
            @Override
            public void run() {
                if (currentPage == data.size()) {
                    currentPage = 0;
                }

                if (binding.vpTour != null) {
                    binding.vpTour.setCurrentItem(currentPage++);
                }
            }
        };
        timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                handler.post(timing);
            }
        }, DELAY_TIME, 5000);
    }


    private void LoadTourList(List<PromationAddApiResponse.Datum> data) {
        mHomeBannerAdapter = new HomeBannerAdapter(getActivity(), data);
        binding.vpTour.setAdapter(mHomeBannerAdapter);
        binding.indicator.setupWithViewPager(binding.vpTour);
        binding.indicator.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if (position == data.size() - 1) {

                } else {

                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        binding.vpTour.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }
}
