package com.globallaundry.fragments;

import android.app.Activity;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.globallaundry.R;
import com.globallaundry.adapters.NotificationListAdapter;
import com.globallaundry.api.CommonApiCalls;
import com.globallaundry.databinding.FragmentMyordersBinding;
import com.globallaundry.databinding.FragmentNotificationBinding;
import com.globallaundry.interfaces.CommonCallback;
import com.globallaundry.models.NotificationListApiResponse;
import com.globallaundry.utils.CommonFunctions;
import com.globallaundry.utils.LanguageConstants;


public class NotificationFragment extends Fragment {
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;
    AppCompatActivity activity;
    private View view;
    FragmentNotificationBinding binding;

    public NotificationFragment() {
        // Required empty public constructor
    }


    public static NotificationFragment newInstance(String param1, String param2) {
        NotificationFragment fragment = new NotificationFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_notification, container, false);
        view = binding.getRoot();

        binding.tvNoDataFound.setText(LanguageConstants.NoOrderFound);

        Toolbar mToolbar = activity.findViewById(R.id.toolbar);
        LinearLayout llhometool = mToolbar.findViewById(R.id.llhometool);
        llhometool.setVisibility(View.GONE);
        ImageView ivTBIcon1 = mToolbar.findViewById(R.id.ivTBIcon1);
        ivTBIcon1.setImageResource(R.drawable.ic_plus);
        ivTBIcon1.setVisibility(View.INVISIBLE);

        notificationApiCall();


        return view;
    }

    private void notificationApiCall() {
        CommonApiCalls.getInstance().notification(getContext(), new CommonCallback.Listener() {
            @Override
            public void onSuccess(Object body) {
                NotificationListApiResponse apiResponse = (NotificationListApiResponse) body;

                if (apiResponse.getStatus().equals(200)) {

                    if (apiResponse.getData() != null && apiResponse.getData().size() > 0) {
                        binding.rvNotification.setVisibility(View.VISIBLE);
                        binding.tvNoDataFound.setVisibility(View.GONE);

                        NotificationListAdapter notificationListAdapter = new NotificationListAdapter(getActivity(), apiResponse.getData());
                        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
                        binding.rvNotification.setLayoutManager(mLayoutManager);
                        //binding.rvNotification.setItemAnimator(new DefaultItemAnimator());
                        binding.rvNotification.setAdapter(notificationListAdapter);
                        binding.rvNotification.setNestedScrollingEnabled(false);


                    } else {
                        binding.rvNotification.setVisibility(View.GONE);
                        binding.tvNoDataFound.setVisibility(View.VISIBLE);
                    }
                }
            }

            @Override
            public void onFailure(String reason) {
                CommonFunctions.getInstance().validationInfoError(activity, reason);
            }
        });
    }



    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        }
        if (context instanceof Activity) {
            activity = (AppCompatActivity) context;
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }
}
