package com.globallaundry.fragments;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.globallaundry.R;
import com.globallaundry.activity.AddressSelectionMap;
import com.globallaundry.adapters.AddressAdapter1;
import com.globallaundry.api.CommonApiCalls;
import com.globallaundry.apimodel.AddressListApiResponse;
import com.globallaundry.databinding.FragmentAddressbookBinding;
import com.globallaundry.interfaces.CommonCallback;
import com.globallaundry.interfaces.IMethodCaller;
import com.globallaundry.utils.AppConstants;
import com.globallaundry.utils.CommonFunctions;
import com.globallaundry.utils.LanguageConstants;


public class AddressBookFragment extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    public static Boolean isNeedToRefresh = false;


    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;
    private AppCompatActivity activity;
    FragmentAddressbookBinding binding;

    public AddressBookFragment() {
    }

    // TODO: Rename and change types and number of parameters
    public static AddressBookFragment newInstance(String param1, String param2) {
        AddressBookFragment fragment = new AddressBookFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_addressbook, container, false);
        View view = binding.getRoot();

        Toolbar mToolbar = activity.findViewById(R.id.toolbar);
        ImageView ivTBIcon1 = mToolbar.findViewById(R.id.ivTBIcon1);
        ivTBIcon1.setVisibility(View.GONE);
        ivTBIcon1.setImageResource(R.drawable.ic_plus);
        LinearLayout llhometool = mToolbar.findViewById(R.id.llhometool);
        llhometool.setVisibility(View.INVISIBLE);
        isNeedToRefresh = false;
        initView();

        binding.rlyoutAddAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Bundle mBundle = new Bundle();
                mBundle.putString(AppConstants.AddressMapType, CommonFunctions.AddressMapType.AddAddress.getValue() + "");
                CommonFunctions.getInstance().newIntent(activity, AddressSelectionMap.class, mBundle, false);
            }
        });
        initView();
        LoadAddress();


        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (isNeedToRefresh) {
            isNeedToRefresh = false;
            LoadAddress();
        }
    }

    private void initView() {
        binding.tvNoDateMessage.setText(LanguageConstants.noaddressfound);
        binding.txtAddAddress.setText(LanguageConstants.addAddress);
    }

    private void LoadAddress() {

        CommonApiCalls.getInstance().addressList(getContext(), new CommonCallback.Listener() {
            @Override
            public void onSuccess(Object body) {
                AddressListApiResponse mAddressListData = (AddressListApiResponse) body;
                if (mAddressListData.getData() != null && mAddressListData.getData().size() > 0) {


                    AddressAdapter1 adapter = new AddressAdapter1(activity, mAddressListData.getData(), CommonFunctions.AddressListType.AddressList.getValue(),
                            new IMethodCaller() {
                                @Override
                                public void LoadListView() {
                                    LoadAddress();
                                }

                                @Override
                                public void selectedAddress(Object selectedAddress) {

                                }
                            });
                    binding.rvAddressList.setAdapter(adapter);
                    binding.rvAddressList.setNestedScrollingEnabled(false);

                    LinearLayoutManager horizontalLayoutManagaer = new LinearLayoutManager(activity, RecyclerView.VERTICAL, false);
                    binding.rvAddressList.setLayoutManager(horizontalLayoutManagaer);
                    binding.rvAddressList.setVisibility(View.VISIBLE);
                    binding.llNoContentView.setVisibility(View.GONE);
                } else {
                    binding.rvAddressList.setVisibility(View.GONE);
                    binding.llNoContentView.setVisibility(View.VISIBLE);
                }


            }

            @Override
            public void onFailure(String reason) {

            }
        });


    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        }
        activity = (AppCompatActivity) context;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
