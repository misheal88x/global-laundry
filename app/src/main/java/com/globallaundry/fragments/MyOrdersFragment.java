package com.globallaundry.fragments;

import android.app.Activity;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.globallaundry.R;
import com.globallaundry.adapters.MyOrdersAdapter;
import com.globallaundry.api.CommonApiCalls;
import com.globallaundry.apimodel.OrderListApiResponse;
import com.globallaundry.databinding.FragmentMyordersBinding;
import com.globallaundry.interfaces.CommonCallback;
import com.globallaundry.utils.LanguageConstants;

public class MyOrdersFragment extends Fragment {
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;
    AppCompatActivity activity;
    private View view;
    FragmentMyordersBinding binding;

    public MyOrdersFragment() {
    }

    public static MyOrdersFragment newInstance(String param1, String param2) {
        MyOrdersFragment fragment = new MyOrdersFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_myorders, container, false);
        View view = binding.getRoot();

        binding.tvNoDateMessage.setText(LanguageConstants.NoOrderFound);

        Toolbar mToolbar = activity.findViewById(R.id.toolbar);
        LinearLayout llhometool = mToolbar.findViewById(R.id.llhometool);
        llhometool.setVisibility(View.GONE);
        ImageView ivTBIcon1 = mToolbar.findViewById(R.id.ivTBIcon1);
        ivTBIcon1.setImageResource(R.drawable.ic_plus);
        ivTBIcon1.setVisibility(View.INVISIBLE);

        LoadOrders();

        return view;
    }

    private void LoadOrders() {
        CommonApiCalls.getInstance().orderList(getContext(), new CommonCallback.Listener() {
            @Override
            public void onSuccess(Object body) {
                OrderListApiResponse mOrderListApiResponse = (OrderListApiResponse) body;
                if (mOrderListApiResponse.getData() != null && mOrderListApiResponse.getData().size() > 0) {
                    MyOrdersAdapter adapter = new MyOrdersAdapter(activity, mOrderListApiResponse.getData());
                    binding.rvMyOrders.setAdapter(adapter);
                    binding.rvMyOrders.setNestedScrollingEnabled(false);
                    LinearLayoutManager horizontalLayoutManagaer = new LinearLayoutManager(getActivity(), RecyclerView.VERTICAL, false);
                    binding.rvMyOrders.setLayoutManager(horizontalLayoutManagaer);
                } else {
                    binding.rvMyOrders.setVisibility(View.GONE);
                    binding.llNoContentView.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onFailure(String reason) {

            }
        });

    }

    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        }
        if (context instanceof Activity) {
            activity = (AppCompatActivity) context;
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }
}
