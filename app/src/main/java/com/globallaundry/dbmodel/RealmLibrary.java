package com.globallaundry.dbmodel;


import android.content.Context;

import com.globallaundry.app_model.SizeModel;

import java.util.Arrays;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmResults;

/*
 * Created by AMSHEER on 19-01-2018.
 */

public class RealmLibrary {

    private static RealmLibrary realmInstance = new RealmLibrary();

    public static RealmLibrary getInstance() {
        return realmInstance;
    }

    /*
     * @param itemId
     * @param item_key
     * @param item_name
     * @param item_image
     * @param item_price_per_unit
     * @param category_id
     * @param ingredient_name
     * @param vendor_key
     * @param addOrDelete // 1 - add
                 // 2 - delete
     * @param Price
     * @param order_total
     * @param ingredientsList
     * @param is_ingredient
     * @param General_request
     */
    public void insertItem(final Context context, final String cloth_type_key,
                           final String item_name,
                           final String item_image,
                           final Integer item_type,
                           final Double item_price_per_unit,
                           final String category_id,
                           final String addOrDelete,
                           final List<SizeModel> sizeModels,
                           final Integer is_ingredient,
                           final String General_request, String itemQuantity,
                           String express_amount, Integer is_express,
                           Integer additional_quantity) {

        Realm realm = Realm.getDefaultInstance();

        Boolean updateIngredients = false;
        Boolean sameIngredients = false;
        Boolean isDelete = false;
        realm.beginTransaction();
        RealmResults<ItemRealmList> itemRealmLists = realm.where(ItemRealmList.class).equalTo("cloth_type_key", cloth_type_key).findAll();
        ItemRealmList itemRealmList = null;
        if (itemRealmLists.size() == 0) {
            itemRealmList = new ItemRealmList();
            Number maxId = realm.where(ItemRealmList.class).max("id");
            int nextId = (maxId == null) ? 1 : maxId.intValue() + 1;
            itemRealmList.setId(nextId);
            itemRealmList.setQuantity(itemQuantity);
            itemRealmList.setCloth_type_key(cloth_type_key);
        } else {
            for (int itemCount = 0; itemCount < itemRealmLists.size(); itemCount++) {
                itemRealmList = itemRealmLists.get(itemCount);
                if (itemRealmList.getCloth_type_key().equals(cloth_type_key)) {
                    if (is_ingredient.equals(1)) {
                        RealmList<SizeRealmList> existIng = itemRealmList.getSize();
                        String existingIng[] = new String[existIng.size()];
                        String currentIng[] = new String[sizeModels.size()];

                        for (int count = 0; count < existIng.size(); count++) {
                            existingIng[count] = existIng.get(count).getSize_key();
                        }
                        for (int count = 0; count < sizeModels.size(); count++) {
                            currentIng[count] = sizeModels.get(count).getSize_key();
                        }
                        Arrays.sort(existingIng);
                        Arrays.sort(currentIng);
                        Boolean insert = (Arrays.equals(existingIng, currentIng));
                        if (!insert) {
                            sameIngredients = true;
                        } else {
                            if (itemRealmList.getQuantity().equals("1") && addOrDelete.equals("2")) {
                                itemRealmList.deleteFromRealm();
                                isDelete = true;
                            } else {
                                String qty = addOrDelete.equals("1") ? String.valueOf(Integer.parseInt(itemRealmList.getQuantity()) + Integer.parseInt(itemQuantity)) : String.valueOf(Integer.parseInt(itemRealmList.getQuantity()) - 1);
                                itemRealmList.setQuantity(qty);
                                updateIngredients = true;
                                sameIngredients = false;
                            }
                            break;
                        }

                    } else {
                        if (itemRealmList.getQuantity().equals("1") && addOrDelete.equals("2")) {
                            itemRealmList.deleteFromRealm();
                            isDelete = true;
                            break;
                        } else {
                            String qty = addOrDelete.equals("1") ? String.valueOf(Integer.parseInt(itemRealmList.getQuantity()) + 1) : String.valueOf(Integer.parseInt(itemRealmList.getQuantity()) - 1);
                            itemRealmList.setQuantity(qty);
                        }
                    }
                } else {
                    itemRealmList = new ItemRealmList();
                    itemRealmList.setQuantity(itemQuantity);
                    itemRealmList.setCloth_type_key(cloth_type_key);
                    Number maxId = realm.where(ItemRealmList.class).max("id");
                    int nextId = (maxId == null) ? 1 : maxId.intValue() + 1;
                    itemRealmList.setId(nextId);
                }
            }
        }
        if (!isDelete) {
            if (sameIngredients) {
                itemRealmList = new ItemRealmList();
                itemRealmList.setQuantity(itemQuantity);
                itemRealmList.setCloth_type_key(cloth_type_key);
                Number maxId = realm.where(ItemRealmList.class).max("id");
                int nextId = (maxId == null) ? 1 : maxId.intValue() + 1;
                itemRealmList.setId(nextId);
            }


            if (itemRealmList != null) {
                itemRealmList.setCloth_type_key(cloth_type_key);
                itemRealmList.setCloth_type_name(item_name);
                itemRealmList.setCloth_type_image(item_image);
                itemRealmList.setItem_type(item_type);
                itemRealmList.setPrice(item_price_per_unit);
                itemRealmList.setCategory_key(category_id);
                itemRealmList.setIs_express(is_express);
                itemRealmList.setAdditional_quantity(additional_quantity);
                itemRealmList.setExpress_amount(express_amount);
                itemRealmList.setGeneralRequest(General_request);
//                itemRealmList.setIs_express(is_ingredient);


                if (!updateIngredients) {
                    RealmList<SizeRealmList> sizeRealmLists = itemRealmList.getSize();
                    for (int count = 0; count < sizeModels.size(); count++) {
                        SizeModel ingredient = sizeModels.get(count);
                        SizeRealmList ingredientsRealmModel = new SizeRealmList();

                        ingredientsRealmModel.setSize_name(ingredient.getSize_name());
                        ingredientsRealmModel.setSize_key(ingredient.getSize_key());
                        ingredientsRealmModel.setSize_quantity(ingredient.getSize_quantity());
                        ingredientsRealmModel.setPrice(ingredient.getPrice());

                        sizeRealmLists.add(ingredientsRealmModel);
                    }


                    itemRealmList.setSize(sizeRealmLists);
                }

                realm.copyToRealmOrUpdate(itemRealmList);
            } else {
//                MyApplication.displayUnKnownError();
            }
        }
        realm.commitTransaction();

    }


    public void deleteItem(final Integer id) {
        Realm realm = Realm.getDefaultInstance();
        realm.beginTransaction();
        ItemRealmList item = realm.where(ItemRealmList.class).equalTo("id", id).findFirst();
        item.deleteFromRealm();
        realm.commitTransaction();
    }

    public void clearCart() {
        Realm realm = Realm.getDefaultInstance();
        realm.beginTransaction();
        realm.deleteAll();
        realm.commitTransaction();
    }

    public RealmResults<ItemRealmList> getCartList() {
        Realm realm = Realm.getDefaultInstance();
        return realm.where(ItemRealmList.class).findAll();
    }

    public int getItems(String item_key) {
        Realm realm = Realm.getDefaultInstance();
        RealmResults<ItemRealmList> items = realm.where(ItemRealmList.class).equalTo("cloth_type_key", item_key).findAll();
        int qty = 0;
        for (int count = 0; count < items.size(); count++) {
            qty = qty + Integer.parseInt(items.get(count).getQuantity());
        }

        return qty;
    }

    public void updateSize(SizeRealmList sizeRealmList, Integer qty) {
        Realm realm = Realm.getDefaultInstance();
        realm.beginTransaction();
        sizeRealmList.setSize_quantity(qty);
        realm.commitTransaction();
    }

    public void removeSize(ItemRealmList listResponses, SizeRealmList sizeRealmList) {
        Realm realm = Realm.getDefaultInstance();
        realm.beginTransaction();
        listResponses.getSize().remove(sizeRealmList);
        if (listResponses.getSize().size() == 0) {
            listResponses.deleteFromRealm();
        }
        realm.commitTransaction();
    }
}

