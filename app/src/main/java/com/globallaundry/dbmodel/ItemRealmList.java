package com.globallaundry.dbmodel;

import java.util.List;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class ItemRealmList extends RealmObject {

    @PrimaryKey
    Integer id;

    private String cloth_type_key;
    private String quantity;
    private Integer additional_quantity;
    private Integer is_express;
    private Double price;
    private Integer item_type;
    private String category_key;
    private String cloth_type_image;
    private String cloth_type_name;
    private String express_amount;
    private String generalRequest;
//    private String measure;
    private RealmList<SizeRealmList> size = new RealmList<>();



    public String getGeneralRequest() {
        return generalRequest;
    }

    public void setGeneralRequest(String generalRequest) {
        this.generalRequest = generalRequest;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCloth_type_key() {
        return cloth_type_key;
    }

    public void setCloth_type_key(String cloth_type_key) {
        this.cloth_type_key = cloth_type_key;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public Integer getAdditional_quantity() {
        return additional_quantity;
    }

    public void setAdditional_quantity(Integer additional_quantity) {
        this.additional_quantity = additional_quantity;
    }

    public Integer getIs_express() {
        return is_express;
    }

    public void setIs_express(Integer is_express) {
        this.is_express = is_express;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Integer getItem_type() {
        return item_type;
    }

    public void setItem_type(Integer item_type) {
        this.item_type = item_type;
    }

    public String getCategory_key() {
        return category_key;
    }

    public void setCategory_key(String category_key) {
        this.category_key = category_key;
    }

    public String getCloth_type_image() {
        return cloth_type_image;
    }

    public void setCloth_type_image(String cloth_type_image) {
        this.cloth_type_image = cloth_type_image;
    }

    public String getCloth_type_name() {
        return cloth_type_name;
    }

    public void setCloth_type_name(String cloth_type_name) {
        this.cloth_type_name = cloth_type_name;
    }

    public String getExpress_amount() {
        return express_amount;
    }

    public void setExpress_amount(String express_amount) {
        this.express_amount = express_amount;
    }

    public RealmList<SizeRealmList> getSize() {
        return size;
    }

    public void setSize(RealmList<SizeRealmList> size) {
        this.size = size;
    }

}