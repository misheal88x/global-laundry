package com.globallaundry.dbmodel;

import io.realm.RealmObject;

public class SizeRealmList extends RealmObject {

    private String size_name;
    private String size_key;
    private Integer size_quantity;
    private Double price;

    public String getSize_name() {
        return size_name;
    }

    public void setSize_name(String size_name) {
        this.size_name = size_name;
    }

    public String getSize_key() {
        return size_key;
    }

    public void setSize_key(String size_key) {
        this.size_key = size_key;
    }

    public Integer getSize_quantity() {
        return size_quantity;
    }

    public void setSize_quantity(Integer size_quantity) {
        this.size_quantity = size_quantity;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

}