package com.globallaundry.interfaces;

public interface SelectArea {
    void onClick(int id, String name);
}
