package com.globallaundry.interfaces;


import com.globallaundry.models.NavigationMenuModel;

public interface NavigationClick {
    void onClick(NavigationMenuModel navigationMenuModel);
}
