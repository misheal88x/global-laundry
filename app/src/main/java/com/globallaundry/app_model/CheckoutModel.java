package com.globallaundry.app_model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


public class CheckoutModel implements Serializable {


    @SerializedName("items")
    @Expose
    private List<Item> items = new ArrayList<Item>();
    @SerializedName("pickup_date")
    @Expose
    private String pickup_date;
    @SerializedName("pickup_time")
    @Expose
    private String pickup_time;
    @SerializedName("delivery_date")
    @Expose
    private String delivery_date;
    @SerializedName("delivery_time")
    @Expose
    private String delivery_time;
    @SerializedName("pickup_user_address_key")
    @Expose
    private String pickup_user_address_key;
    @SerializedName("use_reward_point")
    @Expose
    private Integer use_reward_point;
    @SerializedName("payment_type")
    @Expose
    private Integer payment_type;
    @SerializedName("currency_type")
    @Expose
    private Integer currency_type;
    @SerializedName("order_message")
    @Expose
    private String order_message;

    public List<Item> getItems() {
        return items;
    }

    public void setItems(List<Item> items) {
        this.items = items;
    }

    public String getPickup_date() {
        return pickup_date;
    }

    public void setPickup_date(String pickup_date) {
        this.pickup_date = pickup_date;
    }

    public String getPickup_time() {
        return pickup_time;
    }

    public void setPickup_time(String pickup_time) {
        this.pickup_time = pickup_time;
    }

    public String getDelivery_date() {
        return delivery_date;
    }

    public void setDelivery_date(String delivery_date) {
        this.delivery_date = delivery_date;
    }

    public String getDelivery_time() {
        return delivery_time;
    }

    public void setDelivery_time(String delivery_time) {
        this.delivery_time = delivery_time;
    }

    public String getPickup_user_address_key() {
        return pickup_user_address_key;
    }

    public void setPickup_user_address_key(String pickup_user_address_key) {
        this.pickup_user_address_key = pickup_user_address_key;
    }

    public Integer getPayment_type() {
        return payment_type;
    }

    public void setPayment_type(Integer payment_type) {
        this.payment_type = payment_type;
    }

    public Integer getCurrency_type() {
        return currency_type;
    }

    public void setCurrency_type(Integer currency_type) {
        this.currency_type = currency_type;
    }

    public String getOrder_message() {
        return order_message;
    }

    public void setOrder_message(String order_message) {
        this.order_message = order_message;
    }

    public Integer getUse_reward_point() {
        return use_reward_point;
    }

    public void setUse_reward_point(Integer use_reward_point) {
        this.use_reward_point = use_reward_point;
    }

    public static class Item {

        @SerializedName("cloth_type_key")
        @Expose
        private String cloth_type_key;
        @SerializedName("quantity")
        @Expose
        private String quantity;
        @SerializedName("additional_quantity")
        @Expose
        private Integer additional_quantity;
        @SerializedName("is_express")
        @Expose
        private Integer is_express;
        @SerializedName("size")
        @Expose
        private List<Size> size = new ArrayList<Size>();

        public String getCloth_type_key() {
            return cloth_type_key;
        }

        public void setCloth_type_key(String cloth_type_key) {
            this.cloth_type_key = cloth_type_key;
        }

        public String getQuantity() {
            return quantity;
        }

        public void setQuantity(String quantity) {
            this.quantity = quantity;
        }

        public Integer getAdditional_quantity() {
            return additional_quantity;
        }

        public void setAdditional_quantity(Integer additional_quantity) {
            this.additional_quantity = additional_quantity;
        }

        public Integer getIs_express() {
            return is_express;
        }

        public void setIs_express(Integer is_express) {
            this.is_express = is_express;
        }

        public List<Size> getSize() {
            return size;
        }

        public void setSize(List<Size> size) {
            this.size = size;
        }

    }

    public static class Size {

        @SerializedName("size_key")
        @Expose
        private String size_key;
        @SerializedName("size_quantity")
        @Expose
        private Integer size_quantity;

        public String getSize_key() {
            return size_key;
        }

        public void setSize_key(String size_key) {
            this.size_key = size_key;
        }

        public Integer getSize_quantity() {
            return size_quantity;
        }

        public void setSize_quantity(Integer size_quantity) {
            this.size_quantity = size_quantity;
        }

    }
}