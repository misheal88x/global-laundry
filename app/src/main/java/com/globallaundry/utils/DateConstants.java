package com.globallaundry.utils;

public class DateConstants {
    public static final String DATE_FORMAT_YYYY = "yyyy";
    public static final String DATE_FORMAT_MM = "MM";
    public static final String DATE_FORMAT_DD = "dd";
    public static String DATE_FORMAT_MM_DD_YYYY = "EEE MMM dd HH:mm:ss 'GMT' yyyy";
    public static String DATE_FORMAT_DD_MM_YYYY = "dd-MM-yyyy";
    public static String DATE_FORMAT_YYYY_DD_MM = "yyyy-MM-dd";
    public static String DATE_FORMAT_DD_MM_YYYY_NE = "dd/MM/yyyy";
}