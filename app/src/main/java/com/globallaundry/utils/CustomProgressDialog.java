package com.globallaundry.utils;

import android.app.Dialog;
import android.content.Context;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.globallaundry.R;


public class CustomProgressDialog {
    private static CustomProgressDialog ourInstance = new CustomProgressDialog();
    //    ACProgressFlower dialog;
    Dialog dialog;

    public static CustomProgressDialog getInstance() {
        return ourInstance;
    }

    private CustomProgressDialog() {

    }

    public void show(Context context) {
        /*dialog = new ACProgressFlower.Builder(context)
                .direction(ACProgressConstant.DIRECT_CLOCKWISE)
                .themeColor(TabaogoApplication.context.getResources().getColor(R.color.white))
                .bgColor(TabaogoApplication.context.getResources().getColor(R.color.colorPrimary))
                .fadeColor(TabaogoApplication.context.getResources().getColor(R.color.colorPrimary)).build();
        dialog.show();*/

        try {
            dialog = new Dialog(context);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setCancelable(false);
            dialog.setContentView(R.layout.pdialog);
            LinearLayout mkLoader=dialog.findViewById(R.id.llView);
            ImageView imgLoader = dialog.findViewById(R.id.imgLoader);
//            Glide.with(context).asGif().load(R.raw.logo).into(imgLoader);

            mkLoader.setBackgroundColor(context.getResources().getColor(android.R.color.transparent));
            dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
            dialog.show();
        }
        catch (Exception ignored){

        }

    }

    public void dismiss() {
        if (dialog != null)
            dialog.dismiss();
    }

    public boolean isShowing() {
        if (dialog != null && dialog.isShowing())
            return true;
        else return false;
    }
}
