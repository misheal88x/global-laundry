package com.globallaundry.utils;

import android.Manifest;
import android.app.Activity;
import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.text.Html;
import android.text.Spanned;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.globallaundry.R;
import com.globallaundry.activity.SplashActivity;
import com.globallaundry.apimodel.GetSubscribedItemsTypes;
import com.globallaundry.interfaces.CommonCallback;
import com.globallaundry.models.ErrorApiResponse;
import com.google.android.material.snackbar.Snackbar;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.valdesekamdem.library.mdtoast.MDToast;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.DecimalFormat;
import java.text.MessageFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.regex.Pattern;

import okhttp3.ResponseBody;


public class CommonFunctions {
    public static String DBName="GlobalLaundry_DB";
    private static CommonFunctions commonFunctionsInstance = new CommonFunctions();
    public static String strPermission = "";
    public static String MAP_KEY = "AIzaSyAuP-zWnG_J1Dcof7XDGn_bHrlUFyAKtmY";
    public static String DeviceType = "1";




    public static String selectAreaName = "";
    public static int dayCount = 0;

    public enum AddressListType {
        AddressList("AddressList", 1),
        PickUpAddress("PickupAddress", 2),
        DeliveryAddress("DeliveryAddress", 3);

        private String stringValue;
        private int intValue;

        private AddressListType(String toString, int value) {
            stringValue = toString;
            intValue = value;
        }

        public static AddressListType fromInteger(int x) {
            switch (x) {
                case 1:
                    return AddressList;
                case 2:
                    return PickUpAddress;
                case 3:
                    return DeliveryAddress;

            }
            return null;
        }

        public int getValue() {
            return intValue;
        }

        @Override
        public String toString() {
            return stringValue;
        }
    }

    public enum AddressMapType {
        AddAddress("AddAddress", 1),
        UpdateAddress("UpdateAddress", 2);

        private String stringValue;
        private int intValue;

        private AddressMapType(String toString, int value) {
            stringValue = toString;
            intValue = value;
        }

        public static AddressMapType fromInteger(int x) {
            switch (x) {
                case 1:
                    return AddAddress;
                case 2:
                    return UpdateAddress;
            }
            return null;
        }

        public int getValue() {
            return intValue;
        }

        @Override
        public String toString() {
            return stringValue;
        }
    }

    public static CommonFunctions getInstance() {
        return commonFunctionsInstance;
    }

    /**
     * @param context          Context fromactivity or fragment
     * @param bundle           Bundle of values for next Activity
     * @param destinationClass Destination Activity
     * @param isFinish         Current activity need to finish or not
     */
    public void newIntent(Context context, Class destinationClass, Bundle bundle, boolean isFinish) {
        Intent intent = new Intent(context, destinationClass);
        intent.putExtras(bundle);
        context.startActivity(intent);
        if (isFinish) {
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            ((Activity) context).finish();
        }
    }

    /**
     * @param context          Context fromactivity or fragment
     * @param bundle           Bundle of values for next Activity
     * @param destinationClass Destination Activity
     * @param requestCode
     */
    public void newIntentWithResult(Context context, Class destinationClass, Bundle bundle, int requestCode) {
        Intent intent = new Intent(context, destinationClass);
        intent.putExtras(bundle);
        ((AppCompatActivity) context).startActivityForResult(intent, requestCode);

    }

    /**
     * Format string
     *
     * @param text
     * @param replaceText
     * @return
     */
    public String messageFormatPattern(String text, String... replaceText) {
        String finalText = "";
        for (String aReplaceText : replaceText) {
            finalText = MessageFormat.format(text, aReplaceText);
        }
        return finalText;
    }

    public void FinishActivityWithDelay(final Activity activity) {
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {
                activity.finish();
            }
        }, 1500);
    }

    // -- TYPE_WARNING
    public void validationWarningError(Context context, String message) {
        View parent = ((Activity) context).getWindow().getDecorView().findViewById(android.R.id.content);
        Snackbar snackbar;
        snackbar = Snackbar.make(parent, message, Snackbar.LENGTH_SHORT);
        View snackBarView = snackbar.getView();
        snackBarView.setBackgroundColor(context.getResources().getColor(R.color.colorPrimary));
        TextView textView = (TextView) snackBarView.findViewById(R.id.snackbar_text);
        textView.setTextColor(context.getResources().getColor(R.color.white));
        //snackbar.show();

        MDToast mdToast = MDToast.makeText(context, message, Toast.LENGTH_SHORT, MDToast.TYPE_WARNING);
        mdToast.show();
    }

    // --- TYPE_ERROR
    public void validationEmptyError(Context context, String message) {
        View parent = ((Activity) context).getWindow().getDecorView().findViewById(android.R.id.content);
        Snackbar snackbar;
        snackbar = Snackbar.make(parent, message, Snackbar.LENGTH_SHORT);
        View snackBarView = snackbar.getView();
        snackBarView.setBackgroundColor(context.getResources().getColor(R.color.colorIndicator));
        TextView textView = (TextView) snackBarView.findViewById(R.id.snackbar_text);
        textView.setTextColor(context.getResources().getColor(R.color.colorWhite));
        //snackbar.show();

        MDToast mdToast = MDToast.makeText(context, message, Toast.LENGTH_SHORT, MDToast.TYPE_ERROR);
        mdToast.show();
    }

    // --- TYPE_INFO
    public void validationInfoError(Context context, String message) {
        View parent = ((Activity) context).getWindow().getDecorView().findViewById(android.R.id.content);
        Snackbar snackbar;
        snackbar = Snackbar.make(parent, message, Snackbar.LENGTH_SHORT);
        View snackBarView = snackbar.getView();
        snackBarView.setBackgroundColor(context.getResources().getColor(R.color.colorIndicator));
        TextView textView = (TextView) snackBarView.findViewById(R.id.snackbar_text);
        textView.setTextColor(context.getResources().getColor(R.color.colorWhite));
        //snackbar.show();

        MDToast mdToast = MDToast.makeText(context, message, Toast.LENGTH_SHORT, MDToast.TYPE_INFO);
        mdToast.show();
    }

    // --- TYPE_SUCCESS
    public void successResponseToast(Context context, String message) {
        View parent = ((Activity) context).getWindow().getDecorView().findViewById(android.R.id.content);
        Snackbar snackbar;
        snackbar = Snackbar.make(parent, message, Snackbar.LENGTH_SHORT);
        View snackBarView = snackbar.getView();
        snackBarView.setBackgroundColor(context.getResources().getColor(R.color.green));
        TextView textView = (TextView) snackBarView.findViewById(R.id.snackbar_text);
        textView.setTextColor(context.getResources().getColor(R.color.colorWhite));
        //snackbar.show();

        MDToast mdToast = MDToast.makeText(context, message, Toast.LENGTH_SHORT, MDToast.TYPE_SUCCESS);
        mdToast.show();
    }

    /**
     * Validation error on snackbar
     *
     * @param context
     * @param message
     * @param validationField
     */
    public void validationError(Context context, String message, String validationField) {
        View parent = ((Activity) context).getWindow().getDecorView().findViewById(android.R.id.content);
        Snackbar snackbar;
        String result = messageFormatPattern(message, validationField);
        snackbar = Snackbar.make(parent, result, Snackbar.LENGTH_SHORT);
        View snackBarView = snackbar.getView();
        snackBarView.setBackgroundColor(ContextCompat.getColor(context, R.color.black));
        TextView textView = snackBarView.findViewById(R.id.snackbar_text);
        textView.setTextColor(ContextCompat.getColor(context, R.color.white));
        snackbar.show();
    }

    /**
     * Round doube values
     *
     * @param price
     * @param isLeft
     * @return
     */
    public String roundOffDecimalValue(Double price, Boolean isLeft) {
        if (isLeft) {
            return LanguageConstants.LL + " " + new DecimalFormat("0.00").format(price);
        } else {
            return new DecimalFormat("0.00").format(price) + " " + LanguageConstants.LL;
        }
    }

    public boolean isValidEmail(CharSequence target) {
        return !TextUtils.isEmpty(target) && Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }

    public boolean isValidMobile(String phone) {
        boolean check;
        //  check = Pattern.matches("[0-9]{11,12}", phone) && !(phone.length() < 11 || phone.length() > 12);

        check = Pattern.matches("[0-9]{10,11}", phone) && !(phone.length() < 10 || phone.length() > 11);

        return check;

   /*     String pattern = "[60][0-9]{8,12}";
        if (phone.matches(pattern)){
            return  true;
        }
        return false;*/
    }


    public static boolean CheckInternetConnection() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) MyApplication.context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();

    }

    public static class Permissions {
        private static Permissions ourInstance = new Permissions();

        public static Permissions getInstance() {
            return ourInstance;
        }

        public enum Type {
            STORAGE("storage", 0),
            CONTACTS("Contacts", 1);

            private String stringValue;
            private int intValue;

            private Type(String toString, int value) {
                stringValue = toString;
                intValue = value;
            }

            public static Type fromInteger(int x) {
                switch (x) {
                    case 0:
                        return STORAGE;
                    case 1:
                        return CONTACTS;
                }
                return null;
            }

            public int getValue() {
                return intValue;
            }

            @Override
            public String toString() {
                return stringValue;
            }
        }


        public void ShowPermission(Activity activity, Permissions.Type type) {
            if (type == Type.STORAGE) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (activity.checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                        Log.v("TAG", "Permission is granted");
                        strPermission = "Permission is granted";
                    } else {
                        ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 999);
                    }
                }
            }
        }
    }


    public enum AppMode {
        Dev("Dev", 1),
        Staging("Staging", 2),
        Prod("Prod", 3);

        private String stringValue;
        private int intValue;

        private AppMode(String toString, int value) {
            stringValue = toString;
            intValue = value;
        }

        public static AppMode fromInteger(int x) {
            switch (x) {
                case 1:
                    return Dev;
                case 2:
                    return Staging;
                case 3:
                    return Prod;
            }
            return null;
        }

        public int getValue() {
            return intValue;
        }

        @Override
        public String toString() {
            return stringValue;
        }
    }

    /**
     * When the Api throw an error this function will call
     * <p>
     * *** Uses
     * * Dismiss Dialog
     * * Print stack
     * * Return error block
     * * Display error
     *
     * @param errorBody
     * @param listener
     */
    public void apiErrorConverter(Object errorBody, CommonCallback.Listener listener) {
        try {
            InputStream inputStream = ((ResponseBody) errorBody).byteStream();
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));

            Gson gson = new GsonBuilder().create();
            ErrorApiResponse mError = gson.fromJson(bufferedReader.readLine(), ErrorApiResponse.class);
            if (mError.getStatus().equals(422)) {
                MyApplication.displayKnownError(mError.getMessage());
            } else {
                MyApplication.displayKnownError(mError.getMessage());
            }


        } catch (Exception e) {
            e.printStackTrace();
            MyApplication.displayUnKnownError();
            listener.onFailure(LanguageConstants.somethingWentWrong);
        }
        CustomProgressDialog.getInstance().dismiss();
    }


    /**
     * Check activity is running or not
     *
     * @param ctx
     * @return
     */
    public boolean isActivityRunning(Context ctx) {
        ActivityManager activityManager = (ActivityManager) ctx.getSystemService(Context.ACTIVITY_SERVICE);
        assert activityManager != null;
        List<ActivityManager.RunningTaskInfo> tasks = activityManager.getRunningTasks(Integer.MAX_VALUE);

        for (ActivityManager.RunningTaskInfo task : tasks) {
            if (ctx.getPackageName().equalsIgnoreCase(task.baseActivity.getPackageName()))
                return true;
        }

        return false;
    }

    @SuppressWarnings("deprecation")
    public static Spanned fromHtml(String html) {
        Spanned result;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            result = Html.fromHtml(html, Html.FROM_HTML_MODE_LEGACY);
        } else {
            result = Html.fromHtml(html);
        }
        return result;
    }


    public String GeoCodingAddress_details(Activity activity, double workLatitude, double workLongitude) {

        List<Address> addresses = null;


        Geocoder mGeocoder = new Geocoder(activity, Locale.getDefault());

        try {
            addresses = mGeocoder.getFromLocation(workLatitude, workLongitude,
                    // In this sample, get just a single address.
                    1);
        } catch (IOException e) {
            e.printStackTrace();
        }

        String mPlaceName = "", mSubPlace = "", mLocality = "", mState = "", mCountry = "";
        try {
            if (addresses != null && addresses.size() != 0) {
                for (int i = 0; i < addresses.size(); i++) {
                    mPlaceName = addresses.get(i).getFeatureName();
                    mSubPlace = addresses.get(i).getSubLocality();
                    mLocality = addresses.get(i).getLocality();
                    mState = addresses.get(i).getAdminArea();
                    mCountry = addresses.get(i).getCountryName();
                }
            }


        } catch (NullPointerException npe) {
            npe.printStackTrace();
        }
        String addressFull = "";
        if (!TextUtils.isEmpty(mPlaceName)) {
            addressFull = addressFull + mPlaceName;
        }
        if (!TextUtils.isEmpty(mSubPlace)) {
            addressFull = addressFull + "," + mSubPlace;
        }
        if (!TextUtils.isEmpty(mLocality)) {
            addressFull = addressFull + "," + mLocality;
        }
        if (!TextUtils.isEmpty(mState)) {
            addressFull = addressFull + "," + mState;
        }
        if (!TextUtils.isEmpty(mCountry)) {
            addressFull = addressFull + "," + mCountry;
        }

        return addressFull;
    }

    public void hideSoftKeyboard(Activity activity) {
        InputMethodManager inputManager = (InputMethodManager) activity.getSystemService(
                Context.INPUT_METHOD_SERVICE);
        View focusedView = activity.getCurrentFocus();
        if (focusedView != null) {
            inputManager.hideSoftInputFromWindow(focusedView.getWindowToken(),
                    InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }


    public void Intent(Context context) {
        Intent intent = new Intent(context, SplashActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
    }

    /**
     * Convert date format from one to another
     *
     * @param currentFormat
     * @param requiredFormat
     * @param dateString
     * @return
     */
    public String changeDateFormat(String currentFormat, String requiredFormat, String dateString) {
        String result = "";
        if (dateString == null || dateString.isEmpty()) {
            return result;
        }
        SimpleDateFormat formatterOld = new SimpleDateFormat(currentFormat, Locale.ENGLISH);
        SimpleDateFormat formatterNew = new SimpleDateFormat(requiredFormat, Locale.ENGLISH);
        Date date = null;
        try {
            date = formatterOld.parse(dateString);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        if (date != null) {
            result = formatterNew.format(date);
        }
        return result;
    }
    public void chaneTextColorBasedOnRestaurant(String colorCode, TextView... textViews) {
        for (int count = 0; count < textViews.length; count++) {
            textViews[count].setTextColor(Color.parseColor(colorCode));
        }

    }
    public void PasswordMatch(Context context, String message1, String message2) {
        View parent = ((Activity) context).getWindow().getDecorView().findViewById(android.R.id.content);
        Snackbar snackbar;
        String result = MessageFormat.format(LanguageConstants.Didnotmatch, message1, message2);
        snackbar = Snackbar.make(parent, result, Snackbar.LENGTH_SHORT);
        View snackBarView = snackbar.getView();
        snackBarView.setBackgroundColor(context.getResources().getColor(R.color.colorIndicator));
        TextView textView = (TextView) snackBarView.findViewById(R.id.snackbar_text);
        textView.setTextColor(context.getResources().getColor(R.color.colorWhite));
        //snackbar.show();

        MDToast mdToast = MDToast.makeText(context, result, Toast.LENGTH_LONG, MDToast.TYPE_ERROR);
        mdToast.show();
    }

    public int findItemInMyPackages(String clothTypeKeyOfItem, int qty, List<GetSubscribedItemsTypes.Item> items) {
        for (GetSubscribedItemsTypes.Item item : items) {
            if (item.getClothTypeKey().trim().equals(clothTypeKeyOfItem.trim())) {
                return item.getCountInPackages() - qty;
            }
        }

        return Integer.MIN_VALUE;
    }

}
