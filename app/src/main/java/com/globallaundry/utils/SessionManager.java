package com.globallaundry.utils;

import android.app.Activity;
import android.content.SharedPreferences;


public class SessionManager {

    private static final String APP_PREFERENCE_NAME = "connect_pref";
    private static final String USER_KEY = "pref_userKey";
    private static final String REDEEMPOINT = "pref_redeemPoint";
    private static final String REFER_KEY = "pref_referKey";
    private static final String CATEGORY_KEY = "pref_categoryKey";
    private static final String CLOTHTYPE_KEY = "pref_clothTypeKey";
    private static final String LANGUAGE_KEY = "pref_language_key";
    private static final String CURRENCY_CODE = "pref_currency_code";
    private static final String TOURPAGE = "pref_tour_page";
    private static final String USER_TYPE = "pref_user_type";
    private static final String APP_MODE = "pref_add_mode";
    private static final String ACCESS_TOKEN = "pref_access_token";
    private static final String USER_NAME = "pref_user_name";
    private static final String USER_EMAIL = "pref_user_email";
    private static final String USER_MOBILE_NUMBER = "pref_user_mobile_number";
    private static final String USER_PIC = "pref_user_pic";

    private static SessionManager ourInstance = new SessionManager();


    public static SessionManager getInstance() {
        return ourInstance;
    }


    public String getCurrencyCode() {
        SharedPreferences pref = MyApplication.context.getSharedPreferences(APP_PREFERENCE_NAME, Activity.MODE_PRIVATE);
        return pref.getString(CURRENCY_CODE, "L.L");
    }


    // ---- Language Code
    public void setAppLanguageCode(String languageCode) {
        SharedPreferences pref = MyApplication.context.getSharedPreferences(APP_PREFERENCE_NAME, Activity.MODE_PRIVATE);
        pref.edit().putString(LANGUAGE_KEY, languageCode).apply();
    }


    public String getAppLanguageCode() {
        SharedPreferences pref = MyApplication.context.getSharedPreferences(APP_PREFERENCE_NAME, Activity.MODE_PRIVATE);
        return pref.getString(LANGUAGE_KEY, "");
    }

    // ---- Logout
    public void Logout() {
        String deviceID = this.getDeviceToken();

        SharedPreferences prefs = MyApplication.context.getSharedPreferences(APP_PREFERENCE_NAME, Activity.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.clear();
        editor.apply();

        this.setDeviceToken(deviceID);
    }

    // ------ User Email
    public String getUserEmail() {
        SharedPreferences prefs = MyApplication.context.getSharedPreferences(APP_PREFERENCE_NAME, Activity.MODE_PRIVATE);
        return prefs.getString(USER_EMAIL, "");
    }

    public void setUserEmail(String useremail) {
        SharedPreferences pref = MyApplication.context.getSharedPreferences(APP_PREFERENCE_NAME, Activity.MODE_PRIVATE);
        pref.edit().putString(USER_EMAIL, useremail).apply();
    }

    // ------ Tour Page

    public String getTourpage() {
        SharedPreferences prefs = MyApplication.context.getSharedPreferences(APP_PREFERENCE_NAME, Activity.MODE_PRIVATE);
        return prefs.getString(TOURPAGE, "");
    }

    public void setTourpage(String strTourpage) {
        SharedPreferences pref = MyApplication.context.getSharedPreferences(APP_PREFERENCE_NAME, Activity.MODE_PRIVATE);
        pref.edit().putString(TOURPAGE, strTourpage).apply();
    }


    // ------ User Key
    public String getUserKey() {
        SharedPreferences prefs = MyApplication.context.getSharedPreferences(APP_PREFERENCE_NAME, Activity.MODE_PRIVATE);
        return prefs.getString(USER_KEY, "");
    }

    public void setUserKey(String strUserKey) {
        SharedPreferences pref = MyApplication.context.getSharedPreferences(APP_PREFERENCE_NAME, Activity.MODE_PRIVATE);
        pref.edit().putString(USER_KEY, strUserKey).apply();
    }

    public String getRedeemPoint() {
        SharedPreferences prefs = MyApplication.context.getSharedPreferences(APP_PREFERENCE_NAME, Activity.MODE_PRIVATE);
        return prefs.getString(REDEEMPOINT, "");
    }

    public void setRedeemPoint(String strUserKey) {
        SharedPreferences pref = MyApplication.context.getSharedPreferences(APP_PREFERENCE_NAME, Activity.MODE_PRIVATE);
        pref.edit().putString(REDEEMPOINT, strUserKey).apply();
    }

    // ------ Refer Key
    public String getReferKey() {
        SharedPreferences prefs = MyApplication.context.getSharedPreferences(APP_PREFERENCE_NAME, Activity.MODE_PRIVATE);
        return prefs.getString(REFER_KEY, "");
    }

    public void setReferKey(String strUserKey) {
        SharedPreferences pref = MyApplication.context.getSharedPreferences(APP_PREFERENCE_NAME, Activity.MODE_PRIVATE);
        pref.edit().putString(REFER_KEY, strUserKey).apply();
    }

    // ------ Category Key
    public String getCategoryKey() {
        SharedPreferences prefs = MyApplication.context.getSharedPreferences(APP_PREFERENCE_NAME, Activity.MODE_PRIVATE);
        return prefs.getString(CATEGORY_KEY, "");
    }

    public void setCategoryKey(String categoryKey) {
        SharedPreferences pref = MyApplication.context.getSharedPreferences(APP_PREFERENCE_NAME, Activity.MODE_PRIVATE);
        pref.edit().putString(CATEGORY_KEY, categoryKey).apply();
    }

    // ------ Clothtype Key
    public String getClothTypeKey() {
        SharedPreferences prefs = MyApplication.context.getSharedPreferences(APP_PREFERENCE_NAME, Activity.MODE_PRIVATE);
        return prefs.getString(CLOTHTYPE_KEY, "");
    }

    public void setClothTypeKey(String clothTypeKey) {
        SharedPreferences pref = MyApplication.context.getSharedPreferences(APP_PREFERENCE_NAME, Activity.MODE_PRIVATE);
        pref.edit().putString(CLOTHTYPE_KEY, clothTypeKey).apply();
    }

    // ------ User Mobile Number
    public String getUserMobileNumber() {
        SharedPreferences prefs = MyApplication.context.getSharedPreferences(APP_PREFERENCE_NAME, Activity.MODE_PRIVATE);
        return prefs.getString(USER_MOBILE_NUMBER, "");
    }

    public void setUserMobileNumber(String userMobileNumber) {
        SharedPreferences pref = MyApplication.context.getSharedPreferences(APP_PREFERENCE_NAME, Activity.MODE_PRIVATE);
        pref.edit().putString(USER_MOBILE_NUMBER, userMobileNumber).apply();
    }

    // ------ User Mobile Number
    public String getUserPic() {
        SharedPreferences prefs = MyApplication.context.getSharedPreferences(APP_PREFERENCE_NAME, Activity.MODE_PRIVATE);
        return prefs.getString(USER_PIC, "");
    }

    public void setUserPic(String userPic) {
        SharedPreferences pref = MyApplication.context.getSharedPreferences(APP_PREFERENCE_NAME, Activity.MODE_PRIVATE);
        pref.edit().putString(USER_PIC, userPic).apply();
    }

    // ------ User Type
    public String getUserType() {
        SharedPreferences prefs = MyApplication.context.getSharedPreferences(APP_PREFERENCE_NAME, Activity.MODE_PRIVATE);
        return prefs.getString(USER_TYPE, "");
    }

    public void setUserType(String strUserType) {
        SharedPreferences pref = MyApplication.context.getSharedPreferences(APP_PREFERENCE_NAME, Activity.MODE_PRIVATE);
        pref.edit().putString(USER_TYPE, strUserType).apply();
    }

    // ------ Access Token
    public String getAccessToken() {
        SharedPreferences prefs = MyApplication.context.getSharedPreferences(APP_PREFERENCE_NAME, Activity.MODE_PRIVATE);
        return prefs.getString(ACCESS_TOKEN, "");
    }

    public void setAccessToken(String accessToken) {
        SharedPreferences pref = MyApplication.context.getSharedPreferences(APP_PREFERENCE_NAME, Activity.MODE_PRIVATE);
        pref.edit().putString(ACCESS_TOKEN, accessToken).apply();
    }

    // ------ User Name
    public String getUserName() {
        SharedPreferences prefs = MyApplication.context.getSharedPreferences(APP_PREFERENCE_NAME, Activity.MODE_PRIVATE);
        return prefs.getString(USER_NAME, "");
    }

    public void setUserName(String username) {
        SharedPreferences pref = MyApplication.context.getSharedPreferences(APP_PREFERENCE_NAME, Activity.MODE_PRIVATE);
        pref.edit().putString(USER_NAME, username).apply();
    }

    public void setDeviceToken(String deviceToken) {
        SharedPreferences pref = MyApplication.context.getSharedPreferences(APP_PREFERENCE_NAME, Activity.MODE_PRIVATE);
        pref.edit().putString("DeviceToken", (deviceToken == null ? "" : deviceToken)).apply();
    }

    public String getDeviceToken() {
        SharedPreferences prefs = MyApplication.context.getSharedPreferences(APP_PREFERENCE_NAME, Activity.MODE_PRIVATE);
        return prefs.getString("DeviceToken", "");
    }


    // ------------ App Mode
    public void setAppMode(CommonFunctions.AppMode mAppMode) {
        SharedPreferences pref = MyApplication.context.getSharedPreferences(APP_PREFERENCE_NAME, Activity.MODE_PRIVATE);
        pref.edit().putInt(APP_MODE, mAppMode.getValue()).apply();
    }

    public CommonFunctions.AppMode getAppMode() {
        SharedPreferences pref = MyApplication.context.getSharedPreferences(APP_PREFERENCE_NAME, Activity.MODE_PRIVATE);
        int status = pref.getInt(APP_MODE, CommonFunctions.AppMode.Staging.getValue());
        return CommonFunctions.AppMode.fromInteger(status);
    }

    public static class AddressProperties {
        private static AddressProperties ourInstance = new AddressProperties();

        public static AddressProperties getInstance() {
            return ourInstance;
        }

        public void setPickUpAddressKey(String value) {
            SharedPreferences pref = MyApplication.context.getSharedPreferences(APP_PREFERENCE_NAME, Activity.MODE_PRIVATE);
            pref.edit().putString("PickUpAddressKey", value).apply();
        }

        public String getPickUpAddressKey() {
            SharedPreferences prefs = MyApplication.context.getSharedPreferences(APP_PREFERENCE_NAME, Activity.MODE_PRIVATE);
            return prefs.getString("PickUpAddressKey", "");
        }

        public void setPickUpAddress(String value) {
            SharedPreferences pref = MyApplication.context.getSharedPreferences(APP_PREFERENCE_NAME, Activity.MODE_PRIVATE);
            pref.edit().putString("PickUpAddress", value).apply();
        }

        public String getPickUpAddress() {
            SharedPreferences prefs = MyApplication.context.getSharedPreferences(APP_PREFERENCE_NAME, Activity.MODE_PRIVATE);
            return prefs.getString("PickUpAddress", "");
        }

        public void setDeliveryAddressKey(String value) {
            SharedPreferences pref = MyApplication.context.getSharedPreferences(APP_PREFERENCE_NAME, Activity.MODE_PRIVATE);
            pref.edit().putString("DeliveryAddressKey", value).apply();
        }

        public String getDeliveryAddressKey() {
            SharedPreferences prefs = MyApplication.context.getSharedPreferences(APP_PREFERENCE_NAME, Activity.MODE_PRIVATE);
            return prefs.getString("DeliveryAddressKey", "");
        }

        public void setDeliveryAddress(String value) {
            SharedPreferences pref = MyApplication.context.getSharedPreferences(APP_PREFERENCE_NAME, Activity.MODE_PRIVATE);
            pref.edit().putString("DeliveryAddress", value).apply();
        }

        public String getDeliveryAddress() {
            SharedPreferences prefs = MyApplication.context.getSharedPreferences(APP_PREFERENCE_NAME, Activity.MODE_PRIVATE);
            return prefs.getString("DeliveryAddress", "");
        }

        public void setPickUpDate(String value) {
            SharedPreferences pref = MyApplication.context.getSharedPreferences(APP_PREFERENCE_NAME, Activity.MODE_PRIVATE);
            pref.edit().putString("PickUpDate", value).apply();
        }

        public String getPickUpDate() {
            SharedPreferences prefs = MyApplication.context.getSharedPreferences(APP_PREFERENCE_NAME, Activity.MODE_PRIVATE);
            return prefs.getString("PickUpDate", "");
        }

        public void setPickUpTime(String value) {
            SharedPreferences pref = MyApplication.context.getSharedPreferences(APP_PREFERENCE_NAME, Activity.MODE_PRIVATE);
            pref.edit().putString("PickUpTime", value).apply();
        }

        public String getPickUpTime() {
            SharedPreferences prefs = MyApplication.context.getSharedPreferences(APP_PREFERENCE_NAME, Activity.MODE_PRIVATE);
            return prefs.getString("PickUpTime", "");
        }

        public void setDeliveryDate(String value) {
            SharedPreferences pref = MyApplication.context.getSharedPreferences(APP_PREFERENCE_NAME, Activity.MODE_PRIVATE);
            pref.edit().putString("DeliveryDate", value).apply();
        }

        public String getDeliveryDate() {
            SharedPreferences prefs = MyApplication.context.getSharedPreferences(APP_PREFERENCE_NAME, Activity.MODE_PRIVATE);
            return prefs.getString("DeliveryDate", "");
        }

        public void setDeliveryTime(String value) {
            SharedPreferences pref = MyApplication.context.getSharedPreferences(APP_PREFERENCE_NAME, Activity.MODE_PRIVATE);
            pref.edit().putString("DeliveryTime", value).apply();
        }

        public String getDeliveryTime() {
            SharedPreferences prefs = MyApplication.context.getSharedPreferences(APP_PREFERENCE_NAME, Activity.MODE_PRIVATE);
            return prefs.getString("DeliveryTime", "");
        }

    }

    public static class LocationProperties {
        private static LocationProperties ourInstance = new LocationProperties();

        public static LocationProperties getInstance() {
            return ourInstance;
        }

        public void setCityKey(String value) {
            SharedPreferences pref = MyApplication.context.getSharedPreferences(APP_PREFERENCE_NAME, Activity.MODE_PRIVATE);
            pref.edit().putString("cityKey", value).apply();
        }

        public String getCityKey() {
            SharedPreferences prefs = MyApplication.context.getSharedPreferences(APP_PREFERENCE_NAME, Activity.MODE_PRIVATE);
            return prefs.getString("cityKey", "");
        }

        public void setCityName(String value) {
            SharedPreferences pref = MyApplication.context.getSharedPreferences(APP_PREFERENCE_NAME, Activity.MODE_PRIVATE);
            pref.edit().putString("cityName", value).apply();
        }

        public String getCityName() {
            SharedPreferences prefs = MyApplication.context.getSharedPreferences(APP_PREFERENCE_NAME, Activity.MODE_PRIVATE);
            return prefs.getString("cityName", "");
        }

        public void setAreaKey(String value) {
            SharedPreferences pref = MyApplication.context.getSharedPreferences(APP_PREFERENCE_NAME, Activity.MODE_PRIVATE);
            pref.edit().putString("areaKey", value).apply();
        }

        public String getAreaKey() {
            SharedPreferences prefs = MyApplication.context.getSharedPreferences(APP_PREFERENCE_NAME, Activity.MODE_PRIVATE);
            return prefs.getString("areaKey", "");
        }

        public void setAreaName(String value) {
            SharedPreferences pref = MyApplication.context.getSharedPreferences(APP_PREFERENCE_NAME, Activity.MODE_PRIVATE);
            pref.edit().putString("areaName", value).apply();
        }

        public String getAreaName() {
            SharedPreferences prefs = MyApplication.context.getSharedPreferences(APP_PREFERENCE_NAME, Activity.MODE_PRIVATE);
            return prefs.getString("areaName", "");
        }

        public void setLatitude(String value) {
            SharedPreferences pref = MyApplication.context.getSharedPreferences(APP_PREFERENCE_NAME, Activity.MODE_PRIVATE);
            pref.edit().putString("latitude", value).apply();
        }

        public String getLatitude() {
            SharedPreferences prefs = MyApplication.context.getSharedPreferences(APP_PREFERENCE_NAME, Activity.MODE_PRIVATE);
            return prefs.getString("latitude", "");
        }

        public void setLongitude(String value) {
            SharedPreferences pref = MyApplication.context.getSharedPreferences(APP_PREFERENCE_NAME, Activity.MODE_PRIVATE);
            pref.edit().putString("Longitude", value).apply();
        }

        public String getLongitude() {
            SharedPreferences prefs = MyApplication.context.getSharedPreferences(APP_PREFERENCE_NAME, Activity.MODE_PRIVATE);
            return prefs.getString("Longitude", "");
        }

    }
}
