package com.globallaundry.utils;

import android.annotation.SuppressLint;
import android.app.Application;
import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import android.widget.Toast;

import androidx.annotation.NonNull;


import com.crashlytics.android.Crashlytics;
import com.onesignal.OSNotification;
import com.onesignal.OneSignal;
import com.valdesekamdem.library.mdtoast.MDToast;

import io.fabric.sdk.android.Fabric;
import org.json.JSONObject;

import java.util.Locale;

import io.realm.Realm;
import io.realm.RealmConfiguration;

@SuppressLint("Registered")
public class MyApplication extends Application {

    @SuppressLint("StaticFieldLeak")
    public static Context context;

    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics());
        context = getBaseContext();
        LanguageConstants.getInstance().languageConstants();
        OneSignal.startInit(this)
                .inFocusDisplaying(OneSignal.OSInFocusDisplayOption.Notification)
                .setNotificationReceivedHandler(new NotificationReceivedHandler())
                .unsubscribeWhenNotificationsAreDisabled(true)
                .init();
        Realm.init(context);
        RealmConfiguration config = new RealmConfiguration.Builder()
                .name(CommonFunctions.DBName.toLowerCase() + ".realm")
                .schemaVersion(0)
                .deleteRealmIfMigrationNeeded()
                .build();

        Realm.setDefaultConfiguration(config);
    }

    public static void displayKnownError(String message) {
        MDToast mdToast = MDToast.makeText(context, message, Toast.LENGTH_LONG, MDToast.TYPE_ERROR);
        mdToast.show();
    }

    public static void displayUnKnownError() {
        MDToast mdToast = MDToast.makeText(context, LanguageConstants.somethingWentWrong, Toast.LENGTH_LONG, MDToast.TYPE_ERROR);
        mdToast.show();
    }

    public static void displayKnownErrorSuccess(Context mContext, String message) {
        //CommonFunctions.getInstance().ShowSnackBar(mContext, message);
        MDToast mdToast = MDToast.makeText(context, message, Toast.LENGTH_LONG, MDToast.TYPE_SUCCESS);
        mdToast.show();
    }

    public static void result(String message) {
        Toast.makeText(context, message, Toast.LENGTH_LONG).show();
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);

    }
    class NotificationReceivedHandler implements OneSignal.NotificationReceivedHandler {
        @Override
        public void notificationReceived(OSNotification notification) {
            JSONObject data = notification.payload.additionalData;
            String customKey;
        }
    }


    // -------- Local Change Language
    public void refreshLocale(@NonNull Context context, boolean force) {
        // final String language = QuranSettings.getInstance(this).isArabicNames() ? "ar" : null;

        String language = null;
        if (SessionManager.getInstance().getAppLanguageCode().isEmpty()) {
            language = "";
        } else if (SessionManager.getInstance().getAppLanguageCode().equals("en")) {
            language = "en";
        } else if (SessionManager.getInstance().getAppLanguageCode().equals("ar")) {
            language = "ar";
        }
        final Locale locale;
        if ("ar".equals(language)) {
            locale = new Locale("ar");
        } else if (force) {
            // get the system locale (since we overwrote the default locale)
            locale = Resources.getSystem().getConfiguration().locale;
        } else {
            // nothing to do...
            locale = new Locale("en");
            return;
        }

        updateLocale(context, locale);
        final Context appContext = context.getApplicationContext();
        if (context != appContext) {
            updateLocale(appContext, locale);
        }
    }


    private void updateLocale(@NonNull Context context, @NonNull Locale locale) {
        final Resources resources = context.getResources();
        Configuration config = resources.getConfiguration();
        config.locale = locale;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            config.setLayoutDirection(config.locale);
        }
        resources.updateConfiguration(config, resources.getDisplayMetrics());
    }


}
