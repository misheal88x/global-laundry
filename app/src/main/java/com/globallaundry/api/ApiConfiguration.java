package com.globallaundry.api;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;


import com.globallaundry.utils.CommonFunctions;
import com.globallaundry.utils.CustomProgressDialog;
import com.globallaundry.utils.MyApplication;
import com.globallaundry.utils.SessionManager;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class ApiConfiguration {

    public String ProdOrDev = "?env";

    private static ApiConfiguration ourInstance = new ApiConfiguration();
    Retrofit mRetrofit;

    public static ApiConfiguration getInstance() {
        if (ourInstance == null) {
            synchronized (ApiConfiguration.class) {
                if (ourInstance == null)
                    ourInstance = new ApiConfiguration();
            }
        }
        ourInstance.config();
        return ourInstance;
    }

    private ApiConfiguration() {
    }

    private void config() {


        Gson gson = new GsonBuilder()
                .setLenient()
                .create();
        String BASE_URL = Urls.BASE_URL + Urls.VERSIONN;
        if (SessionManager.getInstance().getAppMode() == CommonFunctions.AppMode.Dev) {
            BASE_URL = Urls.BASE_URL_DEV + Urls.VERSIONN;
        } else if (SessionManager.getInstance().getAppMode() == CommonFunctions.AppMode.Prod) {
            BASE_URL = Urls.BASE_URL_PROD + Urls.VERSIONN;
        }
        mRetrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(getRequestHeader(SessionManager.getInstance().getAccessToken()))
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();
    }

    private OkHttpClient getRequestHeader(final String token) {

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient.Builder okHttpClientBuild = new OkHttpClient.Builder()
                .addNetworkInterceptor(interceptor)
                /*.addInterceptor(new ChuckInterceptor(GohaikuApplication.context))*/
                .addInterceptor(new Interceptor() {
                    @Override
                    public okhttp3.Response intercept(Chain chain) throws IOException {
                        Request newRequest = chain.request().newBuilder()
                                .addHeader("Authorization", "Bearer " + token)
                                .addHeader("Accept-Language", SessionManager.getInstance().getAppLanguageCode())
                                .build();
                        if (!CheckInternetConnection()) {
                            CustomProgressDialog.getInstance().dismiss();

                            throw new NoConnectivityException();
                        } else {
                            okhttp3.Response response = chain.proceed(newRequest);
                            return response;
                        }
                    }
                })
                .readTimeout(60, TimeUnit.SECONDS)
                .connectTimeout(60, TimeUnit.SECONDS);

        if (ProdOrDev != "") {
            okHttpClientBuild.interceptors().add(new Interceptor() {
                @Override
                public okhttp3.Response intercept(Chain chain) throws IOException {
                    Request request = chain.request();
                    HttpUrl url = request.url().newBuilder().addQueryParameter("env", "dev").build();
                    request = request.newBuilder().url(url).build();

                    if (!CheckInternetConnection()) {
                        CustomProgressDialog.getInstance().dismiss();
                        throw new NoConnectivityException();
                    } else {
                        okhttp3.Response response = chain.proceed(request);
                        return response;
                    }
                }
            });
        }
        OkHttpClient okHttpClient = okHttpClientBuild.build();
        return okHttpClient;
    }

    public Retrofit getApiBuilder() {

        return mRetrofit;
    }

    public class NoConnectivityException extends IOException {
        @Override
        public String getMessage() {
            return "";
        }
    }

    public boolean CheckInternetConnection() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) MyApplication.context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();

    }
}