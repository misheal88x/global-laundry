package com.globallaundry.api;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import com.globallaundry.R;
import com.globallaundry.activity.OtpVerificationActivity;
import com.globallaundry.activity.SignupActivity;
import com.globallaundry.apimodel.AddAddressApiResponse;
import com.globallaundry.apimodel.AddRatingApiResponse;
import com.globallaundry.apimodel.AddressListApiResponse;
import com.globallaundry.apimodel.AddressTypeApiResponse;
import com.globallaundry.apimodel.AreaListApiResponse;
import com.globallaundry.apimodel.CalculateApiResponse;
import com.globallaundry.apimodel.CategoryApiResponse;
import com.globallaundry.apimodel.ChangeAddressApiResponse;
import com.globallaundry.apimodel.ChangeDeliveryAddressApiResponse;
import com.globallaundry.apimodel.ChangePasswordApiResponse;
import com.globallaundry.apimodel.CityListApiResponse;
import com.globallaundry.apimodel.ClothTypeApiResponse;
import com.globallaundry.apimodel.ClothTypeListViewApiResponse;
import com.globallaundry.apimodel.ContactusSubmitApiResponse;
import com.globallaundry.apimodel.DeleteAddressApiResponse;
import com.globallaundry.apimodel.EditAddressApiResponse;
import com.globallaundry.apimodel.ForgotPasswordApiResponse;
import com.globallaundry.apimodel.GetAvailablePackagesApiResponse;
import com.globallaundry.apimodel.GetContactusApiResponse;
import com.globallaundry.apimodel.GetMySubscribedPackagesApiResponse;
import com.globallaundry.apimodel.GetPaymentMethodsAPIResponse;
import com.globallaundry.apimodel.GetProfileApiResponse;
import com.globallaundry.apimodel.GetRushHoursApiResponse;
import com.globallaundry.apimodel.GetSubscribedItemsTypes;
import com.globallaundry.apimodel.GetTermsConditionsApiResponse;
import com.globallaundry.apimodel.HelpApiResponse;
import com.globallaundry.apimodel.LoginApiResponse;
import com.globallaundry.apimodel.MarkAsDefaultApiResponse;
import com.globallaundry.apimodel.OrderDetailsApiResponse;
import com.globallaundry.apimodel.OrderListApiResponse;
import com.globallaundry.apimodel.PickUpTimeSlotApiResponse;
import com.globallaundry.apimodel.PlaceOrderApiResponse;
import com.globallaundry.apimodel.PromationAddApiResponse;
import com.globallaundry.apimodel.RegistrationApiResponse;
import com.globallaundry.apimodel.ResendOTPApiResponse;
import com.globallaundry.apimodel.ResetPasswordResponse;
import com.globallaundry.apimodel.ReviewListApiResponse;
import com.globallaundry.apimodel.SendOtpBeforeLoginApiResponse;
import com.globallaundry.apimodel.SubscribePackageApiResponse;
import com.globallaundry.apimodel.TimeSlotApiResponse;
import com.globallaundry.apimodel.UpdateProfileApiResponse;
import com.globallaundry.apimodel.UpdateProfileImageApiResponse;
import com.globallaundry.apimodel.VerifyOTPApiResponse;
import com.globallaundry.interfaces.CommonCallback;
import com.globallaundry.models.ErrorApiResponse;
import com.globallaundry.models.NotificationListApiResponse;
import com.globallaundry.models.ReadNotificationApiResponse;
import com.globallaundry.models.SocialLoginApiResponse;
import com.globallaundry.models.UpdateVerificationStatusApiResponse;
import com.globallaundry.utils.CommonFunctions;
import com.globallaundry.utils.CustomProgressDialog;
import com.globallaundry.utils.LanguageConstants;
import com.globallaundry.utils.MyApplication;
import com.globallaundry.utils.SessionManager;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStream;
import java.io.InputStreamReader;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class CommonApiCalls {
    private static CommonApiCalls ourInstance;

    public static CommonApiCalls getInstance() {
        ourInstance = new CommonApiCalls();
        return ourInstance;
    }


    // ---POST Method ---  Login
    public void Login(Context context, String username, String password, String device_type, String device_token, final CommonCallback.Listener listener) {
        if (!CustomProgressDialog.getInstance().isShowing()) {
            CustomProgressDialog.getInstance().show(context);
        }
        ApiInterface apiInterface = ApiConfiguration.getInstance().getApiBuilder().create(ApiInterface.class);
        Call<LoginApiResponse> call = apiInterface.login(username, password, device_type, device_token);
        call.enqueue(new Callback<LoginApiResponse>() {
            @Override
            public void onResponse(Call<LoginApiResponse> call, Response<LoginApiResponse> response) {
                CustomProgressDialog.getInstance().dismiss();
                if (response.isSuccessful()) {
                    if (response.body().getStatus() == 200) {
                        MyApplication.displayKnownErrorSuccess(context, response.body().getMessage());
                        listener.onSuccess(response.body());
                    } else {
                        MyApplication.displayKnownError(response.body().getMessage());
                        listener.onFailure(response.message());
                    }
                } else if (response.code() == 403) {
                    CustomProgressDialog.getInstance().dismiss();

                    final Dialog dialog = new Dialog(context);
                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    dialog.setContentView(R.layout.dialog_otpverification);
                    dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
                    dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
                    TextView tvQue = dialog.findViewById(R.id.tvQue);
                    Button btnCancel = dialog.findViewById(R.id.btnCancel);
                    Button btnYes = dialog.findViewById(R.id.btnYes);
                    btnCancel.setText(LanguageConstants.no);
                    btnYes.setText(LanguageConstants.yes);
                    tvQue.setText(LanguageConstants.Otp_not_yet_verified_Do_you_want_verify_now);

                    //FontFunctions.getInstance().changeFont(2, context, tvQue, btnCancel, btnYes);
                    btnYes.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.dismiss();
                            if (!CustomProgressDialog.getInstance().isShowing()) {
                                CustomProgressDialog.getInstance().show(context);
                            }

                            ApiInterface apiInterface = ApiConfiguration.getInstance().getApiBuilder().create(ApiInterface.class);
                            Call<SendOtpBeforeLoginApiResponse> call = apiInterface.sendOtpBeforeLogin(username);
                            call.enqueue(new Callback<SendOtpBeforeLoginApiResponse>() {
                                @Override
                                public void onResponse(Call<SendOtpBeforeLoginApiResponse> call, Response<SendOtpBeforeLoginApiResponse> response) {
                                    if (response.isSuccessful()) {
                                        CustomProgressDialog.getInstance().dismiss();
                                        //listener.onSuccess(response.body());
                                        if (response.body().getStatus().equals(200)) {
                                            if (response.body().getData() != null) {
                                                SendOtpBeforeLoginApiResponse.Data data = response.body().getData();
                                                Bundle mBundle = new Bundle();
                                                mBundle.putString("user_key", data.getUser_key());
                                                mBundle.putString("mobile_number", data.getMobile_number());
                                                mBundle.putString("TYPE", "1");
                                                CommonFunctions.getInstance().newIntent(context, OtpVerificationActivity.class, mBundle, false);
                                            }
                                        }
                                    } else {
                                        CommonFunctions.getInstance().apiErrorConverter(response.errorBody(), listener);
                                    }
                                }

                                @Override
                                public void onFailure(Call<SendOtpBeforeLoginApiResponse> call, Throwable t) {
                                    t.printStackTrace();
                                    CommonFunctions.getInstance().apiErrorConverter(t.getMessage(), listener);
                                }
                            });

                        }
                    });

                    btnCancel.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.dismiss();
                        }
                    });
                    Window window = dialog.getWindow();
                    WindowManager.LayoutParams wlp = window.getAttributes();
                    wlp.gravity = Gravity.CENTER;
                    window.setAttributes(wlp);
                    dialog.show();
                } else {
                    try {
                        InputStream inputStream = ((ResponseBody) response.errorBody()).byteStream();
                        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));

                        Gson gson = new GsonBuilder().create();
                        ErrorApiResponse mError = gson.fromJson(bufferedReader.readLine(), ErrorApiResponse.class);

                        if (mError.getStatus().equals(422)) {
                            MyApplication.displayKnownError(mError.getMessage());
                        } else {
                            MyApplication.displayKnownError(mError.getMessage());
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                        listener.onFailure(response.message());
                        MyApplication.displayKnownError(response.message());
                    }
                }
            }

            @Override
            public void onFailure(Call<LoginApiResponse> call, Throwable t) {
                CustomProgressDialog.getInstance().dismiss();
                t.printStackTrace();
                MyApplication.displayUnKnownError();
            }
        });
    }

    // ---POST Method ---  Register
    public void Register(Context context, String username, String email, String mobile_number, String password,
                         String acceptTerms, String firstname, String referralCode, final CommonCallback.Listener listener) {
        if (!CustomProgressDialog.getInstance().isShowing()) {
            CustomProgressDialog.getInstance().show(context);
        }
        ApiInterface apiInterface = ApiConfiguration.getInstance().getApiBuilder().create(ApiInterface.class);
        Call<RegistrationApiResponse> call = apiInterface.Registration(username, email, mobile_number, password,
            "1", firstname, referralCode);
        call.enqueue(new Callback<RegistrationApiResponse>() {
            @Override
            public void onResponse(Call<RegistrationApiResponse> call, Response<RegistrationApiResponse> response) {
                CustomProgressDialog.getInstance().dismiss();
                if (response.isSuccessful()) {
                    if (response.body().getStatus() == 200) {
                        MyApplication.displayKnownErrorSuccess(context, response.body().getMessage());
                        listener.onSuccess(response.body());
                    } else {
                        MyApplication.displayKnownError(response.body().getMessage());
                        listener.onFailure(response.message());
                    }
                } else {
                    try {
                        InputStream inputStream = ((ResponseBody) response.errorBody()).byteStream();
                        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));

                        Gson gson = new GsonBuilder().create();
                        ErrorApiResponse mError = gson.fromJson(bufferedReader.readLine(), ErrorApiResponse.class);

                        if (mError.getStatus().equals(422)) {
                            MyApplication.displayKnownError(mError.getMessage());
                        } else {
                            MyApplication.displayKnownError(mError.getMessage());
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                        listener.onFailure(response.message());
                        MyApplication.displayKnownError(response.message());
                    }
                }
            }

            @Override
            public void onFailure(Call<RegistrationApiResponse> call, Throwable t) {
                CustomProgressDialog.getInstance().dismiss();
                t.printStackTrace();
                MyApplication.displayUnKnownError();
            }
        });
    }

    public void verifyOTP(final Context context, final String otp, final String otp_id,
                          final String device_type, final String device_token, final String type, String mobile_number,
                          final CommonCallback.Listener listener) {
        if (!CustomProgressDialog.getInstance().isShowing()) {
            CustomProgressDialog.getInstance().show(context);
        }
        ApiInterface apiInterface = ApiConfiguration.getInstance().getApiBuilder().create(ApiInterface.class);
        Call<VerifyOTPApiResponse> call = apiInterface.VerifyOTP(otp, otp_id, device_type, device_token, type, mobile_number);
        call.enqueue(new Callback<VerifyOTPApiResponse>() {
            @Override
            public void onResponse(Call<VerifyOTPApiResponse> call, Response<VerifyOTPApiResponse> response) {
                CustomProgressDialog.getInstance().dismiss();
                if (response.isSuccessful()) {
                    if (response.body().getStatus() == 200) {
                        MyApplication.displayKnownErrorSuccess(context, response.body().getMessage());
                        listener.onSuccess(response.body());
                    } else {
                        MyApplication.displayKnownError(response.body().getMessage());
                        listener.onFailure(response.message());
                    }
                } else {
                    try {
                        InputStream inputStream = ((ResponseBody) response.errorBody()).byteStream();
                        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));

                        Gson gson = new GsonBuilder().create();
                        ErrorApiResponse mError = gson.fromJson(bufferedReader.readLine(), ErrorApiResponse.class);

                        if (mError.getStatus().equals(422)) {
                            MyApplication.displayKnownError(mError.getMessage());
                        } else {
                            MyApplication.displayKnownError(mError.getMessage());
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                        listener.onFailure(response.message());
                        MyApplication.displayKnownError(response.message());
                    }
                }
            }

            @Override
            public void onFailure(Call<VerifyOTPApiResponse> call, Throwable t) {
                CustomProgressDialog.getInstance().dismiss();
                t.printStackTrace();
                MyApplication.displayUnKnownError();
            }
        });

    }

    public void resendOTP(final Context context, final String otp_id,
                          final CommonCallback.Listener listener) {
        if (!CustomProgressDialog.getInstance().isShowing()) {
            CustomProgressDialog.getInstance().show(context);
        }
        ApiInterface apiInterface = ApiConfiguration.getInstance().getApiBuilder().create(ApiInterface.class);
        Call<ResendOTPApiResponse> call = apiInterface.resendOTP(otp_id);
        call.enqueue(new Callback<ResendOTPApiResponse>() {
            @Override
            public void onResponse(Call<ResendOTPApiResponse> call, Response<ResendOTPApiResponse> response) {
                CustomProgressDialog.getInstance().dismiss();
                if (response.isSuccessful()) {
                    if (response.body().getStatus() == 200) {
                        MyApplication.displayKnownErrorSuccess(context, response.body().getMessage());
                        listener.onSuccess(response.body());
                    } else {
                        MyApplication.displayKnownError(response.body().getMessage());
                        listener.onFailure(response.message());
                    }
                } else {
                    try {
                        InputStream inputStream = ((ResponseBody) response.errorBody()).byteStream();
                        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));

                        Gson gson = new GsonBuilder().create();
                        ErrorApiResponse mError = gson.fromJson(bufferedReader.readLine(), ErrorApiResponse.class);

                        if (mError.getStatus().equals(422)) {
                            MyApplication.displayKnownError(mError.getMessage());
                        } else {
                            MyApplication.displayKnownError(mError.getMessage());
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                        listener.onFailure(response.message());
                        MyApplication.displayKnownError(response.message());
                    }
                }
            }

            @Override
            public void onFailure(Call<ResendOTPApiResponse> call, Throwable t) {
                CustomProgressDialog.getInstance().dismiss();
                t.printStackTrace();
                MyApplication.displayUnKnownError();
                ;
            }
        });

    }

    public void pickTimeSlot(final Context context, final CommonCallback.Listener listener) {
        if (!CustomProgressDialog.getInstance().isShowing()) {
            CustomProgressDialog.getInstance().show(context);
        }
        ApiInterface apiInterface = ApiConfiguration.getInstance().getApiBuilder().create(ApiInterface.class);
        Call<PickUpTimeSlotApiResponse> call = apiInterface.pickupTimeSlot();
        call.enqueue(new Callback<PickUpTimeSlotApiResponse>() {
            @Override
            public void onResponse(Call<PickUpTimeSlotApiResponse> call, Response<PickUpTimeSlotApiResponse> response) {
                CustomProgressDialog.getInstance().dismiss();
                if (response.isSuccessful()) {
                    if (response.body().getStatus() == 200) {
                        MyApplication.displayKnownErrorSuccess(context, response.body().getMessage());
                        listener.onSuccess(response.body());
                    } else {
                        MyApplication.displayKnownError(response.body().getMessage());
                        listener.onFailure(response.message());
                    }
                } else {
                    try {
                        InputStream inputStream = ((ResponseBody) response.errorBody()).byteStream();
                        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));

                        Gson gson = new GsonBuilder().create();
                        ErrorApiResponse mError = gson.fromJson(bufferedReader.readLine(), ErrorApiResponse.class);

                        if (mError.getStatus().equals(422)) {
                            MyApplication.displayKnownError(mError.getMessage());
                        } else {
                            MyApplication.displayKnownError(mError.getMessage());
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                        listener.onFailure(response.message());
                        MyApplication.displayKnownError(response.message());
                    }
                }
            }

            @Override
            public void onFailure(Call<PickUpTimeSlotApiResponse> call, Throwable t) {
                CustomProgressDialog.getInstance().dismiss();
                t.printStackTrace();
                MyApplication.displayUnKnownError();
            }
        });

    }

    public void forgotPassword(final Context context,
                               final String email,
                               final CommonCallback.Listener listener) {
        if (!CustomProgressDialog.getInstance().isShowing()) {
            CustomProgressDialog.getInstance().show(context);
        }
        ApiInterface apiInterface = ApiConfiguration.getInstance().getApiBuilder().create(ApiInterface.class);
        Call<ForgotPasswordApiResponse> call = apiInterface.forgotPassword(email);
        call.enqueue(new Callback<ForgotPasswordApiResponse>() {
            @Override
            public void onResponse(Call<ForgotPasswordApiResponse> call, Response<ForgotPasswordApiResponse> response) {
                CustomProgressDialog.getInstance().dismiss();
                if (response.isSuccessful()) {
                    if (response.body().getStatus() == 200) {
                        MyApplication.displayKnownErrorSuccess(context, response.body().getMessage());
                        listener.onSuccess(response.body());
                    } else {
                        MyApplication.displayKnownError(response.body().getMessage());
                        listener.onFailure(response.message());
                    }
                } else {
                    try {
                        InputStream inputStream = ((ResponseBody) response.errorBody()).byteStream();
                        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));

                        Gson gson = new GsonBuilder().create();
                        ErrorApiResponse mError = gson.fromJson(bufferedReader.readLine(), ErrorApiResponse.class);

                        if (mError.getStatus().equals(422)) {
                            MyApplication.displayKnownError(mError.getMessage());
                        } else {
                            MyApplication.displayKnownError(mError.getMessage());
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                        listener.onFailure(response.message());
                        MyApplication.displayKnownError(response.message());
                    }
                }
            }

            @Override
            public void onFailure(Call<ForgotPasswordApiResponse> call, Throwable t) {
                CustomProgressDialog.getInstance().dismiss();
                t.printStackTrace();
                MyApplication.displayUnKnownError();
            }
        });

    }


    public void resetPassword(final Context context,
                              final String resetToken,
                              final String mNewPassword,
                              final String mConfirmPassword,
                              final CommonCallback.Listener listener) {
        if (!CustomProgressDialog.getInstance().isShowing()) {
            CustomProgressDialog.getInstance().show(context);
        }
        ApiInterface apiInterface = ApiConfiguration.getInstance().getApiBuilder().create(ApiInterface.class);
        Call<ResetPasswordResponse> call = apiInterface.resetPassword(resetToken, mNewPassword, mConfirmPassword);
        call.enqueue(new Callback<ResetPasswordResponse>() {
            @Override
            public void onResponse(Call<ResetPasswordResponse> call, Response<ResetPasswordResponse> response) {
                CustomProgressDialog.getInstance().dismiss();
                if (response.isSuccessful()) {
                    if (response.body().getStatus() == 200) {
                        MyApplication.displayKnownErrorSuccess(context, response.body().getMessage());
                        listener.onSuccess(response.body());
                    } else {
                        MyApplication.displayKnownError(response.body().getMessage());
                        listener.onFailure(response.message());
                    }
                } else {
                    try {
                        InputStream inputStream = ((ResponseBody) response.errorBody()).byteStream();
                        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));

                        Gson gson = new GsonBuilder().create();
                        ErrorApiResponse mError = gson.fromJson(bufferedReader.readLine(), ErrorApiResponse.class);

                        if (mError.getStatus().equals(422)) {
                            MyApplication.displayKnownError(mError.getMessage());
                        } else {
                            MyApplication.displayKnownError(mError.getMessage());
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                        listener.onFailure(response.message());
                        MyApplication.displayKnownError(response.message());
                    }
                }
            }

            @Override
            public void onFailure(Call<ResetPasswordResponse> call, Throwable t) {
                CustomProgressDialog.getInstance().dismiss();
                t.printStackTrace();
                MyApplication.displayUnKnownError();
            }
        });

    }


    public void getTermsAndConditions(final Context context,
                                      final CommonCallback.Listener listener) {
        if (!CustomProgressDialog.getInstance().isShowing()) {
            CustomProgressDialog.getInstance().show(context);
        }
        ApiInterface apiInterface = ApiConfiguration.getInstance().getApiBuilder().create(ApiInterface.class);
        Call<GetTermsConditionsApiResponse> call = apiInterface.getTermsCondtions();
        call.enqueue(new Callback<GetTermsConditionsApiResponse>() {
            @Override
            public void onResponse(Call<GetTermsConditionsApiResponse> call, Response<GetTermsConditionsApiResponse> response) {
                CustomProgressDialog.getInstance().dismiss();
                if (response.isSuccessful()) {
                    if (response.body().getStatus() == 200) {
                        // MyApplication.displayKnownErrorSuccess(context, response.body().getMessage());
                        listener.onSuccess(response.body());
                    } else {
                        MyApplication.displayKnownError(response.body().getMessage());
                        listener.onFailure(response.message());
                    }
                } else {
                    try {
                        InputStream inputStream = ((ResponseBody) response.errorBody()).byteStream();
                        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));

                        Gson gson = new GsonBuilder().create();
                        ErrorApiResponse mError = gson.fromJson(bufferedReader.readLine(), ErrorApiResponse.class);

                        if (mError.getStatus().equals(422)) {
                            MyApplication.displayKnownError(mError.getMessage());
                        } else {
                            MyApplication.displayKnownError(mError.getMessage());
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                        listener.onFailure(response.message());
                        MyApplication.displayKnownError(response.message());
                    }
                }
            }

            @Override
            public void onFailure(Call<GetTermsConditionsApiResponse> call, Throwable t) {
                CustomProgressDialog.getInstance().dismiss();
                t.printStackTrace();
                MyApplication.displayUnKnownError();
            }
        });

    }


    public void markAsDefaultAddress(final Context context, String addressKey, final CommonCallback.Listener listener) {
        if (!CustomProgressDialog.getInstance().isShowing()) {
            CustomProgressDialog.getInstance().show(context);
        }
        ApiInterface apiInterface = ApiConfiguration.getInstance().getApiBuilder().create(ApiInterface.class);
        Call<MarkAsDefaultApiResponse> call = apiInterface.markAsDefaultAddress(addressKey);
        call.enqueue(new Callback<MarkAsDefaultApiResponse>() {
            @Override
            public void onResponse(Call<MarkAsDefaultApiResponse> call, Response<MarkAsDefaultApiResponse> response) {
                CustomProgressDialog.getInstance().dismiss();
                if (response.isSuccessful()) {
                    if (response.body().getStatus() == 200) {
                        MyApplication.displayKnownErrorSuccess(context, response.body().getMessage());
                        listener.onSuccess(response.body());
                    } else {
                        MyApplication.displayKnownError(response.body().getMessage());
                        listener.onFailure(response.message());
                    }
                } else {
                    try {
                        InputStream inputStream = ((ResponseBody) response.errorBody()).byteStream();
                        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));

                        Gson gson = new GsonBuilder().create();
                        ErrorApiResponse mError = gson.fromJson(bufferedReader.readLine(), ErrorApiResponse.class);

                        if (mError.getStatus().equals(422)) {
                            MyApplication.displayKnownError(mError.getMessage());
                        } else {
                            MyApplication.displayKnownError(mError.getMessage());
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                        listener.onFailure(response.message());
                        MyApplication.displayKnownError(response.message());
                    }
                }
            }

            @Override
            public void onFailure(Call<MarkAsDefaultApiResponse> call, Throwable t) {
                CustomProgressDialog.getInstance().dismiss();
                t.printStackTrace();
                MyApplication.displayUnKnownError();
            }
        });

    }

    public void deleteAddress(final Context context, String address_key, final CommonCallback.Listener listener) {
        if (!CustomProgressDialog.getInstance().isShowing()) {
            CustomProgressDialog.getInstance().show(context);
        }
        ApiInterface apiInterface = ApiConfiguration.getInstance().getApiBuilder().create(ApiInterface.class);
        Call<DeleteAddressApiResponse> call = apiInterface.deleteAddress(address_key);
        call.enqueue(new Callback<DeleteAddressApiResponse>() {
            @Override
            public void onResponse(Call<DeleteAddressApiResponse> call, Response<DeleteAddressApiResponse> response) {
                CustomProgressDialog.getInstance().dismiss();
                if (response.isSuccessful()) {
                    if (response.body().getStatus() == 200) {
                        MyApplication.displayKnownErrorSuccess(context, response.body().getMessage());
                        listener.onSuccess(response.body());
                    } else {
                        MyApplication.displayKnownError(response.body().getMessage());
                        listener.onFailure(response.message());
                    }
                } else {
                    try {
                        InputStream inputStream = ((ResponseBody) response.errorBody()).byteStream();
                        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));

                        Gson gson = new GsonBuilder().create();
                        ErrorApiResponse mError = gson.fromJson(bufferedReader.readLine(), ErrorApiResponse.class);

                        if (mError.getStatus().equals(422)) {
                            MyApplication.displayKnownError(mError.getMessage());
                        } else {
                            MyApplication.displayKnownError(mError.getMessage());
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                        listener.onFailure(response.message());
                        MyApplication.displayKnownError(response.message());
                    }
                }
            }

            @Override
            public void onFailure(Call<DeleteAddressApiResponse> call, Throwable t) {
                CustomProgressDialog.getInstance().dismiss();
                t.printStackTrace();
                MyApplication.displayUnKnownError();
            }
        });
    }

    public void getContactDetails(final Context context,
                                  final CommonCallback.Listener listener) {
        if (!CustomProgressDialog.getInstance().isShowing()) {
            CustomProgressDialog.getInstance().show(context);
        }
        ApiInterface apiInterface = ApiConfiguration.getInstance().getApiBuilder().create(ApiInterface.class);
        Call<GetContactusApiResponse> call = apiInterface.getContactusDetail();
        call.enqueue(new Callback<GetContactusApiResponse>() {
            @Override
            public void onResponse(Call<GetContactusApiResponse> call, Response<GetContactusApiResponse> response) {
                CustomProgressDialog.getInstance().dismiss();
                if (response.isSuccessful()) {
                    if (response.body().getStatus() == 200) {
                        //MyApplication.displayKnownErrorSuccess(context, response.body().getMessage());
                        listener.onSuccess(response.body());
                    } else {
                        MyApplication.displayKnownError(response.body().getMessage());
                        listener.onFailure(response.message());
                    }
                } else {
                    try {
                        InputStream inputStream = ((ResponseBody) response.errorBody()).byteStream();
                        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));

                        Gson gson = new GsonBuilder().create();
                        ErrorApiResponse mError = gson.fromJson(bufferedReader.readLine(), ErrorApiResponse.class);

                        if (mError.getStatus().equals(422)) {
                            MyApplication.displayKnownError(mError.getMessage());
                        } else {
                            MyApplication.displayKnownError(mError.getMessage());
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                        listener.onFailure(response.message());
                        MyApplication.displayKnownError(response.message());
                    }
                }
            }

            @Override
            public void onFailure(Call<GetContactusApiResponse> call, Throwable t) {
                CustomProgressDialog.getInstance().dismiss();
                t.printStackTrace();
                MyApplication.displayUnKnownError();
            }
        });

    }

    public void addressList(final Context context,
                            final CommonCallback.Listener listener) {
        if (!CustomProgressDialog.getInstance().isShowing()) {
            CustomProgressDialog.getInstance().show(context);
        }
        ApiInterface apiInterface = ApiConfiguration.getInstance().getApiBuilder().create(ApiInterface.class);
        Call<AddressListApiResponse> call = apiInterface.addressList();
        call.enqueue(new Callback<AddressListApiResponse>() {
            @Override
            public void onResponse(Call<AddressListApiResponse> call, Response<AddressListApiResponse> response) {
                CustomProgressDialog.getInstance().dismiss();
                if (response.isSuccessful()) {
                    if (response.body().getStatus() == 200) {
                        //MyApplication.displayKnownErrorSuccess(context, response.body().getMessage());
                        listener.onSuccess(response.body());
                    } else {
                        MyApplication.displayKnownError(response.body().getMessage());
                        listener.onFailure(response.message());
                    }
                } else {
                    try {
                        InputStream inputStream = ((ResponseBody) response.errorBody()).byteStream();
                        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));

                        Gson gson = new GsonBuilder().create();
                        ErrorApiResponse mError = gson.fromJson(bufferedReader.readLine(), ErrorApiResponse.class);

                        if (mError.getStatus().equals(422)) {
                            MyApplication.displayKnownError(mError.getMessage());
                        } else {
                            MyApplication.displayKnownError(mError.getMessage());
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                        listener.onFailure(response.message());
                        MyApplication.displayKnownError(response.message());
                    }
                }
            }

            @Override
            public void onFailure(Call<AddressListApiResponse> call, Throwable t) {
                CustomProgressDialog.getInstance().dismiss();
                t.printStackTrace();
                MyApplication.displayUnKnownError();
            }
        });

    }

    public void getProfile(final Context context,
                           final CommonCallback.Listener listener) {
        if (!CustomProgressDialog.getInstance().isShowing()) {
            CustomProgressDialog.getInstance().show(context);
        }
        ApiInterface apiInterface = ApiConfiguration.getInstance().getApiBuilder().create(ApiInterface.class);
        Call<GetProfileApiResponse> call = apiInterface.getProfile();
        call.enqueue(new Callback<GetProfileApiResponse>() {
            @Override
            public void onResponse(Call<GetProfileApiResponse> call, Response<GetProfileApiResponse> response) {
                CustomProgressDialog.getInstance().dismiss();
                if (response.isSuccessful()) {
                    if (response.body().getStatus() == 200) {
                        //MyApplication.displayKnownErrorSuccess(context, response.body().getMessage());
                        listener.onSuccess(response.body());
                    } else {
                        MyApplication.displayKnownError(response.body().getMessage());
                        listener.onFailure(response.message());
                    }
                } else {
                    try {
                        InputStream inputStream = ((ResponseBody) response.errorBody()).byteStream();
                        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));

                        Gson gson = new GsonBuilder().create();
                        ErrorApiResponse mError = gson.fromJson(bufferedReader.readLine(), ErrorApiResponse.class);

                        if (mError.getStatus().equals(422)) {
                            MyApplication.displayKnownError(mError.getMessage());
                        } else {
                            MyApplication.displayKnownError(mError.getMessage());
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                        listener.onFailure(response.message());
                        MyApplication.displayKnownError(response.message());
                    }
                }
            }

            @Override
            public void onFailure(Call<GetProfileApiResponse> call, Throwable t) {
                CustomProgressDialog.getInstance().dismiss();
                t.printStackTrace();
                MyApplication.displayUnKnownError();
            }
        });

    }

    public void getReviewList(final Context context,
                              final CommonCallback.Listener listener) {
        if (!CustomProgressDialog.getInstance().isShowing()) {
            CustomProgressDialog.getInstance().show(context);
        }
        ApiInterface apiInterface = ApiConfiguration.getInstance().getApiBuilder().create(ApiInterface.class);
        Call<ReviewListApiResponse> call = apiInterface.getReview();
        call.enqueue(new Callback<ReviewListApiResponse>() {
            @Override
            public void onResponse(Call<ReviewListApiResponse> call, Response<ReviewListApiResponse> response) {
                CustomProgressDialog.getInstance().dismiss();
                if (response.isSuccessful()) {
                    if (response.body().getStatus() == 200) {
                        //MyApplication.displayKnownErrorSuccess(context, response.body().getMessage());
                        listener.onSuccess(response.body());
                    } else {
                        MyApplication.displayKnownError(response.body().getMessage());
                        listener.onFailure(response.message());
                    }
                } else {
                    try {
                        InputStream inputStream = ((ResponseBody) response.errorBody()).byteStream();
                        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));

                        Gson gson = new GsonBuilder().create();
                        ErrorApiResponse mError = gson.fromJson(bufferedReader.readLine(), ErrorApiResponse.class);

                        if (mError.getStatus().equals(422)) {
                            MyApplication.displayKnownError(mError.getMessage());
                        } else {
                            MyApplication.displayKnownError(mError.getMessage());
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                        listener.onFailure(response.message());
                        MyApplication.displayKnownError(response.message());
                    }
                }
            }

            @Override
            public void onFailure(Call<ReviewListApiResponse> call, Throwable t) {
                CustomProgressDialog.getInstance().dismiss();
                t.printStackTrace();
                MyApplication.displayUnKnownError();
            }
        });

    }

    public void addressType(final Context context,
                            final CommonCallback.Listener listener) {
        if (!CustomProgressDialog.getInstance().isShowing()) {
            CustomProgressDialog.getInstance().show(context);
        }
        ApiInterface apiInterface = ApiConfiguration.getInstance().getApiBuilder().create(ApiInterface.class);
        Call<AddressTypeApiResponse> call = apiInterface.addressType();
        call.enqueue(new Callback<AddressTypeApiResponse>() {
            @Override
            public void onResponse(Call<AddressTypeApiResponse> call, Response<AddressTypeApiResponse> response) {
                CustomProgressDialog.getInstance().dismiss();
                if (response.isSuccessful()) {
                    if (response.body().getStatus() == 200) {
                        //MyApplication.displayKnownErrorSuccess(context, response.body().getMessage());
                        listener.onSuccess(response.body());
                    } else {
                        MyApplication.displayKnownError(response.body().getMessage());
                        listener.onFailure(response.message());
                    }
                } else {
                    try {
                        InputStream inputStream = ((ResponseBody) response.errorBody()).byteStream();
                        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));

                        Gson gson = new GsonBuilder().create();
                        ErrorApiResponse mError = gson.fromJson(bufferedReader.readLine(), ErrorApiResponse.class);

                        if (mError.getStatus().equals(422)) {
                            MyApplication.displayKnownError(mError.getMessage());
                        } else {
                            MyApplication.displayKnownError(mError.getMessage());
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                        listener.onFailure(response.message());
                        MyApplication.displayKnownError(response.message());
                    }
                }
            }

            @Override
            public void onFailure(Call<AddressTypeApiResponse> call, Throwable t) {
                CustomProgressDialog.getInstance().dismiss();
                t.printStackTrace();
                MyApplication.displayUnKnownError();
            }
        });

    }

    public void addAddress(final Context context,
                           final String mAddressTypeId,
                           final String mCityId,
                           final String mAreaId,
                           final String mFlatNo,
                           final String mApartment,
                           final String mStreetName,
                           final String mCompany,
                           final String mLandMark,
                           final String mLatitude,
                           final String mLongitude,
                           final CommonCallback.Listener listener) {
        if (!CustomProgressDialog.getInstance().isShowing()) {
            CustomProgressDialog.getInstance().show(context);
        }
        ApiInterface apiInterface = ApiConfiguration.getInstance().getApiBuilder().create(ApiInterface.class);
        Call<AddAddressApiResponse> call = apiInterface.addAddress(mAddressTypeId, mCityId, mAreaId, mFlatNo, mApartment, mStreetName, mCompany, mLandMark, mLatitude, mLongitude);
        call.enqueue(new Callback<AddAddressApiResponse>() {
            @Override
            public void onResponse(Call<AddAddressApiResponse> call, Response<AddAddressApiResponse> response) {
                CustomProgressDialog.getInstance().dismiss();
                if (response.isSuccessful()) {
                    if (response.body().getStatus() == 200) {
                        MyApplication.displayKnownErrorSuccess(context, response.body().getMessage());
                        listener.onSuccess(response.body());
                    } else {
                        MyApplication.displayKnownError(response.body().getMessage());
                        listener.onFailure(response.message());
                    }
                } else {
                    try {
                        InputStream inputStream = ((ResponseBody) response.errorBody()).byteStream();
                        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));

                        Gson gson = new GsonBuilder().create();
                        ErrorApiResponse mError = gson.fromJson(bufferedReader.readLine(), ErrorApiResponse.class);

                        if (mError.getStatus().equals(422)) {
                            MyApplication.displayKnownError(mError.getMessage());
                        } else {
                            MyApplication.displayKnownError(mError.getMessage());
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                        listener.onFailure(response.message());
                        MyApplication.displayKnownError(response.message());
                    }
                }
            }

            @Override
            public void onFailure(Call<AddAddressApiResponse> call, Throwable t) {
                CustomProgressDialog.getInstance().dismiss();
                t.printStackTrace();
                MyApplication.displayUnKnownError();
            }
        });

    }

    public void cityList(final Context context,
                         final CommonCallback.Listener listener) {
        if (!CustomProgressDialog.getInstance().isShowing()) {
            CustomProgressDialog.getInstance().show(context);
        }
        ApiInterface apiInterface = ApiConfiguration.getInstance().getApiBuilder().create(ApiInterface.class);
        Call<CityListApiResponse> call = apiInterface.cityList();
        call.enqueue(new Callback<CityListApiResponse>() {
            @Override
            public void onResponse(Call<CityListApiResponse> call, Response<CityListApiResponse> response) {
                CustomProgressDialog.getInstance().dismiss();
                if (response.isSuccessful()) {
                    if (response.body().getStatus() == 200) {
                        //MyApplication.displayKnownErrorSuccess(context, response.body().getMessage());
                        listener.onSuccess(response.body());
                    } else {
                        MyApplication.displayKnownError(response.body().getMessage());
                        listener.onFailure(response.message());
                    }
                } else {
                    try {
                        InputStream inputStream = ((ResponseBody) response.errorBody()).byteStream();
                        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));

                        Gson gson = new GsonBuilder().create();
                        ErrorApiResponse mError = gson.fromJson(bufferedReader.readLine(), ErrorApiResponse.class);

                        if (mError.getStatus().equals(422)) {
                            MyApplication.displayKnownError(mError.getMessage());
                        } else {
                            MyApplication.displayKnownError(mError.getMessage());
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                        listener.onFailure(response.message());
                        MyApplication.displayKnownError(response.message());
                    }
                }
            }

            @Override
            public void onFailure(Call<CityListApiResponse> call, Throwable t) {
                CustomProgressDialog.getInstance().dismiss();
                t.printStackTrace();
                MyApplication.displayUnKnownError();
            }
        });

    }

    public void areaList(final Context context,
                         String cityId,
                         final CommonCallback.Listener listener) {
        if (!CustomProgressDialog.getInstance().isShowing()) {
            CustomProgressDialog.getInstance().show(context);
        }
        ApiInterface apiInterface = ApiConfiguration.getInstance().getApiBuilder().create(ApiInterface.class);
        Call<AreaListApiResponse> call = apiInterface.areaList(cityId);
        call.enqueue(new Callback<AreaListApiResponse>() {
            @Override
            public void onResponse(Call<AreaListApiResponse> call, Response<AreaListApiResponse> response) {
                CustomProgressDialog.getInstance().dismiss();
                if (response.isSuccessful()) {
                    if (response.body().getStatus() == 200) {
                        //MyApplication.displayKnownErrorSuccess(context, response.body().getMessage());
                        listener.onSuccess(response.body());
                    } else {
                        MyApplication.displayKnownError(response.body().getMessage());
                        listener.onFailure(response.message());
                    }
                } else {
                    try {
                        InputStream inputStream = ((ResponseBody) response.errorBody()).byteStream();
                        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));

                        Gson gson = new GsonBuilder().create();
                        ErrorApiResponse mError = gson.fromJson(bufferedReader.readLine(), ErrorApiResponse.class);

                        if (mError.getStatus().equals(422)) {
                            MyApplication.displayKnownError(mError.getMessage());
                        } else {
                            MyApplication.displayKnownError(mError.getMessage());
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                        listener.onFailure(response.message());
                        MyApplication.displayKnownError(response.message());
                    }
                }
            }

            @Override
            public void onFailure(Call<AreaListApiResponse> call, Throwable t) {
                CustomProgressDialog.getInstance().dismiss();
                t.printStackTrace();
                MyApplication.displayUnKnownError();
            }
        });

    }

    public void categoryList(final Context context,
                             final CommonCallback.Listener listener) {
        if (!CustomProgressDialog.getInstance().isShowing()) {
            CustomProgressDialog.getInstance().show(context);
        }
        ApiInterface apiInterface = ApiConfiguration.getInstance().getApiBuilder().create(ApiInterface.class);
        Call<CategoryApiResponse> call = apiInterface.categoryList();
        call.enqueue(new Callback<CategoryApiResponse>() {
            @Override
            public void onResponse(Call<CategoryApiResponse> call, Response<CategoryApiResponse> response) {
                CustomProgressDialog.getInstance().dismiss();
                if (response.isSuccessful()) {
                    if (response.body().getStatus() == 200) {
                        //MyApplication.displayKnownErrorSuccess(context, response.body().getMessage());
                        listener.onSuccess(response.body());
                    } else {
                        MyApplication.displayKnownError(response.body().getMessage());
                        listener.onFailure(response.message());
                    }
                } else {
                    try {
                        InputStream inputStream = ((ResponseBody) response.errorBody()).byteStream();
                        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));

                        Gson gson = new GsonBuilder().create();
                        ErrorApiResponse mError = gson.fromJson(bufferedReader.readLine(), ErrorApiResponse.class);

                        if (mError.getStatus().equals(422)) {
                            MyApplication.displayKnownError(mError.getMessage());
                        } else {
                            MyApplication.displayKnownError(mError.getMessage());
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                        listener.onFailure(response.message());
                        MyApplication.displayKnownError(response.message());
                    }
                }
            }

            @Override
            public void onFailure(Call<CategoryApiResponse> call, Throwable t) {
                CustomProgressDialog.getInstance().dismiss();
                t.printStackTrace();
                MyApplication.displayUnKnownError();
            }
        });

    }

    public void clothTypeList(final Context context,
                              final CommonCallback.Listener listener) {
        if (!CustomProgressDialog.getInstance().isShowing()) {
            CustomProgressDialog.getInstance().show(context);
        }
        ApiInterface apiInterface = ApiConfiguration.getInstance().getApiBuilder().create(ApiInterface.class);
        Call<ClothTypeApiResponse> call = apiInterface.clothTypeList(SessionManager.getInstance().getCategoryKey());
        call.enqueue(new Callback<ClothTypeApiResponse>() {
            @Override
            public void onResponse(Call<ClothTypeApiResponse> call, Response<ClothTypeApiResponse> response) {


                Log.d("ggggggggggggg", response.raw().toString());
                CustomProgressDialog.getInstance().dismiss();
                if (response.isSuccessful()) {
                    if (response.body().getStatus() == 200) {
                        //MyApplication.displayKnownErrorSuccess(context, response.body().getMessage());
                        listener.onSuccess(response.body());
                        Log.d("ggggggggggggg", response.body().getData().size() + " xxxx");
                        Log.d("ggggggggggggg", SessionManager.getInstance().getCategoryKey());

                    } else {
                        MyApplication.displayKnownError(response.body().getMessage());
                        listener.onFailure(response.message());
                    }
                } else {
                    try {
                        InputStream inputStream = ((ResponseBody) response.errorBody()).byteStream();
                        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));

                        Gson gson = new GsonBuilder().create();
                        ErrorApiResponse mError = gson.fromJson(bufferedReader.readLine(), ErrorApiResponse.class);

                        if (mError.getStatus().equals(422)) {
                            MyApplication.displayKnownError(mError.getMessage());
                        } else {
                            MyApplication.displayKnownError(mError.getMessage());
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                        listener.onFailure(response.message());
                        MyApplication.displayKnownError(response.message());
                    }
                }
            }

            @Override
            public void onFailure(Call<ClothTypeApiResponse> call, Throwable t) {
                CustomProgressDialog.getInstance().dismiss();
                t.printStackTrace();
                MyApplication.displayUnKnownError();
            }
        });

    }

    public void clothTypeThreeList(final Context context,
                                   final CommonCallback.Listener listener) {
        if (!CustomProgressDialog.getInstance().isShowing()) {
            CustomProgressDialog.getInstance().show(context);
        }
        ApiInterface apiInterface = ApiConfiguration.getInstance().getApiBuilder().create(ApiInterface.class);
        Call<ClothTypeListViewApiResponse> call = apiInterface.clothTypeListView(SessionManager.getInstance().getClothTypeKey());
        call.enqueue(new Callback<ClothTypeListViewApiResponse>() {
            @Override
            public void onResponse(Call<ClothTypeListViewApiResponse> call, Response<ClothTypeListViewApiResponse> response) {
                CustomProgressDialog.getInstance().dismiss();
                if (response.isSuccessful()) {
                    if (response.body().getStatus() == 200) {
                        //MyApplication.displayKnownErrorSuccess(context, response.body().getMessage());
                        listener.onSuccess(response.body());
                    } else {
                        MyApplication.displayKnownError(response.body().getMessage());
                        listener.onFailure(response.message());
                    }
                } else {
                    try {
                        InputStream inputStream = ((ResponseBody) response.errorBody()).byteStream();
                        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));

                        Gson gson = new GsonBuilder().create();
                        ErrorApiResponse mError = gson.fromJson(bufferedReader.readLine(), ErrorApiResponse.class);

                        if (mError.getStatus().equals(422)) {
                            MyApplication.displayKnownError(mError.getMessage());
                        } else {
                            MyApplication.displayKnownError(mError.getMessage());
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                        listener.onFailure(response.message());
                        MyApplication.displayKnownError(response.message());
                    }
                }
            }

            @Override
            public void onFailure(Call<ClothTypeListViewApiResponse> call, Throwable t) {
                CustomProgressDialog.getInstance().dismiss();
                t.printStackTrace();
                MyApplication.displayUnKnownError();
            }
        });

    }

    public void placeOrder(final Context context, final String body, final CommonCallback.Listener listener) {

        if (!CustomProgressDialog.getInstance().isShowing()) {
            CustomProgressDialog.getInstance().show(context);
        }

        RequestBody body_ = RequestBody.create(MediaType.parse("text/plain"), body);

        ApiInterface apiInterface = ApiConfiguration.getInstance().getApiBuilder().create(ApiInterface.class);
        Call<PlaceOrderApiResponse> call = apiInterface.placeOrder(body_);
        call.enqueue(new Callback<PlaceOrderApiResponse>() {
            @Override
            public void onResponse(Call<PlaceOrderApiResponse> call, Response<PlaceOrderApiResponse> response) {
                CustomProgressDialog.getInstance().dismiss();
                if (response.isSuccessful()) {
                    if (response.body().getStatus() == 200) {
                        //MyApplication.displayKnownErrorSuccess(context, response.body().getMessage());
                        listener.onSuccess(response.body());
                    } else {
                        MyApplication.displayKnownError(response.body().getMessage());
                        listener.onFailure(response.message());
                    }
                } else {
                    try {
                        InputStream inputStream = ((ResponseBody) response.errorBody()).byteStream();
                        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));

                        Gson gson = new GsonBuilder().create();
                        ErrorApiResponse mError = gson.fromJson(bufferedReader.readLine(), ErrorApiResponse.class);

                        if (mError.getStatus().equals(422)) {
                            MyApplication.displayKnownError(mError.getMessage());
                        } else {
                            MyApplication.displayKnownError(mError.getMessage());
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                        listener.onFailure(response.message());
                        MyApplication.displayKnownError(response.message());
                    }
                }
            }

            @Override
            public void onFailure(Call<PlaceOrderApiResponse> call, Throwable t) {
                CustomProgressDialog.getInstance().dismiss();
                t.printStackTrace();
                MyApplication.displayUnKnownError();
            }
        });

    }

    public void calculateBasket(final Context context, final String body, final CommonCallback.Listener listener) {

        if (!CustomProgressDialog.getInstance().isShowing()) {
            CustomProgressDialog.getInstance().show(context);
        }

        RequestBody body_ = RequestBody.create(MediaType.parse("text/plain"), body);

        ApiInterface apiInterface = ApiConfiguration.getInstance().getApiBuilder().create(ApiInterface.class);
        Call<CalculateApiResponse> call = apiInterface.calculateBasket(body_);
        call.enqueue(new Callback<CalculateApiResponse>() {
            @Override
            public void onResponse(Call<CalculateApiResponse> call, Response<CalculateApiResponse> response) {
                CustomProgressDialog.getInstance().dismiss();
                if (response.isSuccessful()) {
                    if (response.body().getStatus() == 200) {
                        //MyApplication.displayKnownErrorSuccess(context, response.body().getMessage());
                        listener.onSuccess(response.body());
                    } else {
                        MyApplication.displayKnownError(response.body().getMessage());
                        listener.onFailure(response.message());
                    }
                } else {
                    try {
                        InputStream inputStream = ((ResponseBody) response.errorBody()).byteStream();
                        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));

                        Gson gson = new GsonBuilder().create();
                        ErrorApiResponse mError = gson.fromJson(bufferedReader.readLine(), ErrorApiResponse.class);

                        if (mError.getStatus().equals(422)) {
                            MyApplication.displayKnownError(mError.getMessage());
                        } else {
                            MyApplication.displayKnownError(mError.getMessage());
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                        listener.onFailure(response.message());
                        MyApplication.displayKnownError(response.message());
                    }
                }
            }

            @Override
            public void onFailure(Call<CalculateApiResponse> call, Throwable t) {
                CustomProgressDialog.getInstance().dismiss();
                t.printStackTrace();
                MyApplication.displayUnKnownError();
            }
        });

    }


    public void orderList(final Context context,
                          final CommonCallback.Listener listener) {
        if (!CustomProgressDialog.getInstance().isShowing()) {
            CustomProgressDialog.getInstance().show(context);
        }
        ApiInterface apiInterface = ApiConfiguration.getInstance().getApiBuilder().create(ApiInterface.class);
        Call<OrderListApiResponse> call = apiInterface.orderList();
        call.enqueue(new Callback<OrderListApiResponse>() {
            @Override
            public void onResponse(Call<OrderListApiResponse> call, Response<OrderListApiResponse> response) {
                CustomProgressDialog.getInstance().dismiss();
                if (response.isSuccessful()) {
                    if (response.body().getStatus() == 200) {
                        //MyApplication.displayKnownErrorSuccess(context, response.body().getMessage());
                        listener.onSuccess(response.body());
                    } else {
                        MyApplication.displayKnownError(response.body().getMessage());
                        listener.onFailure(response.message());
                    }
                } else {
                    try {
                        InputStream inputStream = ((ResponseBody) response.errorBody()).byteStream();
                        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));

                        Gson gson = new GsonBuilder().create();
                        ErrorApiResponse mError = gson.fromJson(bufferedReader.readLine(), ErrorApiResponse.class);

                        if (mError.getStatus().equals(422)) {
                            MyApplication.displayKnownError(mError.getMessage());
                        } else {
                            MyApplication.displayKnownError(mError.getMessage());
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                        listener.onFailure(response.message());
                        MyApplication.displayKnownError(response.message());
                    }
                }
            }

            @Override
            public void onFailure(Call<OrderListApiResponse> call, Throwable t) {
                CustomProgressDialog.getInstance().dismiss();
                t.printStackTrace();
                MyApplication.displayUnKnownError();
            }
        });

    }

    public void changeDeliveryAddress(final Context context, String orderKey, String useraddressKey, final CommonCallback.Listener listener) {
        if (!CustomProgressDialog.getInstance().isShowing()) {
            CustomProgressDialog.getInstance().show(context);
        }
        ApiInterface apiInterface = ApiConfiguration.getInstance().getApiBuilder().create(ApiInterface.class);
        Call<ChangeAddressApiResponse> call = apiInterface.changedelvieryAddressApi(orderKey, useraddressKey);
        call.enqueue(new Callback<ChangeAddressApiResponse>() {
            @Override
            public void onResponse(Call<ChangeAddressApiResponse> call, Response<ChangeAddressApiResponse> response) {
                CustomProgressDialog.getInstance().dismiss();
                if (response.isSuccessful()) {
                    if (response.body().getStatus() == 200) {
                        listener.onSuccess(response.body());
                    } else {
                        MyApplication.displayKnownError(response.body().getMessage());
                        listener.onFailure(response.message());
                    }
                } else {
                    try {
                        InputStream inputStream = ((ResponseBody) response.errorBody()).byteStream();
                        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));

                        Gson gson = new GsonBuilder().create();
                        ErrorApiResponse mError = gson.fromJson(bufferedReader.readLine(), ErrorApiResponse.class);

                        if (mError.getStatus().equals(422)) {
                            MyApplication.displayKnownError(mError.getMessage());
                        } else {
                            MyApplication.displayKnownError(mError.getMessage());
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                        listener.onFailure(response.message());
                        MyApplication.displayKnownError(response.message());
                    }
                }
            }

            @Override
            public void onFailure(Call<ChangeAddressApiResponse> call, Throwable t) {
                CustomProgressDialog.getInstance().dismiss();
                t.printStackTrace();
                MyApplication.displayUnKnownError();
            }
        });

    }

    public void promationAdds(final Context context, final CommonCallback.Listener listener) {
        if (!CustomProgressDialog.getInstance().isShowing()) {
            CustomProgressDialog.getInstance().show(context);
        }
        ApiInterface apiInterface = ApiConfiguration.getInstance().getApiBuilder().create(ApiInterface.class);
        Call<PromationAddApiResponse> call = apiInterface.promationApi();
        call.enqueue(new Callback<PromationAddApiResponse>() {
            @Override
            public void onResponse(Call<PromationAddApiResponse> call, Response<PromationAddApiResponse> response) {
                CustomProgressDialog.getInstance().dismiss();
                if (response.isSuccessful()) {
                    if (response.body().getStatus() == 200) {
                        //MyApplication.displayKnownErrorSuccess(context, response.body().getMessage());
                        listener.onSuccess(response.body());
                    } else {
                        MyApplication.displayKnownError(response.body().getMessage());
                        listener.onFailure(response.message());
                    }
                } else {
                    try {
                        InputStream inputStream = ((ResponseBody) response.errorBody()).byteStream();
                        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));

                        Gson gson = new GsonBuilder().create();
                        ErrorApiResponse mError = gson.fromJson(bufferedReader.readLine(), ErrorApiResponse.class);

                        if (mError.getStatus().equals(422)) {
                            MyApplication.displayKnownError(mError.getMessage());
                        } else {
                            MyApplication.displayKnownError(mError.getMessage());
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                        listener.onFailure(response.message());
                        MyApplication.displayKnownError(response.message());
                    }
                }
            }

            @Override
            public void onFailure(Call<PromationAddApiResponse> call, Throwable t) {
                CustomProgressDialog.getInstance().dismiss();
                t.printStackTrace();
                MyApplication.displayUnKnownError();
            }
        });

    }

    public void AddRatingApi(final Context context,
                             String orderKey,
                             final String ratingtxt,
                             final String desc,
                             final CommonCallback.Listener listener) {
        if (!CustomProgressDialog.getInstance().isShowing()) {
            CustomProgressDialog.getInstance().show(context);
        }
        ApiInterface apiInterface = ApiConfiguration.getInstance().getApiBuilder().create(ApiInterface.class);
        Call<AddRatingApiResponse> call = apiInterface.addRating(orderKey, ratingtxt, desc);
        call.enqueue(new Callback<AddRatingApiResponse>() {
            @Override
            public void onResponse(Call<AddRatingApiResponse> call, Response<AddRatingApiResponse> response) {
                CustomProgressDialog.getInstance().dismiss();
                if (response.isSuccessful()) {
                    if (response.body().getStatus() == 200) {
                        //MyApplication.displayKnownErrorSuccess(context, response.body().getMessage());
                        listener.onSuccess(response.body());
                    } else {
                        MyApplication.displayKnownError(response.body().getMessage());
                        listener.onFailure(response.message());
                    }
                } else {
                    try {
                        InputStream inputStream = ((ResponseBody) response.errorBody()).byteStream();
                        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));

                        Gson gson = new GsonBuilder().create();
                        ErrorApiResponse mError = gson.fromJson(bufferedReader.readLine(), ErrorApiResponse.class);

                        if (mError.getStatus().equals(422)) {
                            MyApplication.displayKnownError(mError.getMessage());
                        } else {
                            MyApplication.displayKnownError(mError.getMessage());
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                        listener.onFailure(response.message());
                        MyApplication.displayKnownError(response.message());
                    }
                }
            }

            @Override
            public void onFailure(Call<AddRatingApiResponse> call, Throwable t) {
                CustomProgressDialog.getInstance().dismiss();
                t.printStackTrace();
                MyApplication.displayUnKnownError();
            }
        });

    }

    public void changeOrderDeliveryAddress(final Context context,
                                           String orderKey,
                                           final CommonCallback.Listener listener) {
        if (!CustomProgressDialog.getInstance().isShowing()) {
            CustomProgressDialog.getInstance().show(context);
        }
        ApiInterface apiInterface = ApiConfiguration.getInstance().getApiBuilder().create(ApiInterface.class);
        Call<ChangeDeliveryAddressApiResponse> call = apiInterface.changeOrderdelvieryAddressApi(orderKey);
        call.enqueue(new Callback<ChangeDeliveryAddressApiResponse>() {
            @Override
            public void onResponse(Call<ChangeDeliveryAddressApiResponse> call, Response<ChangeDeliveryAddressApiResponse> response) {
                CustomProgressDialog.getInstance().dismiss();
                if (response.isSuccessful()) {
                    if (response.body().getStatus() == 200) {
                        //MyApplication.displayKnownErrorSuccess(context, response.body().getMessage());
                        listener.onSuccess(response.body());
                    } else {
                        MyApplication.displayKnownError(response.body().getMessage());
                        listener.onFailure(response.message());
                    }
                } else {
                    try {
                        InputStream inputStream = ((ResponseBody) response.errorBody()).byteStream();
                        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));

                        Gson gson = new GsonBuilder().create();
                        ErrorApiResponse mError = gson.fromJson(bufferedReader.readLine(), ErrorApiResponse.class);

                        if (mError.getStatus().equals(422)) {
                            MyApplication.displayKnownError(mError.getMessage());
                        } else {
                            MyApplication.displayKnownError(mError.getMessage());
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                        listener.onFailure(response.message());
                        MyApplication.displayKnownError(response.message());
                    }
                }
            }

            @Override
            public void onFailure(Call<ChangeDeliveryAddressApiResponse> call, Throwable t) {
                CustomProgressDialog.getInstance().dismiss();
                t.printStackTrace();
                MyApplication.displayUnKnownError();
            }
        });

    }

    public void orderDetails(final Context context,
                             String orderKey,
                             final CommonCallback.Listener listener) {
        if (!CustomProgressDialog.getInstance().isShowing()) {
            CustomProgressDialog.getInstance().show(context);
        }
        ApiInterface apiInterface = ApiConfiguration.getInstance().getApiBuilder().create(ApiInterface.class);
        Call<OrderDetailsApiResponse> call = apiInterface.orderDetails(orderKey);
        call.enqueue(new Callback<OrderDetailsApiResponse>() {
            @Override
            public void onResponse(Call<OrderDetailsApiResponse> call, Response<OrderDetailsApiResponse> response) {
                CustomProgressDialog.getInstance().dismiss();
                if (response.isSuccessful()) {
                    if (response.body().getStatus() == 200) {
                        //MyApplication.displayKnownErrorSuccess(context, response.body().getMessage());
                        listener.onSuccess(response.body());
                    } else {
                        MyApplication.displayKnownError(response.body().getMessage());
                        listener.onFailure(response.message());
                    }
                } else {
                    try {
                        InputStream inputStream = ((ResponseBody) response.errorBody()).byteStream();
                        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));

                        Gson gson = new GsonBuilder().create();
                        ErrorApiResponse mError = gson.fromJson(bufferedReader.readLine(), ErrorApiResponse.class);

                        if (mError.getStatus().equals(422)) {
                            MyApplication.displayKnownError(mError.getMessage());
                        } else {
                            MyApplication.displayKnownError(mError.getMessage());
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                        listener.onFailure(response.message());
                        MyApplication.displayKnownError(response.message());
                    }
                }
            }

            @Override
            public void onFailure(Call<OrderDetailsApiResponse> call, Throwable t) {
                CustomProgressDialog.getInstance().dismiss();
                t.printStackTrace();
                MyApplication.displayUnKnownError();
            }
        });

    }

    public void updateAddress(final Context context, String mAddressKey, String user_address_key, String city_key, String area_key, String flatno,
                              String apartment, String streetname, String company, String landmark, String latitude,
                              String longitude, final CommonCallback.Listener listener) {

        if (!CustomProgressDialog.getInstance().isShowing()) {
            CustomProgressDialog.getInstance().show(context);
        }
        ApiInterface apiInterface = ApiConfiguration.getInstance().getApiBuilder().create(ApiInterface.class);
        Call<EditAddressApiResponse> call = apiInterface.updateAddress(mAddressKey, user_address_key, city_key, area_key, flatno, apartment, streetname,
            company, landmark, latitude, longitude);
        call.enqueue(new Callback<EditAddressApiResponse>() {
            @Override
            public void onResponse(Call<EditAddressApiResponse> call, Response<EditAddressApiResponse> response) {
                CustomProgressDialog.getInstance().dismiss();
                if (response.isSuccessful()) {
                    if (response.body().getStatus() == 200) {
                        //MyApplication.displayKnownErrorSuccess(context, response.body().getMessage());
                        listener.onSuccess(response.body());
                    } else {
                        MyApplication.displayKnownError(response.body().getMessage());
                        listener.onFailure(response.message());
                    }
                } else {
                    try {
                        InputStream inputStream = ((ResponseBody) response.errorBody()).byteStream();
                        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));

                        Gson gson = new GsonBuilder().create();
                        ErrorApiResponse mError = gson.fromJson(bufferedReader.readLine(), ErrorApiResponse.class);

                        if (mError.getStatus().equals(422)) {
                            MyApplication.displayKnownError(mError.getMessage());
                        } else {
                            MyApplication.displayKnownError(mError.getMessage());
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                        listener.onFailure(response.message());
                        MyApplication.displayKnownError(response.message());
                    }
                }
            }

            @Override
            public void onFailure(Call<EditAddressApiResponse> call, Throwable t) {
                CustomProgressDialog.getInstance().dismiss();
                t.printStackTrace();
                MyApplication.displayUnKnownError();
            }
        });


    }

    public void helpList(final Context context, final CommonCallback.Listener listener) {

        if (!CustomProgressDialog.getInstance().isShowing()) {
            CustomProgressDialog.getInstance().show(context);
        }
        ApiInterface apiInterface = ApiConfiguration.getInstance().getApiBuilder().create(ApiInterface.class);
        Call<HelpApiResponse> call = apiInterface.helpApi();
        call.enqueue(new Callback<HelpApiResponse>() {
            @Override
            public void onResponse(Call<HelpApiResponse> call, Response<HelpApiResponse> response) {
                CustomProgressDialog.getInstance().dismiss();
                if (response.isSuccessful()) {
                    if (response.body().getStatus() == 200) {
                        listener.onSuccess(response.body());
                    } else {
                        MyApplication.displayKnownError(response.body().getMessage());
                        listener.onFailure(response.message());
                    }
                } else {
                    try {
                        InputStream inputStream = ((ResponseBody) response.errorBody()).byteStream();
                        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));

                        Gson gson = new GsonBuilder().create();
                        ErrorApiResponse mError = gson.fromJson(bufferedReader.readLine(), ErrorApiResponse.class);

                        if (mError.getStatus().equals(422)) {
                            MyApplication.displayKnownError(mError.getMessage());
                        } else {
                            MyApplication.displayKnownError(mError.getMessage());
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                        listener.onFailure(response.message());
                        MyApplication.displayKnownError(response.message());
                    }
                }
            }

            @Override
            public void onFailure(Call<HelpApiResponse> call, Throwable t) {
                CustomProgressDialog.getInstance().dismiss();
                t.printStackTrace();
                MyApplication.displayUnKnownError();
            }
        });


    }

    public void updateProfileApi(final Context context, final String firstname,
                                 final String email,
                                 final String phonenuber, final CommonCallback.Listener listener) {

        if (!CustomProgressDialog.getInstance().isShowing()) {
            CustomProgressDialog.getInstance().show(context);
        }
        ApiInterface apiInterface = ApiConfiguration.getInstance().getApiBuilder().create(ApiInterface.class);
        Call<UpdateProfileApiResponse> call = apiInterface.updateProfile(firstname, email, phonenuber);
        call.enqueue(new Callback<UpdateProfileApiResponse>() {
            @Override
            public void onResponse(Call<UpdateProfileApiResponse> call, Response<UpdateProfileApiResponse> response) {
                CustomProgressDialog.getInstance().dismiss();
                if (response.isSuccessful()) {
                    if (response.body().getStatus() == 200) {
                        listener.onSuccess(response.body());
                    } else {
                        MyApplication.displayKnownError(response.body().getMessage());
                        listener.onFailure(response.message());
                    }
                } else {
                    try {
                        InputStream inputStream = ((ResponseBody) response.errorBody()).byteStream();
                        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));

                        Gson gson = new GsonBuilder().create();
                        ErrorApiResponse mError = gson.fromJson(bufferedReader.readLine(), ErrorApiResponse.class);

                        if (mError.getStatus().equals(422)) {
                            MyApplication.displayKnownError(mError.getMessage());
                        } else {
                            MyApplication.displayKnownError(mError.getMessage());
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                        listener.onFailure(response.message());
                        MyApplication.displayKnownError(response.message());
                    }
                }
            }

            @Override
            public void onFailure(Call<UpdateProfileApiResponse> call, Throwable t) {
                CustomProgressDialog.getInstance().dismiss();
                t.printStackTrace();
                MyApplication.displayUnKnownError();
            }
        });


    }

    public void changePassword(final Context context, final String mOldPassword,
                               final String mNewPassword,
                               final String mConfirmPassword, final CommonCallback.Listener listener) {

        if (!CustomProgressDialog.getInstance().isShowing()) {
            CustomProgressDialog.getInstance().show(context);
        }
        ApiInterface apiInterface = ApiConfiguration.getInstance().getApiBuilder().create(ApiInterface.class);
        Call<ChangePasswordApiResponse> call = apiInterface.changePassword(mOldPassword, mNewPassword, mConfirmPassword);
        call.enqueue(new Callback<ChangePasswordApiResponse>() {
            @Override
            public void onResponse(Call<ChangePasswordApiResponse> call, Response<ChangePasswordApiResponse> response) {
                CustomProgressDialog.getInstance().dismiss();
                if (response.isSuccessful()) {
                    if (response.body().getStatus() == 200) {
                        listener.onSuccess(response.body());
                    } else {
                        MyApplication.displayKnownError(response.body().getMessage());
                        listener.onFailure(response.message());
                    }
                } else {
                    try {
                        InputStream inputStream = ((ResponseBody) response.errorBody()).byteStream();
                        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));

                        Gson gson = new GsonBuilder().create();
                        ErrorApiResponse mError = gson.fromJson(bufferedReader.readLine(), ErrorApiResponse.class);

                        if (mError.getStatus().equals(422)) {
                            MyApplication.displayKnownError(mError.getMessage());
                        } else {
                            MyApplication.displayKnownError(mError.getMessage());
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                        listener.onFailure(response.message());
                        MyApplication.displayKnownError(response.message());
                    }
                }
            }

            @Override
            public void onFailure(Call<ChangePasswordApiResponse> call, Throwable t) {
                CustomProgressDialog.getInstance().dismiss();
                t.printStackTrace();
                MyApplication.displayUnKnownError();
            }
        });


    }

    public void contactUssubmit(final Context context, final String username,
                                final String email,
                                final String mobile_number, final String message, final CommonCallback.Listener listener) {

        if (!CustomProgressDialog.getInstance().isShowing()) {
            CustomProgressDialog.getInstance().show(context);
        }
        ApiInterface apiInterface = ApiConfiguration.getInstance().getApiBuilder().create(ApiInterface.class);
        Call<ContactusSubmitApiResponse> call = apiInterface.contactus(username, email, mobile_number, message);
        call.enqueue(new Callback<ContactusSubmitApiResponse>() {
            @Override
            public void onResponse(Call<ContactusSubmitApiResponse> call, Response<ContactusSubmitApiResponse> response) {
                CustomProgressDialog.getInstance().dismiss();
                if (response.isSuccessful()) {
                    if (response.body().getStatus() == 200) {
                        listener.onSuccess(response.body());
                    } else {
                        MyApplication.displayKnownError(response.body().getMessage());
                        listener.onFailure(response.message());
                    }
                } else {
                    try {
                        InputStream inputStream = ((ResponseBody) response.errorBody()).byteStream();
                        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));

                        Gson gson = new GsonBuilder().create();
                        ErrorApiResponse mError = gson.fromJson(bufferedReader.readLine(), ErrorApiResponse.class);

                        if (mError.getStatus().equals(422)) {
                            MyApplication.displayKnownError(mError.getMessage());
                        } else {
                            MyApplication.displayKnownError(mError.getMessage());
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                        listener.onFailure(response.message());
                        MyApplication.displayKnownError(response.message());
                    }
                }
            }

            @Override
            public void onFailure(Call<ContactusSubmitApiResponse> call, Throwable t) {
                CustomProgressDialog.getInstance().dismiss();
                t.printStackTrace();
                MyApplication.displayUnKnownError();
            }
        });


    }

    public void updateProfileImage(final Context context, File profilepic, final CommonCallback.Listener listener) {

        if (!CustomProgressDialog.getInstance().isShowing()) {
            CustomProgressDialog.getInstance().show(context);
        }
        MultipartBody.Part body = null;
        if (profilepic != null) {
            RequestBody fbody = RequestBody.create(MediaType.parse("image/jpg"), profilepic);
            body = MultipartBody.Part.createFormData("profile_image", profilepic.getName(), fbody);
        }
        ApiInterface apiInterface = ApiConfiguration.getInstance().getApiBuilder().create(ApiInterface.class);
        Call<UpdateProfileImageApiResponse> call = apiInterface.updateProfileImage(body);
        call.enqueue(new Callback<UpdateProfileImageApiResponse>() {
            @Override
            public void onResponse(Call<UpdateProfileImageApiResponse> call, Response<UpdateProfileImageApiResponse> response) {
                CustomProgressDialog.getInstance().dismiss();
                if (response.isSuccessful()) {
                    if (response.body().getStatus() == 200) {
                        listener.onSuccess(response.body());
                    } else {
                        MyApplication.displayKnownError(response.body().getMessage());
                        listener.onFailure(response.message());
                    }
                } else {
                    try {
                        InputStream inputStream = ((ResponseBody) response.errorBody()).byteStream();
                        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));

                        Gson gson = new GsonBuilder().create();
                        ErrorApiResponse mError = gson.fromJson(bufferedReader.readLine(), ErrorApiResponse.class);

                        if (mError.getStatus().equals(422)) {
                            MyApplication.displayKnownError(mError.getMessage());
                        } else {
                            MyApplication.displayKnownError(mError.getMessage());
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                        listener.onFailure(response.message());
                        MyApplication.displayKnownError(response.message());
                    }
                }
            }

            @Override
            public void onFailure(Call<UpdateProfileImageApiResponse> call, Throwable t) {
                CustomProgressDialog.getInstance().dismiss();
                t.printStackTrace();
                MyApplication.displayUnKnownError();
            }
        });

    }


    // ---- GET Call Notification list
    public void notification(final Context context, final CommonCallback.Listener listener) {
        if (!CustomProgressDialog.getInstance().isShowing()) {
            CustomProgressDialog.getInstance().show(context);
        }
        ApiInterface apiInterface = ApiConfiguration.getInstance().getApiBuilder().create(ApiInterface.class);
        Call<NotificationListApiResponse> call = apiInterface.notification_list();
        call.enqueue(new Callback<NotificationListApiResponse>() {
            @Override
            public void onResponse(Call<NotificationListApiResponse> call, Response<NotificationListApiResponse> response) {
                CustomProgressDialog.getInstance().dismiss();
                if (response.isSuccessful()) {
                    if (response.body().getStatus() == 200) {
                        //MyApplication.displayKnownErrorSuccess(context, response.body().getMessage());
                        listener.onSuccess(response.body());
                    } else {
                        MyApplication.displayKnownError(response.body().getMessage());
                        listener.onFailure(response.message());
                    }
                } else {
                    try {
                        InputStream inputStream = ((ResponseBody) response.errorBody()).byteStream();
                        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));

                        Gson gson = new GsonBuilder().create();
                        ErrorApiResponse mError = gson.fromJson(bufferedReader.readLine(), ErrorApiResponse.class);

                        if (mError.getStatus().equals(422)) {
                            MyApplication.displayKnownError(mError.getMessage());
                        } else {
                            MyApplication.displayKnownError(mError.getMessage());
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                        listener.onFailure(response.message());
                        MyApplication.displayKnownError(response.message());
                    }
                }
            }

            @Override
            public void onFailure(Call<NotificationListApiResponse> call, Throwable t) {
                CustomProgressDialog.getInstance().dismiss();
                t.printStackTrace();
                MyApplication.displayUnKnownError();
            }
        });

    }

    // ---- POST Call Read Notification
    public void readedNotification(final Context context, String notification_key, final CommonCallback.Listener listener) {
        if (!CustomProgressDialog.getInstance().isShowing()) {
            CustomProgressDialog.getInstance().show(context);
        }
        ApiInterface apiInterface = ApiConfiguration.getInstance().getApiBuilder().create(ApiInterface.class);
        Call<ReadNotificationApiResponse> call = apiInterface.read_notification(notification_key);
        call.enqueue(new Callback<ReadNotificationApiResponse>() {
            @Override
            public void onResponse(Call<ReadNotificationApiResponse> call, Response<ReadNotificationApiResponse> response) {
                CustomProgressDialog.getInstance().dismiss();
                if (response.isSuccessful()) {
                    if (response.body().getStatus() == 200) {
                        //MyApplication.displayKnownErrorSuccess(context, response.body().getMessage());
                        listener.onSuccess(response.body());
                    } else {
                        MyApplication.displayKnownError(response.body().getMessage());
                        listener.onFailure(response.message());
                    }
                } else {
                    try {
                        InputStream inputStream = ((ResponseBody) response.errorBody()).byteStream();
                        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));

                        Gson gson = new GsonBuilder().create();
                        ErrorApiResponse mError = gson.fromJson(bufferedReader.readLine(), ErrorApiResponse.class);

                        if (mError.getStatus().equals(422)) {
                            MyApplication.displayKnownError(mError.getMessage());
                        } else {
                            MyApplication.displayKnownError(mError.getMessage());
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                        listener.onFailure(response.message());
                        MyApplication.displayKnownError(response.message());
                    }
                }
            }

            @Override
            public void onFailure(Call<ReadNotificationApiResponse> call, Throwable t) {
                CustomProgressDialog.getInstance().dismiss();
                t.printStackTrace();
                MyApplication.displayUnKnownError();
            }
        });

    }

    // ---- POST Call Read Notification
    public void socialLogin(final Context context, String email,
                            String id, String firstName, String deviceType, String deviceToken, String type, final CommonCallback.Listener listener) {
        if (!CustomProgressDialog.getInstance().isShowing()) {
            CustomProgressDialog.getInstance().show(context);
        }
        ApiInterface apiInterface = ApiConfiguration.getInstance().getApiBuilder().create(ApiInterface.class);
        Call<SocialLoginApiResponse> call = apiInterface.social_login(id, type, email, firstName, deviceToken, deviceType);
        call.enqueue(new Callback<SocialLoginApiResponse>() {
            @Override
            public void onResponse(Call<SocialLoginApiResponse> call, Response<SocialLoginApiResponse> response) {
                CustomProgressDialog.getInstance().dismiss();
                if (response.isSuccessful()) {
                    if (response.body().getStatus() == 200) {
                        //MyApplication.displayKnownErrorSuccess(context, response.body().getMessage());
                        listener.onSuccess(response.body());
                    } else {
                        MyApplication.displayKnownError(response.body().getMessage());
                        listener.onFailure(response.message());
                    }
                } else {
                    try {
                        InputStream inputStream = ((ResponseBody) response.errorBody()).byteStream();
                        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));

                        Gson gson = new GsonBuilder().create();
                        ErrorApiResponse mError = gson.fromJson(bufferedReader.readLine(), ErrorApiResponse.class);

                        if (mError.getStatus().equals(422)) {
                            MyApplication.displayKnownError(mError.getMessage());
                        } else {
                            MyApplication.displayKnownError(mError.getMessage());
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                        listener.onFailure(response.message());
                        MyApplication.displayKnownError(response.message());
                    }
                }
            }

            @Override
            public void onFailure(Call<SocialLoginApiResponse> call, Throwable t) {
                CustomProgressDialog.getInstance().dismiss();
                t.printStackTrace();
                MyApplication.displayUnKnownError();
            }
        });

    }

    public void verifiedOTP(final Context context, final String user_key,
                            final CommonCallback.Listener listener) {
        if (!CustomProgressDialog.getInstance().isShowing()) {
            CustomProgressDialog.getInstance().show(context);
        }
        ApiInterface apiInterface = ApiConfiguration.getInstance().getApiBuilder().create(ApiInterface.class);
        Call<UpdateVerificationStatusApiResponse> call = apiInterface.verified_otp(user_key);
        call.enqueue(new Callback<UpdateVerificationStatusApiResponse>() {
            @Override
            public void onResponse(Call<UpdateVerificationStatusApiResponse> call, Response<UpdateVerificationStatusApiResponse> response) {
                CustomProgressDialog.getInstance().dismiss();
                if (response.isSuccessful()) {
                    if (response.body().getStatus().equals(200)) {
                        MyApplication.displayKnownErrorSuccess(context, response.body().getMessage());
                        listener.onSuccess(response.body());
                    } else {
                        MyApplication.displayKnownError(response.body().getMessage());
                        listener.onFailure(response.message());
                    }
                }
            }

            @Override
            public void onFailure(Call<UpdateVerificationStatusApiResponse> call, Throwable t) {
                CustomProgressDialog.getInstance().dismiss();
                t.printStackTrace();
                MyApplication.displayUnKnownError();
            }
        });

    }

    // ---- GET Method -- RUSH HOURS
    public void getRushhours(final Context context, final CommonCallback.Listener listener) {
        if (!CustomProgressDialog.getInstance().isShowing()) {
            CustomProgressDialog.getInstance().show(context);
        }
        ApiInterface apiInterface = ApiConfiguration.getInstance().getApiBuilder().create(ApiInterface.class);
        Call<GetRushHoursApiResponse> call = apiInterface.getRush_Hours();
        call.enqueue(new Callback<GetRushHoursApiResponse>() {
            @Override
            public void onResponse(Call<GetRushHoursApiResponse> call, Response<GetRushHoursApiResponse> response) {
                CustomProgressDialog.getInstance().dismiss();
                if (response.isSuccessful()) {
                    if (response.body().getStatus().equals(200)) {
                        // MyApplication.displayKnownErrorSuccess(context, response.body().getMessage());
                        listener.onSuccess(response.body());
                    } else {
                        MyApplication.displayKnownError(response.body().getMessage());
                        listener.onFailure(response.message());
                    }
                } else {
                    try {
                        InputStream inputStream = ((ResponseBody) response.errorBody()).byteStream();
                        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));

                        Gson gson = new GsonBuilder().create();
                        ErrorApiResponse mError = gson.fromJson(bufferedReader.readLine(), ErrorApiResponse.class);

                        if (mError.getStatus().equals(422)) {
                            MyApplication.displayKnownError(mError.getMessage());
                        } else {
                            MyApplication.displayKnownError(mError.getMessage());
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                        listener.onFailure(response.message());
                        MyApplication.displayKnownError(response.message());
                    }
                }
            }

            @Override
            public void onFailure(Call<GetRushHoursApiResponse> call, Throwable t) {
                CustomProgressDialog.getInstance().dismiss();
                t.printStackTrace();
                MyApplication.displayUnKnownError();
            }
        });

    }

    // --- POST PickUp and Delivery Time Slot
    public void pickUpAndDeliveryTimeSlot(final Context context, final CommonCallback.Listener listener) {
        if (!CustomProgressDialog.getInstance().isShowing()) {
            CustomProgressDialog.getInstance().show(context);
        }
        ApiInterface apiInterface = ApiConfiguration.getInstance().getApiBuilder().create(ApiInterface.class);
        Call<TimeSlotApiResponse> call = apiInterface.pickup_delivery_timeslot();
        call.enqueue(new Callback<TimeSlotApiResponse>() {
            @Override
            public void onResponse(Call<TimeSlotApiResponse> call, Response<TimeSlotApiResponse> response) {
                CustomProgressDialog.getInstance().dismiss();
                if (response.isSuccessful()) {
                    if (response.body().getStatus().equals(200)) {
                        // MyApplication.displayKnownErrorSuccess(context, response.body().getMessage());
                        listener.onSuccess(response.body());
                    } else {
                        MyApplication.displayKnownError(response.body().getMessage());
                        listener.onFailure(response.message());
                    }
                } else {
                    try {
                        InputStream inputStream = ((ResponseBody) response.errorBody()).byteStream();
                        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));

                        Gson gson = new GsonBuilder().create();
                        ErrorApiResponse mError = gson.fromJson(bufferedReader.readLine(), ErrorApiResponse.class);

                        if (mError.getStatus().equals(422)) {
                            MyApplication.displayKnownError(mError.getMessage());
                        } else {
                            MyApplication.displayKnownError(mError.getMessage());
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                        listener.onFailure(response.message());
                        MyApplication.displayKnownError(response.message());
                    }
                }
            }

            @Override
            public void onFailure(Call<TimeSlotApiResponse> call, Throwable t) {
                CustomProgressDialog.getInstance().dismiss();
                t.printStackTrace();
                MyApplication.displayUnKnownError();
            }
        });

    }




    public void getAvailablePackages(final Context context, final CommonCallback.Listener listener) {
        if (!CustomProgressDialog.getInstance().isShowing()) {
            CustomProgressDialog.getInstance().show(context);
        }
        ApiInterface apiInterface = ApiConfiguration.getInstance().getApiBuilder().create(ApiInterface.class);


        Call<GetAvailablePackagesApiResponse> call = apiInterface.getAvailablePackages();
        call.enqueue(new Callback<GetAvailablePackagesApiResponse>() {
            @Override
            public void onResponse(Call<GetAvailablePackagesApiResponse> call,
                                   Response<GetAvailablePackagesApiResponse> response) {

                CustomProgressDialog.getInstance().dismiss();
                if (response.isSuccessful()) {
                    if (response.body().getStatus().equals(200)) {
                        listener.onSuccess(response.body());
                    } else {
                        MyApplication.displayKnownError(response.body().getMessage());
                        listener.onFailure(response.message());
                    }
                } else {
                    try {
                        InputStream inputStream = ((ResponseBody) response.errorBody()).byteStream();
                        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));

                        Gson gson = new GsonBuilder().create();
                        ErrorApiResponse mError = gson.fromJson(bufferedReader.readLine(), ErrorApiResponse.class);

                        if (mError.getStatus().equals(422)) {
                            MyApplication.displayKnownError(mError.getMessage());
                        } else {
                            MyApplication.displayKnownError(mError.getMessage());
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                        listener.onFailure(response.message());
                        MyApplication.displayKnownError(response.message());
                    }
                }

            }

            @Override
            public void onFailure(Call<GetAvailablePackagesApiResponse> call, Throwable t) {
                CustomProgressDialog.getInstance().dismiss();
                t.printStackTrace();
                MyApplication.displayUnKnownError();
            }
        });
    }



    public void getMySubscribedPackages(final Context context, final CommonCallback.Listener listener) {
        if (!CustomProgressDialog.getInstance().isShowing()) {
            CustomProgressDialog.getInstance().show(context);
        }
        ApiInterface apiInterface = ApiConfiguration.getInstance().getApiBuilder().create(ApiInterface.class);


        Call<GetMySubscribedPackagesApiResponse> call = apiInterface.getMySubscribedPackages();
        call.enqueue(new Callback<GetMySubscribedPackagesApiResponse>() {
            @Override
            public void onResponse(Call<GetMySubscribedPackagesApiResponse> call,
                                   Response<GetMySubscribedPackagesApiResponse> response) {

                CustomProgressDialog.getInstance().dismiss();
                if (response.isSuccessful()) {
                    if (response.body().getStatus().equals(200)) {
                        listener.onSuccess(response.body());
                    } else {
                        MyApplication.displayKnownError(response.body().getMessage());
                        listener.onFailure(response.message());
                    }
                } else {
                    try {
                        InputStream inputStream = ((ResponseBody) response.errorBody()).byteStream();
                        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));

                        Gson gson = new GsonBuilder().create();
                        ErrorApiResponse mError = gson.fromJson(bufferedReader.readLine(), ErrorApiResponse.class);

                        if (mError.getStatus().equals(422)) {
                            MyApplication.displayKnownError(mError.getMessage());
                        } else {
                            MyApplication.displayKnownError(mError.getMessage());
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                        listener.onFailure(response.message());
                        MyApplication.displayKnownError(response.message());
                    }
                }

            }

            @Override
            public void onFailure(Call<GetMySubscribedPackagesApiResponse> call, Throwable t) {
                CustomProgressDialog.getInstance().dismiss();
                t.printStackTrace();
                MyApplication.displayUnKnownError();
            }
        });
    }




    public void subscribePackage(final Context context,
                                 int packageId,
                                 final CommonCallback.Listener listener) {
        if (!CustomProgressDialog.getInstance().isShowing()) {
            CustomProgressDialog.getInstance().show(context);
        }
        ApiInterface apiInterface = ApiConfiguration.getInstance().getApiBuilder().create(ApiInterface.class);


        Call<SubscribePackageApiResponse> call = apiInterface.subscribePackage(packageId);
        call.enqueue(new Callback<SubscribePackageApiResponse>() {
            @Override
            public void onResponse(Call<SubscribePackageApiResponse> call,
                                   Response<SubscribePackageApiResponse> response) {

                CustomProgressDialog.getInstance().dismiss();
                if (response.isSuccessful()) {
                    if (response.body().getStatus().equals(200)) {
                        listener.onSuccess(response.body());
                    } else {
                        MyApplication.displayKnownError(response.body().getMessage());
                        listener.onFailure(response.message());
                    }
                } else {
                    try {
                        InputStream inputStream = ((ResponseBody) response.errorBody()).byteStream();
                        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));

                        Gson gson = new GsonBuilder().create();
                        ErrorApiResponse mError = gson.fromJson(bufferedReader.readLine(), ErrorApiResponse.class);

                        if (mError.getStatus().equals(422)) {
                            MyApplication.displayKnownError(mError.getMessage());
                        } else {
                            MyApplication.displayKnownError(mError.getMessage());
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                        listener.onFailure(response.message());
                        MyApplication.displayKnownError(response.message());
                    }
                }

            }

            @Override
            public void onFailure(Call<SubscribePackageApiResponse> call, Throwable t) {
                CustomProgressDialog.getInstance().dismiss();
                t.printStackTrace();
                MyApplication.displayUnKnownError();
            }
        });
    }



    public void getPaymentMethods(final Context context, final CommonCallback.Listener listener)
    {
        if (!CustomProgressDialog.getInstance().isShowing()) {
            CustomProgressDialog.getInstance().show(context);
        }
        ApiInterface apiInterface = ApiConfiguration.getInstance().getApiBuilder().create(ApiInterface.class);

        Call<GetPaymentMethodsAPIResponse> call = apiInterface.getPaymentMethods();
        call.enqueue(new Callback<GetPaymentMethodsAPIResponse>() {
            @Override
            public void onResponse(Call<GetPaymentMethodsAPIResponse> call, Response<GetPaymentMethodsAPIResponse> response) {
                CustomProgressDialog.getInstance().dismiss();
                if (response.isSuccessful()) {
                    if (response.body().getStatus().equals(200)) {
                        listener.onSuccess(response.body());
                    } else {
                        MyApplication.displayKnownError(response.body().getMessage());
                        listener.onFailure(response.message());
                    }
                } else {
                    try {
                        InputStream inputStream = ((ResponseBody) response.errorBody()).byteStream();
                        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));

                        Gson gson = new GsonBuilder().create();
                        ErrorApiResponse mError = gson.fromJson(bufferedReader.readLine(), ErrorApiResponse.class);

                        if (mError.getStatus().equals(422)) {
                            MyApplication.displayKnownError(mError.getMessage());
                        } else {
                            MyApplication.displayKnownError(mError.getMessage());
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                        listener.onFailure(response.message());
                        MyApplication.displayKnownError(response.message());
                    }
                }

            }

            @Override
            public void onFailure(Call<GetPaymentMethodsAPIResponse> call, Throwable t) {
                CustomProgressDialog.getInstance().dismiss();
                t.printStackTrace();
                MyApplication.displayUnKnownError();
            }
        });
    }


    public void getSubscribedItemsTypes(final Context context, final CommonCallback.Listener listener) {
        if (!CustomProgressDialog.getInstance().isShowing()) {
            CustomProgressDialog.getInstance().show(context);
        }
        ApiInterface apiInterface = ApiConfiguration.getInstance().getApiBuilder().create(ApiInterface.class);


        Call<GetSubscribedItemsTypes> call = apiInterface.getSubscribedItemsTypes();
        call.enqueue(new Callback<GetSubscribedItemsTypes>() {
            @Override
            public void onResponse(Call<GetSubscribedItemsTypes> call,
                                   Response<GetSubscribedItemsTypes> response) {

                CustomProgressDialog.getInstance().dismiss();
                if (response.isSuccessful()) {
                    if (response.body().getStatus().equals(200)) {
                        listener.onSuccess(response.body());
                    } else {
                        MyApplication.displayKnownError(response.body().getMessage());
                        listener.onFailure(response.message());
                    }
                } else {
                    try {
                        InputStream inputStream = ((ResponseBody) response.errorBody()).byteStream();
                        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));

                        Gson gson = new GsonBuilder().create();
                        ErrorApiResponse mError = gson.fromJson(bufferedReader.readLine(), ErrorApiResponse.class);

                        if (mError.getStatus().equals(422)) {
                            MyApplication.displayKnownError(mError.getMessage());
                        } else {
                            MyApplication.displayKnownError(mError.getMessage());
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                        listener.onFailure(response.message());
                        MyApplication.displayKnownError(response.message());
                    }
                }

            }

            @Override
            public void onFailure(Call<GetSubscribedItemsTypes> call, Throwable t) {
                CustomProgressDialog.getInstance().dismiss();
                t.printStackTrace();
                MyApplication.displayUnKnownError();
            }
        });
    }

}
