package com.globallaundry.api;

public class Urls {

    /*Demo Url*/
    public static final String BASE_URL = "https://globallaundry.co/";



    static final String BASE_URL_DEV = "https://globallaundry.co/";
    static final String BASE_URL_PROD = "https://globallaundry.co/";

    public static String VERSIONN = "api/v1/";

    static final String Login = "user/login";
    static final String GetUser = "user/get-user";
    static final String Registration = "user/register";
    static final String VerifyOTP = "user/verify-otp";
    static final String ResendOTP = "user/resend-otp";
    static final String UpdateVerificationStatus = "user/update-verification-status";
    static final String ForgotPassword = "user/forgot-password";
    static final String UPDATEPROFILE = "user/update";
    static final String ChangePassword = "user/change-password";
    static final String RESETPASSWORD = "user/reset-password";
    static final String FACEBOOK_LOGIN = "user/facebook-login";
    static final String City = "city";
    static final String BranchCity = "city/branch-city";
    static final String Area = "area";
    static final String BranchArea = "area/branch-area";
    static final String Category = "category";
    static final String CLOTHTYPE = "cloth-type";
    static final String CLOTHTYPEVIEW = "cloth-type/{cloth_type_key}";
    static final String Checkout = "order/calculate-data";
    static final String PlaceOrder = "order/place-order";
    static final String AddressType = "user-address/address-type-list";
    static final String AddressList = "user-address";
    static final String AddressListByArea = "user-address/get-address-details";
    static final String AddAddress = "user-address/create";
    static final String OrderList = "order";
    static final String OrderDetatails = "order/view";
    static final String PickUpTimeSlot = "cloth-type/load-pickup-timeslot";
    static final String DeliveryTimeSlot = "cloth-type/load-delivery-timeslot";
    static final String GETTERMSCONDITION = "user/terms-and-conditions";
    static final String DEFAULT_ADDRESS = "user-address/set-default";
    static final String UPDATE_ADDRESS = "user-address/update";
    static final String GETCONTACTUS = "contactus/contact-details";
    static final String CONTACTUS = "contactus/contact";
    static final String CHANGEADDRESS = "order/change-address";
    static final String ORDERCHANGEADDRESS = "user-address/user-order-address";
    static final String PromotionAds = "promotion-ads";
    static final String CMS = "cms";
    static final String UPLOAD_PROFILE_IMAGE = "user/update-profile-image";
    static final String CALCULATEBASKET = "order/calculate-data";
    static final String GETPROFILE = "user/view";
    static final String ADD_RATING = "rating";
    static final String GETREVIEW = "rating";
    static final String NOTIFICATION = "notification";
    static final String NOTIFICATION_UPDATE = "notification/update";
    static final String SOCIAL_LOGIN = "user/social-login";
    static final String RUSH_HOURS = "user/get-rush-hours";
    static final String clothTypeLoadTimeSlot = "cloth-type/load-timeslot";


    // My Implementations

    //TODO write the api link.
    static final String AVAILABLE_PACKAGES = "user/get-all-packages";
    public static final String MY_SUBSCRIBED_PACKAGES = "user/get-user-packages";
    public static final String SUBSCRIBE_PACKAGE = "user/add-package";
    public static final String SUBSCRIBED_ITEMS_TYPE = "cloth-type?env=dev&category_key=rip6n8bcfp5if45l";
    public static final String PAYMENT_METHODS = "order/get-payment-methods";
}
