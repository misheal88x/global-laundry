package com.globallaundry.api;


import com.globallaundry.apimodel.AddAddressApiResponse;
import com.globallaundry.apimodel.AddRatingApiResponse;
import com.globallaundry.apimodel.AddressListApiResponse;
import com.globallaundry.apimodel.AddressTypeApiResponse;
import com.globallaundry.apimodel.AreaListApiResponse;
import com.globallaundry.apimodel.CalculateApiResponse;
import com.globallaundry.apimodel.CategoryApiResponse;
import com.globallaundry.apimodel.ChangeAddressApiResponse;
import com.globallaundry.apimodel.ChangeDeliveryAddressApiResponse;
import com.globallaundry.apimodel.ChangePasswordApiResponse;
import com.globallaundry.apimodel.CityListApiResponse;
import com.globallaundry.apimodel.ClothTypeApiResponse;
import com.globallaundry.apimodel.ClothTypeListViewApiResponse;
import com.globallaundry.apimodel.ContactusSubmitApiResponse;
import com.globallaundry.apimodel.DeleteAddressApiResponse;
import com.globallaundry.apimodel.EditAddressApiResponse;
import com.globallaundry.apimodel.ForgotPasswordApiResponse;
import com.globallaundry.apimodel.GetAvailablePackagesApiResponse;
import com.globallaundry.apimodel.GetContactusApiResponse;
import com.globallaundry.apimodel.GetMySubscribedPackagesApiResponse;
import com.globallaundry.apimodel.GetPaymentMethodsAPIResponse;
import com.globallaundry.apimodel.GetProfileApiResponse;
import com.globallaundry.apimodel.GetRushHoursApiResponse;
import com.globallaundry.apimodel.GetSubscribedItemsTypes;
import com.globallaundry.apimodel.GetTermsConditionsApiResponse;
import com.globallaundry.apimodel.HelpApiResponse;
import com.globallaundry.apimodel.LoginApiResponse;
import com.globallaundry.apimodel.MarkAsDefaultApiResponse;
import com.globallaundry.apimodel.OrderDetailsApiResponse;
import com.globallaundry.apimodel.OrderListApiResponse;
import com.globallaundry.apimodel.PickUpTimeSlotApiResponse;
import com.globallaundry.apimodel.PlaceOrderApiResponse;
import com.globallaundry.apimodel.PromationAddApiResponse;
import com.globallaundry.apimodel.RegistrationApiResponse;
import com.globallaundry.apimodel.ResendOTPApiResponse;
import com.globallaundry.apimodel.ResetPasswordResponse;
import com.globallaundry.apimodel.ReviewListApiResponse;
import com.globallaundry.apimodel.SendOtpBeforeLoginApiResponse;
import com.globallaundry.apimodel.SubscribePackageApiResponse;
import com.globallaundry.apimodel.TimeSlotApiResponse;
import com.globallaundry.apimodel.UpdateProfileApiResponse;
import com.globallaundry.apimodel.UpdateProfileImageApiResponse;
import com.globallaundry.apimodel.VerifyOTPApiResponse;
import com.globallaundry.models.NotificationListApiResponse;
import com.globallaundry.models.ReadNotificationApiResponse;
import com.globallaundry.models.SocialLoginApiResponse;
import com.globallaundry.models.UpdateVerificationStatusApiResponse;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.HTTP;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface ApiInterface {


    @FormUrlEncoded
    @POST(Urls.Login)
    Call<LoginApiResponse> login(@Field("username") String username,
                                 @Field("password") String password,
                                 @Field("device_type_id") String deviceType,
                                 @Field("device_token") String deviceToken);


    @GET(Urls.GetUser)
    Call<SendOtpBeforeLoginApiResponse> sendOtpBeforeLogin(@Query("username") String username);

    @FormUrlEncoded
    @POST(Urls.Registration)
    Call<RegistrationApiResponse> Registration(@Field("username") String mUsername,
                                               @Field("email") String mEmail,
                                               @Field("mobile_number") String mMobileNumber,
                                               @Field("password_hash") String mPassword,
                                               @Field("accept_terms") String mAcceptTerms,
                                               @Field("first_name") String mFirstName,
                                               @Field("referral_code") String referralCode);

    @FormUrlEncoded
    @POST(Urls.VerifyOTP)
    Call<VerifyOTPApiResponse> VerifyOTP(@Field("otp") String mOtp,
                                         @Field("otp_id") String mOtpId,
                                         @Field("device_type") String mDeviceType,
                                         @Field("device_token") String mDeviceToken,
                                         @Field("type") String mType,
                                         @Field("mobile_number") String mMobile_number);

    @FormUrlEncoded
    @POST(Urls.ResendOTP)
    Call<ResendOTPApiResponse> resendOTP(@Field("otp_id") String mEmail);

    @FormUrlEncoded
    @POST(Urls.UpdateVerificationStatus)
    Call<UpdateVerificationStatusApiResponse> verified_otp(@Field("user_key") String user_key);

    @FormUrlEncoded
    @POST(Urls.RESETPASSWORD)
    Call<ResetPasswordResponse> resetPassword(@Field("reset_token") String mResetToken,
                                              @Field("password_new") String mPasswordNew,
                                              @Field("confirm_password") String mConfirmPassword);

    @FormUrlEncoded
    @POST(Urls.ForgotPassword)
    Call<ForgotPasswordApiResponse> forgotPassword(@Field("email") String mEmail);

    @POST(Urls.PickUpTimeSlot)
    Call<PickUpTimeSlotApiResponse> pickupTimeSlot();

    @GET(Urls.CMS)
    Call<GetTermsConditionsApiResponse> getTermsCondtions();

    @PUT(Urls.DEFAULT_ADDRESS)
    Call<MarkAsDefaultApiResponse> markAsDefaultAddress(@Query("user_address_key") String user_address_key);

    @HTTP(method = "DELETE", path = "user-address/delete", hasBody = true)
    Call<DeleteAddressApiResponse> deleteAddress(@Query("user_address_key") String userAddressKey);

    @GET(Urls.AddressList)
    Call<AddressListApiResponse> addressList();

    @GET(Urls.GETCONTACTUS)
    Call<GetContactusApiResponse> getContactusDetail();

    @GET(Urls.AddressType)
    Call<AddressTypeApiResponse> addressType();

    @GET(Urls.GETPROFILE)
    Call<GetProfileApiResponse> getProfile();

    @GET(Urls.GETREVIEW)
    Call<ReviewListApiResponse> getReview();


    @FormUrlEncoded
    @POST(Urls.AddAddress)
    Call<AddAddressApiResponse> addAddress(@Field("address_type_key") String mAddressTypeId,
                                           @Field("city_key") String mCityId,
                                           @Field("area_key") String mAreaId,
                                           @Field("flat_no") String mFlatNo,
                                           @Field("apartment") String mApartment,
                                           @Field("street_name") String mStreetNumber,
                                           @Field("company") String mCompany,
                                           @Field("landmark") String mLandmark,
                                           @Field("latitude") String mLatitude,
                                           @Field("longitude") String mLongitude);

    @GET(Urls.City)
    Call<CityListApiResponse> cityList();

    @GET(Urls.Area)
    Call<AreaListApiResponse> areaList(@Query("id") String mCityId);

    @GET(Urls.Category)
    Call<CategoryApiResponse> categoryList();

    @GET(Urls.CLOTHTYPE)
    Call<ClothTypeApiResponse> clothTypeList(@Query("category_key") String mCategoryKey);

    @GET(Urls.CLOTHTYPEVIEW)
    Call<ClothTypeListViewApiResponse> clothTypeListView(@Path(value = "cloth_type_key", encoded = true) String mClothTypeKey);

    @POST(Urls.PlaceOrder)
    Call<PlaceOrderApiResponse> placeOrder(@Body RequestBody body);

    @POST(Urls.CALCULATEBASKET)
    Call<CalculateApiResponse> calculateBasket(@Body RequestBody body);

    @GET(Urls.OrderList)
    Call<OrderListApiResponse> orderList();

    @GET(Urls.OrderDetatails)
    Call<OrderDetailsApiResponse> orderDetails(@Query("order_key") String mOrderKey);

    @FormUrlEncoded
    @PUT(Urls.UPDATE_ADDRESS)
    Call<EditAddressApiResponse> updateAddress(@Field("user_address_key") String user_address_key,
                                               @Field("address_type_key") String address_type_key,
                                               @Field("city_key") String mCityId,
                                               @Field("area_key") String mAreaId,
                                               @Field("flat_no") String mFlatNo,
                                               @Field("apartment") String mApartment,
                                               @Field("street_name") String mStreetNumber,
                                               @Field("company") String mCompany,
                                               @Field("landmark") String mLandmark,
                                               @Field("latitude") String mLatitude,
                                               @Field("longitude") String mLongitude);

    @FormUrlEncoded
    @POST(Urls.CONTACTUS)
    Call<ContactusSubmitApiResponse> contactus(
        @Field("username") String first_name,
        @Field("email") String email,
        @Field("mobile_number") String mobile_number,
        @Field("comments") String subject);


    @FormUrlEncoded
    @POST(Urls.CHANGEADDRESS)
    Call<ChangeAddressApiResponse> changedelvieryAddressApi(@Field("order_key") String order_key,
                                                            @Field("user_address_key") String user_address_key);

    @GET(Urls.ORDERCHANGEADDRESS)
    Call<ChangeDeliveryAddressApiResponse> changeOrderdelvieryAddressApi(@Query("order_key") String order_key);

    @GET(Urls.PromotionAds)
    Call<PromationAddApiResponse> promationApi();

    @GET(Urls.CMS)
    Call<HelpApiResponse> helpApi();

    @Multipart
    @POST(Urls.UPLOAD_PROFILE_IMAGE)
    Call<UpdateProfileImageApiResponse> updateProfileImage(@Part MultipartBody.Part profile_image);

    @FormUrlEncoded
    @POST(Urls.ChangePassword)
    Call<ChangePasswordApiResponse> changePassword(@Field("password") String mPassword,
                                                   @Field("password_new") String mPasswordNew,
                                                   @Field("confirm_password") String mConfirmPassword);

    @FormUrlEncoded
    @POST(Urls.UPDATEPROFILE)
    Call<UpdateProfileApiResponse> updateProfile(@Field("first_name") String mFirstName,
                                                 @Field("email") String mEmail,
                                                 @Field("mobile_number") String mMobileNumber);


    @FormUrlEncoded
    @POST(Urls.ADD_RATING)
    Call<AddRatingApiResponse> addRating(@Field("order_key") String order_key,
                                         @Field("rating") String rating,
                                         @Field("review") String review);


    @GET(Urls.NOTIFICATION)
    Call<NotificationListApiResponse> notification_list();


    @FormUrlEncoded
    @POST(Urls.NOTIFICATION_UPDATE)
    Call<ReadNotificationApiResponse> read_notification(@Field("notification_key") String notification_key);


    @FormUrlEncoded
    @POST(Urls.SOCIAL_LOGIN)
    Call<SocialLoginApiResponse> social_login(@Field("social_token") String social_token,
                                              @Field("social_login_type") String social_login_type,
                                              @Field("email") String email,
                                              @Field("first_name") String first_name,
                                              @Field("device_token") String device_token,
                                              @Field("device_type") String device_type);


    @GET(Urls.RUSH_HOURS)
    Call<GetRushHoursApiResponse> getRush_Hours();


    @POST(Urls.clothTypeLoadTimeSlot)
    Call<TimeSlotApiResponse> pickup_delivery_timeslot();


    // TODO write the required parameters.
    @GET(Urls.AVAILABLE_PACKAGES)
    Call<GetAvailablePackagesApiResponse> getAvailablePackages();

    // TODO write the required parameters.
    @POST(Urls.MY_SUBSCRIBED_PACKAGES)
    Call<GetMySubscribedPackagesApiResponse> getMySubscribedPackages();

    // TODO write the required parameters.

    @FormUrlEncoded
    @POST(Urls.SUBSCRIBE_PACKAGE)
    Call<SubscribePackageApiResponse> subscribePackage(@Field("id") int packageId);

    @GET(Urls.SUBSCRIBED_ITEMS_TYPE)
    Call<GetSubscribedItemsTypes> getSubscribedItemsTypes();

    @GET(Urls.PAYMENT_METHODS)
    Call<GetPaymentMethodsAPIResponse> getPaymentMethods();
}
