package com.globallaundry.activity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.os.Vibrator;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import com.bumptech.glide.Glide;
import com.globallaundry.R;
import com.globallaundry.apimodel.ClothTypeApiResponse;
import com.globallaundry.databinding.ActivityItemDetailsBinding;
import com.globallaundry.dbmodel.RealmLibrary;
import com.globallaundry.events.CartRefresh;
import com.globallaundry.utils.CommonFunctions;
import com.globallaundry.utils.LanguageConstants;
import com.globallaundry.utils.SessionManager;
import com.google.gson.Gson;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;

import hari.bounceview.BounceView;

public class ItemDetails extends AppCompatActivity implements View.OnClickListener {
    ActivityItemDetailsBinding binding;
    private String size;
    ClothTypeApiResponse.Datum responses;
    private Integer isExpress;
    public static Vibrator vibrator;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_item_details);
        if (SessionManager.getInstance().getAppLanguageCode().equals("ar")) {
            getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
            binding.ivBackArrow.setImageResource(R.drawable.ic_right_arrow);
        } else {
            getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
            binding.ivBackArrow.setImageResource(R.drawable.ic_back_arrow);
        }
        if (getIntent().getExtras() != null) {
            size = getIntent().getExtras().getString("SIZE");
            if (size != null) {
                Gson gson = new Gson();
                responses = gson.fromJson(size, ClothTypeApiResponse.Datum.class);
            }
        }
        initView();
        vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
    }

    @SuppressLint("SetTextI18n")
    private void initView() {
        BounceView.addAnimTo(binding.ivBackArrow);
        BounceView.addAnimTo(binding.rlyoutAddCart);
        binding.tlbarText.setText(LanguageConstants.addclothtocart);
        binding.tvAddcart.setText(LanguageConstants.addtoCart);
        binding.checkbox.setText(LanguageConstants.expressdelivery);
        binding.ivBackArrow.setOnClickListener(this);
        binding.rlyoutAddCart.setOnClickListener(this);
        binding.txtPlus.setOnClickListener(this);
        binding.txtMinus.setOnClickListener(this);
        binding.txtName.setText(responses.getCloth_type_name());
        binding.txtPrice.setText(CommonFunctions.getInstance().roundOffDecimalValue(Double.valueOf(responses.getPrice().replace(",","")), true));
        binding.txtExpressdeliveryFee.setText(LanguageConstants.expressdelivery + " " + "(" + CommonFunctions.getInstance().roundOffDecimalValue(Double.valueOf(responses.getExpress_cost().replace(",","")), true) + ")");
        if (responses.getSize_type() == 1) {
            binding.txtMesurement.setText(LanguageConstants.meter);
            binding.lymeasure.setVisibility(View.VISIBLE);
        } else if (responses.getSize_type() == 2) {
            binding.txtMesurement.setText(LanguageConstants.kg);
            binding.lymeasure.setVisibility(View.VISIBLE);
        } else {
            binding.lymeasure.setVisibility(View.GONE);
        }
        if (responses.getCloth_type_image() != null) {
            Glide.with(ItemDetails.this).load(responses.getCloth_type_image()).into(binding.image);
        }
        binding.tvCount.setTag(0);
        binding.tvCount.setText("1");
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ivBackArrow:
                finish();
                break;
            case R.id.rlyoutAddCart:
                // isExpress
                if (binding.checkbox.isChecked()) {
                    isExpress = 1;
                } else {
                    isExpress = 0;
                }
                // add cart
                if (responses.getSize_type() == 1 || responses.getSize_type() == 2) {
                    if (binding.edCount.getText().toString().trim().isEmpty() || Integer.parseInt(binding.edCount.getText().toString().trim()) <= 0) {
                        CommonFunctions.getInstance().validationEmptyError(ItemDetails.this, LanguageConstants.metercantempty);
                    } else {
                        addCartMethod();
                    }
                } else {
                    binding.edCount.setText("0");
                    addCartMethod();
                }
                break;
            case R.id.txtPlus:
                String count = binding.tvCount.getTag().toString();
                Integer count1 = Integer.valueOf(count);
                count1++;
                binding.tvCount.setTag(count1);
                binding.tvCount.setText(count1 + "");
                break;
            case R.id.txtMinus:
                String count2 = binding.tvCount.getTag().toString();
                Integer count3 = Integer.valueOf(count2);
                if (count3 > 1) {
                    count3--;
                    binding.tvCount.setTag(count3);
                    binding.tvCount.setText(count3 + "");
                } else {
                    binding.tvCount.setTag(0);
                }
                break;
            default:
                break;
        }

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    private void addCartMethod() {
        RealmLibrary.getInstance().insertItem(ItemDetails.this, responses.getCloth_type_key(),
                responses.getCloth_type_name(),
                responses.getCloth_type_image(), responses.getSize_type(),
                Double.parseDouble(responses.getPrice().replace(",", "")), SessionManager.getInstance().getCategoryKey(), "1", new ArrayList<>(), 0, "tet",
                binding.tvCount.getText().toString().trim(),
                responses.getExpress_cost(), isExpress, Integer.parseInt(binding.edCount.getText().toString()));
//        System.out.println(RealmLibrary.getInstance().getCartList().size());
        EventBus.getDefault().post(new CartRefresh());
        CommonFunctions.getInstance().successResponseToast(ItemDetails.this, "Cart added success");
        vibrator.vibrate(200);
        finish();

    }
}
