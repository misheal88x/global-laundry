package com.globallaundry.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

import com.globallaundry.R;
import com.globallaundry.adapters.SelectAddressAdapter;
import com.globallaundry.api.CommonApiCalls;
import com.globallaundry.apimodel.ChangeAddressApiResponse;
import com.globallaundry.apimodel.PlaceOrderApiResponse;
import com.globallaundry.databinding.ActivityOrderSuccessBinding;
import com.globallaundry.dbmodel.RealmLibrary;
import com.globallaundry.interfaces.CommonCallback;
import com.globallaundry.interfaces.SelectArea;
import com.globallaundry.utils.CommonFunctions;
import com.globallaundry.utils.LanguageConstants;
import com.globallaundry.utils.SessionManager;
import com.google.gson.Gson;

import hari.bounceview.BounceView;

public class OrderSuccessActivity extends AppCompatActivity implements View.OnClickListener {
    ActivityOrderSuccessBinding binding;
    private String successDataStr = "";
    PlaceOrderApiResponse.Data responses;

    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_order_success);

        if (SessionManager.getInstance().getAppLanguageCode().equals("ar")) {
            getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
            binding.ivBackArrow.setImageResource(R.drawable.ic_right_arrow);
            binding.tvSelectArea.setGravity(Gravity.END);
        } else {
            getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
            binding.ivBackArrow.setImageResource(R.drawable.ic_back_arrow);
            binding.tvSelectArea.setGravity(Gravity.START);
        }

        initView();

        if (getIntent().getExtras() != null) {
            successDataStr = getIntent().getExtras().getString("SUCCESSDATA");
            if (successDataStr != null) {
                Gson gson = new Gson();
                responses = gson.fromJson(successDataStr, PlaceOrderApiResponse.Data.class);
            }
            binding.orderidvalue.setText("#" + " " + responses.getOrder().getOrder_number());
            binding.orderDate.setText(responses.getOrder().getDelivery_datetime());
        }

        RealmLibrary.getInstance().clearCart();
    }

    private void initView() {
        BounceView.addAnimTo(binding.lyoutSelectArea);
        binding.tlbarText.setText(LanguageConstants.orderplacetxt);
        binding.orderidtxt.setText(LanguageConstants.orderidtxt);
        binding.tvcongratulations.setText(LanguageConstants.congratztxt);
        binding.tvordersuccesstxt.setText(LanguageConstants.ordersuccesstxt);
        binding.tvOrderCompletetxt.setText(LanguageConstants.orderCompletetxt);
        binding.tvHome.setText(LanguageConstants.goHome);
        binding.tvSelectArea.setText(LanguageConstants.Changedeliveryaddress);

        binding.ivBackArrow.setOnClickListener(this);
        binding.lyoutSelectArea.setOnClickListener(this);
        binding.rlyoutAddCart.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (v == binding.ivBackArrow) {
            finish();
        } else if (v == binding.lyoutSelectArea) {
            selectAreaDialog();
        } else if (v == binding.rlyoutAddCart) {
            Intent intent = new Intent(OrderSuccessActivity.this, HomeActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            finish();
            //CommonFunctions.getInstance().newIntent(OrderSuccessActivity.this, HomeActivity.class, Bundle.EMPTY, true);
        }
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
    }

    private void selectAreaDialog() {
        final Dialog dialog = new Dialog(OrderSuccessActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_select_area);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);

        ImageView imgClose = (ImageView) dialog.findViewById(R.id.imgClose);
        RecyclerView recyclerView = (RecyclerView) dialog.findViewById(R.id.recyclerView);
        TextView txtNoDataFound = (TextView) dialog.findViewById(R.id.txtNoDataFound);

        txtNoDataFound.setText(LanguageConstants.noDataFound);
        dialog.show();

        if (responses.getUser_address() != null && responses.getUser_address().size() > 0) {
            txtNoDataFound.setVisibility(View.GONE);
            recyclerView.setVisibility(View.VISIBLE);

            @SuppressLint("WrongConstant")
            LinearLayoutManager layoutManager = new LinearLayoutManager(OrderSuccessActivity.this, LinearLayoutManager.VERTICAL, false);
            recyclerView.setLayoutManager(layoutManager);
            SelectAddressAdapter adapter = new SelectAddressAdapter(OrderSuccessActivity.this, responses.getUser_address(), new SelectArea() {
                @Override
                public void onClick(int id, String name) {
                    dialog.dismiss();
                    callChangeAddressApi(responses.getOrder().getOrder_key(), name);
                }
            });
            recyclerView.setAdapter(adapter);
        }
        else {
            txtNoDataFound.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.GONE);
        }

        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });


    }

    private void callChangeAddressApi(String order_key, String selectAreaName) {
        CommonApiCalls.getInstance().changeDeliveryAddress(OrderSuccessActivity.this, order_key, selectAreaName, new CommonCallback.Listener() {
            @Override
            public void onSuccess(Object body) {
                ChangeAddressApiResponse changeAddressApiResponse = (ChangeAddressApiResponse) body;

                if (changeAddressApiResponse.getStatus().equals(200)) {
                    CommonFunctions.getInstance().successResponseToast(OrderSuccessActivity.this, changeAddressApiResponse.getMessage());
                    if (changeAddressApiResponse.getData()!= null){
                        binding.tvSelectArea.setEllipsize(TextUtils.TruncateAt.MARQUEE);
                        binding.tvSelectArea.setText(changeAddressApiResponse.getData().getStreet_name());
                        binding.tvSelectArea.setSelected(true);
                        binding.tvSelectArea.setSingleLine(true);
                    }
                }
            }

            @Override
            public void onFailure(String reason) {
                CommonFunctions.getInstance().validationInfoError(OrderSuccessActivity.this, reason);
            }
        });
    }
}
