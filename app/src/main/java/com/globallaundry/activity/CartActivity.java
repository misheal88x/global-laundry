package com.globallaundry.activity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.globallaundry.R;
import com.globallaundry.adapters.CartAdapter;
import com.globallaundry.api.CommonApiCalls;
import com.globallaundry.apimodel.GetRushHoursApiResponse;
import com.globallaundry.apimodel.GetSubscribedItemsTypes;
import com.globallaundry.databinding.ActivityCartBinding;
import com.globallaundry.dbmodel.ItemRealmList;
import com.globallaundry.dbmodel.RealmLibrary;
import com.globallaundry.interfaces.ChangeInCart;
import com.globallaundry.interfaces.CommonCallback;
import com.globallaundry.models.CacheManager;
import com.globallaundry.utils.CommonFunctions;
import com.globallaundry.utils.LanguageConstants;
import com.globallaundry.utils.MyApplication;
import com.globallaundry.utils.SessionManager;

import java.util.List;

import hari.bounceview.BounceView;
import io.realm.RealmResults;

//import hari.bounceview.BounceView;

public class CartActivity extends AppCompatActivity implements View.OnClickListener {
    ActivityCartBinding binding;

    Double totalPrice = 0.0;
    private List<GetSubscribedItemsTypes.Item> itemTypesList;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_cart);
        if (SessionManager.getInstance().getAppLanguageCode().equals("ar")) {
            getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
            binding.ivBackArrow.setImageResource(R.drawable.ic_right_arrow);
        } else {
            getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
            binding.ivBackArrow.setImageResource(R.drawable.ic_back_arrow);
        }
        initView();
    }

    private void initView() {
        BounceView.addAnimTo(binding.ivBackArrow);
        BounceView.addAnimTo(binding.checkouttxt);
        binding.tlbarText.setText(LanguageConstants.myCart);
        binding.ivBackArrow.setOnClickListener(this);
        binding.checkouttxt.setText(LanguageConstants.Checkout);
        binding.checkouttxt.setOnClickListener(this);

        CommonApiCalls.getInstance().getSubscribedItemsTypes(this, new CommonCallback.Listener() {
            @Override
            public void onSuccess(Object body) {
                GetSubscribedItemsTypes response = (GetSubscribedItemsTypes) body;
                itemTypesList = response.getData();
                CacheManager.getInstance().put("items_types_list", itemTypesList);
                cartApi();
            }

            @Override
            public void onFailure(String reason) {
                MyApplication.displayKnownError(reason);
            }
        });


    }

    private void cartApi() {
        // set adapter
        RealmResults<ItemRealmList> cartList = RealmLibrary.getInstance().getCartList();

        if (cartList != null && cartList.size() > 0) {
            binding.txtPrice.setText(CommonFunctions.getInstance().roundOffDecimalValue(Double.valueOf(total()), true));
            LinearLayoutManager layoutManager = new LinearLayoutManager(CartActivity.this, RecyclerView.VERTICAL, false);
            binding.rvCategory.setLayoutManager(layoutManager);
            CartAdapter adapter = new CartAdapter(CartActivity.this, cartList, () -> {
                cartApi();
                binding.txtPrice.setText(CommonFunctions.getInstance().roundOffDecimalValue(Double.valueOf(total()), true));
            });
            binding.rvCategory.setAdapter(adapter);

        } else {
            /*binding.rlEmptyCart.setVisibility(View.VISIBLE);
            binding.llCartList.setVisibility(View.GONE);*/

        }


    }

    @Override
    public void onClick(View v) {
        if (v == binding.ivBackArrow) {
            finish();
        } else if (v == binding.checkouttxt) {
            getRushHours();
            // CommonFunctions.getInstance().newIntent(CartActivity.this, CalanderActivity.class, Bundle.EMPTY, false);
        }

    }

    // --- Api Call -GET Method [Rush-Hours]
    private void getRushHours() {
        CommonApiCalls.getInstance().getRushhours(CartActivity.this, new CommonCallback.Listener() {
            @Override
            public void onSuccess(Object object) {
                GetRushHoursApiResponse body = (GetRushHoursApiResponse) object;

                if (body.getStatus().equals(200)) {
                    Integer rushHours = body.getSystem().getIs_rush_hour();
                    Bundle bundle = new Bundle();
                    bundle.putString("rushHours", rushHours + "");
                    CommonFunctions.getInstance().newIntent(CartActivity.this, CalanderActivity.class, bundle, false);
                }

            }

            @Override
            public void onFailure(String reason) {
            }
        });
    }

    private String total() {
        RealmResults<ItemRealmList> itemList = RealmLibrary.getInstance().getCartList();

        totalPrice = 0.0;

        for (int count = 0; count < itemList.size(); count++) {
            ItemRealmList item = itemList.get(count);

            int virtualQty = Integer.parseInt(item.getQuantity());

            int checkSubscribedPackage = CommonFunctions.getInstance().findItemInMyPackages(item.getCloth_type_key(),
                    Integer.parseInt(item.getQuantity()), itemTypesList);


            if (checkSubscribedPackage != Integer.MIN_VALUE) {
                if (checkSubscribedPackage > 0)
                    continue;
                else {
                    virtualQty = Math.abs(checkSubscribedPackage);
                }
            }


            Log.d("DDDDDDDD", item.getCloth_type_key());


            Double itemPrice = (item.getPrice());
            Double ingPrice = 0.0;
            for (int count1 = 0; count1 < item.getSize().size(); count1++) {
                if (item.getIs_express() == 1) {
                    ingPrice = ingPrice + ((item.getSize().get(count1).getPrice() + Double.parseDouble(item.getExpress_amount().replace(",", ""))) * item.getSize().get(count1).getSize_quantity());
                } else {
                    ingPrice = ingPrice + ((item.getSize().get(count1).getPrice()) * item.getSize().get(count1).getSize_quantity());
                }
            }

            if (item.getAdditional_quantity() > 0) {
                if (item.getIs_express() == 1) {
                    totalPrice = totalPrice + (((itemPrice + ingPrice + Double.parseDouble(item.getExpress_amount().replace(",", ""))) * virtualQty) * item.getAdditional_quantity());
                } else {
                    totalPrice = totalPrice + (((itemPrice + ingPrice) * virtualQty) * item.getAdditional_quantity());
                }
            } else {
                if (item.getItem_type() == 4) {
                    if (item.getIs_express() == 1) {
                        totalPrice = totalPrice + ((itemPrice + ingPrice + Double.parseDouble(item.getExpress_amount().replace(",", ""))) * virtualQty);
                    } else {
                        totalPrice = totalPrice + ((itemPrice + ingPrice) * virtualQty);
                    }
                } else {
                    totalPrice = totalPrice + ((itemPrice + ingPrice) * virtualQty);
                }

            }

        }

        return String.valueOf(totalPrice);
    }
}
