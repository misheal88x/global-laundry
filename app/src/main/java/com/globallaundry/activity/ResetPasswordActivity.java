package com.globallaundry.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import com.globallaundry.R;
import com.globallaundry.api.CommonApiCalls;
import com.globallaundry.databinding.ActivityResetPasswordBinding;
import com.globallaundry.interfaces.CommonCallback;
import com.globallaundry.utils.CommonFunctions;
import com.globallaundry.utils.LanguageConstants;
import com.globallaundry.utils.SessionManager;


public class ResetPasswordActivity extends AppCompatActivity implements View.OnClickListener {


    Button btnOkay;
    String resetToken = "";
    ActivityResetPasswordBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_reset_password);
        if (SessionManager.getInstance().getAppLanguageCode().equals("ar")) {
            getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
            binding.ivBackArrow.setImageResource(R.drawable.ic_right_arrow);
        } else {
            getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
            binding.ivBackArrow.setImageResource(R.drawable.ic_back_arrow);
        }
        resetToken = getIntent().getStringExtra("RESETLINK");
        initView();

    }

    private void initView() {

        binding.tvChangePassword.setText(LanguageConstants.ResetPassword);
        binding.tlbarText.setText(LanguageConstants.ResetPassword);
        binding.tlbarText.setVisibility(View.VISIBLE);
        binding.tvReset.setText(LanguageConstants.submit);
        binding.ivBackArrow.setOnClickListener(this);
        binding.rlyoutReset.setOnClickListener(this);
    }


    private void callChangePasswordApi() {

        if (binding.edNewPassword.getText().toString().isEmpty()) {
            CommonFunctions.getInstance().validationEmptyError(ResetPasswordActivity.this, LanguageConstants.NewPassword);
            return;
        }
        if (binding.edConfirmPassword.getText().toString().isEmpty()) {
            CommonFunctions.getInstance().validationEmptyError(ResetPasswordActivity.this, LanguageConstants.confirmPassword);
            return;
        }
        if (!binding.edNewPassword.getText().toString().trim().equals(binding.edConfirmPassword.getText().toString().trim())) {
            CommonFunctions.getInstance().validationEmptyError(ResetPasswordActivity.this, LanguageConstants.NewPassword);
            return;
        }

        CommonApiCalls.getInstance().resetPassword(ResetPasswordActivity.this,
                resetToken,
                binding.edNewPassword.getText().toString().trim(),
                binding.edConfirmPassword.getText().toString().trim(),
                new CommonCallback.Listener() {
                    @Override
                    public void onSuccess(Object object) {
                        CommonFunctions.getInstance().FinishActivityWithDelay(ResetPasswordActivity.this);
                    }

                    @Override
                    public void onFailure(String reason) {

                    }
                }
        );

    }

    @Override
    public void onClick(View v) {
        if (v == binding.ivBackArrow) {
            finish();
        } else if (v == binding.rlyoutReset) {
            callChangePasswordApi();
        }
    }
}
