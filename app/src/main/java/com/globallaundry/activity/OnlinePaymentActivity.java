package com.globallaundry.activity;

import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import com.globallaundry.R;
import com.globallaundry.utils.CommonFunctions;
import com.globallaundry.utils.CustomProgressDialog;
import com.google.gson.Gson;

import static com.globallaundry.api.Urls.BASE_URL;


public class OnlinePaymentActivity extends AppCompatActivity {
    WebView webView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_online_payment);

        webView = (WebView) findViewById(R.id.webView);

        webView.getSettings().setJavaScriptEnabled(true);
        webView.setWebViewClient(new MyWebViewClient());

        if (!CustomProgressDialog.getInstance().isShowing()) {
            CustomProgressDialog.getInstance().show(OnlinePaymentActivity.this);
        }


        runOnUiThread(new Runnable() {

            @Override
            public void run() {
                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        //add your code here
                        if (getIntent().getExtras() != null && getIntent().getExtras().getString("paymentUrl") != null) {
                            webView.loadUrl(getIntent().getExtras().getString("paymentUrl"));
                            CustomProgressDialog.getInstance().dismiss();
                        }
                    }
                }, 1000);

            }
        });


    }


    private class MyWebViewClient extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            if (CustomProgressDialog.getInstance().isShowing()) {
                CustomProgressDialog.getInstance().dismiss();
            }
            System.out.println("Finish url : " + url);
            if (url.contains(BASE_URL+"payment/master-card-success?")) {
                Bundle bundle = new Bundle();
                bundle.putString("SUCCESSDATA", getIntent().getExtras().getString("SUCCESSDATA"));
                CommonFunctions.getInstance().newIntent(OnlinePaymentActivity.this, OrderSuccessActivity.class, bundle, true);

            }
        }

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
        }
    }

}
