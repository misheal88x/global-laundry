package com.globallaundry.activity;

import android.content.Intent;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.globallaundry.R;
import com.globallaundry.api.CommonApiCalls;
import com.globallaundry.apimodel.LoginApiResponse;
import com.globallaundry.databinding.ActivitySignInBinding;
import com.globallaundry.interfaces.CommonCallback;
import com.globallaundry.models.SocialLoginApiResponse;
import com.globallaundry.utils.CommonFunctions;
import com.globallaundry.utils.LanguageConstants;
import com.globallaundry.utils.SessionManager;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.Task;

import org.json.JSONObject;

import hari.bounceview.BounceView;

public class SignInActivity extends AppCompatActivity implements View.OnClickListener{
    ActivitySignInBinding binding;
    private boolean isPassword;

    private CallbackManager callbackManager;
    LoginButton fbLogin;

    SignInButton signInButton;
    private static final String TAG = SignInActivity.class.getSimpleName();
    private static final int RC_SIGN_IN = 1007;
    private GoogleSignInClient googleSignInClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());

        binding = DataBindingUtil.setContentView(this, R.layout.activity_sign_in);
        if (SessionManager.getInstance().getAppLanguageCode().equals("ar")) {
            getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
            binding.ivBackArrow.setImageResource(R.drawable.ic_right_arrow);
        } else {
            getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
            binding.ivBackArrow.setImageResource(R.drawable.ic_back_arrow);
        }

        initView();

        callbackManager = CallbackManager.Factory.create();
        fbLogin = (LoginButton) findViewById(R.id.login_button);

        signInButton = (SignInButton) findViewById(R.id.sign_in_button);
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getResources().getString(R.string.web_client_id))
                .requestEmail()
                .build();
        googleSignInClient = GoogleSignIn.getClient(this, gso);
        signInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent signInIntent = googleSignInClient.getSignInIntent();
                startActivityForResult(signInIntent, RC_SIGN_IN);
            }
        });

    }

    private void initView() {
        BounceView.addAnimTo(binding.ivBackArrow);
        //BounceView.addAnimTo(binding.txtView);
        BounceView.addAnimTo(binding.llForgotPassword);
        BounceView.addAnimTo(binding.rlyoutLogin);
        BounceView.addAnimTo(binding.tvSignup);
        binding.edtPassword.setHint(LanguageConstants.password_);
        binding.tvSignin.setText(LanguageConstants.Signin);
        binding.tvtoyourAccount.setText(LanguageConstants.toyouraccount);
        binding.edtemailAddress.setHint(LanguageConstants.enterEmail);
       // binding.txtView.setText(LanguageConstants.txtView);
        binding.llForgotPassword.setOnClickListener(this);
       // binding.txtView.setOnClickListener(this);
        binding.tvForgotPassword.setText(LanguageConstants.forgotPassword);
        binding.rlyoutLogin.setOnClickListener(this);
        binding.tvLogin.setText(LanguageConstants.Signin);
        binding.tvnewuser.setText(LanguageConstants.tvnewuser);
        binding.tvSignup.setText(LanguageConstants.signup);
        binding.tvSignup.setOnClickListener(this);
        binding.ivBackArrow.setOnClickListener(this);
        binding.fbbutton.setOnClickListener(this);
        binding.ivGmail.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (v == binding.llForgotPassword) {
            CommonFunctions.getInstance().newIntent(SignInActivity.this, ForgotPasswordActivity.class, Bundle.EMPTY, false);
        }
        else if (v == binding.rlyoutLogin) {
            if (binding.edtemailAddress.getText().toString().trim().isEmpty()) {
                CommonFunctions.getInstance().validationEmptyError(SignInActivity.this, LanguageConstants.emailFieldCantBlank);
            }
            else if (!CommonFunctions.getInstance().isValidEmail(binding.edtemailAddress.getText().toString().trim())) {
                CommonFunctions.getInstance().validationWarningError(SignInActivity.this, LanguageConstants.enterValidEmail);
            }
            else if (binding.edtPassword.getText().toString().trim().isEmpty()) {
                CommonFunctions.getInstance().validationEmptyError(SignInActivity.this, LanguageConstants.passwordFieldCantBlank);
            }
            else if (binding.edtPassword.getText().toString().trim().length() < 6) {
                CommonFunctions.getInstance().validationInfoError(SignInActivity.this, LanguageConstants.passwordMustBeContainsAleast6Characters);
            }
            else {

                Log.d("XXXXXXXXXXX", SessionManager.getInstance().getDeviceToken());
                loginApiCall();
            }
        }
        else if (v == binding.tvSignup) {
            CommonFunctions.getInstance().newIntent(SignInActivity.this, SignupActivity.class, Bundle.EMPTY, false);
        }
        /*
        else if (v == binding.txtView) {
            if (binding.edtPassword.getText().toString().trim().isEmpty()) {
                CommonFunctions.getInstance().validationEmptyError(SignInActivity.this, LanguageConstants.oldPasswordFieldCantBlank);
            } else {
                if (isPassword) {
                    binding.txtView.setText(LanguageConstants.view);
                    binding.edtPassword.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                    isPassword = false;
                    binding.edtPassword.setSelection(binding.edtPassword.getText().length());
                } else {
                    binding.txtView.setText(LanguageConstants.hide);
                    binding.edtPassword.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
                    isPassword = true;
                    binding.edtPassword.setSelection(binding.edtPassword.getText().length());
                }
            }
        }
        **/

        else if (v == binding.ivBackArrow) {
            finish();
        }
        else if (v == binding.ivGmail){
            Intent signInIntent = googleSignInClient.getSignInIntent();
            startActivityForResult(signInIntent, RC_SIGN_IN);
        }
        else if (v == binding.fbbutton){
            disconnectFromFacebook();
            FacebookSdk.sdkInitialize(getApplicationContext());
            //loginType = 2;
            fbLogin.performClick();
            fbLogin.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
                @Override
                public void onSuccess(LoginResult loginResult) {

                    GraphRequest GraphRequest = com.facebook.GraphRequest.newMeRequest(loginResult.getAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
                        @Override
                        public void onCompleted(JSONObject object, GraphResponse response) {
                            try {
                                String email = "";
                                String birthday = "";
                                String id = "";
                                String firstName = "";
                                String lastName = "";
                                String imageUrl = "https://graph.facebook.com/" + id + "/picture?type=large";

                                try {
                                    email = object.getString("email") == null ? "" : object.getString("email");
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }

                                try {
                                    firstName = object.getString("first_name") == null ? "" : object.getString("first_name");
                                    lastName = object.getString("last_name") == null ? "" : object.getString("last_name");
                                } catch (Exception ex) {
                                    ex.printStackTrace();
                                }

                                try {
                                    birthday = object.getString("birthday") == null ? "" : object.getString("birthday");
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }

                                try {
                                    id = object.getString("id") == null ? "" : object.getString("id");
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                AccessToken token = AccessToken.getCurrentAccessToken();
                                String mToken = null;
                                if (token != null) {
                                    mToken = token.getToken();
                                }
                                signInToTheApp(email, id, firstName, "2");

                            } catch (Exception e) {
                                e.printStackTrace();
                                LoginManager.getInstance().logOut();
                            }

                        }
                    });

                    Bundle parameters = new Bundle();
                    parameters.putString("fields", "id, first_name, last_name, email, gender, birthday");
                    GraphRequest.setParameters(parameters);
                    GraphRequest.executeAsync();

                }

                @Override
                public void onCancel() {
                    // App code
                }

                @Override
                public void onError(FacebookException exception) {
                    // App code
                }
            });
        }


    }

    private void signInToTheApp(String email, String id, String firstName,String type) {
        CommonApiCalls.getInstance().socialLogin(SignInActivity.this, email,
                id, firstName,CommonFunctions.DeviceType, SessionManager.getInstance().getDeviceToken(),type, new CommonCallback.Listener() {
                    @Override
                    public void onSuccess(Object body) {
                        SocialLoginApiResponse apiResponse = (SocialLoginApiResponse) body;
                        if (apiResponse.getStatus().equals(200)) {

                            if (apiResponse.getData() != null) {
                                SocialLoginApiResponse.Data data = apiResponse.getData();

                                CommonFunctions.getInstance().successResponseToast(SignInActivity.this, apiResponse.getMessage());
                                SessionManager.getInstance().setUserKey(data.getUser_key());
                                SessionManager.getInstance().setReferKey(data.getReferral_code());
                                SessionManager.getInstance().setAccessToken(data.getAccess_token());
                                SessionManager.getInstance().setUserName(data.getFirst_name());
                                SessionManager.getInstance().setUserEmail(data.getEmail());
                                SessionManager.getInstance().setUserMobileNumber(data.getMobile_number());
                                SessionManager.getInstance().setRedeemPoint(data.getReward_point() + "");
                                SessionManager.getInstance().setUserPic(data.getProfile_image());

                                Intent i = new Intent(SignInActivity.this, HomeActivity.class);
                                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(i);
                            }
                        }
                    }

                    @Override
                    public void onFailure(String reason) {
                        CommonFunctions.getInstance().validationInfoError(SignInActivity.this, reason);
                    }
                });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        callbackManager.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RC_SIGN_IN) {
            try {
                Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
                GoogleSignInAccount account = task.getResult(ApiException.class);
                String idToken = account.getIdToken();
                String email = account.getEmail();
                String id = account.getId();
                String firstName = account.getDisplayName();

                signInToTheApp(email, id, firstName, "3");
            } catch (ApiException e) {
                // The ApiException status code indicates the detailed failure reason.
                Log.w(TAG, "signInResult:failed code=" + e.getStatusCode());
            }
        }
    }

    public void disconnectFromFacebook() {
        if (AccessToken.getCurrentAccessToken() == null) {
            return; // already logged out
        }
        new GraphRequest(AccessToken.getCurrentAccessToken(), "/me/permissions/", null, HttpMethod.DELETE, new GraphRequest
                .Callback() {
            @Override
            public void onCompleted(GraphResponse graphResponse) {
                LoginManager.getInstance().logOut();

            }
        }).executeAsync();
    }


    // --- Login Api Call
    private void loginApiCall() {
        Log.i("ffgfgfg", "loginApiCall: "+SessionManager.getInstance().getDeviceToken());
        CommonApiCalls.getInstance().Login(SignInActivity.this, binding.edtemailAddress.getText().toString().trim(),
                binding.edtPassword.getText().toString(), CommonFunctions.DeviceType, SessionManager.getInstance().getDeviceToken(), new CommonCallback.Listener() {
                    @Override
                    public void onSuccess(Object body) {
                        LoginApiResponse apiResponse = (LoginApiResponse) body;
                        LoginApiResponse.Data data = apiResponse.getData();
                        CommonFunctions.getInstance().successResponseToast(SignInActivity.this, apiResponse.getMessage());

                        SessionManager.getInstance().setUserKey(data.getUserKey());
                        SessionManager.getInstance().setReferKey(data.getReferral_code());
                        SessionManager.getInstance().setAccessToken(data.getAccessToken());
                        Log.d("ggggggggggggg", data.getAccessToken() + " 1111");
                        SessionManager.getInstance().setUserName(data.getFirstName());
                        SessionManager.getInstance().setUserEmail(data.getEmail());
                        SessionManager.getInstance().setUserMobileNumber(data.getMobileNumber());
                        SessionManager.getInstance().setRedeemPoint(data.getReward_point());
                        SessionManager.getInstance().setUserPic(data.getProfileImage());
                        Intent i = new Intent(SignInActivity.this, HomeActivity.class);
                        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(i);
                    }

                    @Override
                    public void onFailure(String reason) {

                    }
                });
    }

    @Override
    protected void onStart() {
        super.onStart();

    }


}
