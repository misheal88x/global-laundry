package com.globallaundry.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.globallaundry.R;
import com.globallaundry.adapters.PackagesAdapter;
import com.globallaundry.api.CommonApiCalls;
import com.globallaundry.apimodel.GetAvailablePackagesApiResponse;
import com.globallaundry.interfaces.CommonCallback;
import com.globallaundry.utils.MyApplication;
import com.globallaundry.utils.SessionManager;

public class AllAvailablePackagesActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener {

    RecyclerView recyclerView;
    SwipeRefreshLayout refreshLayout;
    TextView tlbarText;
    ImageView backArrow;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_all_available_packages);


        initUI();



    }

    private void initUI() {


        tlbarText = findViewById(R.id.tlbarText);
        recyclerView = findViewById(R.id.all_packages_id);
        refreshLayout = findViewById(R.id.all_packages_refresh_id);
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        refreshLayout.setOnRefreshListener(this);

        backArrow = findViewById(R.id.ivBackArrow);

        if (SessionManager.getInstance().getAppLanguageCode().equals("ar")) {
            getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
            backArrow.setImageResource(R.drawable.ic_right_arrow);
            tlbarText.setText("الحزم المتاحة للإشتراك");
        } else {
            getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
            tlbarText.setText("Available Packages");
            getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
            backArrow.setImageResource(R.drawable.ic_back_arrow);
        }

        backArrow.setOnClickListener(view -> finish());


        loadData();
    }

    private void loadData() {
        CommonApiCalls.getInstance().getAvailablePackages(this, new CommonCallback.Listener() {
            @Override
            public void onSuccess(Object body) {
                GetAvailablePackagesApiResponse response = (GetAvailablePackagesApiResponse) body;
                PackagesAdapter adapter = new PackagesAdapter(AllAvailablePackagesActivity.this, response.getData(), true);
                recyclerView.setAdapter(adapter);
                refreshLayout.setRefreshing(false);
            }

            @Override
            public void onFailure(String reason) {
                MyApplication.displayKnownError(reason);
                refreshLayout.setRefreshing(false);
            }
        });
    }

    @Override
    public void onRefresh() {
        loadData();
    }
}
