package com.globallaundry.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.graphics.Color;
import android.os.Bundle;
import android.text.InputType;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.view.Gravity;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.globallaundry.R;
import com.globallaundry.databinding.ActivityWelcomeBinding;
import com.globallaundry.utils.CommonFunctions;
import com.globallaundry.utils.LanguageConstants;
import com.globallaundry.utils.SessionManager;

import java.text.MessageFormat;

import hari.bounceview.BounceView;

public class WelcomeActivity extends AppCompatActivity implements View.OnClickListener {
    ActivityWelcomeBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_welcome);
        if (SessionManager.getInstance().getAppLanguageCode().equals("ar")) {
            getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
            binding.edtTitle.setGravity(Gravity.RIGHT);
        } else {
            getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
            binding.edtTitle.setGravity(Gravity.LEFT);
        }
        initView();
    }

    private void initView() {
        BounceView.addAnimTo(binding.llContinue);
        binding.tvWelcome.setText(LanguageConstants.welcome);
        binding.tvContinue.setText(LanguageConstants.contiunetxt);
        binding.tvEmailPhone.setHint(LanguageConstants.enterValidEmail);
        binding.llContinue.setOnClickListener(this);
        SpannableString ss = new SpannableString(CommonFunctions.fromHtml(MessageFormat.format(LanguageConstants.enterphoneemail,
                "<font color=\"#f28421\">" + LanguageConstants.email + "</font>",
                "<font color=\"#f28421\">" + LanguageConstants.phonenumber + "</font>")));
        ClickableSpan tAndcSpan = new ClickableSpan() {
            @Override
            public void onClick(View textView) {
                binding.edtTitle.setInputType(InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);
                binding.tvEmailPhone.setHint(LanguageConstants.enterValidEmail);
            }

            @Override
            public void updateDrawState(TextPaint ds) {
                super.updateDrawState(ds);
                ds.setUnderlineText(false);

            }
        };

        String val = CommonFunctions.fromHtml(MessageFormat.format(LanguageConstants.enterphoneemail, LanguageConstants.email,
                LanguageConstants.phonenumber)).toString();

        Integer startTerms = val.indexOf(LanguageConstants.email);
        Integer endTerms = startTerms + LanguageConstants.email.length();
        ss.setSpan(tAndcSpan, startTerms, endTerms, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        ClickableSpan privacySpan = new ClickableSpan() {
            @Override
            public void onClick(View textView) {
                binding.edtTitle.setInputType(InputType.TYPE_CLASS_NUMBER);
                binding.tvEmailPhone.setHint(LanguageConstants.phonenumber);
            }

            @Override
            public void updateDrawState(TextPaint ds) {
                super.updateDrawState(ds);
                ds.setUnderlineText(false);
            }
        };

        Integer startPrivacy = val.indexOf(LanguageConstants.phonenumber);
        Integer endPrivacy = startPrivacy + LanguageConstants.phonenumber.length();
        ss.setSpan(privacySpan, startPrivacy, endPrivacy, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        binding.tvEmailPhone.setText(ss);
        binding.tvEmailPhone.setMovementMethod(LinkMovementMethod.getInstance());


    }

    @Override
    public void onClick(View v) {
        if (v == binding.llContinue) {
            if (binding.edtTitle.getText().toString().trim().isEmpty()) {
                CommonFunctions.getInstance().validationEmptyError(WelcomeActivity.this, LanguageConstants.enterphoneemailtxt);
            } else {
                CommonFunctions.getInstance().newIntent(WelcomeActivity.this, SignInActivity.class, Bundle.EMPTY, false);
            }
        }

    }
}
