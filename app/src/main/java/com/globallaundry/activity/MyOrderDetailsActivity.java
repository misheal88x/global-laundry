package com.globallaundry.activity;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.globallaundry.R;
import com.globallaundry.adapters.CartSummaryAdapter;
import com.globallaundry.adapters.SelectAddressAdapter;
import com.globallaundry.adapters.SelectChangeAddressAdapter;
import com.globallaundry.api.CommonApiCalls;
import com.globallaundry.apimodel.AddRatingApiResponse;
import com.globallaundry.apimodel.ChangeAddressApiResponse;
import com.globallaundry.apimodel.ChangeDeliveryAddressApiResponse;
import com.globallaundry.apimodel.OrderDetailsApiResponse;
import com.globallaundry.databinding.ActivityMyOrderDetailsBinding;
import com.globallaundry.interfaces.CommonCallback;
import com.globallaundry.interfaces.SelectArea;
import com.globallaundry.utils.CommonFunctions;
import com.globallaundry.utils.LanguageConstants;
import com.globallaundry.utils.SessionManager;

import java.util.List;


public class MyOrderDetailsActivity extends AppCompatActivity implements View.OnClickListener {


    String mOrderKey = "";
    ActivityMyOrderDetailsBinding binding;
    private String isChangeAddressKey = "";
    private String selectAreaName = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_my_order_details);

        if (SessionManager.getInstance().getAppLanguageCode().equals("ar")) {
            getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
            binding.ivBackArrow.setImageResource(R.drawable.ic_right_arrow);
            binding.tvCustomerName.setGravity(Gravity.END);
            binding.tvPickupAddress.setGravity(Gravity.END);
        } else {
            getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
            binding.ivBackArrow.setImageResource(R.drawable.ic_back_arrow);
            binding.tvCustomerName.setGravity(Gravity.START);
            binding.tvPickupAddress.setGravity(Gravity.START);
        }

        mOrderKey = getIntent().getStringExtra("key");
        isChangeAddressKey = getIntent().getStringExtra("ISCHANGEADDRESSKEY");

        initView();

        CommonApiCalls.getInstance().orderDetails(MyOrderDetailsActivity.this, mOrderKey, new CommonCallback.Listener() {
            @Override
            public void onSuccess(Object body) {
                OrderDetailsApiResponse mOrderListApiResponse = (OrderDetailsApiResponse) body;
                OrderDetailsApiResponse.Data data = mOrderListApiResponse.getData();

                LoadOrderItems(data.getItems());

                pickAddressDetails(data.getPickup_address());
                deliverAddressDetails(data.getDelivery_address());
                deliveryTimeSlot(data.getDelivery_datetime());
                pickupTimeSlot(data.getPickup_datetime());
                binding.tvCustomerName.setText(data.getCustomer_name());
                binding.tvTotal.setText(data.getOrder_total());
                //binding.tvTotal.setText(data.getOrder_total() + "");
                for (int count = 0; count < data.getDriver_comments().size(); count++) {
                    if (data.getDriver_comments().get(count).getTask_type().equals("1")) {
                        binding.txtPickupDriverCommentValue.setText(data.getDriver_comments().get(count).getComment());
                    } else if (data.getDriver_comments().get(count).getTask_type().equals("2")) {
                        binding.txtCollectionDriverCommentValue.setText(data.getDriver_comments().get(count).getComment());
                    }
                }

                if (binding.txtPickupDriverCommentValue.getText().toString().isEmpty()) {
                    binding.txtPickupDriverCommentValue.setVisibility(View.GONE);
                    binding.txtPickupDriverComment.setVisibility(View.GONE);
                }

                if (binding.txtCollectionDriverCommentValue.getText().toString().isEmpty()) {
                    binding.txtCollectionDriverCommentValue.setVisibility(View.GONE);
                    binding.txtCollectionDriverComment.setVisibility(View.GONE);
                }

                if (data.getOrder_status().equals(5)){
                    if (data.getIs_rated().equals(1)){
                        binding.rlrateorder.setVisibility(View.GONE);
                    }
                    else {
                        binding.rlrateorder.setVisibility(View.VISIBLE);
                    }
                    binding.imgEdit.setVisibility(View.GONE);
                }
                else {
                    binding.rlrateorder.setVisibility(View.GONE);
                    binding.imgEdit.setVisibility(View.VISIBLE);

                    if (isChangeAddressKey.equalsIgnoreCase("1")) {
                        binding.imgEdit.setVisibility(View.VISIBLE);
                    } else {
                        binding.imgEdit.setVisibility(View.GONE);
                    }
                }
            }

            @Override
            public void onFailure(String reason) {
                CommonFunctions.getInstance().validationInfoError(MyOrderDetailsActivity.this, reason);
            }
        });
    }

    @SuppressLint("SetTextI18n")
    private void pickupTimeSlot(OrderDetailsApiResponse.Pickup_datetime pickup_datetime) {
        binding.tvPickupTimeSlot.setText(pickup_datetime.getDate() + "\n" + pickup_datetime.getTime());
    }

    @SuppressLint("SetTextI18n")
    private void deliveryTimeSlot(OrderDetailsApiResponse.Delivery_datetime delivery_datetime) {
        binding.tvDeliveryTimeSlot.setText(delivery_datetime.getDate() + "\n" + delivery_datetime.getTime());
    }

    private void deliverAddressDetails(OrderDetailsApiResponse.Delivery_address delivery_address) {
        String address = "";
        if (delivery_address.getFlat_no() != null && !delivery_address.getFlat_no().isEmpty()) {
            address = address + delivery_address.getFlat_no();
        }

        if (!address.isEmpty() && delivery_address.getApartment() != null && !delivery_address.getApartment().isEmpty()) {
            address = address + "," + delivery_address.getApartment();
        } else if (address.isEmpty() && delivery_address.getApartment() != null && !delivery_address.getApartment().isEmpty()) {
            address = delivery_address.getApartment();
        }

        if (!address.isEmpty() && delivery_address.getStreet_name() != null && !delivery_address.getApartment().isEmpty()) {
            address = address + "," + delivery_address.getStreet_name();
        } else if (address.isEmpty() && delivery_address.getStreet_name() != null && !delivery_address.getStreet_name().isEmpty()) {
            address = delivery_address.getStreet_name();
        }

        if (!address.isEmpty() && delivery_address.getLandmark() != null && !delivery_address.getLandmark().isEmpty()) {
            address = address + "," + delivery_address.getLandmark();
        } else if (address.isEmpty() && delivery_address.getLandmark() != null && !delivery_address.getLandmark().isEmpty()) {
            address = delivery_address.getLandmark();
        }


        if (!address.isEmpty() && delivery_address.getCompany() != null && !delivery_address.getCompany().isEmpty()) {
            address = address + "," + delivery_address.getCompany();
        } else if (address.isEmpty() && delivery_address.getCompany() != null && !delivery_address.getCompany().isEmpty()) {
            address = delivery_address.getCompany();
        }

        if (!address.isEmpty() && delivery_address.getArea_name() != null && !delivery_address.getArea_name().isEmpty()) {
            address = address + "," + delivery_address.getArea_name();
        } else if (address.isEmpty() && delivery_address.getArea_name() != null && !delivery_address.getArea_name().isEmpty()) {
            address = delivery_address.getArea_name();
        }

        if (!address.isEmpty() && delivery_address.getCity_name() != null && !delivery_address.getCity_name().isEmpty()) {
            address = address + "," + delivery_address.getCity_name();
        } else if (address.isEmpty() && delivery_address.getCity_name() != null && !delivery_address.getCity_name().isEmpty()) {
            address = delivery_address.getCity_name();
        }

        binding.tvDeliveryAddress.setEllipsize(TextUtils.TruncateAt.MARQUEE);
        binding.tvDeliveryAddress.setText(address);
        binding.tvDeliveryAddress.setSelected(true);
        binding.tvDeliveryAddress.setSingleLine(true);

    }

    private void pickAddressDetails(OrderDetailsApiResponse.Pickup_address pickup_address) {

        String address = "";
        if (pickup_address.getFlat_no() != null && !pickup_address.getFlat_no().isEmpty()) {
            address = address + pickup_address.getFlat_no();
        }

        if (!address.isEmpty() && pickup_address.getApartment() != null && !pickup_address.getApartment().isEmpty()) {
            address = address + "," + pickup_address.getApartment();
        } else if (address.isEmpty() && pickup_address.getApartment() != null && !pickup_address.getApartment().isEmpty()) {
            address = pickup_address.getApartment();
        }

        if (!address.isEmpty() && pickup_address.getStreet_name() != null && !pickup_address.getApartment().isEmpty()) {
            address = address + "," + pickup_address.getStreet_name();
        } else if (address.isEmpty() && pickup_address.getStreet_name() != null && !pickup_address.getStreet_name().isEmpty()) {
            address = pickup_address.getStreet_name();
        }

        if (!address.isEmpty() && pickup_address.getLandmark() != null && !pickup_address.getLandmark().isEmpty()) {
            address = address + "," + pickup_address.getLandmark();
        } else if (address.isEmpty() && pickup_address.getLandmark() != null && !pickup_address.getLandmark().isEmpty()) {
            address = pickup_address.getLandmark();
        }


        if (!address.isEmpty() && pickup_address.getCompany() != null && !pickup_address.getCompany().isEmpty()) {
            address = address + "," + pickup_address.getCompany();
        } else if (address.isEmpty() && pickup_address.getCompany() != null && !pickup_address.getCompany().isEmpty()) {
            address = pickup_address.getCompany();
        }

        if (!address.isEmpty() && pickup_address.getArea_name() != null && !pickup_address.getArea_name().isEmpty()) {
            address = address + "," + pickup_address.getArea_name();
        } else if (address.isEmpty() && pickup_address.getArea_name() != null && !pickup_address.getArea_name().isEmpty()) {
            address = pickup_address.getArea_name();
        }

        if (!address.isEmpty() && pickup_address.getCity_name() != null && !pickup_address.getCity_name().isEmpty()) {
            address = address + "," + pickup_address.getCity_name();
        } else if (address.isEmpty() && pickup_address.getCity_name() != null && !pickup_address.getCity_name().isEmpty()) {
            address = pickup_address.getCity_name();
        }

        binding.tvPickupAddress.setEllipsize(TextUtils.TruncateAt.MARQUEE);
        binding.tvPickupAddress.setText(address);
        binding.tvPickupAddress.setSelected(true);
        binding.tvPickupAddress.setSingleLine(true);

    }


    private void initView() {
        binding.tlbarText.setText(LanguageConstants.OrderDetails);
        binding.txtOrderCost.setText(LanguageConstants.OrderCost);
        binding.txtPickupAddress.setText(LanguageConstants.pickup);
        binding.txtPickupTimeSlot.setText(LanguageConstants.PickupTimeSlot);
        binding.txtDeliveryAddress.setText(LanguageConstants.DeliveryAddress);
        binding.txtDeliveryTimeSlot.setText(LanguageConstants.DeliveryTimeSlot);
        //binding.txtPickupDriverComment.setText(LanguageConstants.collectionDriverComment);
        //binding.txtCollectionDriverComment.setText(LanguageConstants.pickupDriverComment);
        binding.tvRateOrder.setText(LanguageConstants.rating);

        binding.rlrateorder.setOnClickListener(this);
        binding.imgEdit.setOnClickListener(this);
        binding.ivBackArrow.setOnClickListener(this);
    }


    private void LoadOrderItems(List<OrderDetailsApiResponse.Item> items) {
        CartSummaryAdapter adapter = new CartSummaryAdapter(MyOrderDetailsActivity.this, items, 1);
        binding.rvCartList.setAdapter(adapter);
        binding.rvCartList.setNestedScrollingEnabled(false);
        LinearLayoutManager horizontalLayoutManagaer = new LinearLayoutManager(MyOrderDetailsActivity.this, RecyclerView.VERTICAL, false);
        binding.rvCartList.setLayoutManager(horizontalLayoutManagaer);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ivBackArrow:
                finish();
                break;
            case R.id.imgEdit:
                callAddressApi(mOrderKey);
                break;
            case R.id.rlrateorder:
                showRatingDialog();
                break;
            default:
                break;
        }
    }

    private void showRatingDialog() {
        final Dialog dialog = new Dialog(MyOrderDetailsActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.rating_dialog);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        TextView tvQue = dialog.findViewById(R.id.ratingtitle);
        TextView tvReviewDesc = dialog.findViewById(R.id.tvReviewDesc);
        RatingBar rbRating = dialog.findViewById(R.id.rbRating);
        Button btnCancel = dialog.findViewById(R.id.btnLater);
        Button btnYes = dialog.findViewById(R.id.btnUpdate);
        btnCancel.setText(LanguageConstants.cancel);
        btnYes.setText(LanguageConstants.Yes);
        tvQue.setTextSize(13);
        tvQue.setText(LanguageConstants.add_your_rating);
        tvReviewDesc.setHint(LanguageConstants.txt_enter_comments);
        btnYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!SessionManager.getInstance().getUserKey().isEmpty()) {
                    if (rbRating.getRating() <= 0.0) {
                        Toast.makeText(MyOrderDetailsActivity.this, LanguageConstants.add_your_rating, Toast.LENGTH_SHORT).show();
                        return;
                    }
                    if (tvReviewDesc.getText().toString().trim().isEmpty()) {
                        Toast.makeText(MyOrderDetailsActivity.this, LanguageConstants.txt_enter_comments, Toast.LENGTH_SHORT).show();
                        return;
                    }
                    CommonApiCalls.getInstance().AddRatingApi(MyOrderDetailsActivity.this, mOrderKey, String.valueOf(rbRating.getRating()), tvReviewDesc.getText().toString().trim(), new CommonCallback.Listener() {
                        @Override
                        public void onSuccess(Object object) {
                            AddRatingApiResponse addRatingResponse = (AddRatingApiResponse) object;
                            if (addRatingResponse.getStatus().equals(200)) {
                                CommonFunctions.getInstance().successResponseToast(MyOrderDetailsActivity.this, addRatingResponse.getMessage());
                            } else {

                            }
                        }

                        @Override
                        public void onFailure(String reason) {

                        }
                    });

                } else {
                    CommonFunctions.getInstance().validationEmptyError(MyOrderDetailsActivity.this, LanguageConstants.login);
                }
                dialog.dismiss();
            }
        });
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        Window window = dialog.getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();
        wlp.gravity = Gravity.CENTER;
        window.setAttributes(wlp);
        dialog.show();
    }

    private void callAddressApi(String mOrderKey) {
        CommonApiCalls.getInstance().changeOrderDeliveryAddress(MyOrderDetailsActivity.this, mOrderKey, new CommonCallback.Listener() {
            @Override
            public void onSuccess(Object body) {
                ChangeDeliveryAddressApiResponse changeAddressApiResponse = (ChangeDeliveryAddressApiResponse) body;
                if (changeAddressApiResponse.getStatus().equals(200)) {
                    CommonFunctions.getInstance().successResponseToast(MyOrderDetailsActivity.this, changeAddressApiResponse.getMessage());
                    selectAreaDialog(changeAddressApiResponse);
                } else {

                }
            }

            @Override
            public void onFailure(String reason) {

            }
        });
    }

    private void selectAreaDialog(ChangeDeliveryAddressApiResponse responses) {
        final Dialog dialog = new Dialog(MyOrderDetailsActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_select_area);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);

        ImageView imgClose = (ImageView) dialog.findViewById(R.id.imgClose);
        RecyclerView recyclerView = (RecyclerView) dialog.findViewById(R.id.recyclerView);


        if (responses.getData() != null && responses.getData().size() > 0) {
            @SuppressLint("WrongConstant")
            LinearLayoutManager layoutManager = new LinearLayoutManager(MyOrderDetailsActivity.this, LinearLayoutManager.VERTICAL, false);
            recyclerView.setLayoutManager(layoutManager);
            SelectChangeAddressAdapter adapter = new SelectChangeAddressAdapter(MyOrderDetailsActivity.this, responses.getData(), new SelectArea() {
                @Override
                public void onClick(int id, String name) {
                    dialog.dismiss();
                    callChangeAddressApi(mOrderKey, name);
                }
            });
            recyclerView.setAdapter(adapter);
        }

        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    private void callChangeAddressApi(String order_key, String selectAreaName) {
        CommonApiCalls.getInstance().changeDeliveryAddress(MyOrderDetailsActivity.this, order_key, selectAreaName, new CommonCallback.Listener() {
            @Override
            public void onSuccess(Object body) {
                ChangeAddressApiResponse changeAddressApiResponse = (ChangeAddressApiResponse) body;
                if (changeAddressApiResponse.getStatus().equals(200)) {
                    CommonFunctions.getInstance().successResponseToast(MyOrderDetailsActivity.this, changeAddressApiResponse.getMessage());
                    if (changeAddressApiResponse.getData()!= null){
                        binding.tvPickupAddress.setEllipsize(TextUtils.TruncateAt.MARQUEE);
                        binding.tvPickupAddress.setText(changeAddressApiResponse.getData().getStreet_name());
                        binding.tvPickupAddress.setSelected(true);
                        binding.tvPickupAddress.setSingleLine(true);
                    }
                }
            }

            @Override
            public void onFailure(String reason) {

            }
        });
    }
}
