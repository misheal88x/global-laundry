package com.globallaundry.activity;


import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;

import android.telephony.PhoneNumberUtils;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import com.alimuzaffar.lib.pin.PinEntryEditText;
import com.globallaundry.R;
import com.globallaundry.api.CommonApiCalls;
import com.globallaundry.apimodel.ResendOTPApiResponse;
import com.globallaundry.apimodel.VerifyOTPApiResponse;
import com.globallaundry.databinding.ActivityOtpverificationBinding;
import com.globallaundry.interfaces.CommonCallback;
import com.globallaundry.models.UpdateVerificationStatusApiResponse;
import com.globallaundry.utils.CommonFunctions;
import com.globallaundry.utils.LanguageConstants;
import com.globallaundry.utils.SessionManager;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;

import java.util.concurrent.TimeUnit;


public class OtpVerificationActivity extends AppCompatActivity implements View.OnClickListener {

    private int i = 0;

    private long milli = 30000;
    private CountDownTimer mCountDownTimer;

    String otp_id = "";
    String type = "";
    String user_key = "";
    String phonenumber = "";
    ActivityOtpverificationBinding binding;


    FirebaseAuth auth;
    PhoneAuthProvider.OnVerificationStateChangedCallbacks mCallbacks;
    private String verificationCode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_otpverification);
        if (SessionManager.getInstance().getAppLanguageCode().equals("ar")) {
            getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
            binding.ivBackArrow.setImageResource(R.drawable.ic_right_arrow);
        } else {
            getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
            binding.ivBackArrow.setImageResource(R.drawable.ic_back_arrow);
        }


        user_key = getIntent().getStringExtra("user_key");
        type = getIntent().getStringExtra("TYPE");
        phonenumber = getIntent().getStringExtra("mobile_number");

        if (phonenumber.substring(0,3).contains("961")){
            phonenumber = phonenumber.replace("961","");
        }

        initViews();

        FirebaseUser user= FirebaseAuth.getInstance().getCurrentUser();
        StartFirebaseLogin();
        try {
        }catch (Exception e){
            Toast.makeText(this,"Phone number not found",Toast.LENGTH_SHORT).show();
        }
        getOtp("+961 "+phonenumber);


        binding.firstBar.setProgress(i);
        mCountDownTimer = new CountDownTimer(milli, 1000) {

            @Override
            public void onTick(long millisUntilFinished) {
                Log.v("Log_tag", "Tick of Progress" + i + millisUntilFinished);
                i++;
                binding.firstBar.setProgress((int) i * 100 / (60000 / 1000));

            }

            @Override
            public void onFinish() {
                i++;
                binding.llResend.setVisibility(View.VISIBLE);
                binding.firstBar.setProgress(100);
            }
        };
        mCountDownTimer.start();

        if (binding.txtPinEntry != null) {
            binding.txtPinEntry.setOnPinEnteredListener(new PinEntryEditText.OnPinEnteredListener() {
                @Override
                public void onPinEntered(CharSequence str) {
                    if (str.toString().length() == 6) {

                       // callVerifyOTPApi();

                         PhoneAuthCredential credential = PhoneAuthProvider.getCredential(verificationCode, binding.txtPinEntry.getText().toString().trim());
                         SigninWithPhone(credential);

                    } else {
                        binding.txtPinEntry.setText(null);
                    }
                }
            });
        }
    }



    private void callVerifyOTPApi() {

        CommonApiCalls.getInstance().verifyOTP(OtpVerificationActivity.this, binding.txtPinEntry.getText().toString(),
                otp_id,
                CommonFunctions.DeviceType,
                SessionManager.getInstance().getDeviceToken(),
                type, phonenumber,
                new CommonCallback.Listener() {
                    @Override
                    public void onSuccess(Object object) {
                        if (type.equals("1")) {
                            VerifyOTPApiResponse body = (VerifyOTPApiResponse) object;
                            VerifyOTPApiResponse.Data data = body.getData();

                            SessionManager.getInstance().setUserKey(data.getUserKey());
                            SessionManager.getInstance().setReferKey(data.getReferral_code());
                            SessionManager.getInstance().setUserName(data.getFirstName());
                            SessionManager.getInstance().setUserName(data.getFirstName());
                            SessionManager.getInstance().setUserEmail(data.getEmail());
                            SessionManager.getInstance().setUserMobileNumber(data.getMobileNumber());
                            SessionManager.getInstance().setUserPic(data.getProfileImage());
                            SessionManager.getInstance().setAccessToken(data.getAccessToken());
                            SessionManager.getInstance().setRedeemPoint(data.getReward_point());
                            Intent i = new Intent(OtpVerificationActivity.this, HomeActivity.class);
                            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(i);

                        } else {
                            VerifyOTPApiResponse body = (VerifyOTPApiResponse) object;
                            VerifyOTPApiResponse.Data data = body.getData();
                            Bundle bundle = new Bundle();
                            bundle.putString("RESETLINK", data.getReset_token());
                            CommonFunctions.getInstance().newIntent(OtpVerificationActivity.this, ResetPasswordActivity.class, bundle, true);
                        }
                    }

                    @Override
                    public void onFailure(String reason) {

                    }
                }
        );
    }

    private void callResendOTP() {

        CommonApiCalls.getInstance().resendOTP(OtpVerificationActivity.this,
                otp_id,
                new CommonCallback.Listener() {
                    @Override
                    public void onSuccess(Object object) {
                        ResendOTPApiResponse body = (ResendOTPApiResponse) object;
                        ResendOTPApiResponse.Data data = body.getData();

                    }

                    @Override
                    public void onFailure(String reason) {

                    }
                }
        );
    }

    private void initViews() {
        binding.txtVerification.setText(LanguageConstants.Verification);
        binding.tvEnterFourDigitCode.setText(LanguageConstants.enterFourDigitCode);
        binding.tvResendOTPMessage.setText(LanguageConstants.youmayRequestAnothercode);
        binding.tvDidntReceiveTheCall.setText(LanguageConstants.DidreceivetheCode);
        binding.tvRequestACall.setText(LanguageConstants.ResendCode);
        binding.ivBackArrow.setOnClickListener(this);
        binding.llResend.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        if (v == binding.ivBackArrow) {
            finish();
        } else if (v == binding.llResend) {
            //callResendOTP();
            getOtp("+961 "+phonenumber);
        }

    }


 // -------------------

    private void getOtp(String phoneNumber) {
        phoneNumber = phoneNumber;
       boolean b =  PhoneNumberUtils.isGlobalPhoneNumber(phoneNumber);

        PhoneAuthProvider.getInstance().verifyPhoneNumber(
                phoneNumber,                     // Phone number to verify
                60,                           // Timeout duration
                TimeUnit.SECONDS,                // Unit of timeout
                OtpVerificationActivity.this,        // Activity (for callback binding)
                mCallbacks);
    }

    private void StartFirebaseLogin() {

        auth = FirebaseAuth.getInstance();
        mCallbacks = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {

            @Override
            public void onVerificationCompleted(PhoneAuthCredential phoneAuthCredential) {
                Toast.makeText(OtpVerificationActivity.this,"Verification completed",Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onVerificationFailed(FirebaseException e) {
                Toast.makeText(OtpVerificationActivity.this,"Verification failed",Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onCodeSent(String s, PhoneAuthProvider.ForceResendingToken forceResendingToken) {
                super.onCodeSent(s, forceResendingToken);
                verificationCode = s;
                Toast.makeText(OtpVerificationActivity.this,"OTP Code sent to your phone number",Toast.LENGTH_SHORT).show();
            }
        };
    }


    private void SigninWithPhone(PhoneAuthCredential credential) {
        auth.signInWithCredential(credential)
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            Toast.makeText(OtpVerificationActivity.this, "OTP verified", Toast.LENGTH_SHORT).show();
                            verifiedOtpApiCall();
                        } else {
                            Toast.makeText(OtpVerificationActivity.this,"Incorrect OTP",Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }

    private void verifiedOtpApiCall() {
        CommonApiCalls.getInstance().verifiedOTP(OtpVerificationActivity.this, user_key, new CommonCallback.Listener() {
                    @Override
                    public void onSuccess(Object object) {
                        if (type.equals("1")) {
                            UpdateVerificationStatusApiResponse body = (UpdateVerificationStatusApiResponse) object;
                            UpdateVerificationStatusApiResponse.Data data = body.getData();

                            SessionManager.getInstance().setUserKey(data.getUser_key());
                            SessionManager.getInstance().setReferKey(data.getReferral_code());
                            SessionManager.getInstance().setUserName(data.getFirst_name());
                            SessionManager.getInstance().setUserEmail(data.getEmail());
                            SessionManager.getInstance().setUserMobileNumber(data.getMobile_number());
                            SessionManager.getInstance().setUserPic(data.getProfile_image());
                            SessionManager.getInstance().setAccessToken(data.getAccess_token());
                            SessionManager.getInstance().setRedeemPoint(data.getReward_point());
                            Intent i = new Intent(OtpVerificationActivity.this, HomeActivity.class);
                            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(i);

                        } else {
                            VerifyOTPApiResponse body = (VerifyOTPApiResponse) object;
                            VerifyOTPApiResponse.Data data = body.getData();
                            Bundle bundle = new Bundle();
                            bundle.putString("RESETLINK", data.getReset_token());
                            CommonFunctions.getInstance().newIntent(OtpVerificationActivity.this, ResetPasswordActivity.class, bundle, true);
                        }
                    }

                    @Override
                    public void onFailure(String reason) {
                        Toast.makeText(OtpVerificationActivity.this,reason,Toast.LENGTH_SHORT).show();

                    }
                }
        );
    }


}
