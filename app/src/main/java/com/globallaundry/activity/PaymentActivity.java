package com.globallaundry.activity;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.RadioGroup;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.globallaundry.R;
import com.globallaundry.adapters.PaymentDetailsAdapter;
import com.globallaundry.adapters.SelectAreaAdapter;
import com.globallaundry.api.CommonApiCalls;
import com.globallaundry.apimodel.CalculateApiResponse;
import com.globallaundry.apimodel.GetPaymentMethodsAPIResponse;
import com.globallaundry.apimodel.GetProfileApiResponse;
import com.globallaundry.apimodel.GetSubscribedItemsTypes;
import com.globallaundry.apimodel.PlaceOrderApiResponse;
import com.globallaundry.app_model.CheckoutModel;
import com.globallaundry.databinding.ActivityPaymentBinding;
import com.globallaundry.dbmodel.ItemRealmList;
import com.globallaundry.dbmodel.RealmLibrary;
import com.globallaundry.interfaces.CommonCallback;
import com.globallaundry.interfaces.SelectArea;
import com.globallaundry.models.CacheManager;
import com.globallaundry.models.SelectAreaApiResponse;
import com.globallaundry.utils.CommonFunctions;
import com.globallaundry.utils.LanguageConstants;
import com.globallaundry.utils.SessionManager;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import hari.bounceview.BounceView;
import io.realm.RealmResults;

public class PaymentActivity extends AppCompatActivity implements View.OnClickListener {
    ActivityPaymentBinding binding;
    public static String selectAreaName = "";
    Integer paymentType = 0;
    Integer currencyType = 1;
    String userAddressKey = "";
    private CheckoutModel checkoutModel;
    Integer isredeemPointsUse;
    List<CalculateApiResponse.Payment_detail> paymentDetails;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_payment);
        if (SessionManager.getInstance().getAppLanguageCode().equals("ar")) {
            getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
            binding.ivBackArrow.setImageResource(R.drawable.ic_right_arrow);
            binding.tvSelectArea.setGravity(Gravity.RIGHT);
        } else {
            getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
            binding.ivBackArrow.setImageResource(R.drawable.ic_back_arrow);
            binding.tvSelectArea.setGravity(Gravity.LEFT);
        }
        if (getIntent().getExtras() != null) {
            userAddressKey = getIntent().getExtras().getString("USERADDRESSKEY");
        }
        initView();
        checkoutModel = new CheckoutModel();
    }

    @SuppressLint("SetTextI18n")
    private void initView() {
        BounceView.addAnimTo(binding.ivBackArrow);
        BounceView.addAnimTo(binding.lyoutSelectArea);
        BounceView.addAnimTo(binding.lyoutSelectArea);
        binding.tlbarText.setText(LanguageConstants.payment);
        binding.tvPaymenttxt.setText(LanguageConstants.paymentmethod);
        binding.tvSelectArea.setText(LanguageConstants.customerrights);
        binding.orderplacetxt.setText(LanguageConstants.orderplacetxt);
        binding.rdCash.setText(LanguageConstants.cash);
        binding.rdVisa.setText(LanguageConstants.rdVisa);
        binding.tvcouponwallet.setText(LanguageConstants.choosecoupon);
        binding.tvSelectArea.setText(LanguageConstants.selectcoupon);
        binding.tvCurrencyType.setText(LanguageConstants.currencyType);

        binding.tvRedeemtxt.setText(LanguageConstants.yourredeempoint + " " + SessionManager.getInstance().getRedeemPoint());
        if (SessionManager.getInstance().getRedeemPoint().equals("0.0") || SessionManager.getInstance().getRedeemPoint().equals("0")) {
            binding.checkbox.setClickable(false);
            binding.checkbox.setFocusable(false);
            binding.checkbox.setFocusableInTouchMode(false);
        } else {
            binding.checkbox.setClickable(true);
            binding.checkbox.setFocusable(true);
            binding.checkbox.setFocusableInTouchMode(true);

            binding.checkbox.setOnClickListener(this);
        }
        binding.checkbox.setText(LanguageConstants.useyourredeempoints);


        binding.lyoutSelectArea.setOnClickListener(this);
        binding.ivBackArrow.setOnClickListener(this);
        binding.tvcouponwallet.setOnClickListener(this);
        binding.orderplacetxt.setOnClickListener(this);


        binding.rgPayment.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int checkedId) {
                switch (checkedId) {
                    case R.id.rdCash:
                        paymentType = 1;
                        break;
                    case R.id.rdVisa:
                        paymentType = 2;
                        break;
                    default:
                        break;
                }
            }
        });


        binding.rgCurrencyType.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int checkedId) {
                switch (checkedId) {
                    case R.id.rdLL:
                        currencyType = 1;
                        paymentList("1");
                        break;
                    case R.id.rdUSD:
                        currencyType = 2;
                        paymentList("2");
                        break;
                    default:
                        break;
                }
            }
        });


        callShowPaymentMethodsApi();


        callCalculateBasket();
        // bcz of we need redeem point so we call this api here
        callGetprofileApi();
    }

    private void callShowPaymentMethodsApi() {
        CommonApiCalls.getInstance().getPaymentMethods(PaymentActivity.this, new CommonCallback.Listener() {
            @Override
            public void onSuccess(Object body) {
                GetPaymentMethodsAPIResponse response = (GetPaymentMethodsAPIResponse) body;
                if (response.getData().isDelivery()) {
                    binding.rdCash.setVisibility(View.VISIBLE);
                } else {
                    binding.rdCash.setVisibility(View.GONE);
                }


                if (response.getData().isCard()) {
                    binding.rdVisa.setVisibility(View.VISIBLE);
                } else {
                    binding.rdVisa.setVisibility(View.GONE);
                }
            }

            @Override
            public void onFailure(String reason) {

            }
        });
    }

    private void callGetprofileApi() {
        CommonApiCalls.getInstance().getProfile(PaymentActivity.this, new CommonCallback.Listener() {
            @Override
            public void onSuccess(Object body) {
                GetProfileApiResponse getProfileApiResponse = (GetProfileApiResponse) body;
                GetProfileApiResponse.Data data = getProfileApiResponse.getData();
                if (getProfileApiResponse.getStatus().equals(200)) {
                    SessionManager.getInstance().setUserKey(data.getUser_key());
                    SessionManager.getInstance().setReferKey(data.getReferral_code());
                    SessionManager.getInstance().setAccessToken(data.getAccess_token());
                    SessionManager.getInstance().setUserName(data.getFirst_name());
                    SessionManager.getInstance().setUserEmail(data.getEmail());
                    SessionManager.getInstance().setUserMobileNumber(data.getMobile_number());
                    SessionManager.getInstance().setRedeemPoint(data.getReward_point());
                } else {

                }
            }

            @Override
            public void onFailure(String reason) {

            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ivBackArrow:
                finish();
                break;
            case R.id.lyoutSelectArea:
                break;
            case R.id.orderplacetxt:
                if (paymentType != null && !paymentType.equals(0)) {
                    if (binding.checkbox.isChecked()) {
                        isredeemPointsUse = 1;
                    } else {
                        isredeemPointsUse = 0;
                    }
                    placeModel();
                    callPlaceOrderApi();
                } else {
                    CommonFunctions.getInstance().validationEmptyError(PaymentActivity.this, LanguageConstants.choosepaymentType);
                }
                break;
            case R.id.checkbox:
                callCalculateBasket();
                break;
            default:
                break;
        }

    }

    private void callCalculateBasket() {
        if (binding.checkbox.isChecked()) {
            isredeemPointsUse = 1;
        } else {
            isredeemPointsUse = 0;
        }
        beforeplaceModel();
        Gson gson = new Gson();
        String input = gson.toJson(checkoutModel);
        Log.d("DDDDDDDDD_input", input);
        System.out.println("Input ==> " + input);
        CommonApiCalls.getInstance().calculateBasket(PaymentActivity.this, input, new CommonCallback.Listener() {
            @Override
            public void onSuccess(Object body) {
                CalculateApiResponse placeOrderApiResponse = (CalculateApiResponse) body;

                if (placeOrderApiResponse.getStatus().equals(200)) {
                    //CommonFunctions.getInstance().successResponseToast(PaymentActivity.this, placeOrderApiResponse.getMessage());

                    if (placeOrderApiResponse.getData() != null && placeOrderApiResponse.getData().getPayment_details().size() > 0) {
                        paymentDetails = placeOrderApiResponse.getData().getPayment_details();
                        paymentList("1");
                    }
                }
            }

            @Override
            public void onFailure(String reason) {
                CommonFunctions.getInstance().validationInfoError(PaymentActivity.this, reason);
            }
        });
    }

    private void paymentList(String currencyType) {
        LinearLayoutManager layoutManager = new LinearLayoutManager(PaymentActivity.this, RecyclerView.VERTICAL, false);
        binding.rvPayment.setLayoutManager(layoutManager);
        Log.d("AAAA", paymentDetails.toString());
        PaymentDetailsAdapter adapter = new PaymentDetailsAdapter(PaymentActivity.this, paymentDetails, currencyType);
        binding.rvPayment.setAdapter(adapter);
    }

    private void callPlaceOrderApi() {
        placeModel();
        Gson gson = new Gson();
        String input = gson.toJson(checkoutModel);
        System.out.println("Input ==> " + input);
        CommonApiCalls.getInstance().placeOrder(PaymentActivity.this, input, new CommonCallback.Listener() {
            @Override
            public void onSuccess(Object body) {
                PlaceOrderApiResponse placeOrderApiResponse = (PlaceOrderApiResponse) body;
                if (placeOrderApiResponse.getStatus().equals(200)) {

                    if (placeOrderApiResponse.getData().getOrder().getPayment_type().equals(2)) {
                        Bundle bundle = new Bundle();
                        bundle.putString("paymentUrl", placeOrderApiResponse.getData().getPaymentUrl());
                        bundle.putString("paymentSuccessUrl", placeOrderApiResponse.getData().getPaymentSuccessUrl());
                        Gson gson1 = new Gson();
                        String successData = gson1.toJson(placeOrderApiResponse.getData());
                        bundle.putString("SUCCESSDATA", successData);
                        CommonFunctions.getInstance().newIntent(PaymentActivity.this, OnlinePaymentActivity.class, bundle, true);
                    } else {
                        Gson gson1 = new Gson();
                        Bundle bundle = new Bundle();
                        String successData = gson1.toJson(placeOrderApiResponse.getData());
                        bundle.putString("SUCCESSDATA", successData);
                        CommonFunctions.getInstance().newIntent(PaymentActivity.this, OrderSuccessActivity.class, bundle, true);
                    }
                }
            }

            @Override
            public void onFailure(String reason) {

            }
        });
    }

    private void beforeplaceModel() {
        RealmResults<ItemRealmList> cartList1 = RealmLibrary.getInstance().getCartList();
        List<GetSubscribedItemsTypes.Item> itemList = (List<GetSubscribedItemsTypes.Item>)
                CacheManager.getInstance().get("items_types_list");

        Log.d("DDDDDDDDD_input", itemList.toString());

        checkoutModel = new CheckoutModel();
        List<CheckoutModel.Item> items = new ArrayList<>();
        for (int count1 = 0; count1 < cartList1.size(); count1++) {

            ItemRealmList cart = cartList1.get(count1);

            int checkSubscribedPackage = CommonFunctions.getInstance().findItemInMyPackages(cart.getCloth_type_key(),
                    Integer.parseInt(cart.getQuantity()), itemList);


            int virtualQty = Integer.parseInt(cart.getQuantity());
//            if (checkSubscribedPackage != Integer.MIN_VALUE) {
//                if (checkSubscribedPackage > 0)
//                    continue;
//                else {
//                    virtualQty = Math.abs(checkSubscribedPackage);
//                }
//            }


            CheckoutModel.Item item = new CheckoutModel.Item();

            List<CheckoutModel.Size> sizes = new ArrayList<>();

            for (int count2 = 0; count2 < cart.getSize().size(); count2++) {
                CheckoutModel.Size size = new CheckoutModel.Size();
                size.setSize_key(cart.getSize().get(count2).getSize_key());
                size.setSize_quantity(cart.getSize().get(count2).getSize_quantity());
                sizes.add(size);
            }
            item.setSize(sizes);
            item.setAdditional_quantity(cart.getAdditional_quantity());
            item.setCloth_type_key(cart.getCloth_type_key());
            item.setQuantity(virtualQty + "");
            item.setIs_express(cart.getIs_express());
            items.add(item);
        }
        checkoutModel.setItems(items);
        checkoutModel.setOrder_message("");
        checkoutModel.setPayment_type(1);
        checkoutModel.setCurrency_type(1);
        checkoutModel.setUse_reward_point(isredeemPointsUse);
        checkoutModel.setPickup_user_address_key(userAddressKey);
        checkoutModel.setPickup_date(SessionManager.AddressProperties.getInstance().getPickUpDate());
        checkoutModel.setPickup_time(SessionManager.AddressProperties.getInstance().getPickUpTime());
        checkoutModel.setDelivery_date(SessionManager.AddressProperties.getInstance().getDeliveryDate());
        checkoutModel.setDelivery_time(SessionManager.AddressProperties.getInstance().getDeliveryTime());
        System.out.println("safsfsfsf" + checkoutModel);

    }

    private void placeModel() {
        RealmResults<ItemRealmList> cartList1 = RealmLibrary.getInstance().getCartList();
        checkoutModel = new CheckoutModel();
        List<CheckoutModel.Item> items = new ArrayList<>();

        List<GetSubscribedItemsTypes.Item> itemList = (List<GetSubscribedItemsTypes.Item>)
                CacheManager.getInstance().get("items_types_list");

        for (int count1 = 0; count1 < cartList1.size(); count1++) {

            ItemRealmList cart = cartList1.get(count1);

            int checkSubscribedPackage = CommonFunctions.getInstance().findItemInMyPackages(cart.getCloth_type_key(),
                    Integer.parseInt(cart.getQuantity()), itemList);


            int virtualQty = Integer.parseInt(cart.getQuantity());
            if (checkSubscribedPackage != Integer.MIN_VALUE) {
                if (checkSubscribedPackage > 0)
                    continue;
                else {
                    virtualQty = Math.abs(checkSubscribedPackage);
                }
            }

            CheckoutModel.Item item = new CheckoutModel.Item();

            List<CheckoutModel.Size> sizes = new ArrayList<>();

            for (int count2 = 0; count2 < cart.getSize().size(); count2++) {
                CheckoutModel.Size size = new CheckoutModel.Size();
                size.setSize_key(cart.getSize().get(count2).getSize_key());
                size.setSize_quantity(cart.getSize().get(count2).getSize_quantity());
                sizes.add(size);
            }
            item.setSize(sizes);
            item.setAdditional_quantity(cart.getAdditional_quantity());
            item.setCloth_type_key(cart.getCloth_type_key());
            item.setQuantity(virtualQty + "");
            item.setIs_express(cart.getIs_express());
            items.add(item);
        }
        checkoutModel.setItems(items);
        checkoutModel.setOrder_message("");
        checkoutModel.setPayment_type(paymentType);
        checkoutModel.setCurrency_type(currencyType);
        checkoutModel.setUse_reward_point(isredeemPointsUse);
        checkoutModel.setPickup_user_address_key(userAddressKey);
        checkoutModel.setPickup_date(SessionManager.AddressProperties.getInstance().getPickUpDate());
        checkoutModel.setPickup_time(SessionManager.AddressProperties.getInstance().getPickUpTime());
        checkoutModel.setDelivery_date(SessionManager.AddressProperties.getInstance().getDeliveryDate());
        checkoutModel.setDelivery_time(SessionManager.AddressProperties.getInstance().getDeliveryTime());
        System.out.println("safsfsfsf" + checkoutModel);

    }

    private void selectAreaDialog() {
        final Dialog dialog = new Dialog(PaymentActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_select_area);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);

        ImageView imgClose = (ImageView) dialog.findViewById(R.id.imgClose);
        RecyclerView recyclerView = (RecyclerView) dialog.findViewById(R.id.recyclerView);

        List<SelectAreaApiResponse> responses = new ArrayList<>();
        for (int i = 1; i <= 8; i++) {
            SelectAreaApiResponse selectAreaApiResponse = new SelectAreaApiResponse();
            selectAreaApiResponse.setId(i);
            if (i == 0) {
                selectAreaApiResponse.setArea_name("FirstUser");
                responses.add(selectAreaApiResponse);
            } else if (i == 1) {
                selectAreaApiResponse.setArea_name("NewUser");
                responses.add(selectAreaApiResponse);
            } else if (i == 2) {
                selectAreaApiResponse.setArea_name("Coupon");
                responses.add(selectAreaApiResponse);
            } else if (i == 3) {
                selectAreaApiResponse.setArea_name("jan50");
                responses.add(selectAreaApiResponse);
            } else if (i == 4) {
                selectAreaApiResponse.setArea_name("Laundry");
                responses.add(selectAreaApiResponse);
            }
        }
        @SuppressLint("WrongConstant")
        LinearLayoutManager layoutManager = new LinearLayoutManager(PaymentActivity.this, LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);
        SelectAreaAdapter adapter = new SelectAreaAdapter(PaymentActivity.this, responses, new SelectArea() {
            @Override
            public void onClick(int id, String name) {
                binding.tvSelectArea.setText(name);
                binding.tvSelectArea.setTag(name);
                selectAreaName = name;
                dialog.dismiss();
            }
        });
        recyclerView.setAdapter(adapter);

        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }

}
