package com.globallaundry.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import com.globallaundry.R;
import com.globallaundry.databinding.ActivitySplashBinding;
import com.globallaundry.utils.CommonFunctions;
import com.globallaundry.utils.LanguageConstants;
import com.globallaundry.utils.SessionManager;
import com.globallaundry.utils.VersionComparator;
import com.onesignal.OneSignal;

import org.jsoup.Jsoup;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class SplashActivity extends AppCompatActivity {
    ActivitySplashBinding splashBinding;
    long SPLASH_TIME = 1000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        splashBinding = DataBindingUtil.setContentView(this, R.layout.activity_splash);
        //Check internet connection
        if (CommonFunctions.CheckInternetConnection()) {

        } else {
            CommonFunctions.getInstance().validationInfoError(SplashActivity.this, LanguageConstants.kindly_check_internet);
        }

        if (SessionManager.getInstance().getAppLanguageCode().isEmpty()) {
            SessionManager.getInstance().setAppLanguageCode("en");
        } else {
            SessionManager.getInstance().setAppLanguageCode(SessionManager.getInstance().getAppLanguageCode());
        }

        OneSignal.idsAvailable(new OneSignal.IdsAvailableHandler() {
            @Override
            public void idsAvailable(String userId, String registrationId) {
                if (userId != null)
                    SessionManager.getInstance().setDeviceToken(userId);
            }
        });


        LanguageConstants.getInstance().languageConstants();
        LoadSplash();
        generateKeyHash();
    }

    void LoadSplash() {
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                new GetVersionCode().execute();
            }
        }, SPLASH_TIME);
    }

    private class GetVersionCode extends AsyncTask<Void, String, String> {
        @Override
        protected String doInBackground(Void... voids) {

            String newVersion = null;
            try {

                newVersion = Jsoup.connect("https://play.google.com/store/apps/details?id=" + SplashActivity.this.getPackageName() + "&hl=en")
                        .timeout(30000)
                        .userAgent("Mozilla/5.0 (Windows; U; WindowsNT 5.1; en-US; rv1.8.1.6) Gecko/20070725 Firefox/2.0.0.6")
                        .referrer("http://www.google.com")
                        .get()
                        .select("div.hAyfc:nth-child(4) > span:nth-child(2) > div:nth-child(1) > span:nth-child(1)")
                        .first()
                        .ownText();
                return newVersion;
            } catch (Exception e) {
                e.printStackTrace();
                return newVersion;
            }
        }

        @Override
        protected void onPostExecute(String onlineVersion) {
            super.onPostExecute(onlineVersion);
            String currentVersion = null;
            try {
                currentVersion = SplashActivity.this.getPackageManager().getPackageInfo(SplashActivity.this.getPackageName(), 0).versionName;
            } catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();
            }
            if (onlineVersion != null && currentVersion != null && !onlineVersion.isEmpty() && !currentVersion.isEmpty()) {

                int version = VersionComparator.compareVersionNames(currentVersion, onlineVersion);

//                if (!currentVersion.equals(onlineVersion)) {
                if (version < 0) {
                    ShowUpdateDialog();
                } else {
                    LoadNext();
                }
            } else {
                LoadNext();
            }
        }
    }

    private void ShowUpdateDialog() {
        final Dialog dialog = new Dialog(SplashActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.dialog_updateapp);
        dialog.setCancelable(false);
        TextView tvTitle = (TextView) dialog.findViewById(R.id.tvTitle);
        TextView tvMessage = (TextView) dialog.findViewById(R.id.tvMessage);

        Button btnUpdate = (Button) dialog.findViewById(R.id.btnUpdate);
        Button btnLater = (Button) dialog.findViewById(R.id.btnLater);


        tvTitle.setText(LanguageConstants.NewVersion);
        tvMessage.setText(LanguageConstants.NewVersionMessage);
        btnUpdate.setText(LanguageConstants.Update);
        btnLater.setText(LanguageConstants.Later);
        btnLater.setVisibility(View.GONE);

        btnLater.setAllCaps(false);
        btnUpdate.setAllCaps(false);

        btnLater.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LoadNext();
            }
        });

        btnUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse("https://play.google.com/store/apps/details?id=" + SplashActivity.this.getPackageName()));
                startActivity(intent);
            }
        });

        Window window = dialog.getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();
        wlp.gravity = Gravity.CENTER;
        window.setAttributes(wlp);
        if (!SplashActivity.this.isFinishing()) {
            dialog.show();
        }

    }

    void LoadNext() {
        if (CommonFunctions.getInstance().isActivityRunning(SplashActivity.this)) {
            /*if (SessionManager.getInstance().getUserKey().isEmpty()) {
                CommonFunctions.getInstance().newIntent(SplashActivity.this, SignInActivity.class, Bundle.EMPTY, true);
            } else {
                CommonFunctions.getInstance().newIntent(SplashActivity.this, HomeActivity.class, Bundle.EMPTY, true);
            }*/
            CommonFunctions.getInstance().newIntent(SplashActivity.this, HomeActivity.class, Bundle.EMPTY, true);

        }
    }

    // ------ generateKeyHash
    private void generateKeyHash() {
        try {
            PackageInfo info = getPackageManager().getPackageInfo(SplashActivity.this.getPackageName(), PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.i("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {

        } catch (NoSuchAlgorithmException e) {

        }
    }
}
