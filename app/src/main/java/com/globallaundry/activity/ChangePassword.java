package com.globallaundry.activity;

import android.os.Bundle;
import android.view.Gravity;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import com.globallaundry.R;
import com.globallaundry.api.CommonApiCalls;
import com.globallaundry.databinding.ActivityChangePasswordBinding;
import com.globallaundry.interfaces.CommonCallback;
import com.globallaundry.utils.CommonFunctions;
import com.globallaundry.utils.LanguageConstants;
import com.globallaundry.utils.SessionManager;


public class ChangePassword extends AppCompatActivity implements View.OnClickListener {
    ActivityChangePasswordBinding binding;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_change_password);
        if (SessionManager.getInstance().getAppLanguageCode().equals("ar")) {
            getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
            binding.ivBackArrow.setImageResource(R.drawable.ic_right_arrow);
            binding.edOldPassword.setGravity(Gravity.END);
            binding.edNewPassword.setGravity(Gravity.END);
            binding.edConfirmPassword.setGravity(Gravity.END);
        } else {
            getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
            binding.ivBackArrow.setImageResource(R.drawable.ic_back_arrow);
            binding.edOldPassword.setGravity(Gravity.START);
            binding.edNewPassword.setGravity(Gravity.START);
            binding.edConfirmPassword.setGravity(Gravity.START);
        }
        initView();

    }

    private void initView() {
        binding.tvChangePassword.setText(LanguageConstants.ChangePassword);
        binding.tlbarText.setText(LanguageConstants.ChangePassword);
        binding.tlbarText.setVisibility(View.VISIBLE);
        binding.tiloldPassword.setHint(LanguageConstants.oldPassword);
        binding.tilNewPassword.setHint(LanguageConstants.NewPassword);
        binding.tilConfirmPassword.setHint(LanguageConstants.confirmPassword);
        binding.tvOrderOnline.setText(LanguageConstants.submit);
        binding.btnOkay.setOnClickListener(this);
        binding.ivBackArrow.setOnClickListener(this);
    }


    private void callChangePasswordApi() {

        if (binding.edOldPassword.getText().toString().isEmpty()) {
            CommonFunctions.getInstance().validationEmptyError(ChangePassword.this, LanguageConstants.oldPasswordFieldCantBlank);
            return;
        }
        if (binding.edNewPassword.getText().toString().isEmpty()) {
            CommonFunctions.getInstance().validationInfoError(ChangePassword.this, LanguageConstants.newPasswordFieldCantBlank);
            return;
        }
        if (binding.edConfirmPassword.getText().toString().isEmpty()) {
            CommonFunctions.getInstance().validationInfoError(ChangePassword.this, LanguageConstants.confirmPasswordFieldCantBlank);
            return;
        }
        if (!binding.edNewPassword.getText().toString().trim().equals(binding.edConfirmPassword.getText().toString().trim())) {
            CommonFunctions.getInstance().PasswordMatch(ChangePassword.this, LanguageConstants.NewPassword, LanguageConstants.confirmPassword);
            return;
        }

        CommonApiCalls.getInstance().changePassword(ChangePassword.this,
                binding.edOldPassword.getText().toString().trim(),
                binding.edNewPassword.getText().toString().trim(),
                binding.edConfirmPassword.getText().toString().trim(),
                new CommonCallback.Listener() {
                    @Override
                    public void onSuccess(Object object) {
                        CommonFunctions.getInstance().FinishActivityWithDelay(ChangePassword.this);
                    }

                    @Override
                    public void onFailure(String reason) {

                    }
                }
        );

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ivBackArrow:
                finish();
                break;
            case R.id.btnOkay:
                callChangePasswordApi();
                break;

        }
    }
}
