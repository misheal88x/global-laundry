package com.globallaundry.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.globallaundry.R;
import com.globallaundry.adapters.AddressAdapter;
import com.globallaundry.adapters.AddressAdapter1;
import com.globallaundry.adapters.PricingAdapter;
import com.globallaundry.api.CommonApiCalls;
import com.globallaundry.apimodel.AddressListApiResponse;
import com.globallaundry.databinding.ActivitySelectListBinding;
import com.globallaundry.interfaces.CommonCallback;
import com.globallaundry.interfaces.IMethodCaller;
import com.globallaundry.interfaces.OnclickAddress;
import com.globallaundry.models.AddressApiResponse;
import com.globallaundry.models.PricingApiResponse;
import com.globallaundry.utils.AppConstants;
import com.globallaundry.utils.CommonFunctions;
import com.globallaundry.utils.LanguageConstants;
import com.globallaundry.utils.SessionManager;

import java.util.ArrayList;
import java.util.List;

import hari.bounceview.BounceView;

public class SelectList extends AppCompatActivity implements View.OnClickListener {

    ActivitySelectListBinding binding;
    private String userAddressKey = "";
    public static Boolean isNeedToRefresh = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_select_list);
        if (SessionManager.getInstance().getAppLanguageCode().equals("ar")) {
            getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
            binding.ivBackArrow.setImageResource(R.drawable.ic_right_arrow);
        } else {
            getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
            binding.ivBackArrow.setImageResource(R.drawable.ic_back_arrow);
        }
        initView();
        isNeedToRefresh = false;
    }

    private void initView() {
        BounceView.addAnimTo(binding.ivBackArrow);
        BounceView.addAnimTo(binding.mycarttxt);

        binding.tlbarText.setText(LanguageConstants.addressbook);
        binding.mycarttxt.setText(LanguageConstants.payment);
        binding.txtAddAddress.setText(LanguageConstants.addAddress);

        binding.ivBackArrow.setOnClickListener(this);
        binding.mycarttxt.setOnClickListener(this);
        binding.rlyoutAddAddress.setOnClickListener(this);

        pricingApi();
    }

    private void pricingApi() {
        CommonApiCalls.getInstance().addressList(SelectList.this, new CommonCallback.Listener() {
            @Override
            public void onSuccess(Object body) {
                AddressListApiResponse mAddressListData = (AddressListApiResponse) body;
                if (mAddressListData.getData() != null && mAddressListData.getData().size() > 0) {
                    // set adapter
                    LinearLayoutManager layoutManager = new LinearLayoutManager(SelectList.this, RecyclerView.VERTICAL, false);
                    binding.rvCategory.setLayoutManager(layoutManager);
                    AddressAdapter adapter = new AddressAdapter(SelectList.this, mAddressListData.getData(), new OnclickAddress() {
                        @Override
                        public void onClick(String key, Integer value) {
                            if (value == -1) {
                                userAddressKey = "";
                            } else {
                                userAddressKey = key;
                            }
                        }
                    });
                    binding.rvCategory.setAdapter(adapter);
                    binding.llNoContentView.setVisibility(View.GONE);
                    binding.rvCategory.setVisibility(View.VISIBLE);
                } else {
                    binding.rvCategory.setVisibility(View.GONE);
                    binding.llNoContentView.setVisibility(View.VISIBLE);
                    binding.tvNoDateMessage.setText(LanguageConstants.noaddressfound);
                }


            }

            @Override
            public void onFailure(String reason) {

            }
        });

    }

    @Override
    public void onResume() {
        super.onResume();
        if (isNeedToRefresh) {
            isNeedToRefresh = false;
            pricingApi();
        }
    }

    @Override
    public void onClick(View v) {
        if (v == binding.ivBackArrow) {
            finish();
        } else if (v == binding.mycarttxt) {
            if (userAddressKey != null && !userAddressKey.isEmpty()) {
                Bundle bundle = new Bundle();
                bundle.putString("USERADDRESSKEY", userAddressKey);
                CommonFunctions.getInstance().newIntent(SelectList.this, PaymentActivity.class, bundle, false);
            } else {
                CommonFunctions.getInstance().validationEmptyError(SelectList.this, LanguageConstants.addAddress);
            }
        } else if (v == binding.rlyoutAddAddress) {
            Bundle bundle = new Bundle();
            bundle.putString("From", "1");
            bundle.putString(AppConstants.AddressMapType, CommonFunctions.AddressMapType.AddAddress.getValue() + "");
            CommonFunctions.getInstance().newIntent(SelectList.this, AddressSelectionMap.class, bundle, false);
        }
    }
}
