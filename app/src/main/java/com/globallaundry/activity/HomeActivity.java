package com.globallaundry.activity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.ToggleButton;

import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.databinding.DataBindingUtil;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.FragmentManager;

import com.globallaundry.R;
import com.globallaundry.adapters.NavigationMenuAdapters;
import com.globallaundry.databinding.ActivityHomeBinding;
import com.globallaundry.enums.NavigationMenuEnum;
import com.globallaundry.fragments.AddressBookFragment;
import com.globallaundry.fragments.ContactUsFragment;
import com.globallaundry.fragments.HelpFragment;
import com.globallaundry.fragments.HomeFragment;
import com.globallaundry.fragments.MyOrdersFragment;
import com.globallaundry.fragments.NotificationFragment;
import com.globallaundry.fragments.ReferandEarn;
import com.globallaundry.fragments.SubscribedPackagesFragment;
import com.globallaundry.interfaces.NavigationClick;
import com.globallaundry.models.NavigationMenuModel;
import com.globallaundry.utils.CommonFunctions;
import com.globallaundry.utils.LanguageConstants;
import com.globallaundry.utils.SessionManager;
import com.onesignal.OneSignal;
import com.zendesk.logger.Logger;

import java.util.ArrayList;
import java.util.List;

import zendesk.core.AnonymousIdentity;
import zendesk.core.Identity;
import zendesk.core.Zendesk;
import zendesk.support.Support;
import zendesk.support.request.RequestActivity;

public class HomeActivity extends AppCompatActivity {
    ActivityHomeBinding homeBinding;
    private FragmentManager fragmentManager;
    private boolean doubleBackToExitPressedOnce = false;
    Toolbar toolbar;
    ImageView ivNavIcon;
    TextView tvToolBarTitle;
    private DrawerLayout drawer;
    LinearLayout llEditArrow;
    TextView tvUserName;
    private TextView tvUserMobile;
    private TextView tvLanguageText;
    private ToggleButton tlbarLangswitchToggle;
    private LinearLayout llhometool;
    private TextView tvName;
    private TextView tvPhoneNumber;
    private ImageView ivEdit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        homeBinding = DataBindingUtil.setContentView(this, R.layout.activity_home);
        fragmentManager = getSupportFragmentManager();

        loadNavigationMenu();

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        ivNavIcon = (ImageView) findViewById(R.id.ivNavIcon);
        tvToolBarTitle = (TextView) findViewById(R.id.tvToolBarTitle);
        tvName = (TextView) findViewById(R.id.tvName);
        tvPhoneNumber = (TextView) findViewById(R.id.tvPhoneNumber);
        ivEdit = (ImageView) findViewById(R.id.ivEdit);
        llhometool = (LinearLayout) findViewById(R.id.llhometool);
        drawer = (DrawerLayout) findViewById(R.id.drawerLayout);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
            this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();
        toggle.setDrawerIndicatorEnabled(false);

        if (SessionManager.getInstance().getAppLanguageCode().equals("ar")) {
            getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
            ivEdit.setImageResource(R.drawable.ic_back_arrow);
        } else {
            getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
            ivEdit.setImageResource(R.drawable.ic_right_arrow);
        }


        OneSignal.idsAvailable(new OneSignal.IdsAvailableHandler() {
            @Override
            public void idsAvailable(String userId, String registrationId) {
                if (userId != null)
                    SessionManager.getInstance().setDeviceToken(userId);
            }
        });

        ivNavIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (drawer.isDrawerVisible(GravityCompat.START)) {
                    drawer.closeDrawer(GravityCompat.START);
                } else {
                    drawer.openDrawer(GravityCompat.START);
                    CommonFunctions.getInstance().hideSoftKeyboard(HomeActivity.this);
                }
            }
        });

        fragmentManager.beginTransaction()
            .replace(R.id.container, new HomeFragment(), "home")
            .commit();
        tvToolBarTitle.setText(LanguageConstants.home);

        llEditArrow = (LinearLayout) findViewById(R.id.llNavHeader);
        tvUserName = (TextView) findViewById(R.id.tvUserName);
        tvUserMobile = (TextView) findViewById(R.id.tvUserMobile);
        tvLanguageText = (TextView) findViewById(R.id.tvLanguageText);
        tlbarLangswitchToggle = (ToggleButton) findViewById(R.id.tlbarLangswitchToggle);
        tvLanguageText.setText(LanguageConstants.language);

        if (SessionManager.getInstance().getAppLanguageCode().equals("en")) {
            tlbarLangswitchToggle.setChecked(true);
        } else {
            tlbarLangswitchToggle.setChecked(false);
        }

        if (SessionManager.getInstance().getUserKey().isEmpty()) {
            tvUserName.setText(LanguageConstants.guest);
            tvUserMobile.setText("");
        } else {
            tvUserName.setText(SessionManager.getInstance().getUserName());
            tvUserMobile.setText(SessionManager.getInstance().getUserMobileNumber());
        }

        llEditArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SessionManager.getInstance().getUserKey().isEmpty()) {
                    CommonFunctions.getInstance().newIntent(HomeActivity.this, SignInActivity.class, Bundle.EMPTY, false);
                } else {
                    CommonFunctions.getInstance().newIntent(HomeActivity.this, ProfileActivity.class, Bundle.EMPTY, false);
                }
                drawer.closeDrawer(GravityCompat.START);
            }
        });

        tlbarLangswitchToggle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (tlbarLangswitchToggle.isChecked()) {
                    SessionManager.getInstance().setAppLanguageCode("en");
                    //SessionManager.AppProperties.getInstance().setAppDirection(ConstantsInternal.AppDirection.fromInteger(1));
                    LanguageConstants.getInstance().languageConstants();
                    HomeActivity.this.finish();
                    CommonFunctions.getInstance().newIntent(HomeActivity.this, HomeActivity.class, Bundle.EMPTY, false);
                    //setLocale(getApplicationContext(), "en");
                } else {
                    SessionManager.getInstance().setAppLanguageCode("ar");
                    //SessionManager.AppProperties.getInstance().setAppDirection(ConstantsInternal.AppDirection.fromInteger(0));
                    LanguageConstants.getInstance().languageConstants();
                    HomeActivity.this.finish();
                    CommonFunctions.getInstance().newIntent(HomeActivity.this, HomeActivity.class, Bundle.EMPTY, false);
                    //setLocale(getApplicationContext(), "no");
                }
            }
        });

    }

    private void loadNavigationMenu() {
        List<NavigationMenuModel> navigationMenuModels = new ArrayList<>();
        NavigationMenuModel navigationMenuModel;

        // --- Home
        navigationMenuModel = new NavigationMenuModel();
        navigationMenuModel.setNavigationItemId(NavigationMenuEnum.HOME);
        navigationMenuModel.setNavigationIcon(R.drawable.side_home);
        navigationMenuModel.setNavigationItemName(LanguageConstants.home);
        navigationMenuModels.add(navigationMenuModel);


        // --- subscribed_packages
        navigationMenuModel = new NavigationMenuModel();
        navigationMenuModel.setNavigationItemId(NavigationMenuEnum.SUBSCRIBED_PACKAGES);
        navigationMenuModel.setNavigationIcon(R.drawable.side_home);
        navigationMenuModel.setNavigationItemName(LanguageConstants.subscribed_packages);
        navigationMenuModels.add(navigationMenuModel);

        navigationMenuModel = new NavigationMenuModel();
        navigationMenuModel.setNavigationItemId(NavigationMenuEnum.ORDERS);
        navigationMenuModel.setNavigationIcon(R.drawable.sidemenu_order);
        navigationMenuModel.setNavigationItemName(LanguageConstants.orders);
        navigationMenuModels.add(navigationMenuModel);

        // --- Account
        /*navigationMenuModel = new NavigationMenuModel();
        navigationMenuModel.setNavigationItemId(NavigationMenuEnum.PAYMENT);
        navigationMenuModel.setNavigationIcon(R.drawable.sidemenu_payment);
        navigationMenuModel.setNavigationItemName(LanguageConstants.payment);
        navigationMenuModels.add(navigationMenuModel);*/

        // --- Favourites
        /*navigationMenuModel = new NavigationMenuModel();
        navigationMenuModel.setNavigationItemId(NavigationMenuEnum.MYWALLET);
        navigationMenuModel.setNavigationIcon(R.drawable.sidemenu_mywallet);
        navigationMenuModel.setNavigationItemName(LanguageConstants.mywallet);
        navigationMenuModels.add(navigationMenuModel);*/

        // --- Bookings
        navigationMenuModel = new NavigationMenuModel();
        navigationMenuModel.setNavigationItemId(NavigationMenuEnum.REFEREARN);
        navigationMenuModel.setNavigationIcon(R.drawable.sidemenu_refer);
        navigationMenuModel.setNavigationItemName(LanguageConstants.referearn);
        navigationMenuModels.add(navigationMenuModel);

        // ---- Settings
        navigationMenuModel = new NavigationMenuModel();
        navigationMenuModel.setNavigationItemId(NavigationMenuEnum.NOTIFICATIOS);
        navigationMenuModel.setNavigationIcon(R.drawable.sidemenu_notification);
        navigationMenuModel.setNavigationItemName(LanguageConstants.notifications);
        navigationMenuModels.add(navigationMenuModel);
        // --- Contactus
        navigationMenuModel = new NavigationMenuModel();
        navigationMenuModel.setNavigationItemId(NavigationMenuEnum.CONTACTUS);
        navigationMenuModel.setNavigationIcon(R.drawable.ic_contact);
        navigationMenuModel.setNavigationItemName(LanguageConstants.contactUs);
        navigationMenuModels.add(navigationMenuModel);
        // ---- Messages
        navigationMenuModel = new NavigationMenuModel();
        navigationMenuModel.setNavigationItemId(NavigationMenuEnum.ADDRESSBOOK);
        navigationMenuModel.setNavigationIcon(R.drawable.side_addressbook);
        navigationMenuModel.setNavigationItemName(LanguageConstants.addressbook);
        navigationMenuModels.add(navigationMenuModel);
        // ---- Chatsupport
        navigationMenuModel = new NavigationMenuModel();
        navigationMenuModel.setNavigationItemId(NavigationMenuEnum.CHATSUPPORT);
        navigationMenuModel.setNavigationIcon(R.drawable.sidemenu_chatsupport);
        navigationMenuModel.setNavigationItemName(LanguageConstants.chatsupport);
        navigationMenuModels.add(navigationMenuModel);
        //---- Help
        navigationMenuModel = new NavigationMenuModel();
        navigationMenuModel.setNavigationItemId(NavigationMenuEnum.HELP);
        navigationMenuModel.setNavigationIcon(R.drawable.ic_help);
        navigationMenuModel.setNavigationItemName(LanguageConstants.help);
        navigationMenuModels.add(navigationMenuModel);

        NavigationMenuAdapters navigationMenuAdapter = new NavigationMenuAdapters(HomeActivity.this, navigationMenuModels, new NavigationClick() {
            @Override
            public void onClick(NavigationMenuModel navigationMenuModel) {
                navigationMenuClick(navigationMenuModel);
            }
        });
        homeBinding.lvNavigation.setAdapter(navigationMenuAdapter);



    }

    // ---- Navigation Menu Click
    private void navigationMenuClick(NavigationMenuModel navigationMenuModel) {
        if (navigationMenuModel.getNavigationItemId() == NavigationMenuEnum.HOME) {
            fragmentManager.beginTransaction()
                .replace(R.id.container, new HomeFragment(), "home")
                .addToBackStack("home")
                .commit();
            tvToolBarTitle.setText(LanguageConstants.home);
            drawer.closeDrawer(GravityCompat.START);
        }
        else if (navigationMenuModel.getNavigationItemId() == NavigationMenuEnum.SUBSCRIBED_PACKAGES) {
//            fragmentManager.beginTransaction()
//                .replace(R.id.container, new SubscribedPackagesFragment(), "subscribed_packages")
//                .commit();
//            tvToolBarTitle.setText(LanguageConstants.subscribed_packages);
            if (SessionManager.getInstance().getUserKey().isEmpty()) {
                CommonFunctions.getInstance().newIntent(HomeActivity.this, SignInActivity.class, Bundle.EMPTY, false);
            } else {
                fragmentManager.beginTransaction()
                    .replace(R.id.container, new SubscribedPackagesFragment(), "subscribed_packages")
                    .commit();
                tvToolBarTitle.setText(LanguageConstants.subscribed_packages);
            }
            drawer.closeDrawer(GravityCompat.START);
        }

        else if (navigationMenuModel.getNavigationItemId() == NavigationMenuEnum.ORDERS) {
            if (SessionManager.getInstance().getUserKey().isEmpty()) {
                CommonFunctions.getInstance().newIntent(HomeActivity.this, SignInActivity.class, Bundle.EMPTY, false);
            } else {
                fragmentManager.beginTransaction()
                    .replace(R.id.container, new MyOrdersFragment(), "myorder")
                    .commit();
                tvToolBarTitle.setText(LanguageConstants.orders);
            }
            drawer.closeDrawer(GravityCompat.START);
        }
        /*else if (navigationMenuModel.getNavigationItemId() == NavigationMenuEnum.PAYMENT) {
         if (CommonFunctions.getInstance().isLoggedIn()) {

                    fragmentManager.beginTransaction()
                            .replace(R.id.flContainer, new ProfileFragment(), "address")
                            .addToBackStack("address")
                            .commit();
                } else {
                    fragmentManager.beginTransaction()
                            .replace(R.id.flContainer, new LoginFragment())
                            .addToBackStack(null)
                            .commit();
                }

        } else if (navigationMenuModel.getNavigationItemId() == NavigationMenuEnum.MYWALLET) {
          if (CommonFunctions.getInstance().isLoggedIn()) {
                    fragmentManager.beginTransaction()
                            .replace(R.id.flContainer, new OrdersFragment(), "orders")
                            .addToBackStack("orders")
                            .commit();
                } else {
                    fragmentManager.beginTransaction()
                            .replace(R.id.flContainer, new OrdersFragment())
                            .addToBackStack(null)
                            .commit();
                }

        }*/
        else if (navigationMenuModel.getNavigationItemId() == NavigationMenuEnum.REFEREARN) {
            if (SessionManager.getInstance().getUserKey().isEmpty()) {
                CommonFunctions.getInstance().newIntent(HomeActivity.this, SignInActivity.class, Bundle.EMPTY, false);
            } else {
                fragmentManager.beginTransaction()
                    .replace(R.id.container, new ReferandEarn(), "refer&earn")
                    .commit();
                tvToolBarTitle.setText(LanguageConstants.referearn);
            }
            drawer.closeDrawer(GravityCompat.START);
        } else if (navigationMenuModel.getNavigationItemId() == NavigationMenuEnum.CONTACTUS) {
            fragmentManager.beginTransaction()
                .replace(R.id.container, new ContactUsFragment(), "contactus")
                .commit();
            tvToolBarTitle.setText(LanguageConstants.contactUs);
            drawer.closeDrawer(GravityCompat.START);
        } else if (navigationMenuModel.getNavigationItemId() == NavigationMenuEnum.NOTIFICATIOS) {
            if (SessionManager.getInstance().getUserKey().isEmpty()) {
                CommonFunctions.getInstance().newIntent(HomeActivity.this, SignInActivity.class, Bundle.EMPTY, false);
            } else {
                fragmentManager.beginTransaction()
                    .replace(R.id.container, new NotificationFragment(), "notification")
                    .commit();
                tvToolBarTitle.setText(LanguageConstants.notifications);
            }
            drawer.closeDrawer(GravityCompat.START);
        } else if (navigationMenuModel.getNavigationItemId() == NavigationMenuEnum.ADDRESSBOOK) {
            if (SessionManager.getInstance().getUserKey().isEmpty()) {
                CommonFunctions.getInstance().newIntent(HomeActivity.this, SignInActivity.class, Bundle.EMPTY, false);
            } else {
                fragmentManager.beginTransaction()
                    .replace(R.id.container, new AddressBookFragment(), "addressbook")
                    .commit();
                tvToolBarTitle.setText(LanguageConstants.addressbook);
            }
            drawer.closeDrawer(GravityCompat.START);
        } else if (navigationMenuModel.getNavigationItemId() == NavigationMenuEnum.CHATSUPPORT) {
            if (SessionManager.getInstance().getUserKey().isEmpty()) {
                CommonFunctions.getInstance().newIntent(HomeActivity.this, SignInActivity.class, Bundle.EMPTY, false);
            } else {
                // Enable logging in Support and Chat SDK
                Logger.setLoggable(true);

                Zendesk.INSTANCE.init(HomeActivity.this, "https://globallaundryhelphelp.zendesk.com",
                    "1c76c3c840dd7432adc7dca5b0bd28ff8f1f0b82f19dd9e3",
                    "mobile_sdk_client_dd2c0dcadc2c5139ef1b");
                Support.INSTANCE.init(Zendesk.INSTANCE);

                Identity identity = new AnonymousIdentity();
                Zendesk.INSTANCE.setIdentity(identity);
                Support.INSTANCE.init(Zendesk.INSTANCE);
                Intent requestActivityIntent = RequestActivity.builder()
                    .withRequestSubject("Testing Support SDK 2.0")
                    .withTags("sdk", "android")
                    .intent(HomeActivity.this);
                startActivity(requestActivityIntent);
            }
        } else if (navigationMenuModel.getNavigationItemId() == NavigationMenuEnum.HELP) {
            fragmentManager.beginTransaction()
                .replace(R.id.container, new HelpFragment(), "Help")
                .commit();
            tvToolBarTitle.setText(LanguageConstants.help);
            drawer.closeDrawer(GravityCompat.START);
        }
    }

    // ----- On BackPressed
    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            FragmentManager manager = getSupportFragmentManager();
            int count = manager.getBackStackEntryCount();
            // Toast.makeText(this, "count:"+count, Toast.LENGTH_SHORT).show();

            if (count == 1) {
                super.onBackPressed();
            }

            if (count == 0) {
                if (doubleBackToExitPressedOnce) {
                    super.onBackPressed();
                    return;
                }
                CommonFunctions.getInstance().validationWarningError(HomeActivity.this, LanguageConstants.pleaseClickBackAgainToExit);
                this.doubleBackToExitPressedOnce = true;
            }
        }
    }


}
