package com.globallaundry.activity;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import com.globallaundry.R;
import com.globallaundry.api.CommonApiCalls;
import com.globallaundry.apimodel.GetTermsConditionsApiResponse;
import com.globallaundry.databinding.ActivityCmsBinding;
import com.globallaundry.interfaces.CommonCallback;
import com.globallaundry.utils.CommonFunctions;
import com.globallaundry.utils.LanguageConstants;
import com.globallaundry.utils.SessionManager;


public class CMSActivity extends AppCompatActivity implements View.OnClickListener {
    ActivityCmsBinding binding;
    private String title = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_cms);
        if (SessionManager.getInstance().getAppLanguageCode().equals("ar")) {
            getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
            binding.ivBackArrow.setImageResource(R.drawable.ic_right_arrow);
        } else {
            getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
            binding.ivBackArrow.setImageResource(R.drawable.ic_back_arrow);
        }

        if (getIntent().getExtras() != null){
            title = getIntent().getExtras().getString("title");
        }

        binding.wvTerms.getSettings().setJavaScriptEnabled(true);
        binding.wvTerms.setWebViewClient(new MyWebViewClient());

        initView();
    }

    private void initView() {
        CommonApiCalls.getInstance().getTermsAndConditions(CMSActivity.this, new CommonCallback.Listener() {
            @Override
            public void onSuccess(Object body) {
                GetTermsConditionsApiResponse mGetTermsConditionsApiResponse = (GetTermsConditionsApiResponse) body;

                if (mGetTermsConditionsApiResponse.getData().size() > 0) {

                    for (int count = 0; count < mGetTermsConditionsApiResponse.getData().size(); count++) {
                        if (title.equals("privacy")) {
                            if (mGetTermsConditionsApiResponse.getData().get(count).getSlug().equals("privacy-policy")) {
                                binding.wvTerms.loadUrl(mGetTermsConditionsApiResponse.getData().get(count).getCms_url());
                                binding.tlbarText.setText(mGetTermsConditionsApiResponse.getData().get(count).getCms_title());
                                break;
                            }
                        }
                        else if (title.equals("terms")){
                            if (mGetTermsConditionsApiResponse.getData().get(count).getSlug().equals("terms-and-conditions")) {
                                binding.wvTerms.loadUrl(mGetTermsConditionsApiResponse.getData().get(count).getCms_url());
                                binding.tlbarText.setText(mGetTermsConditionsApiResponse.getData().get(count).getCms_title());
                                break;
                            }
                        }
                    }

                }
            }

            @Override
            public void onFailure(String reason) {
                CommonFunctions.getInstance().validationInfoError(CMSActivity.this, reason);
            }
        });
    }

    @Override
    public void onClick(View v) {
        if (v == binding.ivBackArrow) {
            finish();
        }

    }

    private class MyWebViewClient extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
        }

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
        }
    }


}
