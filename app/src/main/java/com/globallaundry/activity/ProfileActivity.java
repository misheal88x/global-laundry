package com.globallaundry.activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.globallaundry.R;
import com.globallaundry.adapters.SelectAreaAdapter;
import com.globallaundry.api.CommonApiCalls;
import com.globallaundry.apimodel.UpdateProfileApiResponse;
import com.globallaundry.apimodel.UpdateProfileImageApiResponse;
import com.globallaundry.databinding.ActivityProfileBinding;
import com.globallaundry.interfaces.CommonCallback;
import com.globallaundry.interfaces.SelectArea;
import com.globallaundry.models.SelectAreaApiResponse;
import com.globallaundry.utils.CommonFunctions;
import com.globallaundry.utils.LanguageConstants;
import com.globallaundry.utils.SessionManager;
import com.mikelau.croperino.Croperino;
import com.mikelau.croperino.CroperinoConfig;
import com.mikelau.croperino.CroperinoFileUtil;

import java.io.File;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;

import hari.bounceview.BounceView;

public class ProfileActivity extends AppCompatActivity implements View.OnClickListener {
    ActivityProfileBinding binding;
    Bitmap profilePortfolioBitmap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_profile);

        if (SessionManager.getInstance().getAppLanguageCode().equals("ar")) {
            getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
            binding.ivBackArrow.setImageResource(R.drawable.ic_right_arrow);
            binding.tvName.setGravity(Gravity.END);
            binding.tvPhoneNumber.setGravity(Gravity.END);
            binding.tvEmail.setGravity(Gravity.END);
        } else {
            getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
            binding.ivBackArrow.setImageResource(R.drawable.ic_back_arrow);
            binding.tvName.setGravity(Gravity.START);
            binding.tvPhoneNumber.setGravity(Gravity.START);
            binding.tvEmail.setGravity(Gravity.START);
        }

        new CroperinoConfig("IMG_" + System.currentTimeMillis() + ".jpg", "/Pictures", "/sdcard/Pictures");
        CroperinoFileUtil.setupDirectory(ProfileActivity.this);

        initView();
    }

    private void initView() {
        BounceView.addAnimTo(binding.ivBackArrow);
        BounceView.addAnimTo(binding.lyoutChangeYourPassword);
        binding.tvChangeYourPassword.setText(LanguageConstants.changeYourPassword);
        binding.tvEditProfile.setText(LanguageConstants.editprofiletxt);
        binding.tvNameHint.setHint(LanguageConstants.Name);
        binding.tvPhoneNumberHint.setHint(LanguageConstants.phonenumbertxt);
        binding.tvEmailHint.setHint(LanguageConstants.emailtxt);
        binding.tlbarText.setText(LanguageConstants.profile);
        binding.tvUpdate.setText(LanguageConstants.updateProfile);
        binding.txtLogout.setText(LanguageConstants.logOut);

        binding.lyoutChangeYourPassword.setOnClickListener(this);
        binding.tvEditProfile.setOnClickListener(this);
        binding.rlProfileImage.setOnClickListener(this);
        binding.ivBackArrow.setOnClickListener(this);
        binding.lllogout.setOnClickListener(this);
        binding.btnUpdate.setOnClickListener(this);

       /* SpannableString ss = new SpannableString(CommonFunctions.fromHtml(MessageFormat.format(LanguageConstants.editprofiletxt,
                "<font color=\"\">" + LanguageConstants.edit + "</font>")));
        ClickableSpan tAndcSpan = new ClickableSpan() {
            @Override
            public void onClick(View textView) {
                //CommonFunctions.getInstance().newIntent(ProfileActivity.this, EditProfileActivity.class, Bundle.EMPTY, true);
            }

            @Override
            public void updateDrawState(TextPaint ds) {
                super.updateDrawState(ds);
                ds.setUnderlineText(false);
            }
        };

        String val = CommonFunctions.fromHtml(MessageFormat.format(LanguageConstants.editprofiletxt, LanguageConstants.edit)).toString();
        Integer startTerms = val.indexOf(LanguageConstants.edit);
        Integer endTerms = startTerms + LanguageConstants.edit.length();
        ss.setSpan(tAndcSpan, startTerms, endTerms, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        binding.tvEditProfile.setText(ss);
        binding.tvEditProfile.setMovementMethod(LinkMovementMethod.getInstance());
        binding.tvEditProfile.setHighlightColor(Color.TRANSPARENT);*/

        binding.tvEditProfile.setText(LanguageConstants.editprofiletxt);

        binding.tvName.setText(SessionManager.getInstance().getUserName());
        binding.tvPhoneNumber.setText(SessionManager.getInstance().getUserMobileNumber());
        binding.tvEmail.setText(SessionManager.getInstance().getUserEmail());
        if (SessionManager.getInstance().getUserPic() != null && !SessionManager.getInstance().getUserPic().isEmpty()) {
            Glide.with(ProfileActivity.this).load(SessionManager.getInstance().getUserPic()).apply(RequestOptions.circleCropTransform()).into(binding.ivProfile);
        }
    }

    @Override
    public void onClick(View v) {
        if (v == binding.ivBackArrow) {
            finish();
        } else if (v == binding.rlProfileImage) {
            if (CroperinoFileUtil.verifyStoragePermissions(ProfileActivity.this)) {
                prepareChooser();
            }

        } else if (v == binding.lyoutChangeYourPassword) {
            CommonFunctions.getInstance().newIntent(ProfileActivity.this, ChangePassword.class, Bundle.EMPTY, false);
        } else if (v == binding.lllogout) {
            SessionManager.getInstance().Logout();
            Intent i = new Intent(ProfileActivity.this, SignInActivity.class);
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(i);

        } else if (v == binding.btnUpdate) {
            if (binding.tvName.getText().toString().trim().isEmpty()) {
                CommonFunctions.getInstance().validationEmptyError(ProfileActivity.this, LanguageConstants.userNameFieldCantBlank);
                return;
            }
            if (binding.tvPhoneNumber.getText().toString().trim().isEmpty()) {
                CommonFunctions.getInstance().validationEmptyError(ProfileActivity.this, LanguageConstants.mobileNumberFieldCantBlank);
            }
            if (binding.tvPhoneNumber.getText().toString().trim().length() < 8 || binding.tvPhoneNumber.getText().toString().trim().length() > 12) {
                CommonFunctions.getInstance().validationWarningError(ProfileActivity.this, LanguageConstants.enterValidMobileNumber);
                return;
            }
            if (binding.tvEmail.getText().toString().trim().isEmpty()) {
                CommonFunctions.getInstance().validationEmptyError(ProfileActivity.this, LanguageConstants.emailFieldCantBlank);
                return;
            }
            if (!CommonFunctions.getInstance().isValidEmail(binding.tvEmail.getText().toString().trim())) {
                CommonFunctions.getInstance().validationWarningError(ProfileActivity.this, LanguageConstants.emailaddress);
            }
            callUpdateProfileApi();
        }

    }

    private void callUpdateProfileApi() {
        CommonApiCalls.getInstance().updateProfileApi(ProfileActivity.this, binding.tvName.getText().toString().trim(), binding.tvEmail.getText().toString().trim(), binding.tvPhoneNumber.getText().toString(), new CommonCallback.Listener() {
            @Override
            public void onSuccess(Object object) {
                UpdateProfileApiResponse body = (UpdateProfileApiResponse) object;
                if (body.getStatus().equals(200)) {
                    CommonFunctions.getInstance().successResponseToast(ProfileActivity.this, body.getMessage());
                    if (body.getData() != null) {
                        UpdateProfileApiResponse.Data data = body.getData();
                        SessionManager.getInstance().setUserKey(data.getUser_key());
                        SessionManager.getInstance().setReferKey(data.getReferral_code());
                        SessionManager.getInstance().setUserName(data.getFirst_name());
                        SessionManager.getInstance().setUserName(data.getFirst_name());
                        SessionManager.getInstance().setUserEmail(data.getEmail());
                        SessionManager.getInstance().setUserMobileNumber(data.getMobile_number());
                        SessionManager.getInstance().setUserPic(data.getProfile_image());
                        SessionManager.getInstance().setAccessToken(data.getAccess_token());
                        SessionManager.getInstance().setRedeemPoint(data.getReward_point());
                    }
                }

            }

            @Override
            public void onFailure(String reason) {
                CommonFunctions.getInstance().validationInfoError(ProfileActivity.this, reason);
            }
        });
    }


    // ---- Pick Image Crop
    private void prepareChooser() {
        Croperino.prepareChooser(ProfileActivity.this, LanguageConstants.selectImageFrom, ContextCompat.getColor(ProfileActivity.this, R.color.colorPrimary));
    }

    private void prepareCamera() {
        Croperino.prepareCamera(ProfileActivity.this);
    }

    // ---  On-Request--Permissions--Result
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == CroperinoFileUtil.REQUEST_CAMERA) {
            for (int i = 0; i < permissions.length; i++) {
                String permission = permissions[i];
                int grantResult = grantResults[i];

                if (permission.equals(Manifest.permission.CAMERA)) {
                    if (grantResult == PackageManager.PERMISSION_GRANTED) {
                        prepareCamera();
                    }
                }
            }
        } else if (requestCode == CroperinoFileUtil.REQUEST_EXTERNAL_STORAGE) {
            boolean wasReadGranted = false;
            boolean wasWriteGranted = false;

            for (int i = 0; i < permissions.length; i++) {
                String permission = permissions[i];
                int grantResult = grantResults[i];

                if (permission.equals(Manifest.permission.READ_EXTERNAL_STORAGE)) {
                    if (grantResult == PackageManager.PERMISSION_GRANTED) {
                        wasReadGranted = true;
                    }
                }
                if (permission.equals(Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                    if (grantResult == PackageManager.PERMISSION_GRANTED) {
                        wasWriteGranted = true;
                    }
                }
            }

            if (wasReadGranted && wasWriteGranted) {
                prepareChooser();
            }
        }
    }

    // -- On Activity Result
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // ---- Image Crop
        switch (requestCode) {
            case CroperinoConfig.REQUEST_TAKE_PHOTO:
                if (resultCode == Activity.RESULT_OK) {
                    Croperino.runCropImage(CroperinoFileUtil.getTempFile(), ProfileActivity.this, true, 1, 1, R.color.gray, R.color.gray_variant);
                }
                break;
            case CroperinoConfig.REQUEST_PICK_FILE:
                if (resultCode == Activity.RESULT_OK) {
                    CroperinoFileUtil.newGalleryFile(data, ProfileActivity.this);
                    Croperino.runCropImage(CroperinoFileUtil.getTempFile(), ProfileActivity.this, true, 1, 1, R.color.gray, R.color.gray_variant);
                }
                break;
            case CroperinoConfig.REQUEST_CROP_PHOTO:
                if (resultCode == Activity.RESULT_OK) {
                    Uri uri = Uri.fromFile(CroperinoFileUtil.getTempFile());
                    File mFileRelationPhoto = CroperinoFileUtil.getTempFile();

                    String filePath = mFileRelationPhoto.getPath();
                    profilePortfolioBitmap = BitmapFactory.decodeFile(filePath);
                    Glide.with(ProfileActivity.this).load(filePath).apply(RequestOptions.circleCropTransform()).into(binding.ivProfile);
                    callChangeProfilePicApi(mFileRelationPhoto);
                }
                break;
            default:
                break;
        }

    }

    private void callChangeProfilePicApi(File filePath) {
        CommonApiCalls.getInstance().updateProfileImage(ProfileActivity.this, filePath, new CommonCallback.Listener() {
            @Override
            public void onSuccess(Object object) {
                UpdateProfileImageApiResponse body = (UpdateProfileImageApiResponse) object;
                if (body.getStatus().equals(200)) {
                    CommonFunctions.getInstance().successResponseToast(ProfileActivity.this, body.getMessage());

                    if (body.getData() != null) {
                        UpdateProfileImageApiResponse.Data data = body.getData();
                        SessionManager.getInstance().setUserPic(data.getProfile_image());
                        Glide.with(ProfileActivity.this).load(data.getProfile_image()).apply(RequestOptions.circleCropTransform()).into(binding.ivProfile);
                    }
                }
            }

            @Override
            public void onFailure(String reason) {
                CommonFunctions.getInstance().validationInfoError(ProfileActivity.this, reason);
            }
        });
    }

}
