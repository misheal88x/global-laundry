package com.globallaundry.activity;

import android.graphics.Color;
import android.os.Bundle;
import android.text.InputType;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import com.globallaundry.R;
import com.globallaundry.api.CommonApiCalls;
import com.globallaundry.apimodel.RegistrationApiResponse;
import com.globallaundry.databinding.ActivitySignupBinding;
import com.globallaundry.interfaces.CommonCallback;
import com.globallaundry.utils.CommonFunctions;
import com.globallaundry.utils.LanguageConstants;
import com.globallaundry.utils.SessionManager;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;

import java.text.MessageFormat;
import java.util.concurrent.TimeUnit;

import hari.bounceview.BounceView;

import static android.text.Spanned.*;

public class SignupActivity extends AppCompatActivity implements View.OnClickListener {
    ActivitySignupBinding binding;
    private boolean isPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_signup);
        if (SessionManager.getInstance().getAppLanguageCode().equals("ar")) {
            getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
            binding.ivBackArrow.setImageResource(R.drawable.ic_right_arrow);
        } else {
            getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
            binding.ivBackArrow.setImageResource(R.drawable.ic_back_arrow);
        }
        initView();

    }


    private void initView() {
        BounceView.addAnimTo(binding.ivBackArrow);
        BounceView.addAnimTo(binding.txtView);
        BounceView.addAnimTo(binding.cbterms);
        BounceView.addAnimTo(binding.rlyoutLogin);
        BounceView.addAnimTo(binding.tvSignin);
        binding.edtPassword.setHint(LanguageConstants.password);
        binding.tvSignin.setText(LanguageConstants.signup);
        binding.tvtoyourAccount.setText(LanguageConstants.fornewaccount);
        binding.edtemailAddress.setHint(LanguageConstants.enterEmail);
        binding.edtfullname.setHint(LanguageConstants.fullname);
        binding.edtPhone.setHint(LanguageConstants.phonenumber);
        binding.edtReferralCode.setHint(LanguageConstants.referralCode);
        binding.txtView.setText(LanguageConstants.txtView);

      //  binding.tvbycontinue.setText(LanguageConstants.agreeterms);

        SpannableString ss = new SpannableString(CommonFunctions.getInstance().fromHtml(MessageFormat.format(LanguageConstants.agreeterms,
                "<font color=\"#f28421\">" + LanguageConstants.termsConditions + "</font>",
                "<font color=\"#f28421\">" + LanguageConstants.privacyPolicy + "</font>")));
        ClickableSpan tAndcSpan = new ClickableSpan() {
            @Override
            public void onClick(View textView) {
                Bundle mBundle = new Bundle();
                mBundle.putString("title", "terms");
                CommonFunctions.getInstance().newIntent(SignupActivity.this, CMSActivity.class, mBundle, false);
            }

            @Override
            public void updateDrawState(TextPaint ds) {
                super.updateDrawState(ds);
                ds.setUnderlineText(false);

            }
        };

        String val = CommonFunctions.getInstance().fromHtml(MessageFormat.format(LanguageConstants.agreeterms, LanguageConstants.termsConditions,
                LanguageConstants.privacyPolicy)).toString();

        Integer startTerms = val.indexOf(LanguageConstants.termsConditions);
        Integer endTerms = startTerms + LanguageConstants.termsConditions.length();
        ss.setSpan(tAndcSpan, startTerms, endTerms, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        ClickableSpan privacySpan = new ClickableSpan() {
            @Override
            public void onClick(View textView) {
                Bundle mBundle = new Bundle();
                mBundle.putString("title", "privacy");
                CommonFunctions.getInstance().newIntent(SignupActivity.this, CMSActivity.class, mBundle, false);
            }

            @Override
            public void updateDrawState(TextPaint ds) {
                super.updateDrawState(ds);
                ds.setUnderlineText(false);
            }
        };

        Integer startPrivacy = val.indexOf(LanguageConstants.privacyPolicy);
        Integer endPrivacy = startPrivacy + LanguageConstants.privacyPolicy.length();
        ss.setSpan(privacySpan, startPrivacy, endPrivacy, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        binding.tvbycontinue.setText(ss);
        binding.tvbycontinue.setMovementMethod(LinkMovementMethod.getInstance());
        binding.tvbycontinue.setHighlightColor(Color.TRANSPARENT);


        binding.txtView.setOnClickListener(this);
        binding.rlyoutLogin.setOnClickListener(this);
        binding.tvLogin.setText(LanguageConstants.signup);
        binding.tvnewuser.setText(LanguageConstants.iAlreadyHaveAnAccount);
        binding.tvSignup.setText(LanguageConstants.Signin);
        binding.tvSignup.setOnClickListener(this);
        binding.ivBackArrow.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (v == binding.rlyoutLogin) {
            if (binding.edtfullname.getText().toString().trim().isEmpty()) {
                CommonFunctions.getInstance().validationEmptyError(SignupActivity.this, LanguageConstants.nameFieldCantBlank);
            } else if (binding.edtPhone.getText().toString().trim().isEmpty()) {
                CommonFunctions.getInstance().validationEmptyError(SignupActivity.this, LanguageConstants.phoneNumberFieldCantBlank);
            } else if (binding.edtPhone.getText().toString().trim().length() < 7 || binding.edtPhone.getText().toString().trim().length() > 12) {
                CommonFunctions.getInstance().validationWarningError(SignupActivity.this, LanguageConstants.enterValidPhoneNumber);
            } else if (binding.edtemailAddress.getText().toString().trim().isEmpty()) {
                CommonFunctions.getInstance().validationEmptyError(SignupActivity.this, LanguageConstants.emailFieldCantBlank);
            } else if (!CommonFunctions.getInstance().isValidEmail(binding.edtemailAddress.getText().toString().trim())) {
                CommonFunctions.getInstance().validationWarningError(SignupActivity.this, LanguageConstants.enterValidEmail);
            } else if (binding.edtPassword.getText().toString().trim().isEmpty()) {
                CommonFunctions.getInstance().validationEmptyError(SignupActivity.this, LanguageConstants.passwordFieldCantBlank);
            } else if (binding.edtPassword.getText().toString().trim().length() < 6) {
                CommonFunctions.getInstance().validationInfoError(SignupActivity.this, LanguageConstants.passwordMustBeContainsAleast6Characters);
            } else if (!binding.cbterms.isChecked()) {
                CommonFunctions.getInstance().validationWarningError(SignupActivity.this, LanguageConstants.PleaseAcceptTermsAndCondition);
            } else {
                registerApiCall();
            }
        } else if (v == binding.tvSignup) {
            CommonFunctions.getInstance().newIntent(SignupActivity.this, SignInActivity.class, Bundle.EMPTY, false);
        } else if (v == binding.txtView) {
            if (binding.edtPassword.getText().toString().trim().isEmpty()) {
                CommonFunctions.getInstance().validationEmptyError(SignupActivity.this, LanguageConstants.oldPasswordFieldCantBlank);
            } else {
                if (isPassword) {
                    binding.txtView.setText(LanguageConstants.view);
                    binding.edtPassword.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                    isPassword = false;
                    binding.edtPassword.setSelection(binding.edtPassword.getText().length());
                } else {
                    binding.txtView.setText(LanguageConstants.hide);
                    binding.edtPassword.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
                    isPassword = true;
                    binding.edtPassword.setSelection(binding.edtPassword.getText().length());
                }
            }
        } else if (v == binding.ivBackArrow) {
            finish();

        }
    }

    private void registerApiCall() {
        CommonApiCalls.getInstance().Register(SignupActivity.this, binding.edtfullname.getText().toString().trim(),
                binding.edtemailAddress.getText().toString().trim(), binding.edtPhone.getText().toString().trim(),
                binding.edtPassword.getText().toString().trim(), "1", binding.
                        edtfullname.getText().toString().trim(),binding.edtReferralCode.getText().toString().trim(), new CommonCallback.Listener() {
                    @Override
                    public void onSuccess(Object object) {
                        RegistrationApiResponse body = (RegistrationApiResponse) object;
                        RegistrationApiResponse.Data data = body.getData();

                        Bundle mBundle = new Bundle();
                        mBundle.putString("user_key", data.getUser_key());
                        mBundle.putString("mobile_number", data.getMobile_number());
                        mBundle.putString("TYPE", "1");
                        CommonFunctions.getInstance().newIntent(SignupActivity.this, OtpVerificationActivity.class, mBundle, false);
                    }

                    @Override
                    public void onFailure(String reason) {
                        Toast.makeText(SignupActivity.this, "ERRRRR", Toast.LENGTH_SHORT).show();
                        CommonFunctions.getInstance().validationInfoError(SignupActivity.this, reason);
                    }
                });
    }
}
