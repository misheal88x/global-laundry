package com.globallaundry.activity;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.CalendarView;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.globallaundry.R;
import com.globallaundry.adapters.SelectDeliveryTimeAdapter;
import com.globallaundry.adapters.SelectPickUpTimeAdapter;
import com.globallaundry.adapters.SelectTimeAdapter;
import com.globallaundry.api.CommonApiCalls;
import com.globallaundry.apimodel.PickUpTimeSlotApiResponse;
import com.globallaundry.apimodel.TimeSlotApiResponse;
import com.globallaundry.databinding.ActivityCalanderBinding;
import com.globallaundry.interfaces.CommonCallback;
import com.globallaundry.interfaces.SelectTime;
import com.globallaundry.utils.CommonFunctions;
import com.globallaundry.utils.DateConstants;
import com.globallaundry.utils.LanguageConstants;
import com.globallaundry.utils.SessionManager;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import hari.bounceview.BounceView;

public class CalanderActivity extends AppCompatActivity implements View.OnClickListener {
    ActivityCalanderBinding binding;


    private List<PickUpTimeSlotApiResponse.Datum> dateArrayList;
    List<PickUpTimeSlotApiResponse.Time> timeList;

    // -- PickUp
    List<TimeSlotApiResponse.Pickup> pickUpDateAndTimeArrayList;
    List<TimeSlotApiResponse.Time> pickUpTimeList;

    private String pickupDate = "";
    public static String pickupTime = "";

    // -- Delivery
    List<TimeSlotApiResponse.Delivery> deliveryDateAndTimeArrayList;
    List<TimeSlotApiResponse.Time_> deliveryTimeList;

    private String deliveryDate = "";
    public static String deliveryTime = "";

    private String rushHours = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_calander);

        if (SessionManager.getInstance().getAppLanguageCode().equals("ar")) {
            getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
            binding.ivBackArrow.setImageResource(R.drawable.ic_right_arrow);
        } else {
            getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
            binding.ivBackArrow.setImageResource(R.drawable.ic_back_arrow);
        }

        if (getIntent().getExtras() != null){
            // 0 - pickup and delivery time
            // 1 - pickup time
            rushHours = getIntent().getExtras().getString("rushHours");
        }

        initView();

        // Pickup and delivery time - 0
        if (rushHours.equals("0")){

            binding.lyoutPickupOnly.setVisibility(View.GONE);
            binding.lyoutPickUpDelivery.setVisibility(View.VISIBLE);

            pickUpAndDeliveryTimeSlot();
        }
        // Pickup only  - 1
        else if (rushHours.equals("1")){
            binding.lyoutPickupOnly.setVisibility(View.VISIBLE);
            binding.lyoutPickUpDelivery.setVisibility(View.GONE);
            callPickTimeApi();
        }


        binding.simpleCalendarView.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {
            @Override
            public void onSelectedDayChange(CalendarView view, int year, int month, int dayOfMonth) {
                //Toast.makeText(getApplicationContext(), dayOfMonth + "-" + (month + 1) + "-" + year, Toast.LENGTH_LONG).show();

                pickupDate = dayOfMonth + "-" + (month + 1) + "-" + year;
                pickupDate = CommonFunctions.getInstance().changeDateFormat(DateConstants.DATE_FORMAT_DD_MM_YYYY, DateConstants.DATE_FORMAT_YYYY_DD_MM, pickupDate);

                for (int i = 0; i < dateArrayList.size(); i++) {
                    if (pickupDate.equals(dateArrayList.get(i).getDate())) {
                        timeList = new ArrayList<>();
                        timeList = dateArrayList.get(i).getTime();
                        if (timeList.size() > 0) {
                            selectAreaDialog(timeList);
                        }
                    }
                }
            }
        });

    // ----- Pick Up Time Slot

        binding.calendarViewPickUp.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {
            @Override
            public void onSelectedDayChange(CalendarView view, int year, int month, int dayOfMonth) {
                //Toast.makeText(getApplicationContext(), dayOfMonth + "-" + (month + 1) + "-" + year, Toast.LENGTH_LONG).show();

                pickupDate = dayOfMonth + "-" + (month + 1) + "-" + year;
                pickupDate = CommonFunctions.getInstance().changeDateFormat(DateConstants.DATE_FORMAT_DD_MM_YYYY, DateConstants.DATE_FORMAT_YYYY_DD_MM, pickupDate);

                for (int i = 0; i < pickUpDateAndTimeArrayList.size(); i++) {
                    if (pickupDate.equals(pickUpDateAndTimeArrayList.get(i).getDate())) {
                        pickUpTimeList = new ArrayList<>();
                        pickUpTimeList = pickUpDateAndTimeArrayList.get(i).getTimes();
                        if (pickUpTimeList.size() > 0) {
                            selectPickUpTimeListDialog(pickUpTimeList);
                        }
                    }
                }
            }
        });


        // ----- Delivery Time Slot

        binding.calendarViewDelivery.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {
            @Override
            public void onSelectedDayChange(CalendarView view, int year, int month, int dayOfMonth) {
                //Toast.makeText(getApplicationContext(), dayOfMonth + "-" + (month + 1) + "-" + year, Toast.LENGTH_LONG).show();

                deliveryDate = dayOfMonth + "-" + (month + 1) + "-" + year;
                deliveryDate = CommonFunctions.getInstance().changeDateFormat(DateConstants.DATE_FORMAT_DD_MM_YYYY, DateConstants.DATE_FORMAT_YYYY_DD_MM, deliveryDate);

                for (int i = 0; i < deliveryDateAndTimeArrayList.size(); i++) {
                    if (deliveryDate.equals(deliveryDateAndTimeArrayList.get(i).getDate())) {
                        deliveryTimeList = new ArrayList<>();
                        deliveryTimeList = deliveryDateAndTimeArrayList.get(i).getTimes();
                        if (deliveryTimeList.size() > 0) {
                            selectDeliveryTimeListDialog(deliveryTimeList);
                        }
                    }
                }
            }
        });
    }


    private void initView() {
        BounceView.addAnimTo(binding.ivBackArrow);
        BounceView.addAnimTo(binding.txtselectaddress);
        binding.tlbarText.setText(LanguageConstants.schedualdate);
        binding.txtscheduledtxt.setText(LanguageConstants.txtscheduldtxt);
        binding.txtselectaddress.setText(LanguageConstants.selectaddress);
        binding.tvSelectArea.setText(LanguageConstants.selectpicktime);

        binding.txtPickup.setText(LanguageConstants.pickupDateAndTime);
        binding.txtDelivery.setText(LanguageConstants.deliveryDateAndTime);
        binding.tvPickUpTime.setText(LanguageConstants.pickUpTime);
        binding.tvDeliveryTime.setText(LanguageConstants.deliveryTime);

        binding.ivBackArrow.setOnClickListener(this);
        binding.txtselectaddress.setOnClickListener(this);
        binding.lyoutSelectArea.setOnClickListener(this);

        binding.lyoutSelectPickUp.setOnClickListener(this);
        binding.lyoutSelectDelivery.setOnClickListener(this);

        binding.rlyoutPickUp.setOnClickListener(this);
        binding.rlyoutDelivery.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ivBackArrow:
                finish();
                break;
            case R.id.txtselectaddress:
                // Pickup and delivery time - 0
                if (rushHours.equals("0")){
                    if (pickupDate == null || pickupDate.isEmpty()) {
                        CommonFunctions.getInstance().validationEmptyError(CalanderActivity.this, LanguageConstants.selectDate);
                        return;
                    }
                    if (pickupTime == null || pickupTime.isEmpty()) {
                        CommonFunctions.getInstance().validationEmptyError(CalanderActivity.this, LanguageConstants.selectTime);
                        return;
                    }

                    if (deliveryDate == null || deliveryDate.isEmpty()) {
                        CommonFunctions.getInstance().validationEmptyError(CalanderActivity.this, LanguageConstants.selectDeliveryDate);
                        return;
                    }
                    if (deliveryTime == null || deliveryTime.isEmpty()) {
                        CommonFunctions.getInstance().validationEmptyError(CalanderActivity.this, LanguageConstants.selectDeliveryTime);
                        return;
                    }
                    SessionManager.AddressProperties.getInstance().setPickUpDate(pickupDate);
                    SessionManager.AddressProperties.getInstance().setPickUpTime(pickupTime);

                    SessionManager.AddressProperties.getInstance().setDeliveryDate(deliveryDate);
                    SessionManager.AddressProperties.getInstance().setDeliveryTime(deliveryTime);

                    CommonFunctions.getInstance().newIntent(CalanderActivity.this, SelectList.class, Bundle.EMPTY, false);
                    pickupTime = "";
                    deliveryTime = "";

                }
                // Pickup only  - 1
                else if (rushHours.equals("1")){
                    if (pickupDate == null || pickupDate.isEmpty()) {
                        CommonFunctions.getInstance().validationEmptyError(CalanderActivity.this, LanguageConstants.selectDate);
                        return;
                    }
                    if (pickupTime == null || pickupTime.isEmpty()) {
                        CommonFunctions.getInstance().validationEmptyError(CalanderActivity.this, LanguageConstants.selectTime);
                        return;
                    }
                    SessionManager.AddressProperties.getInstance().setPickUpDate(pickupDate);
                    SessionManager.AddressProperties.getInstance().setPickUpTime(pickupTime);

                    SessionManager.AddressProperties.getInstance().setDeliveryDate("");
                    SessionManager.AddressProperties.getInstance().setDeliveryTime("");
                    deliveryTime = "";

                    CommonFunctions.getInstance().newIntent(CalanderActivity.this, SelectList.class, Bundle.EMPTY, false);
                    pickupTime = "";
                }
                break;
            case R.id.lyoutSelectArea:
                if (timeList != null && timeList.size() > 0) {
                    selectAreaDialog(timeList);
                } else {
                    CommonFunctions.getInstance().validationEmptyError(CalanderActivity.this, LanguageConstants.selectDatefirst);
                }
                break;
            case R.id.lyoutSelectPickUp:
                if (pickUpTimeList != null && pickUpTimeList.size() > 0) {
                    selectPickUpTimeListDialog(pickUpTimeList);
                } else {
                    CommonFunctions.getInstance().validationEmptyError(CalanderActivity.this, LanguageConstants.selectDatefirst);
                }
                break;
            case R.id.lyoutSelectDelivery:
                if (deliveryTimeList != null && deliveryTimeList.size() > 0) {
                    selectDeliveryTimeListDialog(deliveryTimeList);
                } else {
                    if (deliveryTimeList.size() == 0){
                        CommonFunctions.getInstance().validationEmptyError(CalanderActivity.this, LanguageConstants.timeslotEmpty);
                    }
                    else {
                        CommonFunctions.getInstance().validationEmptyError(CalanderActivity.this, LanguageConstants.selectDatefirst);
                    }
                }
                break;

            case R.id.rlyoutPickUp:
                binding.viewPickup.setVisibility(View.VISIBLE);
                binding.viewDelivery.setVisibility(View.GONE);
                binding.lyoutPickup.setVisibility(View.VISIBLE);
                binding.lyoutDelivery.setVisibility(View.GONE);
                break;

            case R.id.rlyoutDelivery:
                binding.viewPickup.setVisibility(View.GONE);
                binding.viewDelivery.setVisibility(View.VISIBLE);
                binding.lyoutPickup.setVisibility(View.GONE);
                binding.lyoutDelivery.setVisibility(View.VISIBLE);
                break;

        }
    }



// --- PickUp and Delivery Time Slot
    private void pickUpAndDeliveryTimeSlot() {
        CommonApiCalls.getInstance().pickUpAndDeliveryTimeSlot(CalanderActivity.this, new CommonCallback.Listener() {
            @Override
            public void onSuccess(Object object) {
                TimeSlotApiResponse body = (TimeSlotApiResponse) object;

                if (body.getStatus().equals(200)){
                    if (body.getData() != null){
                        // --- Pick Up Date Time
                        if (body.getData().getPickup() != null && body.getData().getPickup().size() > 0){

                            pickUpDateAndTimeArrayList = body.getData().getPickup();

                            if (pickUpDateAndTimeArrayList.size() > 0) {
                                // Start Date
                                String startDate = pickUpDateAndTimeArrayList.get(0).getDate();
                                String[] startSpl = startDate.split("-");

                                String endDate = pickUpDateAndTimeArrayList.get(pickUpDateAndTimeArrayList.size() - 1).getDate();
                                String[] endSpl = endDate.split("-");

                                Calendar c = Calendar.getInstance();
                                c.set(Integer.parseInt(endSpl[0]), (Integer.parseInt(endSpl[1]) - 1), Integer.parseInt(endSpl[2]));//Year,Mounth -1,Day
                                binding.calendarViewPickUp.setMaxDate(c.getTimeInMillis());

                                Calendar c1 = Calendar.getInstance();
                                c1.set(Integer.parseInt(startSpl[0]), (Integer.parseInt(startSpl[1]) - 1), Integer.parseInt(startSpl[2]));//Year,Mounth -1,Day
                                binding.calendarViewPickUp.setMinDate(c1.getTimeInMillis());
                                binding.calendarViewPickUp.setDate(c1.getTimeInMillis());
                            }

                            for (int i = 0; i < pickUpDateAndTimeArrayList.size(); i++) {
                                if (pickUpDateAndTimeArrayList.get(0).getDate().equals(pickUpDateAndTimeArrayList.get(i).getDate())) {
                                    pickUpTimeList = new ArrayList<>();
                                    pickUpTimeList = pickUpDateAndTimeArrayList.get(i).getTimes();
                                    pickupDate = pickUpDateAndTimeArrayList.get(i).getDate();
                                    pickupDate = CommonFunctions.getInstance().changeDateFormat(DateConstants.DATE_FORMAT_YYYY_DD_MM, DateConstants.DATE_FORMAT_YYYY_DD_MM, pickupDate);
                                    break;
                                }
                            }

                        }


                        // --- Delivery Date and Time
                        if (body.getData().getDelivery() != null && body.getData().getDelivery().size() > 0){

                            deliveryDateAndTimeArrayList = body.getData().getDelivery();

                            if (deliveryDateAndTimeArrayList.size() > 0) {
                                // Start Date
                                String startDate = deliveryDateAndTimeArrayList.get(0).getDate();
                                String[] startSpl = startDate.split("-");

                                String endDate = deliveryDateAndTimeArrayList.get(deliveryDateAndTimeArrayList.size() - 1).getDate();
                                String[] endSpl = endDate.split("-");

                                Calendar c = Calendar.getInstance();
                                c.set(Integer.parseInt(endSpl[0]), (Integer.parseInt(endSpl[1]) - 1), Integer.parseInt(endSpl[2]));//Year,Mounth -1,Day
                                binding.calendarViewDelivery.setMaxDate(c.getTimeInMillis());

                                Calendar c1 = Calendar.getInstance();
                                c1.set(Integer.parseInt(startSpl[0]), (Integer.parseInt(startSpl[1]) - 1), Integer.parseInt(startSpl[2]));//Year,Mounth -1,Day
                                binding.calendarViewDelivery.setMinDate(c1.getTimeInMillis());
                                binding.calendarViewDelivery.setDate(c1.getTimeInMillis());
                            }

                            for (int i = 0; i < deliveryDateAndTimeArrayList.size(); i++) {
                                if (deliveryDateAndTimeArrayList.get(0).getDate().equals(deliveryDateAndTimeArrayList.get(i).getDate())) {
                                    deliveryTimeList = new ArrayList<>();
                                    deliveryTimeList = deliveryDateAndTimeArrayList.get(i).getTimes();
                                    deliveryDate = deliveryDateAndTimeArrayList.get(i).getDate();
                                    deliveryDate = CommonFunctions.getInstance().changeDateFormat(DateConstants.DATE_FORMAT_YYYY_DD_MM, DateConstants.DATE_FORMAT_YYYY_DD_MM, deliveryDate);

                                    break;
                                }
                            }

                        }
                    }
                }



            }

            @Override
            public void onFailure(String reason) {
            }
        });
    }

// ---- Pick Up Time Slot
    private void selectPickUpTimeListDialog(List<TimeSlotApiResponse.Time> pickUpTimeList) {
        final Dialog dialog = new Dialog(CalanderActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_select_area);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);

        ImageView imgClose = (ImageView) dialog.findViewById(R.id.imgClose);
        RecyclerView recyclerView = (RecyclerView) dialog.findViewById(R.id.recyclerView);


        @SuppressLint("WrongConstant")
        LinearLayoutManager layoutManager = new LinearLayoutManager(CalanderActivity.this, LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);
        SelectPickUpTimeAdapter adapter = new SelectPickUpTimeAdapter(CalanderActivity.this, pickUpTimeList, new SelectTime() {
            @Override
            public void onClick(String value, String name) {
                binding.tvPickUpTime.setText(name);
                pickupTime = value;
                dialog.dismiss();
            }
        });
        recyclerView.setAdapter(adapter);

        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }



// ---- Delivery Time Slot
    private void selectDeliveryTimeListDialog(List<TimeSlotApiResponse.Time_> deliveryTimeList) {
        final Dialog dialog = new Dialog(CalanderActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_select_area);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);

        ImageView imgClose = (ImageView) dialog.findViewById(R.id.imgClose);
        RecyclerView recyclerView = (RecyclerView) dialog.findViewById(R.id.recyclerView);


        @SuppressLint("WrongConstant")
        LinearLayoutManager layoutManager = new LinearLayoutManager(CalanderActivity.this, LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);
        SelectDeliveryTimeAdapter adapter = new SelectDeliveryTimeAdapter(CalanderActivity.this, deliveryTimeList, new SelectTime() {
            @Override
            public void onClick(String value, String name) {
                binding.tvDeliveryTime.setText(name);
                deliveryTime = value;
                dialog.dismiss();
            }
        });
        recyclerView.setAdapter(adapter);

        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }




    private void callPickTimeApi() {
        CommonApiCalls.getInstance().pickTimeSlot(CalanderActivity.this, new CommonCallback.Listener() {
                    @Override
                    public void onSuccess(Object object) {
                        PickUpTimeSlotApiResponse body = (PickUpTimeSlotApiResponse) object;
                        dateArrayList = body.getData();

                        if (dateArrayList.size() > 0) {
                            // Start Date
                            String startDate = dateArrayList.get(0).getDate();
                            String[] startSpl = startDate.split("-");

                            String endDate = dateArrayList.get(dateArrayList.size() - 1).getDate();
                            String[] endSpl = endDate.split("-");

                            Calendar c = Calendar.getInstance();
                            c.set(Integer.parseInt(endSpl[2]), (Integer.parseInt(endSpl[1]) - 1), Integer.parseInt(endSpl[0]));//Year,Mounth -1,Day
                            binding.simpleCalendarView.setMaxDate(c.getTimeInMillis());

                            Calendar c1 = Calendar.getInstance();
                            c1.set(Integer.parseInt(startSpl[2]), (Integer.parseInt(startSpl[1]) - 1), Integer.parseInt(startSpl[0]));//Year,Mounth -1,Day
                            binding.simpleCalendarView.setMinDate(c1.getTimeInMillis());
                            binding.simpleCalendarView.setDate(c1.getTimeInMillis());
                        }

                        for (int i = 0; i < dateArrayList.size(); i++) {
                            if (dateArrayList.get(0).getDate().equals(dateArrayList.get(i).getDate())) {
                                timeList = new ArrayList<>();
                                timeList = dateArrayList.get(i).getTime();
                                pickupDate = dateArrayList.get(i).getDate();
                                pickupDate = CommonFunctions.getInstance().changeDateFormat(DateConstants.DATE_FORMAT_DD_MM_YYYY, DateConstants.DATE_FORMAT_YYYY_DD_MM, pickupDate);

                                break;
                            }
                        }
                    }

                    @Override
                    public void onFailure(String reason) {
                    }
                });
    }

   private void selectAreaDialog(List<PickUpTimeSlotApiResponse.Time> timeList) {
        final Dialog dialog = new Dialog(CalanderActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_select_area);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);

        ImageView imgClose = (ImageView) dialog.findViewById(R.id.imgClose);
        RecyclerView recyclerView = (RecyclerView) dialog.findViewById(R.id.recyclerView);


        @SuppressLint("WrongConstant")
        LinearLayoutManager layoutManager = new LinearLayoutManager(CalanderActivity.this, LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);
        SelectTimeAdapter adapter = new SelectTimeAdapter(CalanderActivity.this, timeList, new SelectTime() {
            @Override
            public void onClick(String value, String name) {
                binding.tvSelectArea.setText(value);
                pickupTime = value;
                dialog.dismiss();
            }
        });
        recyclerView.setAdapter(adapter);

        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }


    @Override
    protected void onResume() {
        super.onResume();
        binding.tvSelectArea.setText(LanguageConstants.selectpicktime);
        pickupTime = "";

        binding.tvPickUpTime.setText(LanguageConstants.pickUpTime);

        binding.tvDeliveryTime.setText(LanguageConstants.deliveryTime);
        deliveryTime = "";
    }
}
