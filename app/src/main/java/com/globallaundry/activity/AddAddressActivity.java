package com.globallaundry.activity;

import android.os.Bundle;
import android.view.Gravity;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import com.globallaundry.R;
import com.globallaundry.api.CommonApiCalls;
import com.globallaundry.apimodel.AddressTypeApiResponse;
import com.globallaundry.apimodel.AreaListApiResponse;
import com.globallaundry.apimodel.CityListApiResponse;
import com.globallaundry.databinding.ActivityAddAddressBinding;
import com.globallaundry.fragments.AddressBookFragment;
import com.globallaundry.interfaces.CommonCallback;
import com.globallaundry.spinnerdialog.OnSpinerItemClick;
import com.globallaundry.spinnerdialog.SpinnerDialog;
import com.globallaundry.spinnerdialog.SpinnerModel;
import com.globallaundry.utils.CommonFunctions;
import com.globallaundry.utils.LanguageConstants;
import com.globallaundry.utils.SessionManager;

import java.util.ArrayList;
import java.util.List;


public class AddAddressActivity extends AppCompatActivity implements View.OnClickListener {

    ActivityAddAddressBinding binding;
    List<AddressTypeApiResponse.Datum> mAddressTypeData = null;
    List<CityListApiResponse.Datum> mCityListData = null;
    List<AreaListApiResponse.Datum> mAreaListData = null;
    String SELECEDADDRESS = "";
    String destClass = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_add_address);
        if (SessionManager.getInstance().getAppLanguageCode().equals("ar")) {
            getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
            binding.ivBackArrow.setImageResource(R.drawable.ic_right_arrow);
            binding.edFlatNo.setGravity(Gravity.END);
            binding.edApartment.setGravity(Gravity.END);
            binding.edStreetName.setGravity(Gravity.END);
            binding.edCompanyName.setGravity(Gravity.END);
            binding.edLandMark.setGravity(Gravity.END);

        } else {
            getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
            binding.ivBackArrow.setImageResource(R.drawable.ic_back_arrow);
            binding.edFlatNo.setGravity(Gravity.START);
            binding.edApartment.setGravity(Gravity.START);
            binding.edStreetName.setGravity(Gravity.START);
            binding.edCompanyName.setGravity(Gravity.START);
            binding.edLandMark.setGravity(Gravity.START);
        }
        if (getIntent().getExtras() != null) {
            SELECEDADDRESS = getIntent().getExtras().getString("SELECEDADDRESS");
            destClass = getIntent().getExtras().getString("From");
        }
        LoadAddressType();

        initViews();
    }

    private void LoadAddressType() {

        CommonApiCalls.getInstance().addressType(AddAddressActivity.this, new CommonCallback.Listener() {
            @Override
            public void onSuccess(Object body) {
                AddressTypeApiResponse mAddressTypeApiResponse = (AddressTypeApiResponse) body;
                mAddressTypeData = mAddressTypeApiResponse.getData();
            }

            @Override
            public void onFailure(String reason) {

            }
        });

        binding.edCity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (mCityListData == null)
                    return;

                final ArrayList<SpinnerModel> mSpinnerModels = new ArrayList<>();
                SpinnerModel mSpinnerModel;
                for (int count = 0; count < mCityListData.size(); count++) {
                    mSpinnerModel = new SpinnerModel();
                    mSpinnerModel.setId(mCityListData.get(count).getCityKey());
                    mSpinnerModel.setName(mCityListData.get(count).getCityName());
                    mSpinnerModels.add(mSpinnerModel);
                }
                SpinnerDialog spinnerDialog;
                spinnerDialog = new SpinnerDialog(AddAddressActivity.this, mSpinnerModels, LanguageConstants.City);
                spinnerDialog.bindOnSpinerListener(new OnSpinerItemClick() {
                    @Override
                    public void onClick(String item, int position) {
                        binding.edCity.setText(mSpinnerModels.get(position).getName());
                        binding.edCity.setTag(mSpinnerModels.get(position).getId());


                        callAreaApi(mSpinnerModels.get(position).getId());
                    }
                });
                spinnerDialog.showSpinerDialog();
            }
        });

        binding.edArea.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (mAreaListData == null)
                    return;

                final ArrayList<SpinnerModel> mSpinnerModels = new ArrayList<>();
                SpinnerModel mSpinnerModel;
                for (int count = 0; count < mAreaListData.size(); count++) {
                    mSpinnerModel = new SpinnerModel();
                    mSpinnerModel.setId(mAreaListData.get(count).getAreaKey());
                    mSpinnerModel.setName(mAreaListData.get(count).getAreaName());
                    mSpinnerModels.add(mSpinnerModel);
                }
                SpinnerDialog spinnerDialog;
                spinnerDialog = new SpinnerDialog(AddAddressActivity.this, mSpinnerModels, LanguageConstants.area);
                spinnerDialog.bindOnSpinerListener(new OnSpinerItemClick() {
                    @Override
                    public void onClick(String item, int position) {
                        binding.edArea.setText(mSpinnerModels.get(position).getName());
                        binding.edArea.setTag(mSpinnerModels.get(position).getId());
                    }
                });
                spinnerDialog.showSpinerDialog();
            }
        });


        callCityApi();

    }

    private void callCityApi() {

        CommonApiCalls.getInstance().cityList(AddAddressActivity.this, new CommonCallback.Listener() {
            @Override
            public void onSuccess(Object body) {
                CityListApiResponse mCityListApiResponse = (CityListApiResponse) body;
                mCityListData = mCityListApiResponse.getData();
            }

            @Override
            public void onFailure(String reason) {

            }
        });
    }

    private void callAreaApi(String cityId) {
       // binding.edArea.setHint(LanguageConstants.area);
        binding.edArea.setTag(null);

        CommonApiCalls.getInstance().areaList(AddAddressActivity.this, cityId, new CommonCallback.Listener() {
            @Override
            public void onSuccess(Object body) {
                AreaListApiResponse mAreaListApiResponse = (AreaListApiResponse) body;
                mAreaListData = mAreaListApiResponse.getData();
            }

            @Override
            public void onFailure(String reason) {

            }
        });
    }

    private void initViews() {
        binding.ivRightIcon.setVisibility(View.GONE);
        binding.tlbarText.setText(LanguageConstants.addAddress);
        binding.ivBackArrow.setOnClickListener(this);
        binding.edAddressType.setOnClickListener(this);
        binding.btnSubmit.setOnClickListener(this);
        binding.tiAddressType.setHint(LanguageConstants.AddressType);
        binding.tiArea.setHint(LanguageConstants.area);
        binding.tiCity.setHint(LanguageConstants.City);
        binding.tiFlatNo.setHint(LanguageConstants.FlatNo);
        binding.tiApartment.setHint(LanguageConstants.Apartment);
        binding.tiStreetName.setHint(LanguageConstants.StreetName);
        binding.tiCompanyName.setHint(LanguageConstants.CompanyName + " " + LanguageConstants.Optional);
        binding.tiLandMark.setHint(LanguageConstants.LandMark + " " + LanguageConstants.Optional);
        binding.tvSubmit.setText(LanguageConstants.Add);
        binding.edStreetName.setText(SELECEDADDRESS);

        binding.ivAddressTypeArrow.setOnClickListener(this);
        binding.ivAreaArrow.setOnClickListener(this);
        binding.ivCityArrow.setOnClickListener(this);

        binding.btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                loadAddAddressApi();


            }
        });

    }

    private void loadAddAddressApi() {

        if (binding.edAddressType.getTag() == null) {
            CommonFunctions.getInstance().validationEmptyError(AddAddressActivity.this, LanguageConstants.AddressType);
            return;
        }
        if (binding.edCity.getTag() == null) {
            CommonFunctions.getInstance().validationEmptyError(AddAddressActivity.this, LanguageConstants.City);
            return;
        }
        if (binding.edArea.getTag() == null) {
            CommonFunctions.getInstance().validationEmptyError(AddAddressActivity.this, LanguageConstants.area);
            return;
        }
      /*  if (edFlatNo.getText().toString().isEmpty()) {
            CommonFunctions.getInstance().EmptyField(AddAddressActivity.this, Constants.FlatNo);
            return;
        }
        if (edApartment.getText().toString().isEmpty()) {
            CommonFunctions.getInstance().EmptyField(AddAddressActivity.this, Constants.Apartment);
            return;
        }*/
        if (binding.edStreetName.getText().toString().isEmpty()) {
            CommonFunctions.getInstance().validationEmptyError(AddAddressActivity.this, LanguageConstants.StreetName);
            return;
        }

        CommonApiCalls.getInstance().addAddress(AddAddressActivity.this, binding.edAddressType.getTag().toString(), binding.edCity.getTag().toString(), binding.edArea.getTag().toString(),
                binding.edFlatNo.getText().toString().trim(),
                binding.edApartment.getText().toString().trim(),
                binding.edStreetName.getText().toString().trim(),
                binding.edCompanyName.getText().toString().trim(),
                binding.edLandMark.getText().toString().trim(),
                SessionManager.LocationProperties.getInstance().getLatitude(),
                SessionManager.LocationProperties.getInstance().getLongitude(),
                new CommonCallback.Listener() {
                    @Override
                    public void onSuccess(Object object) {

                        CommonFunctions.getInstance().FinishActivityWithDelay(AddAddressActivity.this);
                        AddressBookFragment.isNeedToRefresh = true;
                        SelectList.isNeedToRefresh = true;
                        //PickupDetailsActivity.isNeedToRefresh = true;
                        //DeliveryDetailsActivity.isNeedToRefresh = true;

                    }

                    @Override
                    public void onFailure(String reason) {

                    }
                }
        );


    }

    @Override
    public void onClick(View view) {
        if (view == binding.ivBackArrow) {
            finish();
        } else if (view == binding.edAddressType) {

            if (mAddressTypeData != null) {

                final ArrayList<SpinnerModel> mSpinnerModels = new ArrayList<>();
                SpinnerModel mSpinnerModel;
                for (int count = 0; count < mAddressTypeData.size(); count++) {
                    mSpinnerModel = new SpinnerModel();
                    mSpinnerModel.setId(mAddressTypeData.get(count).getAddressTypeKey());
                    mSpinnerModel.setName(mAddressTypeData.get(count).getAddressTypeName());
                    mSpinnerModels.add(mSpinnerModel);
                }
                SpinnerDialog spinnerDialog;
                spinnerDialog = new SpinnerDialog(AddAddressActivity.this, mSpinnerModels, LanguageConstants.AddressType);
                spinnerDialog.bindOnSpinerListener(new OnSpinerItemClick() {
                    @Override
                    public void onClick(String item, int position) {
                        binding.edAddressType.setText(mSpinnerModels.get(position).getName());
                        binding.edAddressType.setTag(mSpinnerModels.get(position).getId());
                    }
                });
                spinnerDialog.showSpinerDialog();
            } else if (view == binding.btnSubmit) {
                if (destClass.equalsIgnoreCase("1")) {
                    CommonFunctions.getInstance().newIntent(AddAddressActivity.this, SelectList.class, Bundle.EMPTY, true);
                }
                finish();
            }
        } else if (view == binding.ivAddressTypeArrow) {
            binding.edAddressType.performClick();
        } else if (view == binding.ivCityArrow) {
            binding.edCity.performClick();
        } else if (view == binding.ivAreaArrow) {
            binding.edArea.performClick();
        }
    }
}
