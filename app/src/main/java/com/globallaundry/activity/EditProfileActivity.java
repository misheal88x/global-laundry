package com.globallaundry.activity;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.view.Gravity;
import android.view.View;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.globallaundry.R;
import com.globallaundry.databinding.EditActivityProfileBinding;
import com.globallaundry.dbmodel.RealmLibrary;
import com.globallaundry.utils.CommonFunctions;
import com.globallaundry.utils.LanguageConstants;
import com.globallaundry.utils.SessionManager;
import com.mikelau.croperino.Croperino;
import com.mikelau.croperino.CroperinoConfig;
import com.mikelau.croperino.CroperinoFileUtil;
import java.io.File;
import java.text.MessageFormat;
import hari.bounceview.BounceView;


public class EditProfileActivity extends AppCompatActivity implements View.OnClickListener {
    EditActivityProfileBinding binding;
    Bitmap profilePortfolioBitmap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = DataBindingUtil.setContentView(this, R.layout.edit_activity_profile);

        if (SessionManager.getInstance().getAppLanguageCode().equals("ar")) {
            getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
            binding.ivBackArrow.setImageResource(R.drawable.ic_right_arrow);
            binding.tvChangePassword.setGravity(Gravity.RIGHT);
        } else {
            getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
            binding.ivBackArrow.setImageResource(R.drawable.ic_back_arrow);
            binding.tvChangePassword.setGravity(Gravity.LEFT);
        }

        new CroperinoConfig("IMG_" + System.currentTimeMillis() + ".jpg", "/Pictures", "/sdcard/Pictures");
        CroperinoFileUtil.setupDirectory(EditProfileActivity.this);

        initView();
    }

    private void initView() {
        BounceView.addAnimTo(binding.ivBackArrow);
        BounceView.addAnimTo(binding.lyoutChangePassword);
        binding.tvChangePassword.setText(LanguageConstants.changeYourPassword);
        binding.tvNameHint.setHint(LanguageConstants.Name);
        binding.tvLastNameHint.setHint(LanguageConstants.LastName);
        binding.tvPhoneNumberHint.setHint(LanguageConstants.phonenumbertxt);
        binding.tvEmailHint.setHint(LanguageConstants.emailtxt);
        binding.tvPasswordHint.setHint(LanguageConstants.passwordtxt);
        binding.tlbarText.setText(LanguageConstants.profile);
        binding.tvEditProfile.setText(LanguageConstants.editprofiletxt);
        binding.tvUpdate.setText(LanguageConstants.updateProfile);

        binding.lyoutChangePassword.setOnClickListener(this);
        binding.tvEditProfile.setOnClickListener(this);
        binding.rlProfileImage.setOnClickListener(this);
        binding.ivBackArrow.setOnClickListener(this);
        binding.lllogout.setOnClickListener(this);

        SpannableString ss = new SpannableString(CommonFunctions.fromHtml(MessageFormat.format(LanguageConstants.editprofiletxt,
                "<font color=\"\">" + LanguageConstants.edit + "</font>")));
        ClickableSpan tAndcSpan = new ClickableSpan() {
            @Override
            public void onClick(View textView) {

            }

            @Override
            public void updateDrawState(TextPaint ds) {
                super.updateDrawState(ds);
                ds.setUnderlineText(false);
            }
        };

        String val = CommonFunctions.fromHtml(MessageFormat.format(LanguageConstants.editprofiletxt, LanguageConstants.edit)).toString();
        Integer startTerms = val.indexOf(LanguageConstants.edit);
        Integer endTerms = startTerms + LanguageConstants.edit.length();
        ss.setSpan(tAndcSpan, startTerms, endTerms, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        binding.tvEditProfile.setText(ss);
        binding.tvEditProfile.setMovementMethod(LinkMovementMethod.getInstance());
        binding.tvEditProfile.setHighlightColor(Color.TRANSPARENT);

        binding.tvName.setText(SessionManager.getInstance().getUserName());
        binding.tvPhoneNumber.setText(SessionManager.getInstance().getUserMobileNumber());
        binding.tvEmail.setText(SessionManager.getInstance().getUserEmail());
    }

    @Override
    public void onClick(View v) {
        if (v == binding.ivBackArrow) {
            finish();
        } else if (v == binding.rlProfileImage) {
            if (CroperinoFileUtil.verifyStoragePermissions(EditProfileActivity.this)) {
                prepareChooser();
            }

        } else if (v == binding.lyoutChangePassword) {
            CommonFunctions.getInstance().newIntent(EditProfileActivity.this, ChangePassword.class, Bundle.EMPTY, false);
        }
        else if (v == binding.lllogout) {
            SessionManager.getInstance().Logout();
            RealmLibrary.getInstance().clearCart();
            Intent i = new Intent(EditProfileActivity.this, SignInActivity.class);
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(i);
        }

    }

    // ---- Pick Image Crop
    private void prepareChooser() {
        Croperino.prepareChooser(EditProfileActivity.this, LanguageConstants.selectImageFrom, ContextCompat.getColor(EditProfileActivity.this, R.color.colorPrimary));
    }

    private void prepareCamera() {
        Croperino.prepareCamera(EditProfileActivity.this);
    }

    // ---  On-Request--Permissions--Result
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == CroperinoFileUtil.REQUEST_CAMERA) {
            for (int i = 0; i < permissions.length; i++) {
                String permission = permissions[i];
                int grantResult = grantResults[i];

                if (permission.equals(Manifest.permission.CAMERA)) {
                    if (grantResult == PackageManager.PERMISSION_GRANTED) {
                        prepareCamera();
                    }
                }
            }
        } else if (requestCode == CroperinoFileUtil.REQUEST_EXTERNAL_STORAGE) {
            boolean wasReadGranted = false;
            boolean wasWriteGranted = false;

            for (int i = 0; i < permissions.length; i++) {
                String permission = permissions[i];
                int grantResult = grantResults[i];

                if (permission.equals(Manifest.permission.READ_EXTERNAL_STORAGE)) {
                    if (grantResult == PackageManager.PERMISSION_GRANTED) {
                        wasReadGranted = true;
                    }
                }
                if (permission.equals(Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                    if (grantResult == PackageManager.PERMISSION_GRANTED) {
                        wasWriteGranted = true;
                    }
                }
            }

            if (wasReadGranted && wasWriteGranted) {
                prepareChooser();
            }
        }
    }

    // -- On Activity Result
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // ---- Image Crop
        switch (requestCode) {
            case CroperinoConfig.REQUEST_TAKE_PHOTO:
                if (resultCode == Activity.RESULT_OK) {
                    Croperino.runCropImage(CroperinoFileUtil.getTempFile(), EditProfileActivity.this, true, 1, 1, R.color.gray, R.color.gray_variant);
                }
                break;
            case CroperinoConfig.REQUEST_PICK_FILE:
                if (resultCode == Activity.RESULT_OK) {
                    CroperinoFileUtil.newGalleryFile(data, EditProfileActivity.this);
                    Croperino.runCropImage(CroperinoFileUtil.getTempFile(), EditProfileActivity.this, true, 1, 1, R.color.gray, R.color.gray_variant);
                }
                break;
            case CroperinoConfig.REQUEST_CROP_PHOTO:
                if (resultCode == Activity.RESULT_OK) {
                    Uri uri = Uri.fromFile(CroperinoFileUtil.getTempFile());
                    File mFileRelationPhoto = CroperinoFileUtil.getTempFile();

                    String filePath = mFileRelationPhoto.getPath();
                    profilePortfolioBitmap = BitmapFactory.decodeFile(filePath);
                    Glide.with(EditProfileActivity.this).load(filePath).apply(RequestOptions.circleCropTransform()).into(binding.ivProfile);

                }
                break;
            default:
                break;
        }

    }

}
