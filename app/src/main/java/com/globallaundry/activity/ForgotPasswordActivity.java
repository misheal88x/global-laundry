package com.globallaundry.activity;

import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import com.globallaundry.R;
import com.globallaundry.api.CommonApiCalls;
import com.globallaundry.apimodel.ForgotPasswordApiResponse;
import com.globallaundry.databinding.ActivityForgotPasswordBinding;
import com.globallaundry.interfaces.CommonCallback;
import com.globallaundry.utils.CommonFunctions;
import com.globallaundry.utils.LanguageConstants;
import com.globallaundry.utils.SessionManager;

import hari.bounceview.BounceView;

public class ForgotPasswordActivity extends AppCompatActivity implements View.OnClickListener {
    ActivityForgotPasswordBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_forgot_password);
        if (SessionManager.getInstance().getAppLanguageCode().equals("ar")) {
            getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
            binding.ivBackArrow.setImageResource(R.drawable.ic_right_arrow);
        } else {
            getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
            binding.ivBackArrow.setImageResource(R.drawable.ic_back_arrow);
        }
        initView();
    }

    private void initView() {
        BounceView.addAnimTo(binding.ivBackArrow);
        BounceView.addAnimTo(binding.rlyoutLogin);
        binding.tvForgotPasswordtxt.setText(LanguageConstants.forgotPassword);
        binding.edtemailAddress.setHint(LanguageConstants.enterEmail);
        binding.tvsubmit.setText(LanguageConstants.submit);
        binding.rlyoutLogin.setOnClickListener(this);
        binding.ivBackArrow.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (v == binding.rlyoutLogin) {
            if (binding.edtemailAddress.getText().toString().isEmpty()) {
                CommonFunctions.getInstance().validationEmptyError(ForgotPasswordActivity.this, LanguageConstants.emailFieldCantBlank);
                return;
            }
            if (!CommonFunctions.getInstance().isValidEmail(binding.edtemailAddress.getText().toString().trim())) {
                CommonFunctions.getInstance().validationWarningError(ForgotPasswordActivity.this, LanguageConstants.enterValidEmail);
                return;
            }
            CommonApiCalls.getInstance().forgotPassword(ForgotPasswordActivity.this, binding.edtemailAddress.getText().toString(),
                    new CommonCallback.Listener() {
                        @Override
                        public void onSuccess(Object object) {
                            ForgotPasswordApiResponse body = (ForgotPasswordApiResponse) object;
                            ForgotPasswordApiResponse.Data data = body.getData();
                           /* Bundle mBundle = new Bundle();
                            mBundle.putString("otp_id", data.getOtp_id() + "");
                            mBundle.putString("TYPE","2");
                            mBundle.putString("MOBILE_NUMBER",binding.edtemailAddress.getText().toString());
                            CommonFunctions.getInstance().newIntent(ForgotPasswordActivity.this, OtpVerificationActivity.class, mBundle, true);*/

                        }

                        @Override
                        public void onFailure(String reason) {

                        }
                    }
            );
        } else if (v == binding.ivBackArrow) {
            finish();
        }

    }
}
