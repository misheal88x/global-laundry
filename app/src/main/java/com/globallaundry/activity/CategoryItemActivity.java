package com.globallaundry.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.GridLayoutManager;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.globallaundry.R;
import com.globallaundry.adapters.CategoryItemAdapter;
import com.globallaundry.api.CommonApiCalls;
import com.globallaundry.apimodel.ClothTypeApiResponse;
import com.globallaundry.apimodel.GetSubscribedItemsTypes;
import com.globallaundry.databinding.ActivityCategoryItemBinding;
import com.globallaundry.dbmodel.ItemRealmList;
import com.globallaundry.dbmodel.RealmLibrary;
import com.globallaundry.events.CartRefresh;
import com.globallaundry.interfaces.CommonCallback;
import com.globallaundry.utils.CommonFunctions;
import com.globallaundry.utils.LanguageConstants;
import com.globallaundry.utils.MyApplication;
import com.globallaundry.utils.SessionManager;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.List;

import hari.bounceview.BounceView;
import io.realm.RealmResults;

public class CategoryItemActivity extends AppCompatActivity implements View.OnClickListener {
    ActivityCategoryItemBinding binding;
    EventBus myEventBus = EventBus.getDefault();
    Double totalPrice = 0.0;
    private List<GetSubscribedItemsTypes.Item> itemTypesList;

    @Override

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_category_item);
        myEventBus.register(this);
        if (SessionManager.getInstance().getAppLanguageCode().equals("ar")) {
            getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
            binding.ivBackArrow.setImageResource(R.drawable.ic_right_arrow);
        } else {
            getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
            binding.ivBackArrow.setImageResource(R.drawable.ic_back_arrow);
        }
        initView();
    }

    private void initView() {
        BounceView.addAnimTo(binding.ivBackArrow);
        binding.tlbarText.setText(LanguageConstants.Services);
        binding.categoryTitle.setText(LanguageConstants.categorytitle);
        binding.ivBackArrow.setOnClickListener(this);
        binding.checkouttxt.setOnClickListener(this);
        categoryItemApi();
        LoadCartCount();
    }

    private void categoryItemApi() {
        CommonApiCalls.getInstance().clothTypeList(CategoryItemActivity.this, new CommonCallback.Listener() {
            @Override
            public void onSuccess(Object body) {
                ClothTypeApiResponse mClothTypeApiResponse = (ClothTypeApiResponse) body;

                Log.d("ggggggggggggg", mClothTypeApiResponse.getData().size() + "");

                // set adapter
                GridLayoutManager layoutManager = new GridLayoutManager(CategoryItemActivity.this, 3);
                binding.rvCategory.setLayoutManager(layoutManager);
                CategoryItemAdapter adapter = new CategoryItemAdapter(CategoryItemActivity.this, mClothTypeApiResponse.getData());
                binding.rvCategory.setAdapter(adapter);
            }

            @Override
            public void onFailure(String reason) {

            }
        });
        // set adapter

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ivBackArrow:
                finish();
                break;
            case R.id.checkouttxt:
                CommonFunctions.getInstance().newIntent(CategoryItemActivity.this, CartActivity.class, Bundle.EMPTY, false);
                break;
            default:
                break;
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        LoadCartCount();
    }

    @SuppressLint("SetTextI18n")
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(CartRefresh event) {
        LoadCartCount();
    }

    private void LoadCartCount() {

        RealmResults<ItemRealmList> items = RealmLibrary.getInstance().getCartList();
        if (items.size() > 0) {
            CommonApiCalls.getInstance().getSubscribedItemsTypes(this, new CommonCallback.Listener() {
                @Override
                public void onSuccess(Object body) {
                    GetSubscribedItemsTypes response = (GetSubscribedItemsTypes) body;
                    itemTypesList = response.getData();


                    binding.llMycart.setVisibility(View.VISIBLE);
                    binding.llBottomlayout.setVisibility(View.GONE);
                    binding.itemCount.setVisibility(View.VISIBLE);
                    binding.txtTotalPrice.setVisibility(View.VISIBLE);
                    binding.itemCount.setText(items.size() + " " + LanguageConstants.items);
                    binding.txtTotalPrice.setText(CommonFunctions.getInstance().roundOffDecimalValue(Double.valueOf(total()), true));
                }

                @Override
                public void onFailure(String reason) {
                    MyApplication.displayKnownError(reason);
                }
            });

        } else {
            binding.itemCount.setVisibility(View.GONE);
            binding.llMycart.setVisibility(View.GONE);
            binding.txtTotalPrice.setVisibility(View.GONE);
            binding.llBottomlayout.setVisibility(View.VISIBLE);
        }
    }

    private String total() {

        totalPrice = 0.0;
        RealmResults<ItemRealmList> itemList = RealmLibrary.getInstance().getCartList();

        for (int count = 0; count < itemList.size(); count++) {
            ItemRealmList item = itemList.get(count);

            int virtualQty = Integer.parseInt(item.getQuantity());

            int checkSubscribedPackage = CommonFunctions.getInstance().findItemInMyPackages(item.getCloth_type_key(),
                Integer.parseInt(item.getQuantity()), itemTypesList);

            if (checkSubscribedPackage != Integer.MIN_VALUE) {
                if (checkSubscribedPackage > 0)
                    continue;
                else {
                    virtualQty = Math.abs(checkSubscribedPackage);
                }

            }

            Log.d("XXXXXXX", "" + virtualQty);

            Double itemPrice = (item.getPrice());
            Double ingPrice = 0.0;
            for (int count1 = 0; count1 < item.getSize().size(); count1++) {
                if (item.getIs_express() == 1) {
                    ingPrice = ingPrice + ((item.getSize().get(count1).getPrice() + Double.parseDouble(item.getExpress_amount().replace(",", ""))) * item.getSize().get(count1).getSize_quantity());
                } else {
                    ingPrice = ingPrice + ((item.getSize().get(count1).getPrice()) * item.getSize().get(count1).getSize_quantity());
                }
            }

            if (item.getAdditional_quantity() > 0) {
                if (item.getIs_express() == 1) {
                    totalPrice = totalPrice + (((itemPrice + ingPrice + Double.parseDouble(item.getExpress_amount().replace(",", ""))) * virtualQty) * item.getAdditional_quantity());
                } else {
                    totalPrice = totalPrice + (((itemPrice + ingPrice) * virtualQty) * item.getAdditional_quantity());
                }
            } else {
                if (item.getItem_type() == 4) {
                    if (item.getIs_express() == 1) {
                        totalPrice = totalPrice + ((itemPrice + ingPrice + Double.parseDouble(item.getExpress_amount().replace(",", ""))) * virtualQty);
                    } else {
                        totalPrice = totalPrice + ((itemPrice + ingPrice) * virtualQty);
                    }
                } else {
                    totalPrice = totalPrice + ((itemPrice + ingPrice) * virtualQty);
                }

            }

        }
        return String.valueOf(totalPrice);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        myEventBus.unregister(this);
    }



}

