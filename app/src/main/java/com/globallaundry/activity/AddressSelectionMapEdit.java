package com.globallaundry.activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;

import com.globallaundry.R;
import com.globallaundry.databinding.ActivityAddressSelectionMapBinding;
import com.globallaundry.utils.AppConstants;
import com.globallaundry.utils.CommonFunctions;
import com.globallaundry.utils.LanguageConstants;
import com.globallaundry.utils.SessionManager;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.tasks.OnSuccessListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import lolodev.permissionswrapper.callback.OnRequestPermissionsCallBack;
import lolodev.permissionswrapper.wrapper.PermissionWrapper;

public class AddressSelectionMapEdit extends AppCompatActivity implements OnMapReadyCallback,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener, AdapterView.OnItemClickListener, View.OnClickListener {


    ActivityAddressSelectionMapBinding binding;
    private LocationManager locationManager;
    private GoogleMap mMap;
    Double mCurrentLatitiude = 0.0, mCurrentLongitude = 0.0;
    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;
    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;
    private Location mLastLocation;
    private static final String PLACES_API_BASE = "https://maps.googleapis.com/maps/api/place";
    private static final String TYPE_AUTOCOMPLETE = "/autocomplete";
    private static final String OUT_JSON = "/json";
    private String str;
    private Activity activity;


    private GoogleApiClient googleApiClient;

    private Marker mCurrLocationMarker;
    private String mAddressKey;
    private String address_type;
    private String address_type_name;
    private String streetName;
    private String company;
    private String landmark;
    private double latitude;
    private double longitude;
    private String city_key;
    private String city_name;
    private String area_key;
    private String area_name;
    private String flatno;
    private String apartment;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_address_selection_map);
        if (SessionManager.getInstance().getAppLanguageCode().equals("ar")) {
            getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
            binding.ivBackArrow.setImageResource(R.drawable.ic_right_arrow);
        } else {
            getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
            binding.ivBackArrow.setImageResource(R.drawable.ic_back_arrow);
        }
        initView();
        activity = this;
        if (!getIntent().getExtras().isEmpty()) {
            mAddressKey = getIntent().getExtras().getString("address_key");
            address_type = getIntent().getExtras().getString("address_type");
            address_type_name = getIntent().getExtras().getString("address_type_name");
            streetName = getIntent().getExtras().getString("streetname");
            company = getIntent().getExtras().getString("company");
            landmark = getIntent().getExtras().getString("landmark");
            latitude = getIntent().getExtras().getDouble("latitude");
            longitude = getIntent().getExtras().getDouble("logitude");
            city_key = getIntent().getExtras().getString("city_key");
            city_name = getIntent().getExtras().getString("city_name");
            area_key = getIntent().getExtras().getString("area_key");
            area_name = getIntent().getExtras().getString("area_name");
            flatno = getIntent().getExtras().getString("flatno");
            apartment = getIntent().getExtras().getString("apartment");
        }

        googleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        isLocationEnabled();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            //checkLocationPermission();
        }

        if (!isLocationEnabled()) {
            AlertDialog.Builder builder = new AlertDialog.Builder(AddressSelectionMapEdit.this);
            builder.setTitle(LanguageConstants.LocationNotEnabled)
                    .setMessage(LanguageConstants.want_to_enable)
                    .setPositiveButton(LanguageConstants.Yes,
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                                    AddressSelectionMapEdit.this.startActivity(intent);

                                }
                            })
                    .setNegativeButton(LanguageConstants.cancel,
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                    Bundle bundle = new Bundle();

                                }
                            });
            AlertDialog alert = builder.create();
            alert.show();
        }
        binding.tvSelectedAddress.setAdapter(new GooglePlacesAutocompleteAdapter(activity.getApplicationContext(), R.layout.autocomplete_list_item));
        binding.tvSelectedAddress.setOnItemClickListener(this);

    }

    public boolean checkLocationPermission() {

        if (ContextCompat.checkSelfPermission(activity,
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(activity,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {
                ActivityCompat.requestPermissions(activity,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);
            } else {
                ActivityCompat.requestPermissions(activity,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);
            }
            return false;
        } else {
            return true;
        }

    }

    private boolean isLocationEnabled() {
        String le = Context.LOCATION_SERVICE;
        locationManager = (LocationManager) AddressSelectionMapEdit.this.getSystemService(le);
        if (!locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
            return false;
        } else {
            return true;
        }
    }

    private void initView() {
        binding.tlbarText.setText(LanguageConstants.UpdateAddress);
        binding.ivRightIcon.setImageResource(R.drawable.ic_tick);
        binding.ivRightIcon.setVisibility(View.VISIBLE);
        binding.tvSelectedAddress.setHint(LanguageConstants.searchAddress);
        binding.ivBackArrow.setOnClickListener(this);
        binding.ivRightIcon.setOnClickListener(this);
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        mMap.getUiSettings().setMyLocationButtonEnabled(true);
        //mMap.getUiSettings().setAllGesturesEnabled(false);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(activity,
                    Manifest.permission.ACCESS_FINE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED) {
                buildGoogleApiClient();
                mMap.setMyLocationEnabled(true);
            }
        } else {
            buildGoogleApiClient();
            mMap.setMyLocationEnabled(true);
        }
        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(new LatLng(latitude, longitude))
                .zoom(17)
                .build();
        googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

        mMap.setOnCameraIdleListener(new GoogleMap.OnCameraIdleListener() {
            @Override
            public void onCameraIdle() {
                //get latlng at the center by calling
                LatLng midLatLng = mMap.getCameraPosition().target;
                Geocoder geocoder;
//                AddressLatLng.lat = midLatLng.latitude;
//                AddressLatLng.lng = midLatLng.longitude;
                List<Address> addressList = null;
                String addressline1 = "";
                geocoder = new Geocoder(AddressSelectionMapEdit.this, Locale.getDefault());
                mCurrentLatitiude = latitude;
                mCurrentLongitude = longitude;
                SessionManager.LocationProperties.getInstance().setLatitude(String.valueOf(mCurrentLatitiude));
                SessionManager.LocationProperties.getInstance().setLongitude(String.valueOf(mCurrentLongitude));

                try {
                    addressList = geocoder.getFromLocation(midLatLng.latitude, midLatLng.longitude, 1);

                    /*content.startRippleAnimation();
                    ivLocation.startAnimation(AnimationUtils.loadAnimation(MapActivity.this, R.anim.wave_scale));*/
                } catch (IOException e) {
                    e.printStackTrace();
                }
                if (addressList != null && addressList.size() > 0) {
                    binding.tvSelectedAddress.setText(addressList.get(0).getAddressLine(0));
                }

            }
        });


    }

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(activity)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        mGoogleApiClient.connect();
    }

    private void positionToAddress(LatLng point) throws IOException {

        Geocoder geocoder;
        List<Address> addresses;
        geocoder = new Geocoder(this, Locale.getDefault());

        addresses = geocoder.getFromLocation(point.latitude, point.longitude, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5

        String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
        String city = addresses.get(0).getLocality();
        String state = addresses.get(0).getAdminArea();
        String country = addresses.get(0).getCountryName();
        String postalCode = addresses.get(0).getPostalCode();
        String knownName = addresses.get(0).getFeatureName();

        String completeAddress = address + " " + city + " " + state + " " + country + " " + postalCode;

        if (completeAddress != null)
            binding.tvSelectedAddress.setText(completeAddress);
    }

    private void getCurrentLocation() {
        new PermissionWrapper.Builder(this)
                .addPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION})
                .addPermissionRationale("Rationale message")
                .addPermissionsGoSettings(true)
                .addRequestPermissionsCallBack(new OnRequestPermissionsCallBack() {
                    @SuppressLint("MissingPermission")
                    @Override
                    public void onGrant() {

                        FusedLocationProviderClient mFusedLocationClient = LocationServices.getFusedLocationProviderClient(AddressSelectionMapEdit.this);

                        mFusedLocationClient.getLastLocation()
                                .addOnSuccessListener(AddressSelectionMapEdit.this, new OnSuccessListener<Location>() {
                                    @Override
                                    public void onSuccess(Location location) {
                                        // Got last known location. In some rare situations, this can be null.
                                        if (location != null) {
                                            mCurrentLongitude = latitude;
                                            mCurrentLatitiude = longitude;
                                            moveMap();
                                        }
                                    }
                                });
                    }

                    @Override
                    public void onDenied(String permission) {
                    }
                }).build().request();

        FusedLocationProviderClient mFusedLocationClient = LocationServices.getFusedLocationProviderClient(AddressSelectionMapEdit.this);

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        mFusedLocationClient.getLastLocation()
                .addOnSuccessListener(AddressSelectionMapEdit.this, new OnSuccessListener<Location>() {
                    @Override
                    public void onSuccess(Location location) {
                        // Got last known location. In some rare situations, this can be null.
                        if (location != null) {
                            mCurrentLongitude = latitude;
                            mCurrentLatitiude = latitude;
                            SessionManager.LocationProperties.getInstance().setLatitude(String.valueOf(mCurrentLatitiude));
                            SessionManager.LocationProperties.getInstance().setLongitude(String.valueOf(mCurrentLongitude));
                            moveMap();
                        }
                    }
                });


    }

    private void moveMap() {

        LatLng latLng = new LatLng(mCurrentLatitiude, mCurrentLongitude);
        /*Marker s = mMap.addMarker(new MarkerOptions()
                .position(latLng)
                .draggable(false)
        );*/

        mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
        mMap.animateCamera(CameraUpdateFactory.zoomTo(17));
        mMap.getUiSettings().setZoomControlsEnabled(false);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        mMap.setMyLocationEnabled(true);

    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        //getCurrentLocation();
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }


    @Override
    protected void onStart() {
        googleApiClient.connect();
        super.onStart();
    }

    @Override
    protected void onStop() {
        googleApiClient.disconnect();
        super.onStop();
    }

    @Override
    public void onClick(View v) {
        if (v == binding.ivBackArrow) {
            finish();
        } else if (v == binding.ivRightIcon) {
            if (!binding.tvSelectedAddress.getText().toString().isEmpty()) {
                Bundle bundle = new Bundle();
                bundle.putString("SELECEDADDRESS", binding.tvSelectedAddress.getText().toString());
                bundle.putString("address_key", mAddressKey);
                bundle.putString("address_type", address_type);
                bundle.putString("address_type_name", address_type_name);
                bundle.putString("streetName", streetName);
                bundle.putString("company", company);
                bundle.putString("landmark", landmark);
                bundle.putString("city_key", city_key);
                bundle.putString("city_name", city_name);
                bundle.putString("area_key", area_key);
                bundle.putString("area_name", area_name);
                bundle.putString("latitude", String.valueOf(latitude));
                bundle.putString("longitude", String.valueOf(longitude));
                bundle.putString("apartment", apartment);
                bundle.putString("flatno", flatno);
                CommonFunctions.getInstance().newIntent(AddressSelectionMapEdit.this, EditAddressActivity.class, bundle, true);
            } else {
                CommonFunctions.getInstance().validationEmptyError(AddressSelectionMapEdit.this, LanguageConstants.searchAddress);
            }


        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        str = (String) parent.getItemAtPosition(position);
        Geocoder coder = new Geocoder(activity);
        try {
            ArrayList<Address> adresses = (ArrayList<Address>) coder.getFromLocationName(str, 10);
            for (Address add : adresses) {
                Double longitude = add.getLongitude();
                Double latitude = add.getLatitude();
                SessionManager.LocationProperties.getInstance().setLatitude(String.valueOf(add.getLatitude()));
                SessionManager.LocationProperties.getInstance().setLongitude(String.valueOf(add.getLongitude()));
                System.out.println(longitude + "  " + latitude);
                CommonFunctions.getInstance().hideSoftKeyboard(activity);
                LatLng mLatLng = new LatLng(latitude, longitude);
                /*mMarker = googleMap.addMarker(new MarkerOptions().position(mLatLng)
                        .draggable(true));
                mMarker.setTag(9999);*/

                CameraPosition cameraPosition = new CameraPosition.Builder()
                        .target(mLatLng)      // Sets the center of the map to location user
                        .zoom(17)                   // Sets the zoom
                        .build();                   // Creates a CameraPosition from the builder
                mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

            }
        } catch (IOException | IllegalArgumentException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        mLastLocation = location;
        if (mCurrLocationMarker != null) {
            mCurrLocationMarker.remove();
        }


        LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
        mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
        mMap.animateCamera(CameraUpdateFactory.zoomTo(17));
        if (mGoogleApiClient != null) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
        }
    }

    private class GooglePlacesAutocompleteAdapter extends ArrayAdapter<String> implements Filterable {

        private ArrayList<String> resultList;

        public GooglePlacesAutocompleteAdapter(Context mContext, int searchplace_list) {
            super(mContext, searchplace_list);
        }

        @Override
        public int getCount() {
            return resultList.size();
        }

        @Override
        public String getItem(int index) {
            return resultList.get(index);
        }

        @Override
        public Filter getFilter() {
            Filter filter = new Filter() {
                @Override
                protected FilterResults performFiltering(CharSequence constraint) {
                    FilterResults filterResults = new FilterResults();
                    if (constraint != null) {
                        // Retrieve the autocomplete results.
                        resultList = autocomplete(constraint.toString());

                        // Assign the data to the FilterResults
                        filterResults.values = resultList;
                        filterResults.count = resultList.size();
                    }
                    return filterResults;
                }

                @Override
                protected void publishResults(CharSequence constraint, FilterResults results) {
                    if (results != null && results.count > 0) {
                        notifyDataSetChanged();
                    } else {
                        notifyDataSetInvalidated();
                    }
                }
            };
            return filter;
        }


        public ArrayList<String> autocomplete(String input) {
            ArrayList<String> resultList = null;


            HttpURLConnection conn = null;
            StringBuilder jsonResults = new StringBuilder();
            try {
                StringBuilder sb = new StringBuilder(PLACES_API_BASE + TYPE_AUTOCOMPLETE + OUT_JSON);
                sb.append("?key=" + getResources().getString(R.string.mapKey));
                sb.append("&components=");
                sb.append("&input=" + URLEncoder.encode(input, "utf8"));


                URL url = new URL(sb.toString());

                System.out.println("URL: " + url);
                conn = (HttpURLConnection) url.openConnection();
                InputStreamReader in = new InputStreamReader(conn.getInputStream());

                // Load the results into a StringBuilder
                int read;
                char[] buff = new char[1024];
                while ((read = in.read(buff)) != -1) {
                    jsonResults.append(buff, 0, read);
                }
            } catch (MalformedURLException e) {
                return resultList;
            } catch (IOException e) {
                return resultList;
            } finally {
                if (conn != null) {
                    conn.disconnect();
                }
            }

            try {

                // Create a JSON object hierarchy from the results
                JSONObject jsonObj = new JSONObject(jsonResults.toString());
                JSONArray predsJsonArray = jsonObj.getJSONArray("predictions");

                // Extract the Place descriptions from the results
                resultList = new ArrayList<String>(predsJsonArray.length());
                for (int i = 0; i < predsJsonArray.length(); i++) {
                    System.out.println(predsJsonArray.getJSONObject(i).getString("description"));
                    //  System.out.println("lat :" + predsJsonArray.getJSONObject(i).getString("lat"));
                    System.out.println("============================================================");
                    // resultList.add(predsJsonArray.getJSONArray(i));
                    resultList.add(predsJsonArray.getJSONObject(i).getString("description"));

                }
            } catch (JSONException e) {

            }


            return resultList;

        }
    }
}
