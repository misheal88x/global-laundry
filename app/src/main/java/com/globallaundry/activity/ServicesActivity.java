package com.globallaundry.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;

import com.globallaundry.R;
import com.globallaundry.adapters.CategoryAdapter;
import com.globallaundry.api.CommonApiCalls;
import com.globallaundry.apimodel.CategoryApiResponse;
import com.globallaundry.databinding.ActivityServicesBinding;
import com.globallaundry.interfaces.CommonCallback;
import com.globallaundry.utils.LanguageConstants;
import com.globallaundry.utils.SessionManager;

import java.util.ArrayList;
import java.util.List;

import hari.bounceview.BounceView;

public class ServicesActivity extends AppCompatActivity implements View.OnClickListener {
    ActivityServicesBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_services);
        if (SessionManager.getInstance().getAppLanguageCode().equals("ar")) {
            getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
            binding.ivBackArrow.setImageResource(R.drawable.ic_right_arrow);
        } else {
            getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
            binding.ivBackArrow.setImageResource(R.drawable.ic_back_arrow);
        }
        initView();
    }

    // ---- Initial View
    private void initView() {
        BounceView.addAnimTo(binding.ivBackArrow);
        binding.tlbarText.setText(LanguageConstants.Services);
        binding.categoryTitle.setText(LanguageConstants.categorytitle);
        binding.ivBackArrow.setOnClickListener(this);
        categoryApi();
    }

    // --- Category APi
    private void categoryApi() {
        CommonApiCalls.getInstance().categoryList(ServicesActivity.this, new CommonCallback.Listener() {
            @Override
            public void onSuccess(Object body) {
                CategoryApiResponse mCategoryApiResponse = (CategoryApiResponse) body;
                // set adapter
                if(mCategoryApiResponse.getData()!=null&&mCategoryApiResponse.getData().size()>0) {
                    LinearLayoutManager layoutManager = new LinearLayoutManager(ServicesActivity.this, RecyclerView.VERTICAL, false);
                    binding.rvCategory.setLayoutManager(layoutManager);
                    CategoryAdapter adapter = new CategoryAdapter(ServicesActivity.this, mCategoryApiResponse.getData());
                    binding.rvCategory.setAdapter(adapter);
                }
            }

            @Override
            public void onFailure(String reason) {

            }
        });
    }

    // ---- On Click
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ivBackArrow:
                finish();
                break;
            default:
                break;
        }

    }
}
