package com.globallaundry.activity;

import android.content.Context;
import android.os.Bundle;
import android.os.Vibrator;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.globallaundry.R;
import com.globallaundry.adapters.PricingAdapter;
import com.globallaundry.api.CommonApiCalls;
import com.globallaundry.apimodel.ClothTypeListViewApiResponse;
import com.globallaundry.app_model.SizeModel;
import com.globallaundry.databinding.ActivityPricingBinding;
import com.globallaundry.dbmodel.RealmLibrary;
import com.globallaundry.events.CartRefresh;
import com.globallaundry.interfaces.CommonCallback;
import com.globallaundry.utils.CommonFunctions;
import com.globallaundry.utils.LanguageConstants;
import com.globallaundry.utils.SessionManager;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

import hari.bounceview.BounceView;

public class PricingActivity extends AppCompatActivity implements View.OnClickListener {
    ActivityPricingBinding binding;
    private Integer isExpress;

    List<ClothTypeListViewApiResponse.Size> selectedSize = new ArrayList<>();
    private ClothTypeListViewApiResponse.Data responses;
    public static Vibrator vibrator;

    @Override

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_pricing);
        if (SessionManager.getInstance().getAppLanguageCode().equals("ar")) {
            getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
            binding.ivBackArrow.setImageResource(R.drawable.ic_right_arrow);
        } else {
            getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
            binding.ivBackArrow.setImageResource(R.drawable.ic_back_arrow);
        }
        initView();

        vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
    }

    private void initView() {
        BounceView.addAnimTo(binding.ivBackArrow);
        BounceView.addAnimTo(binding.rlyoutAddCart);
        binding.tlbarText.setText(LanguageConstants.Pricing);
        binding.ivBackArrow.setOnClickListener(this);
        binding.tvAddcart.setText(LanguageConstants.addtoCart);
        binding.rlyoutAddCart.setOnClickListener(this);
        binding.checkbox.setText(LanguageConstants.expressdelivery);
        pricingApi();
    }

    private void pricingApi() {

        CommonApiCalls.getInstance().clothTypeThreeList(PricingActivity.this, new CommonCallback.Listener() {
            @Override
            public void onSuccess(Object body) {
                responses = ((ClothTypeListViewApiResponse) body).getData();

                Glide.with(PricingActivity.this).load(responses.getCloth_type_image()).into(binding.image);
                binding.txtName.setText(responses.getCloth_type_name());
                binding.txtExpressdeliveryFee.setText(LanguageConstants.expressdelivery + " " + "(" + CommonFunctions.getInstance().roundOffDecimalValue(Double.parseDouble(responses.getExpress_cost()), true) + ")");

                // set adapter
                LinearLayoutManager layoutManager = new LinearLayoutManager(PricingActivity.this, RecyclerView.VERTICAL, false);
                binding.rvCategory.setLayoutManager(layoutManager);
                PricingAdapter adapter = new PricingAdapter(PricingActivity.this, responses.getSize(), selectedSize);
                binding.rvCategory.setAdapter(adapter);
            }

            @Override
            public void onFailure(String reason) {

            }
        });


    }

    @Override
    public void onClick(View v) {
        if (v == binding.ivBackArrow) {
            finish();
        } else if (v == binding.rlyoutAddCart) {
            if (selectedSize.size() > 0) {
                if (binding.checkbox.isChecked()) {
                    isExpress = 1;
                } else {
                    isExpress = 0;
                }

                List<SizeModel> _sizeModels = new ArrayList<>();

                for (int count = 0; count < selectedSize.size(); count++) {
                    ClothTypeListViewApiResponse.Size selected_size = selectedSize.get(count);
                    SizeModel sizeModel = new SizeModel();
                    sizeModel.setPrice(Double.parseDouble(selected_size.getPrice()));
                    sizeModel.setSize_key(selected_size.getCloth_size_type_key());
                    sizeModel.setSize_name(selected_size.getSize_type_name());
                    sizeModel.setSize_quantity(selected_size.getQty());
                    _sizeModels.add(sizeModel);
                }

                RealmLibrary.getInstance().insertItem(PricingActivity.this,
                        responses.getCloth_type_key(),
                        responses.getCloth_type_name(),
                        responses.getCloth_type_image(), responses.getSize_type(),
                        0.0, SessionManager.getInstance().getCategoryKey(), "1", _sizeModels, 0, "tet",
                        "1",
                        responses.getExpress_cost(), isExpress, 0);
                System.out.println(RealmLibrary.getInstance().getCartList().size());
                EventBus.getDefault().post(new CartRefresh());
                CommonFunctions.getInstance().successResponseToast(PricingActivity.this, LanguageConstants.CartaddedSuccess);
                vibrator.vibrate(200);
                finish();

            } else {
                CommonFunctions.getInstance().validationEmptyError(PricingActivity.this, LanguageConstants.enterSize);
            }
        }
    }
}

