package com.globallaundry.activity;

import android.app.Activity;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.globallaundry.R;


public class HelpBrowser extends Activity {


    WebView wvHelp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_help_browser);

        wvHelp = (WebView) findViewById(R.id.wvHelp);
        wvHelp.getSettings().setJavaScriptEnabled(true);
        wvHelp.setWebViewClient(new MyWebViewClient());


        if (getIntent().getExtras() != null && getIntent().getExtras().getString("url") != null) {
            wvHelp.loadUrl(getIntent().getExtras().getString("url"));
        }
       /* if (getIntent().getExtras() != null && getIntent().getExtras().getString("pagetitle") != null) {
            mToolHeading.setText(getIntent().getExtras().getString("pagetitle"));
        }*/


       /* String mLanguageDirection = SessionManager.getInstance().getLTRandRTRid(HelpBrowser.this);


        if (mLanguageDirection.equals("rtl")) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                HelpBrowser.this.getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
                // Constants.allTextfilesArabic(getActivity());
                //mToolHeading.setGravity(Gravity.RIGHT);

                // Constants.SetStringArabic();


                mToolbar.setNavigationIcon(R.drawable.right_back);


            }
        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                HelpBrowser.this.getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
                // Constants.allTextfilesEnglish(getActivity());
                //mToolHeading.setGravity(Gravity.LEFT);

                // Constants.SetStringEnglish();
                mToolbar.setNavigationIcon(R.drawable.ic_back);

            }
        }*/

    }


    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    private class MyWebViewClient extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
        }

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
        }
    }
}
