package com.globallaundry.apimodel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class OrderListApiResponse {

    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("data")
    @Expose
    private List<Datum> data = null;
    @SerializedName("time")
    @Expose
    private Integer time;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("system")
    @Expose
    private System system;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }

    public Integer getTime() {
        return time;
    }

    public void setTime(Integer time) {
        this.time = time;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public System getSystem() {
        return system;
    }

    public void setSystem(System system) {
        this.system = system;
    }


    public class System {

        @SerializedName("nc")
        @Expose
        private Integer nc;

        public Integer getNc() {
            return nc;
        }

        public void setNc(Integer nc) {
            this.nc = nc;
        }

    }

    public class Datum {

        @SerializedName("order_key")
        @Expose
        private String orderKey;
        @SerializedName("order_number")
        @Expose
        private String orderNumber;
        @SerializedName("order_datetime")
        @Expose
        private String orderDatetime;
        @SerializedName("category_id")
        @Expose
        private String categoryId;
        @SerializedName("category_name")
        @Expose
        private String categoryName;
        @SerializedName("order_status")
        @Expose
        private Integer orderStatus;
        @SerializedName("order_status_label")
        @Expose
        private String orderStatusLabel;
        @SerializedName("pickup_datetime")
        @Expose
        private String pickupDatetime;
        @SerializedName("delivery_datetime")
        @Expose
        private String deliveryDatetime;
        @SerializedName("status_color_code")
        @Expose
        private String statusColorCode;
        @SerializedName("order_total")
        @Expose
        private String orderTotal;
        @SerializedName("is_change_address")
        @Expose
        private String is_change_address;


        public String getOrderKey() {
            return orderKey;
        }

        public void setOrderKey(String orderKey) {
            this.orderKey = orderKey;
        }

        public String getOrderNumber() {
            return orderNumber;
        }

        public void setOrderNumber(String orderNumber) {
            this.orderNumber = orderNumber;
        }

        public String getOrderDatetime() {
            return orderDatetime;
        }

        public void setOrderDatetime(String orderDatetime) {
            this.orderDatetime = orderDatetime;
        }

        public String getCategoryId() {
            return categoryId;
        }

        public void setCategoryId(String categoryId) {
            this.categoryId = categoryId;
        }

        public String getCategoryName() {
            return categoryName;
        }

        public void setCategoryName(String categoryName) {
            this.categoryName = categoryName;
        }

        public Integer getOrderStatus() {
            return orderStatus;
        }

        public void setOrderStatus(Integer orderStatus) {
            this.orderStatus = orderStatus;
        }

        public String getOrderStatusLabel() {
            return orderStatusLabel;
        }

        public void setOrderStatusLabel(String orderStatusLabel) {
            this.orderStatusLabel = orderStatusLabel;
        }

        public String getPickupDatetime() {
            return pickupDatetime;
        }

        public void setPickupDatetime(String pickupDatetime) {
            this.pickupDatetime = pickupDatetime;
        }

        public String getDeliveryDatetime() {
            return deliveryDatetime;
        }

        public void setDeliveryDatetime(String deliveryDatetime) {
            this.deliveryDatetime = deliveryDatetime;
        }

        public String getStatusColorCode() {
            return statusColorCode;
        }

        public void setStatusColorCode(String statusColorCode) {
            this.statusColorCode = statusColorCode;
        }

        public String getOrderTotal() {
            return orderTotal;
        }

        public void setOrderTotal(String orderTotal) {
            this.orderTotal = orderTotal;
        }

        public String getIs_change_address() {
            return is_change_address;
        }

        public void setIs_change_address(String is_change_address) {
            this.is_change_address = is_change_address;
        }
    }

}