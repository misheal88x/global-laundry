package com.globallaundry.apimodel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class CalculateApiResponse {

    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("data")
    @Expose
    private Data data;
    @SerializedName("time")
    @Expose
    private Integer time;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("system")
    @Expose
    private System system;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public Integer getTime() {
        return time;
    }

    public void setTime(Integer time) {
        this.time = time;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public System getSystem() {
        return system;
    }

    public void setSystem(System system) {
        this.system = system;
    }
    public class Data {

        @SerializedName("payment_details")
        @Expose
        private List<Payment_detail> payment_details = new ArrayList<Payment_detail>();
        @SerializedName("data")
        @Expose
        private Data_ data;

        public List<Payment_detail> getPayment_details() {
            return payment_details;
        }

        public void setPayment_details(List<Payment_detail> payment_details) {
            this.payment_details = payment_details;
        }

        public Data_ getData() {
            return data;
        }

        public void setData(Data_ data) {
            this.data = data;
        }

    }
    public class Data_ {

        @SerializedName("items")
        @Expose
        private List<Item> items = new ArrayList<Item>();
        @SerializedName("order_message")
        @Expose
        private String order_message;
        @SerializedName("payment_type")
        @Expose
        private Integer payment_type;
        @SerializedName("pickup_date")
        @Expose
        private String pickup_date;
        @SerializedName("pickup_time")
        @Expose
        private String pickup_time;
        @SerializedName("pickup_user_address_key")
        @Expose
        private String pickup_user_address_key;

        public List<Item> getItems() {
            return items;
        }

        public void setItems(List<Item> items) {
            this.items = items;
        }

        public String getOrder_message() {
            return order_message;
        }

        public void setOrder_message(String order_message) {
            this.order_message = order_message;
        }

        public Integer getPayment_type() {
            return payment_type;
        }

        public void setPayment_type(Integer payment_type) {
            this.payment_type = payment_type;
        }

        public String getPickup_date() {
            return pickup_date;
        }

        public void setPickup_date(String pickup_date) {
            this.pickup_date = pickup_date;
        }

        public String getPickup_time() {
            return pickup_time;
        }

        public void setPickup_time(String pickup_time) {
            this.pickup_time = pickup_time;
        }

        public String getPickup_user_address_key() {
            return pickup_user_address_key;
        }

        public void setPickup_user_address_key(String pickup_user_address_key) {
            this.pickup_user_address_key = pickup_user_address_key;
        }

    }
    public class Item {

        @SerializedName("additional_quantity")
        @Expose
        private Integer additional_quantity;
        @SerializedName("cloth_type_key")
        @Expose
        private String cloth_type_key;
        @SerializedName("is_express")
        @Expose
        private Integer is_express;
        @SerializedName("quantity")
        @Expose
        private String quantity;
        @SerializedName("size")
        @Expose
        private List<Size> size = new ArrayList<Size>();

        public Integer getAdditional_quantity() {
            return additional_quantity;
        }

        public void setAdditional_quantity(Integer additional_quantity) {
            this.additional_quantity = additional_quantity;
        }

        public String getCloth_type_key() {
            return cloth_type_key;
        }

        public void setCloth_type_key(String cloth_type_key) {
            this.cloth_type_key = cloth_type_key;
        }

        public Integer getIs_express() {
            return is_express;
        }

        public void setIs_express(Integer is_express) {
            this.is_express = is_express;
        }

        public String getQuantity() {
            return quantity;
        }

        public void setQuantity(String quantity) {
            this.quantity = quantity;
        }

        public List<Size> getSize() {
            return size;
        }

        public void setSize(List<Size> size) {
            this.size = size;
        }

    }
    public class Payment_detail {

        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("value")
        @Expose
        private String value;
        @SerializedName("usd_value")
        @Expose
        private String usd_value;
        @SerializedName("bold")
        @Expose
        private Integer bold;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }

        public String getUsd_value() {
            return usd_value;
        }

        public void setUsd_value(String usd_value) {
            this.usd_value = usd_value;
        }

        public Integer getBold() {
            return bold;
        }

        public void setBold(Integer bold) {
            this.bold = bold;
        }

    }
    public class Size {

        @SerializedName("size_key")
        @Expose
        private String size_key;
        @SerializedName("size_quantity")
        @Expose
        private Integer size_quantity;

        public String getSize_key() {
            return size_key;
        }

        public void setSize_key(String size_key) {
            this.size_key = size_key;
        }

        public Integer getSize_quantity() {
            return size_quantity;
        }

        public void setSize_quantity(Integer size_quantity) {
            this.size_quantity = size_quantity;
        }

    }
    public class System {

        @SerializedName("user_status")
        @Expose
        private Integer user_status;

        public Integer getUser_status() {
            return user_status;
        }

        public void setUser_status(Integer user_status) {
            this.user_status = user_status;
        }

    }

}