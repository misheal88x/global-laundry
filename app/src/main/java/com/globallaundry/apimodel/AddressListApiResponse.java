package com.globallaundry.apimodel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class AddressListApiResponse {

    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("data")
    @Expose
    private List<Datum> data = null;
    @SerializedName("time")
    @Expose
    private Integer time;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("system")
    @Expose
    private System system;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }

    public Integer getTime() {
        return time;
    }

    public void setTime(Integer time) {
        this.time = time;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public System getSystem() {
        return system;
    }

    public void setSystem(System system) {
        this.system = system;
    }

    public class Datum {

        @SerializedName("user_address_key")
        @Expose
        private String userAddressKey;
        @SerializedName("address_type_key")
        @Expose
        private String address_type_key;
        @SerializedName("address_type_id")
        @Expose
        private Integer addressTypeId;
        @SerializedName("address_type_name")
        @Expose
        private String address_type_name;
        @SerializedName("city_id")
        @Expose
        private Integer cityId;
        @SerializedName("city_name")
        @Expose
        private String city_name;
        @SerializedName("city_key")
        @Expose
        private String city_key;
        @SerializedName("area_key")
        @Expose
        private String area_key;
        @SerializedName("area_id")
        @Expose
        private Integer areaId;
        @SerializedName("area_name")
        @Expose
        private String area_name;
        @SerializedName("latitude")
        @Expose
        private String latitude;
        @SerializedName("longitude")
        @Expose
        private String longitude;
        @SerializedName("flat_no")
        @Expose
        private String flatNo;
        @SerializedName("company")
        @Expose
        private String company;
        @SerializedName("apartment")
        @Expose
        private String apartment;
        @SerializedName("landmark")
        @Expose
        private String landmark;
        @SerializedName("street_name")
        @Expose
        private String streetName;
        @SerializedName("is_default")
        @Expose
        private String is_default;

        private Boolean isSelected = false;

        public String getIs_default() {
            return is_default;
        }

        public void setIs_default(String is_default) {
            this.is_default = is_default;
        }

        public String getUserAddressKey() {
            return userAddressKey;
        }

        public void setUserAddressKey(String userAddressKey) {
            this.userAddressKey = userAddressKey;
        }

        public Integer getAddressTypeId() {
            return addressTypeId;
        }

        public void setAddressTypeId(Integer addressTypeId) {
            this.addressTypeId = addressTypeId;
        }

        public Integer getCityId() {
            return cityId;
        }

        public void setCityId(Integer cityId) {
            this.cityId = cityId;
        }

        public Integer getAreaId() {
            return areaId;
        }

        public void setAreaId(Integer areaId) {
            this.areaId = areaId;
        }

        public String getLatitude() {
            return latitude;
        }

        public void setLatitude(String latitude) {
            this.latitude = latitude;
        }

        public String getLongitude() {
            return longitude;
        }

        public void setLongitude(String longitude) {
            this.longitude = longitude;
        }

        public String getFlatNo() {
            return flatNo;
        }

        public void setFlatNo(String flatNo) {
            this.flatNo = flatNo;
        }

        public String getCompany() {
            return company;
        }

        public String getAddress_type_name() {
            return address_type_name;
        }

        public void setAddress_type_name(String address_type_name) {
            this.address_type_name = address_type_name;
        }

        public String getCity_name() {
            return city_name;
        }

        public void setCity_name(String city_name) {
            this.city_name = city_name;
        }

        public String getArea_name() {
            return area_name;
        }

        public void setArea_name(String area_name) {
            this.area_name = area_name;
        }

        public String getApartment() {
            return apartment;
        }

        public void setApartment(String apartment) {
            this.apartment = apartment;
        }

        public void setCompany(String company) {
            this.company = company;
        }

        public String getLandmark() {
            return landmark;
        }

        public void setLandmark(String landmark) {
            this.landmark = landmark;
        }

        public String getStreetName() {
            return streetName;
        }

        public void setStreetName(String streetName) {
            this.streetName = streetName;
        }

        public Boolean getSelected() {
            return isSelected;
        }

        public void setSelected(Boolean selected) {
            isSelected = selected;
        }

        public String getAddress_type_key() {
            return address_type_key;
        }

        public void setAddress_type_key(String address_type_key) {
            this.address_type_key = address_type_key;
        }

        public String getCity_key() {
            return city_key;
        }

        public void setCity_key(String city_key) {
            this.city_key = city_key;
        }

        public String getArea_key() {
            return area_key;
        }

        public void setArea_key(String area_key) {
            this.area_key = area_key;
        }
    }

    public class System {

        @SerializedName("nc")
        @Expose
        private Integer nc;

        public Integer getNc() {
            return nc;
        }

        public void setNc(Integer nc) {
            this.nc = nc;
        }

    }

}