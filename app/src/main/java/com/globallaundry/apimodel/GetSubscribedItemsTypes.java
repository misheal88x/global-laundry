package com.globallaundry.apimodel;

import androidx.annotation.NonNull;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class GetSubscribedItemsTypes {
    @SerializedName("status")
    private Integer status;

    @SerializedName("message")
    private String message;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @SerializedName("data")
    private List<Item> data;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public List<Item> getData() {
        return data;
    }

    public void setData(List<Item> data) {
        this.data = data;
    }

    public class Item {
        @SerializedName("cloth_type_key")
        private String clothTypeKey;

        @SerializedName("count_in_packages")
        private Integer countInPackages;

        public String getClothTypeKey() {
            return clothTypeKey;
        }

        public void setClothTypeKey(String clothTypeKey) {
            this.clothTypeKey = clothTypeKey;
        }

        public Integer getCountInPackages() {
            return countInPackages;
        }

        public void setCountInPackages(Integer countInPackages) {
            this.countInPackages = countInPackages;
        }


        @NonNull
        @Override
        public String toString() {
            return clothTypeKey + "\t" + countInPackages + "\n";
        }
    }
}
