package com.globallaundry.apimodel;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class GetMySubscribedPackagesApiResponse {
    @SerializedName("status")
    private Integer status;
    @SerializedName("message")
    private String message;

    public List<PackageData > getData() {
        return data;
    }

    public void setData(List<PackageData > data) {
        this.data = data;
    }

    @SerializedName("data")
    private List<PackageData> data;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }


}
