package com.globallaundry.apimodel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class ClothTypeApiResponse {

    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("data")
    @Expose
    private List<Datum> data = new ArrayList<Datum>();
    @SerializedName("time")
    @Expose
    private Integer time;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("system")
    @Expose
    private System system;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }

    public Integer getTime() {
        return time;
    }

    public void setTime(Integer time) {
        this.time = time;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public System getSystem() {
        return system;
    }

    public void setSystem(System system) {
        this.system = system;
    }
    public class Datum {

        @SerializedName("cloth_type_key")
        @Expose
        private String cloth_type_key;
        @SerializedName("cloth_type_name")
        @Expose
        private String cloth_type_name;
        @SerializedName("cloth_type_image")
        @Expose
        private String cloth_type_image;
        @SerializedName("size_type")
        @Expose
        private Integer size_type;
        @SerializedName("price")
        @Expose
        private String price;
        @SerializedName("express_cost")
        @Expose
        private String express_cost;

        public String getCloth_type_key() {
            return cloth_type_key;
        }

        public void setCloth_type_key(String cloth_type_key) {
            this.cloth_type_key = cloth_type_key;
        }

        public String getCloth_type_name() {
            return cloth_type_name;
        }

        public void setCloth_type_name(String cloth_type_name) {
            this.cloth_type_name = cloth_type_name;
        }

        public String getCloth_type_image() {
            return cloth_type_image;
        }

        public void setCloth_type_image(String cloth_type_image) {
            this.cloth_type_image = cloth_type_image;
        }

        public Integer getSize_type() {
            return size_type;
        }

        public void setSize_type(Integer size_type) {
            this.size_type = size_type;
        }

        public String getPrice() {
            return price;
        }

        public void setPrice(String  price) {
            this.price = price;
        }

        public String getExpress_cost() {
            return express_cost;
        }

        public void setExpress_cost(String express_cost) {
            this.express_cost = express_cost;
        }

    }
    public class System {

        @SerializedName("user_status")
        @Expose
        private Integer user_status;

        public Integer getUser_status() {
            return user_status;
        }

        public void setUser_status(Integer user_status) {
            this.user_status = user_status;
        }

    }
}