package com.globallaundry.apimodel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class OrderDetailsApiResponse {

@SerializedName("status")
@Expose
private Integer status;
@SerializedName("data")
@Expose
private Data data;
@SerializedName("time")
@Expose
private Integer time;
@SerializedName("message")
@Expose
private String message;
@SerializedName("system")
@Expose
private System system;

public Integer getStatus() {
return status;
}

public void setStatus(Integer status) {
this.status = status;
}

public Data getData() {
return data;
}

public void setData(Data data) {
this.data = data;
}

public Integer getTime() {
return time;
}

public void setTime(Integer time) {
this.time = time;
}

public String getMessage() {
return message;
}

public void setMessage(String message) {
this.message = message;
}

public System getSystem() {
return system;
}

public void setSystem(System system) {
this.system = system;
}


    public class Payment_detail {

        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("value")
        @Expose
        private String value;
        @SerializedName("bold")
        @Expose
        private Integer bold;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }

        public Integer getBold() {
            return bold;
        }

        public void setBold(Integer bold) {
            this.bold = bold;
        }

    }

    public class Pickup_address {

        @SerializedName("user_address_key")
        @Expose
        private String user_address_key;
        @SerializedName("address_type_id")
        @Expose
        private Integer address_type_id;
        @SerializedName("address_type_name")
        @Expose
        private String address_type_name;
        @SerializedName("city_id")
        @Expose
        private Integer city_id;
        @SerializedName("city_name")
        @Expose
        private String city_name;
        @SerializedName("area_id")
        @Expose
        private Integer area_id;
        @SerializedName("area_name")
        @Expose
        private String area_name;
        @SerializedName("latitude")
        @Expose
        private String latitude;
        @SerializedName("longitude")
        @Expose
        private String longitude;
        @SerializedName("flat_no")
        @Expose
        private String flat_no;
        @SerializedName("company")
        @Expose
        private String company;
        @SerializedName("landmark")
        @Expose
        private String landmark;
        @SerializedName("street_name")
        @Expose
        private String street_name;
        @SerializedName("apartment")
        @Expose
        private String apartment;

        public String getUser_address_key() {
            return user_address_key;
        }

        public void setUser_address_key(String user_address_key) {
            this.user_address_key = user_address_key;
        }

        public Integer getAddress_type_id() {
            return address_type_id;
        }

        public void setAddress_type_id(Integer address_type_id) {
            this.address_type_id = address_type_id;
        }

        public String getAddress_type_name() {
            return address_type_name;
        }

        public void setAddress_type_name(String address_type_name) {
            this.address_type_name = address_type_name;
        }

        public Integer getCity_id() {
            return city_id;
        }

        public void setCity_id(Integer city_id) {
            this.city_id = city_id;
        }

        public String getCity_name() {
            return city_name;
        }

        public void setCity_name(String city_name) {
            this.city_name = city_name;
        }

        public Integer getArea_id() {
            return area_id;
        }

        public void setArea_id(Integer area_id) {
            this.area_id = area_id;
        }

        public String getArea_name() {
            return area_name;
        }

        public void setArea_name(String area_name) {
            this.area_name = area_name;
        }

        public String getLatitude() {
            return latitude;
        }

        public void setLatitude(String latitude) {
            this.latitude = latitude;
        }

        public String getLongitude() {
            return longitude;
        }

        public void setLongitude(String longitude) {
            this.longitude = longitude;
        }

        public String getFlat_no() {
            return flat_no;
        }

        public void setFlat_no(String flat_no) {
            this.flat_no = flat_no;
        }

        public String getCompany() {
            return company;
        }

        public void setCompany(String company) {
            this.company = company;
        }

        public String getLandmark() {
            return landmark;
        }

        public void setLandmark(String landmark) {
            this.landmark = landmark;
        }

        public String getStreet_name() {
            return street_name;
        }

        public void setStreet_name(String street_name) {
            this.street_name = street_name;
        }

        public String getApartment() {
            return apartment;
        }

        public void setApartment(String apartment) {
            this.apartment = apartment;
        }

    }

    public class Pickup_datetime {

        @SerializedName("date")
        @Expose
        private String date;
        @SerializedName("time")
        @Expose
        private String time;

        public String getDate() {
            return date;
        }

        public void setDate(String date) {
            this.date = date;
        }

        public String getTime() {
            return time;
        }

        public void setTime(String time) {
            this.time = time;
        }

    }

    public class System {

        @SerializedName("nc")
        @Expose
        private Integer nc;

        public Integer getNc() {
            return nc;
        }

        public void setNc(Integer nc) {
            this.nc = nc;
        }

    }


    public class Coth_type {

        @SerializedName("size")
        @Expose
        private String size;
        @SerializedName("item_quantity")
        @Expose
        private String item_quantity;
        @SerializedName("item_total_price")
        @Expose
        private String item_total_price;
        @SerializedName("cloth_type_name")
        @Expose
        private String cloth_type_name;
        @SerializedName("cloth_type_image")
        @Expose
        private String cloth_type_image;

        public String getSize() {
            return size;
        }

        public void setSize(String size) {
            this.size = size;
        }

        public String getItem_quantity() {
            return item_quantity;
        }

        public void setItem_quantity(String item_quantity) {
            this.item_quantity = item_quantity;
        }

        public String getItem_total_price() {
            return item_total_price;
        }

        public void setItem_total_price(String item_total_price) {
            this.item_total_price = item_total_price;
        }

        public String getCloth_type_name() {
            return cloth_type_name;
        }

        public void setCloth_type_name(String cloth_type_name) {
            this.cloth_type_name = cloth_type_name;
        }

        public String getCloth_type_image() {
            return cloth_type_image;
        }

        public void setCloth_type_image(String cloth_type_image) {
            this.cloth_type_image = cloth_type_image;
        }

    }

    public class Data {

        @SerializedName("customer_name")
        @Expose
        private String customer_name;
        @SerializedName("order_number")
        @Expose
        private String order_number;
        @SerializedName("pickup_address")
        @Expose
        private Pickup_address pickup_address;
        @SerializedName("delivery_address")
        @Expose
        private Delivery_address delivery_address;
        @SerializedName("pickup_datetime")
        @Expose
        private Pickup_datetime pickup_datetime;
        @SerializedName("delivery_datetime")
        @Expose
        private Delivery_datetime delivery_datetime;
        @SerializedName("order_status")
        @Expose
        private Integer order_status;
        @SerializedName("is_rated")
        @Expose
        private Integer is_rated;
        @SerializedName("order_status_label")
        @Expose
        private String order_status_label;
        @SerializedName("status_color_code")
        @Expose
        private String status_color_code;
        @SerializedName("payment_status")
        @Expose
        private Integer payment_status;
        @SerializedName("payment_status_label")
        @Expose
        private String payment_status_label;
        @SerializedName("order_total")
        @Expose
        private String order_total;
        @SerializedName("items")
        @Expose
        private List<Item> items = null;
        @SerializedName("item_total_price")
        @Expose
        private String item_total_price;
        @SerializedName("payment_details")
        @Expose
        private List<Payment_detail> payment_details = null;
        @SerializedName("driver_comments")
        @Expose
        private List<Driver_comment> driver_comments = null;

        public Integer getIs_rated() {
            return is_rated;
        }

        public void setIs_rated(Integer is_rated) {
            this.is_rated = is_rated;
        }

        public List<Driver_comment> getDriver_comments() {
            return driver_comments;
        }

        public void setDriver_comments(List<Driver_comment> driver_comments) {
            this.driver_comments = driver_comments;
        }

        public String getCustomer_name() {
            return customer_name;
        }

        public void setCustomer_name(String customer_name) {
            this.customer_name = customer_name;
        }

        public String getOrder_number() {
            return order_number;
        }

        public void setOrder_number(String order_number) {
            this.order_number = order_number;
        }

        public Pickup_address getPickup_address() {
            return pickup_address;
        }

        public void setPickup_address(Pickup_address pickup_address) {
            this.pickup_address = pickup_address;
        }

        public Delivery_address getDelivery_address() {
            return delivery_address;
        }

        public void setDelivery_address(Delivery_address delivery_address) {
            this.delivery_address = delivery_address;
        }

        public Pickup_datetime getPickup_datetime() {
            return pickup_datetime;
        }

        public void setPickup_datetime(Pickup_datetime pickup_datetime) {
            this.pickup_datetime = pickup_datetime;
        }

        public Delivery_datetime getDelivery_datetime() {
            return delivery_datetime;
        }

        public void setDelivery_datetime(Delivery_datetime delivery_datetime) {
            this.delivery_datetime = delivery_datetime;
        }

        public Integer getOrder_status() {
            return order_status;
        }

        public void setOrder_status(Integer order_status) {
            this.order_status = order_status;
        }

        public String getOrder_status_label() {
            return order_status_label;
        }

        public void setOrder_status_label(String order_status_label) {
            this.order_status_label = order_status_label;
        }

        public String getStatus_color_code() {
            return status_color_code;
        }

        public void setStatus_color_code(String status_color_code) {
            this.status_color_code = status_color_code;
        }

        public Integer getPayment_status() {
            return payment_status;
        }

        public void setPayment_status(Integer payment_status) {
            this.payment_status = payment_status;
        }

        public String getPayment_status_label() {
            return payment_status_label;
        }

        public void setPayment_status_label(String payment_status_label) {
            this.payment_status_label = payment_status_label;
        }

        public String getOrder_total() {
            return order_total;
        }

        public void setOrder_total(String order_total) {
            this.order_total = order_total;
        }

        public List<Item> getItems() {
            return items;
        }

        public void setItems(List<Item> items) {
            this.items = items;
        }

        public String getItem_total_price() {
            return item_total_price;
        }

        public void setItem_total_price(String item_total_price) {
            this.item_total_price = item_total_price;
        }

        public List<Payment_detail> getPayment_details() {
            return payment_details;
        }

        public void setPayment_details(List<Payment_detail> payment_details) {
            this.payment_details = payment_details;
        }

    }

    public class Delivery_address {

        @SerializedName("user_address_key")
        @Expose
        private String user_address_key;
        @SerializedName("address_type_id")
        @Expose
        private Integer address_type_id;
        @SerializedName("address_type_name")
        @Expose
        private String address_type_name;
        @SerializedName("city_id")
        @Expose
        private Integer city_id;
        @SerializedName("city_name")
        @Expose
        private String city_name;
        @SerializedName("area_id")
        @Expose
        private Integer area_id;
        @SerializedName("area_name")
        @Expose
        private String area_name;
        @SerializedName("latitude")
        @Expose
        private String latitude;
        @SerializedName("longitude")
        @Expose
        private String longitude;
        @SerializedName("flat_no")
        @Expose
        private String flat_no;
        @SerializedName("company")
        @Expose
        private String company;
        @SerializedName("landmark")
        @Expose
        private String landmark;
        @SerializedName("street_name")
        @Expose
        private String street_name;
        @SerializedName("apartment")
        @Expose
        private String apartment;

        public String getUser_address_key() {
            return user_address_key;
        }

        public void setUser_address_key(String user_address_key) {
            this.user_address_key = user_address_key;
        }

        public Integer getAddress_type_id() {
            return address_type_id;
        }

        public void setAddress_type_id(Integer address_type_id) {
            this.address_type_id = address_type_id;
        }

        public String getAddress_type_name() {
            return address_type_name;
        }

        public void setAddress_type_name(String address_type_name) {
            this.address_type_name = address_type_name;
        }

        public Integer getCity_id() {
            return city_id;
        }

        public void setCity_id(Integer city_id) {
            this.city_id = city_id;
        }

        public String getCity_name() {
            return city_name;
        }

        public void setCity_name(String city_name) {
            this.city_name = city_name;
        }

        public Integer getArea_id() {
            return area_id;
        }

        public void setArea_id(Integer area_id) {
            this.area_id = area_id;
        }

        public String getArea_name() {
            return area_name;
        }

        public void setArea_name(String area_name) {
            this.area_name = area_name;
        }

        public String getLatitude() {
            return latitude;
        }

        public void setLatitude(String latitude) {
            this.latitude = latitude;
        }

        public String getLongitude() {
            return longitude;
        }

        public void setLongitude(String longitude) {
            this.longitude = longitude;
        }

        public String getFlat_no() {
            return flat_no;
        }

        public void setFlat_no(String flat_no) {
            this.flat_no = flat_no;
        }

        public String getCompany() {
            return company;
        }

        public void setCompany(String company) {
            this.company = company;
        }

        public String getLandmark() {
            return landmark;
        }

        public void setLandmark(String landmark) {
            this.landmark = landmark;
        }

        public String getStreet_name() {
            return street_name;
        }

        public void setStreet_name(String street_name) {
            this.street_name = street_name;
        }

        public String getApartment() {
            return apartment;
        }

        public void setApartment(String apartment) {
            this.apartment = apartment;
        }

    }

    public class Delivery_datetime {

        @SerializedName("date")
        @Expose
        private String date;
        @SerializedName("time")
        @Expose
        private String time;

        public String getDate() {
            return date;
        }

        public void setDate(String date) {
            this.date = date;
        }

        public String getTime() {
            return time;
        }

        public void setTime(String time) {
            this.time = time;
        }

    }

    public class Item {

        @SerializedName("category_id")
        @Expose
        private Integer category_id;
        @SerializedName("category_name")
        @Expose
        private String category_name;
        @SerializedName("category_image")
        @Expose
        private String category_image;
        @SerializedName("coth_types")
        @Expose
        private List<Coth_type> coth_types = null;

        public Integer getCategory_id() {
            return category_id;
        }

        public void setCategory_id(Integer category_id) {
            this.category_id = category_id;
        }

        public String getCategory_name() {
            return category_name;
        }

        public void setCategory_name(String category_name) {
            this.category_name = category_name;
        }

        public String getCategory_image() {
            return category_image;
        }

        public void setCategory_image(String category_image) {
            this.category_image = category_image;
        }

        public List<Coth_type> getCoth_types() {
            return coth_types;
        }

        public void setCoth_types(List<Coth_type> coth_types) {
            this.coth_types = coth_types;
        }

    }

    public class Driver_comment {

        @SerializedName("task_type")
        @Expose
        private String task_type;
        @SerializedName("comment")
        @Expose
        private String comment;

        public String getTask_type() {
            return task_type;
        }

        public void setTask_type(String task_type) {
            this.task_type = task_type;
        }

        public String getComment() {
            return comment;
        }

        public void setComment(String comment) {
            this.comment = comment;
        }

    }

}