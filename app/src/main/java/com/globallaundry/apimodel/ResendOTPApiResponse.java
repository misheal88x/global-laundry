package com.globallaundry.apimodel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ResendOTPApiResponse {

    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("data")
    @Expose
    private Data data;
    @SerializedName("time")
    @Expose
    private Integer time;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("system")
    @Expose
    private System system;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public Integer getTime() {
        return time;
    }

    public void setTime(Integer time) {
        this.time = time;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public System getSystem() {
        return system;
    }

    public void setSystem(System system) {
        this.system = system;
    }

    public class System {

        @SerializedName("nc")
        @Expose
        private Integer nc;

        public Integer getNc() {
            return nc;
        }

        public void setNc(Integer nc) {
            this.nc = nc;
        }

    }

    public class Data {

        @SerializedName("otp_id")
        @Expose
        private Integer otpId;
        @SerializedName("otp")
        @Expose
        private String otp;

        public Integer getOtpId() {
            return otpId;
        }

        public void setOtpId(Integer otpId) {
            this.otpId = otpId;
        }

        public String getOtp() {
            return otp;
        }

        public void setOtp(String otp) {
            this.otp = otp;
        }

    }

}