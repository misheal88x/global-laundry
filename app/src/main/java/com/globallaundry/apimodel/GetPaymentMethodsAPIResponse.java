package com.globallaundry.apimodel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetPaymentMethodsAPIResponse {
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("data")
    @Expose
    private MethodsData data;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public MethodsData getData() {
        return data;
    }

    public void setData(MethodsData data) {
        this.data = data;
    }

    public Integer getTime() {
        return time;
    }

    public void setTime(Integer time) {
        this.time = time;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public GetProfileApiResponse.System getSystem() {
        return system;
    }

    public void setSystem(GetProfileApiResponse.System system) {
        this.system = system;
    }

    @SerializedName("time")
    @Expose
    private Integer time;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("system")
    @Expose
    private GetProfileApiResponse.System system;


    public static class MethodsData {
        @SerializedName("delivery")
        private boolean delivery;

        @SerializedName("card")
        private boolean card;

        public boolean isDelivery() {
            return delivery;
        }

        public void setDelivery(boolean delivery) {
            this.delivery = delivery;
        }

        public boolean isCard() {
            return card;
        }

        public void setCard(boolean card) {
            this.card = card;
        }
    }
}
