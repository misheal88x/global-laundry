package com.globallaundry.apimodel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class PromationAddApiResponse {

    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("data")
    @Expose
    private List<Datum> data = new ArrayList<Datum>();
    @SerializedName("time")
    @Expose
    private Integer time;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("system")
    @Expose
    private System system;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }

    public Integer getTime() {
        return time;
    }

    public void setTime(Integer time) {
        this.time = time;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public System getSystem() {
        return system;
    }

    public void setSystem(System system) {
        this.system = system;
    }


    public class Datum {

        @SerializedName("advertisement_id")
        @Expose
        private Integer advertisement_id;
        @SerializedName("advertisement_key")
        @Expose
        private String advertisement_key;
        @SerializedName("image")
        @Expose
        private String image;
        @SerializedName("type")
        @Expose
        private Integer type;
        @SerializedName("contact_number")
        @Expose
        private String contact_number;

        public Integer getAdvertisement_id() {
            return advertisement_id;
        }

        public void setAdvertisement_id(Integer advertisement_id) {
            this.advertisement_id = advertisement_id;
        }

        public String getAdvertisement_key() {
            return advertisement_key;
        }

        public void setAdvertisement_key(String advertisement_key) {
            this.advertisement_key = advertisement_key;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public Integer getType() {
            return type;
        }

        public void setType(Integer type) {
            this.type = type;
        }

        public String getContact_number() {
            return contact_number;
        }

        public void setContact_number(String contact_number) {
            this.contact_number = contact_number;
        }

    }
    public class System {

        @SerializedName("user_status")
        @Expose
        private Integer user_status;

        public Integer getUser_status() {
            return user_status;
        }

        public void setUser_status(Integer user_status) {
            this.user_status = user_status;
        }

    }

}