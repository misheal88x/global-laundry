package com.globallaundry.apimodel;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class GetAvailablePackagesApiResponse {
    @SerializedName("status")
    private Integer status;
    @SerializedName("message")
    private String message;


    @SerializedName("data")
    private List<PackageData > data;

    public List<PackageData > getData() {
        return data;
    }

    public void setData(List<PackageData > data) {
        this.data = data;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public class Data {
        @SerializedName("id")
        private Integer id;
        @SerializedName("name")
        private String name;
        @SerializedName("validity")
        private String validity;
        @SerializedName("status")
        private Integer status;
        @SerializedName("price")
        private Integer price;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getValidity() {
            return validity;
        }

        public void setValidity(String validity) {
            this.validity = validity;
        }

        public Integer getStatus() {
            return status;
        }

        public void setStatus(Integer status) {
            this.status = status;
        }

        public Integer getPrice() {
            return price;
        }

        public void setPrice(Integer price) {
            this.price = price;
        }

        public List<PackageData.Item> getItems() {
            return items;
        }

        public void setItems(List<PackageData.Item> items) {
            this.items = items;
        }

        @SerializedName("items")
        private List<PackageData.Item> items;

        public class Item {
            @SerializedName("id")
            private Integer id;
            @SerializedName("package_id")
            private Integer packageId;

            public Integer getId() {
                return id;
            }

            public void setId(Integer id) {
                this.id = id;
            }

            public Integer getPackageId() {
                return packageId;
            }

            public void setPackageId(Integer packageId) {
                this.packageId = packageId;
            }

            public Integer getClothTypeId() {
                return clothTypeId;
            }

            public void setClothTypeId(Integer clothTypeId) {
                this.clothTypeId = clothTypeId;
            }

            public Integer getQuantity() {
                return quantity;
            }

            public void setQuantity(Integer quantity) {
                this.quantity = quantity;
            }

            public List<PackageData.Item.ClothType> getClothTypeList() {
                return clothTypeList;
            }

            public void setClothTypeList(List<PackageData.Item.ClothType> clothTypeList) {
                this.clothTypeList = clothTypeList;
            }

            @SerializedName("cloth_type_id")
            private Integer clothTypeId;
            @SerializedName("quantity")
            private Integer quantity;

            @SerializedName("cloth_type")
            private List<PackageData.Item.ClothType> clothTypeList;

            public class ClothType {
                @SerializedName("cloth_type_key")
                private String clothTypeKey;
                @SerializedName("cloth_type_name")
                private String clothTypeName;
                @SerializedName("cloth_type_image")
                private String clothTypeImage;

                @SerializedName("size_type")
                private Integer sizeType;
                @SerializedName("price")
                private String price;

                public String getClothTypeKey() {
                    return clothTypeKey;
                }

                public void setClothTypeKey(String clothTypeKey) {
                    this.clothTypeKey = clothTypeKey;
                }

                public String getClothTypeName() {
                    return clothTypeName;
                }

                public void setClothTypeName(String clothTypeName) {
                    this.clothTypeName = clothTypeName;
                }

                public String getClothTypeImage() {
                    return clothTypeImage;
                }

                public void setClothTypeImage(String clothTypeImage) {
                    this.clothTypeImage = clothTypeImage;
                }

                public Integer getSizeType() {
                    return sizeType;
                }

                public void setSizeType(Integer sizeType) {
                    this.sizeType = sizeType;
                }

                public String getPrice() {
                    return price;
                }

                public void setPrice(String price) {
                    this.price = price;
                }

                public String getExpressCost() {
                    return expressCost;
                }

                public void setExpressCost(String expressCost) {
                    this.expressCost = expressCost;
                }

                public Integer getExpressServiceTime() {
                    return expressServiceTime;
                }

                public void setExpressServiceTime(Integer expressServiceTime) {
                    this.expressServiceTime = expressServiceTime;
                }

                public Integer getHaveExpressService() {
                    return haveExpressService;
                }

                public void setHaveExpressService(Integer haveExpressService) {
                    this.haveExpressService = haveExpressService;
                }

                public Integer getIsOnline() {
                    return isOnline;
                }

                public void setIsOnline(Integer isOnline) {
                    this.isOnline = isOnline;
                }

                @SerializedName("express_cost")
                private String expressCost;

                @SerializedName("express_service_time")
                private Integer expressServiceTime;

                @SerializedName("have_express_service")
                private Integer haveExpressService;

                @SerializedName("is_online")
                private Integer isOnline;
            }
        }
    }
}
