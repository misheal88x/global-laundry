package com.globallaundry.apimodel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetContactusApiResponse {
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("data")
    @Expose
    private Data data;
    @SerializedName("time")
    @Expose
    private Integer time;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("system")
    @Expose
    private System system;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public Integer getTime() {
        return time;
    }

    public void setTime(Integer time) {
        this.time = time;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public System getSystem() {
        return system;
    }

    public void setSystem(System system) {
        this.system = system;
    }

    public class Data {

        @SerializedName("APP_NAME")
        @Expose
        private String aPP_NAME;
        @SerializedName("APP_EMAIL")
        @Expose
        private String aPP_EMAIL;
        @SerializedName("APP_CONTACT_NO")
        @Expose
        private String aPP_CONTACT_NO;
        @SerializedName("APP_CONTACT_ADDRESS")
        @Expose
        private String aPP_CONTACT_ADDRESS;

        public String getAPP_NAME() {
            return aPP_NAME;
        }

        public void setAPP_NAME(String aPP_NAME) {
            this.aPP_NAME = aPP_NAME;
        }

        public String getAPP_EMAIL() {
            return aPP_EMAIL;
        }

        public void setAPP_EMAIL(String aPP_EMAIL) {
            this.aPP_EMAIL = aPP_EMAIL;
        }

        public String getAPP_CONTACT_NO() {
            return aPP_CONTACT_NO;
        }

        public void setAPP_CONTACT_NO(String aPP_CONTACT_NO) {
            this.aPP_CONTACT_NO = aPP_CONTACT_NO;
        }

        public String getAPP_CONTACT_ADDRESS() {
            return aPP_CONTACT_ADDRESS;
        }

        public void setAPP_CONTACT_ADDRESS(String aPP_CONTACT_ADDRESS) {
            this.aPP_CONTACT_ADDRESS = aPP_CONTACT_ADDRESS;
        }

    }
    public class System {

        @SerializedName("nc")
        @Expose
        private Integer nc;

        public Integer getNc() {
            return nc;
        }

        public void setNc(Integer nc) {
            this.nc = nc;
        }

    }
}
