package com.globallaundry.apimodel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class AddressTypeApiResponse {

    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("data")
    @Expose
    private List<Datum> data = null;
    @SerializedName("time")
    @Expose
    private Integer time;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("system")
    @Expose
    private System system;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }

    public Integer getTime() {
        return time;
    }

    public void setTime(Integer time) {
        this.time = time;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public System getSystem() {
        return system;
    }

    public void setSystem(System system) {
        this.system = system;
    }

    public class Datum {

        @SerializedName("address_type_id")
        @Expose
        private Integer addressTypeId;
        @SerializedName("address_type_key")
        @Expose
        private String addressTypeKey;
        @SerializedName("address_type_name")
        @Expose
        private String addressTypeName;

        public Integer getAddressTypeId() {
            return addressTypeId;
        }

        public void setAddressTypeId(Integer addressTypeId) {
            this.addressTypeId = addressTypeId;
        }

        public String getAddressTypeKey() {
            return addressTypeKey;
        }

        public void setAddressTypeKey(String addressTypeKey) {
            this.addressTypeKey = addressTypeKey;
        }

        public String getAddressTypeName() {
            return addressTypeName;
        }

        public void setAddressTypeName(String addressTypeName) {
            this.addressTypeName = addressTypeName;
        }

    }

    public class System {

        @SerializedName("nc")
        @Expose
        private Integer nc;

        public Integer getNc() {
            return nc;
        }

        public void setNc(Integer nc) {
            this.nc = nc;
        }

    }

}