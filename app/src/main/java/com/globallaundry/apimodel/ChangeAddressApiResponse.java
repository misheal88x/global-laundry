package com.globallaundry.apimodel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class ChangeAddressApiResponse {

    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("data")
    @Expose
    private Data data;
    @SerializedName("time")
    @Expose
    private Integer time;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("system")
    @Expose
    private System system;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public Integer getTime() {
        return time;
    }

    public void setTime(Integer time) {
        this.time = time;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public System getSystem() {
        return system;
    }

    public void setSystem(System system) {
        this.system = system;
    }



    public class Data {

        @SerializedName("user_address_key")
        @Expose
        private String user_address_key;
        @SerializedName("address_type_id")
        @Expose
        private Integer address_type_id;
        @SerializedName("address_type_key")
        @Expose
        private String address_type_key;
        @SerializedName("address_type_name")
        @Expose
        private String address_type_name;
        @SerializedName("city_id")
        @Expose
        private Integer city_id;
        @SerializedName("city_key")
        @Expose
        private String city_key;
        @SerializedName("city_name")
        @Expose
        private String city_name;
        @SerializedName("area_id")
        @Expose
        private Integer area_id;
        @SerializedName("area_key")
        @Expose
        private String area_key;
        @SerializedName("area_name")
        @Expose
        private String area_name;
        @SerializedName("latitude")
        @Expose
        private String latitude;
        @SerializedName("longitude")
        @Expose
        private String longitude;
        @SerializedName("flat_no")
        @Expose
        private String flat_no;
        @SerializedName("company")
        @Expose
        private String company;
        @SerializedName("landmark")
        @Expose
        private String landmark;
        @SerializedName("street_name")
        @Expose
        private String street_name;
        @SerializedName("apartment")
        @Expose
        private String apartment;
        @SerializedName("is_default")
        @Expose
        private String is_default;

        public String getUser_address_key() {
            return user_address_key;
        }

        public void setUser_address_key(String user_address_key) {
            this.user_address_key = user_address_key;
        }

        public Integer getAddress_type_id() {
            return address_type_id;
        }

        public void setAddress_type_id(Integer address_type_id) {
            this.address_type_id = address_type_id;
        }

        public String getAddress_type_key() {
            return address_type_key;
        }

        public void setAddress_type_key(String address_type_key) {
            this.address_type_key = address_type_key;
        }

        public String getAddress_type_name() {
            return address_type_name;
        }

        public void setAddress_type_name(String address_type_name) {
            this.address_type_name = address_type_name;
        }

        public Integer getCity_id() {
            return city_id;
        }

        public void setCity_id(Integer city_id) {
            this.city_id = city_id;
        }

        public String getCity_key() {
            return city_key;
        }

        public void setCity_key(String city_key) {
            this.city_key = city_key;
        }

        public String getCity_name() {
            return city_name;
        }

        public void setCity_name(String city_name) {
            this.city_name = city_name;
        }

        public Integer getArea_id() {
            return area_id;
        }

        public void setArea_id(Integer area_id) {
            this.area_id = area_id;
        }

        public String getArea_key() {
            return area_key;
        }

        public void setArea_key(String area_key) {
            this.area_key = area_key;
        }

        public String getArea_name() {
            return area_name;
        }

        public void setArea_name(String area_name) {
            this.area_name = area_name;
        }

        public String getLatitude() {
            return latitude;
        }

        public void setLatitude(String latitude) {
            this.latitude = latitude;
        }

        public String getLongitude() {
            return longitude;
        }

        public void setLongitude(String longitude) {
            this.longitude = longitude;
        }

        public String getFlat_no() {
            return flat_no;
        }

        public void setFlat_no(String flat_no) {
            this.flat_no = flat_no;
        }

        public String getCompany() {
            return company;
        }

        public void setCompany(String company) {
            this.company = company;
        }

        public String getLandmark() {
            return landmark;
        }

        public void setLandmark(String landmark) {
            this.landmark = landmark;
        }

        public String getStreet_name() {
            return street_name;
        }

        public void setStreet_name(String street_name) {
            this.street_name = street_name;
        }

        public String getApartment() {
            return apartment;
        }

        public void setApartment(String apartment) {
            this.apartment = apartment;
        }

        public String getIs_default() {
            return is_default;
        }

        public void setIs_default(String is_default) {
            this.is_default = is_default;
        }

    }



    public class System {

        @SerializedName("user_status")
        @Expose
        private Integer user_status;

        public Integer getUser_status() {
            return user_status;
        }

        public void setUser_status(Integer user_status) {
            this.user_status = user_status;
        }

    }


}