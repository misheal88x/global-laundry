package com.globallaundry.apimodel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class TimeSlotApiResponse {
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("data")
    @Expose
    private Data data;
    @SerializedName("time")
    @Expose
    private Integer time;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("system")
    @Expose
    private System system;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public Integer getTime() {
        return time;
    }

    public void setTime(Integer time) {
        this.time = time;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public System getSystem() {
        return system;
    }

    public void setSystem(System system) {
        this.system = system;
    }





    public class Time_ {

        @SerializedName("value")
        @Expose
        private String value;
        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("active")
        @Expose
        private Integer active;

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public Integer getActive() {
            return active;
        }

        public void setActive(Integer active) {
            this.active = active;
        }

    }




    public class Time {

        @SerializedName("value")
        @Expose
        private String value;
        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("active")
        @Expose
        private Integer active;

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public Integer getActive() {
            return active;
        }

        public void setActive(Integer active) {
            this.active = active;
        }

    }



    public class System {

        @SerializedName("user_status")
        @Expose
        private Integer user_status;
        @SerializedName("is_rush_hour")
        @Expose
        private Integer is_rush_hour;

        public Integer getUser_status() {
            return user_status;
        }

        public void setUser_status(Integer user_status) {
            this.user_status = user_status;
        }

        public Integer getIs_rush_hour() {
            return is_rush_hour;
        }

        public void setIs_rush_hour(Integer is_rush_hour) {
            this.is_rush_hour = is_rush_hour;
        }

    }



    public class Pickup {

        @SerializedName("date")
        @Expose
        private String date;
        @SerializedName("active")
        @Expose
        private Integer active;
        @SerializedName("times")
        @Expose
        private List<Time> times = null;

        public String getDate() {
            return date;
        }

        public void setDate(String date) {
            this.date = date;
        }

        public Integer getActive() {
            return active;
        }

        public void setActive(Integer active) {
            this.active = active;
        }

        public List<Time> getTimes() {
            return times;
        }

        public void setTimes(List<Time> times) {
            this.times = times;
        }

    }




    public class Delivery {

        @SerializedName("date")
        @Expose
        private String date;
        @SerializedName("active")
        @Expose
        private Integer active;
        @SerializedName("times")
        @Expose
        private List<Time_> times = null;

        public String getDate() {
            return date;
        }

        public void setDate(String date) {
            this.date = date;
        }

        public Integer getActive() {
            return active;
        }

        public void setActive(Integer active) {
            this.active = active;
        }

        public List<Time_> getTimes() {
            return times;
        }

        public void setTimes(List<Time_> times) {
            this.times = times;
        }

    }



    public class Data {

        @SerializedName("pickup")
        @Expose
        private List<Pickup> pickup = null;
        @SerializedName("delivery")
        @Expose
        private List<Delivery> delivery = null;

        public List<Pickup> getPickup() {
            return pickup;
        }

        public void setPickup(List<Pickup> pickup) {
            this.pickup = pickup;
        }

        public List<Delivery> getDelivery() {
            return delivery;
        }

        public void setDelivery(List<Delivery> delivery) {
            this.delivery = delivery;
        }

    }


}
