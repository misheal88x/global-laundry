package com.globallaundry.apimodel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class PickUpTimeSlotApiResponse {

    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("data")
    @Expose
    private List<Datum> data = new ArrayList<Datum>();
    @SerializedName("time")
    @Expose
    private Integer time;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("system")
    @Expose
    private System system;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }

    public Integer getTime() {
        return time;
    }

    public void setTime(Integer time) {
        this.time = time;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public System getSystem() {
        return system;
    }

    public void setSystem(System system) {
        this.system = system;
    }
    public class Datum {

        @SerializedName("date")
        @Expose
        private String date;
        @SerializedName("time")
        @Expose
        private List<Time> time = new ArrayList<Time>();

        public String getDate() {
            return date;
        }

        public void setDate(String date) {
            this.date = date;
        }

        public List<Time> getTime() {
            return time;
        }

        public void setTime(List<Time> time) {
            this.time = time;
        }

    }
    public class System {
        @SerializedName("user_status")
        @Expose
        private Integer user_status;
        @SerializedName("is_rush_hour")
        @Expose
        private Integer is_rush_hour;

        public Integer getUser_status() {
            return user_status;
        }

        public void setUser_status(Integer user_status) {
            this.user_status = user_status;
        }

        public Integer getIs_rush_hour() {
            return is_rush_hour;
        }

        public void setIs_rush_hour(Integer is_rush_hour) {
            this.is_rush_hour = is_rush_hour;
        }

    }
    public class Time {

        @SerializedName("value")
        @Expose
        private String value;
        @SerializedName("name")
        @Expose
        private String name;

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

    }



}