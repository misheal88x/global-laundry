package com.globallaundry.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class NotificationListApiResponse {

    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("data")
    @Expose
    private List<Datum> data = null;
    @SerializedName("time")
    @Expose
    private Integer time;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("system")
    @Expose
    private System system;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }

    public Integer getTime() {
        return time;
    }

    public void setTime(Integer time) {
        this.time = time;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public System getSystem() {
        return system;
    }

    public void setSystem(System system) {
        this.system = system;
    }




    public class System {

        @SerializedName("user_status")
        @Expose
        private Integer user_status;

        public Integer getUser_status() {
            return user_status;
        }

        public void setUser_status(Integer user_status) {
            this.user_status = user_status;
        }

    }


    public class Datum {

        @SerializedName("notification_key")
        @Expose
        private String notification_key;
        @SerializedName("notification_title")
        @Expose
        private String notification_title;
        @SerializedName("notification_description")
        @Expose
        private String notification_description;
        @SerializedName("is_read")
        @Expose
        private Integer is_read;
        @SerializedName("created_at")
        @Expose
        private String created_at;

        public String getNotification_key() {
            return notification_key;
        }

        public void setNotification_key(String notification_key) {
            this.notification_key = notification_key;
        }

        public String getNotification_title() {
            return notification_title;
        }

        public void setNotification_title(String notification_title) {
            this.notification_title = notification_title;
        }

        public String getNotification_description() {
            return notification_description;
        }

        public void setNotification_description(String notification_description) {
            this.notification_description = notification_description;
        }

        public Integer getIs_read() {
            return is_read;
        }

        public void setIs_read(Integer is_read) {
            this.is_read = is_read;
        }

        public String getCreated_at() {
            return created_at;
        }

        public void setCreated_at(String created_at) {
            this.created_at = created_at;
        }

    }

}
