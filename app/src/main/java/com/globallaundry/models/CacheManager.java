package com.globallaundry.models;

import java.util.HashMap;

public class CacheManager {
    private static HashMap<String, Object> map = new HashMap<>();

    private CacheManager() {
    }

    public static CacheManager getInstance() {
        return new CacheManager();
    }

    public void put(String key, Object value) {
        map.put(key, value);
    }

    public Object get(String key) {
        return map.get(key);
    }
}
